
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4 0f                	in     $0xf,%al

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 b0 10 00       	mov    $0x10b000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl    $(stack + KSTACKSIZE), %esp
80100028:	bc d0 de 10 80       	mov    $0x8010ded0,%esp
  # Jump to main() and switch to executing at high addresses.
  # Need an explicit long jump (indirect jump would work as
  # well, as would a push/ret trick to save 1 byte) because
  # the assembler produces a PC-relative instruction for a
  # direct jump.
  ljmp    $(SEG_KCODE<<3), $main
8010002d:	ea 56 3d 10 80 08 00 	ljmp   $0x8,$0x80103d56

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	c7 44 24 04 e0 8f 10 	movl   $0x80108fe0,0x4(%esp)
80100041:	80 
80100042:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
80100049:	e8 90 58 00 00       	call   801058de <initlock>

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004e:	c7 05 f0 1d 11 80 e4 	movl   $0x80111de4,0x80111df0
80100055:	1d 11 80 
  bcache.head.next = &bcache.head;
80100058:	c7 05 f4 1d 11 80 e4 	movl   $0x80111de4,0x80111df4
8010005f:	1d 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100062:	c7 45 f4 14 df 10 80 	movl   $0x8010df14,-0xc(%ebp)
80100069:	eb 3a                	jmp    801000a5 <binit+0x71>
    b->next = bcache.head.next;
8010006b:	8b 15 f4 1d 11 80    	mov    0x80111df4,%edx
80100071:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100074:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100077:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007a:	c7 40 0c e4 1d 11 80 	movl   $0x80111de4,0xc(%eax)
    b->dev = -1;
80100081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100084:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008b:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
80100090:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100093:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100096:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100099:	a3 f4 1d 11 80       	mov    %eax,0x80111df4

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009e:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a5:	b8 e4 1d 11 80       	mov    $0x80111de4,%eax
801000aa:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801000ad:	72 bc                	jb     8010006b <binit+0x37>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000af:	c9                   	leave  
801000b0:	c3                   	ret    

801000b1 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint blockno)
{
801000b1:	55                   	push   %ebp
801000b2:	89 e5                	mov    %esp,%ebp
801000b4:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b7:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
801000be:	e8 3c 58 00 00       	call   801058ff <acquire>

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c3:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
801000c8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000cb:	eb 63                	jmp    80100130 <bget+0x7f>
    if(b->dev == dev && b->blockno == blockno){
801000cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000d0:	8b 40 04             	mov    0x4(%eax),%eax
801000d3:	3b 45 08             	cmp    0x8(%ebp),%eax
801000d6:	75 4f                	jne    80100127 <bget+0x76>
801000d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000db:	8b 40 08             	mov    0x8(%eax),%eax
801000de:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e1:	75 44                	jne    80100127 <bget+0x76>
      if(!(b->flags & B_BUSY)){
801000e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e6:	8b 00                	mov    (%eax),%eax
801000e8:	83 e0 01             	and    $0x1,%eax
801000eb:	85 c0                	test   %eax,%eax
801000ed:	75 23                	jne    80100112 <bget+0x61>
        b->flags |= B_BUSY;
801000ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f2:	8b 00                	mov    (%eax),%eax
801000f4:	89 c2                	mov    %eax,%edx
801000f6:	83 ca 01             	or     $0x1,%edx
801000f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000fc:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
801000fe:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
80100105:	e8 5e 58 00 00       	call   80105968 <release>
        return b;
8010010a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010010d:	e9 93 00 00 00       	jmp    801001a5 <bget+0xf4>
      }
      sleep(b, &bcache.lock);
80100112:	c7 44 24 04 e0 de 10 	movl   $0x8010dee0,0x4(%esp)
80100119:	80 
8010011a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010011d:	89 04 24             	mov    %eax,(%esp)
80100120:	e8 f8 54 00 00       	call   8010561d <sleep>
      goto loop;
80100125:	eb 9c                	jmp    801000c3 <bget+0x12>

  acquire(&bcache.lock);

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100127:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010012a:	8b 40 10             	mov    0x10(%eax),%eax
8010012d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100130:	81 7d f4 e4 1d 11 80 	cmpl   $0x80111de4,-0xc(%ebp)
80100137:	75 94                	jne    801000cd <bget+0x1c>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100139:	a1 f0 1d 11 80       	mov    0x80111df0,%eax
8010013e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100141:	eb 4d                	jmp    80100190 <bget+0xdf>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
80100143:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100146:	8b 00                	mov    (%eax),%eax
80100148:	83 e0 01             	and    $0x1,%eax
8010014b:	85 c0                	test   %eax,%eax
8010014d:	75 38                	jne    80100187 <bget+0xd6>
8010014f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100152:	8b 00                	mov    (%eax),%eax
80100154:	83 e0 04             	and    $0x4,%eax
80100157:	85 c0                	test   %eax,%eax
80100159:	75 2c                	jne    80100187 <bget+0xd6>
      b->dev = dev;
8010015b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015e:	8b 55 08             	mov    0x8(%ebp),%edx
80100161:	89 50 04             	mov    %edx,0x4(%eax)
      b->blockno = blockno;
80100164:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100167:	8b 55 0c             	mov    0xc(%ebp),%edx
8010016a:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
8010016d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100170:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100176:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
8010017d:	e8 e6 57 00 00       	call   80105968 <release>
      return b;
80100182:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100185:	eb 1e                	jmp    801001a5 <bget+0xf4>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100187:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010018a:	8b 40 0c             	mov    0xc(%eax),%eax
8010018d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100190:	81 7d f4 e4 1d 11 80 	cmpl   $0x80111de4,-0xc(%ebp)
80100197:	75 aa                	jne    80100143 <bget+0x92>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
80100199:	c7 04 24 e7 8f 10 80 	movl   $0x80108fe7,(%esp)
801001a0:	e8 c4 06 00 00       	call   80100869 <panic>
}
801001a5:	c9                   	leave  
801001a6:	c3                   	ret    

801001a7 <bread>:

// Return a B_BUSY buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801001a7:	55                   	push   %ebp
801001a8:	89 e5                	mov    %esp,%ebp
801001aa:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  b = bget(dev, blockno);
801001ad:	8b 45 0c             	mov    0xc(%ebp),%eax
801001b0:	89 44 24 04          	mov    %eax,0x4(%esp)
801001b4:	8b 45 08             	mov    0x8(%ebp),%eax
801001b7:	89 04 24             	mov    %eax,(%esp)
801001ba:	e8 f2 fe ff ff       	call   801000b1 <bget>
801001bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID)) {
801001c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001c5:	8b 00                	mov    (%eax),%eax
801001c7:	83 e0 02             	and    $0x2,%eax
801001ca:	85 c0                	test   %eax,%eax
801001cc:	75 0b                	jne    801001d9 <bread+0x32>
    iderw(b);
801001ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d1:	89 04 24             	mov    %eax,(%esp)
801001d4:	e8 37 2e 00 00       	call   80103010 <iderw>
  }
  return b;
801001d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001dc:	c9                   	leave  
801001dd:	c3                   	ret    

801001de <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001de:	55                   	push   %ebp
801001df:	89 e5                	mov    %esp,%ebp
801001e1:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
801001e4:	8b 45 08             	mov    0x8(%ebp),%eax
801001e7:	8b 00                	mov    (%eax),%eax
801001e9:	83 e0 01             	and    $0x1,%eax
801001ec:	85 c0                	test   %eax,%eax
801001ee:	75 0c                	jne    801001fc <bwrite+0x1e>
    panic("bwrite");
801001f0:	c7 04 24 f8 8f 10 80 	movl   $0x80108ff8,(%esp)
801001f7:	e8 6d 06 00 00       	call   80100869 <panic>
  b->flags |= B_DIRTY;
801001fc:	8b 45 08             	mov    0x8(%ebp),%eax
801001ff:	8b 00                	mov    (%eax),%eax
80100201:	89 c2                	mov    %eax,%edx
80100203:	83 ca 04             	or     $0x4,%edx
80100206:	8b 45 08             	mov    0x8(%ebp),%eax
80100209:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010020b:	8b 45 08             	mov    0x8(%ebp),%eax
8010020e:	89 04 24             	mov    %eax,(%esp)
80100211:	e8 fa 2d 00 00       	call   80103010 <iderw>
}
80100216:	c9                   	leave  
80100217:	c3                   	ret    

80100218 <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
80100218:	55                   	push   %ebp
80100219:	89 e5                	mov    %esp,%ebp
8010021b:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
8010021e:	8b 45 08             	mov    0x8(%ebp),%eax
80100221:	8b 00                	mov    (%eax),%eax
80100223:	83 e0 01             	and    $0x1,%eax
80100226:	85 c0                	test   %eax,%eax
80100228:	75 0c                	jne    80100236 <brelse+0x1e>
    panic("brelse");
8010022a:	c7 04 24 ff 8f 10 80 	movl   $0x80108fff,(%esp)
80100231:	e8 33 06 00 00       	call   80100869 <panic>

  acquire(&bcache.lock);
80100236:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
8010023d:	e8 bd 56 00 00       	call   801058ff <acquire>

  b->next->prev = b->prev;
80100242:	8b 45 08             	mov    0x8(%ebp),%eax
80100245:	8b 40 10             	mov    0x10(%eax),%eax
80100248:	8b 55 08             	mov    0x8(%ebp),%edx
8010024b:	8b 52 0c             	mov    0xc(%edx),%edx
8010024e:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
80100251:	8b 45 08             	mov    0x8(%ebp),%eax
80100254:	8b 40 0c             	mov    0xc(%eax),%eax
80100257:	8b 55 08             	mov    0x8(%ebp),%edx
8010025a:	8b 52 10             	mov    0x10(%edx),%edx
8010025d:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
80100260:	8b 15 f4 1d 11 80    	mov    0x80111df4,%edx
80100266:	8b 45 08             	mov    0x8(%ebp),%eax
80100269:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
8010026c:	8b 45 08             	mov    0x8(%ebp),%eax
8010026f:	c7 40 0c e4 1d 11 80 	movl   $0x80111de4,0xc(%eax)
  bcache.head.next->prev = b;
80100276:	a1 f4 1d 11 80       	mov    0x80111df4,%eax
8010027b:	8b 55 08             	mov    0x8(%ebp),%edx
8010027e:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
80100281:	8b 45 08             	mov    0x8(%ebp),%eax
80100284:	a3 f4 1d 11 80       	mov    %eax,0x80111df4

  b->flags &= ~B_BUSY;
80100289:	8b 45 08             	mov    0x8(%ebp),%eax
8010028c:	8b 00                	mov    (%eax),%eax
8010028e:	89 c2                	mov    %eax,%edx
80100290:	83 e2 fe             	and    $0xfffffffe,%edx
80100293:	8b 45 08             	mov    0x8(%ebp),%eax
80100296:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80100298:	8b 45 08             	mov    0x8(%ebp),%eax
8010029b:	89 04 24             	mov    %eax,(%esp)
8010029e:	e8 57 54 00 00       	call   801056fa <wakeup>

  release(&bcache.lock);
801002a3:	c7 04 24 e0 de 10 80 	movl   $0x8010dee0,(%esp)
801002aa:	e8 b9 56 00 00       	call   80105968 <release>
}
801002af:	c9                   	leave  
801002b0:	c3                   	ret    
801002b1:	00 00                	add    %al,(%eax)
	...

801002b4 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801002b4:	55                   	push   %ebp
801002b5:	89 e5                	mov    %esp,%ebp
801002b7:	83 ec 14             	sub    $0x14,%esp
801002ba:	8b 45 08             	mov    0x8(%ebp),%eax
801002bd:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002c1:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801002c5:	89 c2                	mov    %eax,%edx
801002c7:	ec                   	in     (%dx),%al
801002c8:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801002cb:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801002cf:	c9                   	leave  
801002d0:	c3                   	ret    

801002d1 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002d1:	55                   	push   %ebp
801002d2:	89 e5                	mov    %esp,%ebp
801002d4:	83 ec 08             	sub    $0x8,%esp
801002d7:	8b 55 08             	mov    0x8(%ebp),%edx
801002da:	8b 45 0c             	mov    0xc(%ebp),%eax
801002dd:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801002e1:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801002e4:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801002e8:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801002ec:	ee                   	out    %al,(%dx)
}
801002ed:	c9                   	leave  
801002ee:	c3                   	ret    

801002ef <cmosread>:
#include "date.h"
#include "x86.h"
#include "cmos.h"

uint cmosread(uint reg)
{
801002ef:	55                   	push   %ebp
801002f0:	89 e5                	mov    %esp,%ebp
801002f2:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
801002f5:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
801002f9:	76 0c                	jbe    80100307 <cmosread+0x18>
    panic("cmosread: invalid register");
801002fb:	c7 04 24 06 90 10 80 	movl   $0x80109006,(%esp)
80100302:	e8 62 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT, reg);
80100307:	8b 45 08             	mov    0x8(%ebp),%eax
8010030a:	0f b6 c0             	movzbl %al,%eax
8010030d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100311:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100318:	e8 b4 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010031d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100324:	e8 52 34 00 00       	call   8010377b <microdelay>
  return inb(CMOS_RETURN);
80100329:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
80100330:	e8 7f ff ff ff       	call   801002b4 <inb>
80100335:	0f b6 c0             	movzbl %al,%eax
}
80100338:	c9                   	leave  
80100339:	c3                   	ret    

8010033a <cmoswrite>:

void cmoswrite(uint reg, uint data)
{
8010033a:	55                   	push   %ebp
8010033b:	89 e5                	mov    %esp,%ebp
8010033d:	83 ec 18             	sub    $0x18,%esp
  if(reg >= CMOS_NMI_BIT)
80100340:	83 7d 08 7f          	cmpl   $0x7f,0x8(%ebp)
80100344:	76 0c                	jbe    80100352 <cmoswrite+0x18>
    panic("cmoswrite: invalid register");
80100346:	c7 04 24 21 90 10 80 	movl   $0x80109021,(%esp)
8010034d:	e8 17 05 00 00       	call   80100869 <panic>
  if(data > 0xff)
80100352:	81 7d 0c ff 00 00 00 	cmpl   $0xff,0xc(%ebp)
80100359:	76 0c                	jbe    80100367 <cmoswrite+0x2d>
    panic("cmoswrite: invalid data");
8010035b:	c7 04 24 3d 90 10 80 	movl   $0x8010903d,(%esp)
80100362:	e8 02 05 00 00       	call   80100869 <panic>

  outb(CMOS_PORT,  reg);
80100367:	8b 45 08             	mov    0x8(%ebp),%eax
8010036a:	0f b6 c0             	movzbl %al,%eax
8010036d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100371:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80100378:	e8 54 ff ff ff       	call   801002d1 <outb>
  microdelay(200);
8010037d:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80100384:	e8 f2 33 00 00       	call   8010377b <microdelay>
  outb(CMOS_RETURN, data);
80100389:	8b 45 0c             	mov    0xc(%ebp),%eax
8010038c:	0f b6 c0             	movzbl %al,%eax
8010038f:	89 44 24 04          	mov    %eax,0x4(%esp)
80100393:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
8010039a:	e8 32 ff ff ff       	call   801002d1 <outb>
}
8010039f:	c9                   	leave  
801003a0:	c3                   	ret    

801003a1 <fill_rtcdate>:

static void fill_rtcdate(struct rtcdate *r)
{
801003a1:	55                   	push   %ebp
801003a2:	89 e5                	mov    %esp,%ebp
801003a4:	83 ec 18             	sub    $0x18,%esp
  r->second = cmosread(CMOS_SECS);
801003a7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801003ae:	e8 3c ff ff ff       	call   801002ef <cmosread>
801003b3:	8b 55 08             	mov    0x8(%ebp),%edx
801003b6:	89 02                	mov    %eax,(%edx)
  r->minute = cmosread(CMOS_MINS);
801003b8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801003bf:	e8 2b ff ff ff       	call   801002ef <cmosread>
801003c4:	8b 55 08             	mov    0x8(%ebp),%edx
801003c7:	89 42 04             	mov    %eax,0x4(%edx)
  r->hour   = cmosread(CMOS_HOURS);
801003ca:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
801003d1:	e8 19 ff ff ff       	call   801002ef <cmosread>
801003d6:	8b 55 08             	mov    0x8(%ebp),%edx
801003d9:	89 42 08             	mov    %eax,0x8(%edx)
  r->day    = cmosread(CMOS_DAY);
801003dc:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
801003e3:	e8 07 ff ff ff       	call   801002ef <cmosread>
801003e8:	8b 55 08             	mov    0x8(%ebp),%edx
801003eb:	89 42 0c             	mov    %eax,0xc(%edx)
  r->month  = cmosread(CMOS_MONTH);
801003ee:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801003f5:	e8 f5 fe ff ff       	call   801002ef <cmosread>
801003fa:	8b 55 08             	mov    0x8(%ebp),%edx
801003fd:	89 42 10             	mov    %eax,0x10(%edx)
  r->year   = cmosread(CMOS_YEAR);
80100400:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
80100407:	e8 e3 fe ff ff       	call   801002ef <cmosread>
8010040c:	8b 55 08             	mov    0x8(%ebp),%edx
8010040f:	89 42 14             	mov    %eax,0x14(%edx)
}
80100412:	c9                   	leave  
80100413:	c3                   	ret    

80100414 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void cmostime(struct rtcdate *r)
{
80100414:	55                   	push   %ebp
80100415:	89 e5                	mov    %esp,%ebp
80100417:	83 ec 58             	sub    $0x58,%esp
  struct rtcdate t1, t2;
  int sb, bcd, tf;

  sb = cmosread(CMOS_STATB);
8010041a:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
80100421:	e8 c9 fe ff ff       	call   801002ef <cmosread>
80100426:	89 45 ec             	mov    %eax,-0x14(%ebp)

  bcd = (sb & CMOS_BINARY_BIT) == 0;
80100429:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010042c:	83 e0 04             	and    $0x4,%eax
8010042f:	85 c0                	test   %eax,%eax
80100431:	0f 94 c0             	sete   %al
80100434:	0f b6 c0             	movzbl %al,%eax
80100437:	89 45 f0             	mov    %eax,-0x10(%ebp)
  tf = (sb & CMOS_24H_BIT) != 0;
8010043a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010043d:	83 e0 02             	and    $0x2,%eax
80100440:	85 c0                	test   %eax,%eax
80100442:	0f 95 c0             	setne  %al
80100445:	0f b6 c0             	movzbl %al,%eax
80100448:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010044b:	eb 01                	jmp    8010044e <cmostime+0x3a>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010044d:	90                   	nop
  bcd = (sb & CMOS_BINARY_BIT) == 0;
  tf = (sb & CMOS_24H_BIT) != 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
8010044e:	8d 45 d4             	lea    -0x2c(%ebp),%eax
80100451:	89 04 24             	mov    %eax,(%esp)
80100454:	e8 48 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
80100459:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100460:	e8 8a fe ff ff       	call   801002ef <cmosread>
80100465:	25 80 00 00 00       	and    $0x80,%eax
8010046a:	85 c0                	test   %eax,%eax
8010046c:	74 03                	je     80100471 <cmostime+0x5d>
      continue;
8010046e:	90                   	nop
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
8010046f:	eb dd                	jmp    8010044e <cmostime+0x3a>
  // make sure CMOS doesn't modify time while we read it
  for(;;){
    fill_rtcdate(&t1);
    if(cmosread(CMOS_STATA) & CMOS_UIP_BIT)
      continue;
    fill_rtcdate(&t2);
80100471:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100474:	89 04 24             	mov    %eax,(%esp)
80100477:	e8 25 ff ff ff       	call   801003a1 <fill_rtcdate>
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
8010047c:	c7 44 24 08 18 00 00 	movl   $0x18,0x8(%esp)
80100483:	00 
80100484:	8d 45 bc             	lea    -0x44(%ebp),%eax
80100487:	89 44 24 04          	mov    %eax,0x4(%esp)
8010048b:	8d 45 d4             	lea    -0x2c(%ebp),%eax
8010048e:	89 04 24             	mov    %eax,(%esp)
80100491:	e8 43 57 00 00       	call   80105bd9 <memcmp>
80100496:	85 c0                	test   %eax,%eax
80100498:	75 b3                	jne    8010044d <cmostime+0x39>
      break;
8010049a:	90                   	nop
  }

  // backup raw values since BCD conversion removes PM bit from hour
  t2 = t1;
8010049b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
8010049e:	89 45 bc             	mov    %eax,-0x44(%ebp)
801004a1:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004a4:	89 45 c0             	mov    %eax,-0x40(%ebp)
801004a7:	8b 45 dc             	mov    -0x24(%ebp),%eax
801004aa:	89 45 c4             	mov    %eax,-0x3c(%ebp)
801004ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
801004b0:	89 45 c8             	mov    %eax,-0x38(%ebp)
801004b3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801004b6:	89 45 cc             	mov    %eax,-0x34(%ebp)
801004b9:	8b 45 e8             	mov    -0x18(%ebp),%eax
801004bc:	89 45 d0             	mov    %eax,-0x30(%ebp)

  // convert t1 from BCD
  if(bcd){
801004bf:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801004c3:	0f 84 a8 00 00 00    	je     80100571 <cmostime+0x15d>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801004c9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801004cc:	89 c2                	mov    %eax,%edx
801004ce:	c1 ea 04             	shr    $0x4,%edx
801004d1:	89 d0                	mov    %edx,%eax
801004d3:	c1 e0 02             	shl    $0x2,%eax
801004d6:	01 d0                	add    %edx,%eax
801004d8:	01 c0                	add    %eax,%eax
801004da:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801004dd:	83 e2 0f             	and    $0xf,%edx
801004e0:	01 d0                	add    %edx,%eax
801004e2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    CONV(minute);
801004e5:	8b 45 d8             	mov    -0x28(%ebp),%eax
801004e8:	89 c2                	mov    %eax,%edx
801004ea:	c1 ea 04             	shr    $0x4,%edx
801004ed:	89 d0                	mov    %edx,%eax
801004ef:	c1 e0 02             	shl    $0x2,%eax
801004f2:	01 d0                	add    %edx,%eax
801004f4:	01 c0                	add    %eax,%eax
801004f6:	8b 55 d8             	mov    -0x28(%ebp),%edx
801004f9:	83 e2 0f             	and    $0xf,%edx
801004fc:	01 d0                	add    %edx,%eax
801004fe:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(hour  );
80100501:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100504:	89 c2                	mov    %eax,%edx
80100506:	c1 ea 04             	shr    $0x4,%edx
80100509:	89 d0                	mov    %edx,%eax
8010050b:	c1 e0 02             	shl    $0x2,%eax
8010050e:	01 d0                	add    %edx,%eax
80100510:	01 c0                	add    %eax,%eax
80100512:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100515:	83 e2 0f             	and    $0xf,%edx
80100518:	01 d0                	add    %edx,%eax
8010051a:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(day   );
8010051d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100520:	89 c2                	mov    %eax,%edx
80100522:	c1 ea 04             	shr    $0x4,%edx
80100525:	89 d0                	mov    %edx,%eax
80100527:	c1 e0 02             	shl    $0x2,%eax
8010052a:	01 d0                	add    %edx,%eax
8010052c:	01 c0                	add    %eax,%eax
8010052e:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100531:	83 e2 0f             	and    $0xf,%edx
80100534:	01 d0                	add    %edx,%eax
80100536:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(month );
80100539:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010053c:	89 c2                	mov    %eax,%edx
8010053e:	c1 ea 04             	shr    $0x4,%edx
80100541:	89 d0                	mov    %edx,%eax
80100543:	c1 e0 02             	shl    $0x2,%eax
80100546:	01 d0                	add    %edx,%eax
80100548:	01 c0                	add    %eax,%eax
8010054a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
8010054d:	83 e2 0f             	and    $0xf,%edx
80100550:	01 d0                	add    %edx,%eax
80100552:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    CONV(year  );
80100555:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100558:	89 c2                	mov    %eax,%edx
8010055a:	c1 ea 04             	shr    $0x4,%edx
8010055d:	89 d0                	mov    %edx,%eax
8010055f:	c1 e0 02             	shl    $0x2,%eax
80100562:	01 d0                	add    %edx,%eax
80100564:	01 c0                	add    %eax,%eax
80100566:	8b 55 e8             	mov    -0x18(%ebp),%edx
80100569:	83 e2 0f             	and    $0xf,%edx
8010056c:	01 d0                	add    %edx,%eax
8010056e:	89 45 e8             	mov    %eax,-0x18(%ebp)
#undef     CONV
  }

  // convert 12 hour format to 24 hour format
  if(!tf){
80100571:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100575:	75 2e                	jne    801005a5 <cmostime+0x191>
    if(t2.hour & CMOS_PM_BIT){
80100577:	8b 45 c4             	mov    -0x3c(%ebp),%eax
8010057a:	25 80 00 00 00       	and    $0x80,%eax
8010057f:	85 c0                	test   %eax,%eax
80100581:	74 22                	je     801005a5 <cmostime+0x191>
      t1.hour = (t1.hour + 12) % 24;
80100583:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100586:	8d 48 0c             	lea    0xc(%eax),%ecx
80100589:	ba ab aa aa aa       	mov    $0xaaaaaaab,%edx
8010058e:	89 c8                	mov    %ecx,%eax
80100590:	f7 e2                	mul    %edx
80100592:	c1 ea 04             	shr    $0x4,%edx
80100595:	89 d0                	mov    %edx,%eax
80100597:	01 c0                	add    %eax,%eax
80100599:	01 d0                	add    %edx,%eax
8010059b:	c1 e0 03             	shl    $0x3,%eax
8010059e:	89 ca                	mov    %ecx,%edx
801005a0:	29 c2                	sub    %eax,%edx
801005a2:	89 55 dc             	mov    %edx,-0x24(%ebp)
    }
  }

  *r = t1;
801005a5:	8b 45 08             	mov    0x8(%ebp),%eax
801005a8:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801005ab:	89 10                	mov    %edx,(%eax)
801005ad:	8b 55 d8             	mov    -0x28(%ebp),%edx
801005b0:	89 50 04             	mov    %edx,0x4(%eax)
801005b3:	8b 55 dc             	mov    -0x24(%ebp),%edx
801005b6:	89 50 08             	mov    %edx,0x8(%eax)
801005b9:	8b 55 e0             	mov    -0x20(%ebp),%edx
801005bc:	89 50 0c             	mov    %edx,0xc(%eax)
801005bf:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801005c2:	89 50 10             	mov    %edx,0x10(%eax)
801005c5:	8b 55 e8             	mov    -0x18(%ebp),%edx
801005c8:	89 50 14             	mov    %edx,0x14(%eax)
  r->year += 2000;
801005cb:	8b 45 08             	mov    0x8(%ebp),%eax
801005ce:	8b 40 14             	mov    0x14(%eax),%eax
801005d1:	8d 90 d0 07 00 00    	lea    0x7d0(%eax),%edx
801005d7:	8b 45 08             	mov    0x8(%ebp),%eax
801005da:	89 50 14             	mov    %edx,0x14(%eax)
}
801005dd:	c9                   	leave  
801005de:	c3                   	ret    
	...

801005e0 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801005e0:	55                   	push   %ebp
801005e1:	89 e5                	mov    %esp,%ebp
801005e3:	83 ec 14             	sub    $0x14,%esp
801005e6:	8b 45 08             	mov    0x8(%ebp),%eax
801005e9:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801005ed:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801005f1:	89 c2                	mov    %eax,%edx
801005f3:	ec                   	in     (%dx),%al
801005f4:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801005f7:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801005fb:	c9                   	leave  
801005fc:	c3                   	ret    

801005fd <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801005fd:	55                   	push   %ebp
801005fe:	89 e5                	mov    %esp,%ebp
80100600:	83 ec 08             	sub    $0x8,%esp
80100603:	8b 55 08             	mov    0x8(%ebp),%edx
80100606:	8b 45 0c             	mov    0xc(%ebp),%eax
80100609:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010060d:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100610:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100614:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100618:	ee                   	out    %al,(%dx)
}
80100619:	c9                   	leave  
8010061a:	c3                   	ret    

8010061b <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
8010061b:	55                   	push   %ebp
8010061c:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
8010061e:	fa                   	cli    
}
8010061f:	5d                   	pop    %ebp
80100620:	c3                   	ret    

80100621 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
80100621:	55                   	push   %ebp
80100622:	89 e5                	mov    %esp,%ebp
80100624:	53                   	push   %ebx
80100625:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100628:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010062c:	74 19                	je     80100647 <printint+0x26>
8010062e:	8b 45 08             	mov    0x8(%ebp),%eax
80100631:	c1 e8 1f             	shr    $0x1f,%eax
80100634:	89 45 10             	mov    %eax,0x10(%ebp)
80100637:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010063b:	74 0a                	je     80100647 <printint+0x26>
    x = -xx;
8010063d:	8b 45 08             	mov    0x8(%ebp),%eax
80100640:	f7 d8                	neg    %eax
80100642:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80100645:	eb 06                	jmp    8010064d <printint+0x2c>
    x = -xx;
  else
    x = xx;
80100647:	8b 45 08             	mov    0x8(%ebp),%eax
8010064a:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
8010064d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
80100654:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80100657:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010065a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010065d:	ba 00 00 00 00       	mov    $0x0,%edx
80100662:	f7 f3                	div    %ebx
80100664:	89 d0                	mov    %edx,%eax
80100666:	0f b6 80 04 a0 10 80 	movzbl -0x7fef5ffc(%eax),%eax
8010066d:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
80100671:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
80100675:	8b 45 0c             	mov    0xc(%ebp),%eax
80100678:	89 45 d4             	mov    %eax,-0x2c(%ebp)
8010067b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010067e:	ba 00 00 00 00       	mov    $0x0,%edx
80100683:	f7 75 d4             	divl   -0x2c(%ebp)
80100686:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100689:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010068d:	75 c5                	jne    80100654 <printint+0x33>

  if(sign)
8010068f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100693:	74 23                	je     801006b8 <printint+0x97>
    buf[i++] = '-';
80100695:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100698:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
8010069d:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801006a1:	eb 16                	jmp    801006b9 <printint+0x98>
    consputc(buf[i]);
801006a3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801006a6:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801006ab:	0f be c0             	movsbl %al,%eax
801006ae:	89 04 24             	mov    %eax,(%esp)
801006b1:	e8 ea 03 00 00       	call   80100aa0 <consputc>
801006b6:	eb 01                	jmp    801006b9 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801006b8:	90                   	nop
801006b9:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801006bd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801006c1:	79 e0                	jns    801006a3 <printint+0x82>
    consputc(buf[i]);
}
801006c3:	83 c4 44             	add    $0x44,%esp
801006c6:	5b                   	pop    %ebx
801006c7:	5d                   	pop    %ebp
801006c8:	c3                   	ret    

801006c9 <cprintf>:
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801006c9:	55                   	push   %ebp
801006ca:	89 e5                	mov    %esp,%ebp
801006cc:	83 ec 38             	sub    $0x38,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801006cf:	a1 34 c6 10 80       	mov    0x8010c634,%eax
801006d4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(locking)
801006d7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801006db:	74 0c                	je     801006e9 <cprintf+0x20>
    acquire(&cons.lock);
801006dd:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
801006e4:	e8 16 52 00 00       	call   801058ff <acquire>

  if (fmt == 0)
801006e9:	8b 45 08             	mov    0x8(%ebp),%eax
801006ec:	85 c0                	test   %eax,%eax
801006ee:	75 0c                	jne    801006fc <cprintf+0x33>
    panic("null fmt");
801006f0:	c7 04 24 55 90 10 80 	movl   $0x80109055,(%esp)
801006f7:	e8 6d 01 00 00       	call   80100869 <panic>

  argp = (uint*)(void*)(&fmt + 1);
801006fc:	8d 45 08             	lea    0x8(%ebp),%eax
801006ff:	83 c0 04             	add    $0x4,%eax
80100702:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100705:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
8010070c:	e9 20 01 00 00       	jmp    80100831 <cprintf+0x168>
    if(c != '%'){
80100711:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
80100715:	74 10                	je     80100727 <cprintf+0x5e>
      consputc(c);
80100717:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010071a:	89 04 24             	mov    %eax,(%esp)
8010071d:	e8 7e 03 00 00       	call   80100aa0 <consputc>
      continue;
80100722:	e9 06 01 00 00       	jmp    8010082d <cprintf+0x164>
    }
    c = fmt[++i] & 0xff;
80100727:	8b 55 08             	mov    0x8(%ebp),%edx
8010072a:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
8010072e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100731:	8d 04 02             	lea    (%edx,%eax,1),%eax
80100734:	0f b6 00             	movzbl (%eax),%eax
80100737:	0f be c0             	movsbl %al,%eax
8010073a:	25 ff 00 00 00       	and    $0xff,%eax
8010073f:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(c == 0)
80100742:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100746:	0f 84 08 01 00 00    	je     80100854 <cprintf+0x18b>
      break;
    switch(c){
8010074c:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010074f:	83 f8 70             	cmp    $0x70,%eax
80100752:	74 4d                	je     801007a1 <cprintf+0xd8>
80100754:	83 f8 70             	cmp    $0x70,%eax
80100757:	7f 13                	jg     8010076c <cprintf+0xa3>
80100759:	83 f8 25             	cmp    $0x25,%eax
8010075c:	0f 84 a6 00 00 00    	je     80100808 <cprintf+0x13f>
80100762:	83 f8 64             	cmp    $0x64,%eax
80100765:	74 14                	je     8010077b <cprintf+0xb2>
80100767:	e9 aa 00 00 00       	jmp    80100816 <cprintf+0x14d>
8010076c:	83 f8 73             	cmp    $0x73,%eax
8010076f:	74 53                	je     801007c4 <cprintf+0xfb>
80100771:	83 f8 78             	cmp    $0x78,%eax
80100774:	74 2b                	je     801007a1 <cprintf+0xd8>
80100776:	e9 9b 00 00 00       	jmp    80100816 <cprintf+0x14d>
    case 'd':
      printint(*argp++, 10, 1);
8010077b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010077e:	8b 00                	mov    (%eax),%eax
80100780:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
80100784:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010078b:	00 
8010078c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80100793:	00 
80100794:	89 04 24             	mov    %eax,(%esp)
80100797:	e8 85 fe ff ff       	call   80100621 <printint>
      break;
8010079c:	e9 8c 00 00 00       	jmp    8010082d <cprintf+0x164>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801007a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007a4:	8b 00                	mov    (%eax),%eax
801007a6:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007aa:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801007b1:	00 
801007b2:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801007b9:	00 
801007ba:	89 04 24             	mov    %eax,(%esp)
801007bd:	e8 5f fe ff ff       	call   80100621 <printint>
      break;
801007c2:	eb 69                	jmp    8010082d <cprintf+0x164>
    case 's':
      if((s = (char*)*argp++) == 0)
801007c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801007c7:	8b 00                	mov    (%eax),%eax
801007c9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801007cc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801007d0:	0f 94 c0             	sete   %al
801007d3:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801007d7:	84 c0                	test   %al,%al
801007d9:	74 20                	je     801007fb <cprintf+0x132>
        s = "(null)";
801007db:	c7 45 f4 5e 90 10 80 	movl   $0x8010905e,-0xc(%ebp)
      for(; *s; s++)
801007e2:	eb 18                	jmp    801007fc <cprintf+0x133>
        consputc(*s);
801007e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007e7:	0f b6 00             	movzbl (%eax),%eax
801007ea:	0f be c0             	movsbl %al,%eax
801007ed:	89 04 24             	mov    %eax,(%esp)
801007f0:	e8 ab 02 00 00       	call   80100aa0 <consputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801007f5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801007f9:	eb 01                	jmp    801007fc <cprintf+0x133>
801007fb:	90                   	nop
801007fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801007ff:	0f b6 00             	movzbl (%eax),%eax
80100802:	84 c0                	test   %al,%al
80100804:	75 de                	jne    801007e4 <cprintf+0x11b>
        consputc(*s);
      break;
80100806:	eb 25                	jmp    8010082d <cprintf+0x164>
    case '%':
      consputc('%');
80100808:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010080f:	e8 8c 02 00 00       	call   80100aa0 <consputc>
      break;
80100814:	eb 17                	jmp    8010082d <cprintf+0x164>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
80100816:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010081d:	e8 7e 02 00 00       	call   80100aa0 <consputc>
      consputc(c);
80100822:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100825:	89 04 24             	mov    %eax,(%esp)
80100828:	e8 73 02 00 00       	call   80100aa0 <consputc>

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010082d:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100831:	8b 55 08             	mov    0x8(%ebp),%edx
80100834:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100837:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010083a:	0f b6 00             	movzbl (%eax),%eax
8010083d:	0f be c0             	movsbl %al,%eax
80100840:	25 ff 00 00 00       	and    $0xff,%eax
80100845:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100848:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010084c:	0f 85 bf fe ff ff    	jne    80100711 <cprintf+0x48>
80100852:	eb 01                	jmp    80100855 <cprintf+0x18c>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80100854:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
80100855:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80100859:	74 0c                	je     80100867 <cprintf+0x19e>
    release(&cons.lock);
8010085b:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100862:	e8 01 51 00 00       	call   80105968 <release>
}
80100867:	c9                   	leave  
80100868:	c3                   	ret    

80100869 <panic>:

void
panic(char *s)
{
80100869:	55                   	push   %ebp
8010086a:	89 e5                	mov    %esp,%ebp
8010086c:	83 ec 48             	sub    $0x48,%esp
  int i;
  uint pcs[10];
  
  cli();
8010086f:	e8 a7 fd ff ff       	call   8010061b <cli>
  cons.locking = 0;
80100874:	c7 05 34 c6 10 80 00 	movl   $0x0,0x8010c634
8010087b:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
8010087e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100884:	0f b6 00             	movzbl (%eax),%eax
80100887:	0f b6 c0             	movzbl %al,%eax
8010088a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010088e:	c7 04 24 65 90 10 80 	movl   $0x80109065,(%esp)
80100895:	e8 2f fe ff ff       	call   801006c9 <cprintf>
  cprintf(s);
8010089a:	8b 45 08             	mov    0x8(%ebp),%eax
8010089d:	89 04 24             	mov    %eax,(%esp)
801008a0:	e8 24 fe ff ff       	call   801006c9 <cprintf>
  cprintf("\n");
801008a5:	c7 04 24 74 90 10 80 	movl   $0x80109074,(%esp)
801008ac:	e8 18 fe ff ff       	call   801006c9 <cprintf>
  getcallerpcs(&s, NELEM(pcs), pcs);
801008b1:	8d 45 cc             	lea    -0x34(%ebp),%eax
801008b4:	89 44 24 08          	mov    %eax,0x8(%esp)
801008b8:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801008bf:	00 
801008c0:	8d 45 08             	lea    0x8(%ebp),%eax
801008c3:	89 04 24             	mov    %eax,(%esp)
801008c6:	e8 ec 50 00 00       	call   801059b7 <getcallerpcs>
  for(i=0; i<10; i++)
801008cb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801008d2:	eb 1b                	jmp    801008ef <panic+0x86>
    cprintf(" %p", pcs[i]);
801008d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801008d7:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801008db:	89 44 24 04          	mov    %eax,0x4(%esp)
801008df:	c7 04 24 76 90 10 80 	movl   $0x80109076,(%esp)
801008e6:	e8 de fd ff ff       	call   801006c9 <cprintf>
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, NELEM(pcs), pcs);
  for(i=0; i<10; i++)
801008eb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801008ef:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801008f3:	7e df                	jle    801008d4 <panic+0x6b>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801008f5:	c7 05 e0 c5 10 80 01 	movl   $0x1,0x8010c5e0
801008fc:	00 00 00 
  for(;;)
    ;
801008ff:	eb fe                	jmp    801008ff <panic+0x96>

80100901 <cgaputc>:
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
80100901:	55                   	push   %ebp
80100902:	89 e5                	mov    %esp,%ebp
80100904:	83 ec 28             	sub    $0x28,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
80100907:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
8010090e:	00 
8010090f:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100916:	e8 e2 fc ff ff       	call   801005fd <outb>
  pos = inb(CRTPORT+1) << 8;
8010091b:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100922:	e8 b9 fc ff ff       	call   801005e0 <inb>
80100927:	0f b6 c0             	movzbl %al,%eax
8010092a:	c1 e0 08             	shl    $0x8,%eax
8010092d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
80100930:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100937:	00 
80100938:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
8010093f:	e8 b9 fc ff ff       	call   801005fd <outb>
  pos |= inb(CRTPORT+1);
80100944:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
8010094b:	e8 90 fc ff ff       	call   801005e0 <inb>
80100950:	0f b6 c0             	movzbl %al,%eax
80100953:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
80100956:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
8010095a:	75 30                	jne    8010098c <cgaputc+0x8b>
    pos += 80 - pos%80;
8010095c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010095f:	ba 67 66 66 66       	mov    $0x66666667,%edx
80100964:	89 c8                	mov    %ecx,%eax
80100966:	f7 ea                	imul   %edx
80100968:	c1 fa 05             	sar    $0x5,%edx
8010096b:	89 c8                	mov    %ecx,%eax
8010096d:	c1 f8 1f             	sar    $0x1f,%eax
80100970:	29 c2                	sub    %eax,%edx
80100972:	89 d0                	mov    %edx,%eax
80100974:	c1 e0 02             	shl    $0x2,%eax
80100977:	01 d0                	add    %edx,%eax
80100979:	c1 e0 04             	shl    $0x4,%eax
8010097c:	89 ca                	mov    %ecx,%edx
8010097e:	29 c2                	sub    %eax,%edx
80100980:	b8 50 00 00 00       	mov    $0x50,%eax
80100985:	29 d0                	sub    %edx,%eax
80100987:	01 45 f4             	add    %eax,-0xc(%ebp)
8010098a:	eb 33                	jmp    801009bf <cgaputc+0xbe>
  else if(c == BACKSPACE){
8010098c:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100993:	75 0c                	jne    801009a1 <cgaputc+0xa0>
    if(pos > 0) --pos;
80100995:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100999:	7e 24                	jle    801009bf <cgaputc+0xbe>
8010099b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
8010099f:	eb 1e                	jmp    801009bf <cgaputc+0xbe>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
801009a1:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009a6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801009a9:	01 d2                	add    %edx,%edx
801009ab:	8d 14 10             	lea    (%eax,%edx,1),%edx
801009ae:	8b 45 08             	mov    0x8(%ebp),%eax
801009b1:	66 25 ff 00          	and    $0xff,%ax
801009b5:	80 cc 07             	or     $0x7,%ah
801009b8:	66 89 02             	mov    %ax,(%edx)
801009bb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)

  if(pos < 0 || pos > 25*80)
801009bf:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009c3:	78 09                	js     801009ce <cgaputc+0xcd>
801009c5:	81 7d f4 d0 07 00 00 	cmpl   $0x7d0,-0xc(%ebp)
801009cc:	7e 0c                	jle    801009da <cgaputc+0xd9>
    panic("pos under/overflow");
801009ce:	c7 04 24 7a 90 10 80 	movl   $0x8010907a,(%esp)
801009d5:	e8 8f fe ff ff       	call   80100869 <panic>
  
  if((pos/80) >= 24){  // Scroll up.
801009da:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801009e1:	7e 53                	jle    80100a36 <cgaputc+0x135>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801009e3:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009e8:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801009ee:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801009f3:	c7 44 24 08 60 0e 00 	movl   $0xe60,0x8(%esp)
801009fa:	00 
801009fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801009ff:	89 04 24             	mov    %eax,(%esp)
80100a02:	e8 2e 52 00 00       	call   80105c35 <memmove>
    pos -= 80;
80100a07:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100a0b:	b8 80 07 00 00       	mov    $0x780,%eax
80100a10:	2b 45 f4             	sub    -0xc(%ebp),%eax
80100a13:	8d 14 00             	lea    (%eax,%eax,1),%edx
80100a16:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a1b:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100a1e:	01 c9                	add    %ecx,%ecx
80100a20:	01 c8                	add    %ecx,%eax
80100a22:	89 54 24 08          	mov    %edx,0x8(%esp)
80100a26:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100a2d:	00 
80100a2e:	89 04 24             	mov    %eax,(%esp)
80100a31:	e8 2c 51 00 00       	call   80105b62 <memset>
  }
  
  outb(CRTPORT, 14);
80100a36:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
80100a3d:	00 
80100a3e:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a45:	e8 b3 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos>>8);
80100a4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a4d:	c1 f8 08             	sar    $0x8,%eax
80100a50:	0f b6 c0             	movzbl %al,%eax
80100a53:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a57:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a5e:	e8 9a fb ff ff       	call   801005fd <outb>
  outb(CRTPORT, 15);
80100a63:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100a6a:	00 
80100a6b:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100a72:	e8 86 fb ff ff       	call   801005fd <outb>
  outb(CRTPORT+1, pos);
80100a77:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100a7a:	0f b6 c0             	movzbl %al,%eax
80100a7d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100a81:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100a88:	e8 70 fb ff ff       	call   801005fd <outb>
  crt[pos] = ' ' | 0x0700;
80100a8d:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100a92:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100a95:	01 d2                	add    %edx,%edx
80100a97:	01 d0                	add    %edx,%eax
80100a99:	66 c7 00 20 07       	movw   $0x720,(%eax)
}
80100a9e:	c9                   	leave  
80100a9f:	c3                   	ret    

80100aa0 <consputc>:

void
consputc(int c)
{
80100aa0:	55                   	push   %ebp
80100aa1:	89 e5                	mov    %esp,%ebp
80100aa3:	83 ec 18             	sub    $0x18,%esp
  if(panicked){
80100aa6:	a1 e0 c5 10 80       	mov    0x8010c5e0,%eax
80100aab:	85 c0                	test   %eax,%eax
80100aad:	74 07                	je     80100ab6 <consputc+0x16>
    cli();
80100aaf:	e8 67 fb ff ff       	call   8010061b <cli>
    for(;;)
      ;
80100ab4:	eb fe                	jmp    80100ab4 <consputc+0x14>
  }

  if(c == BACKSPACE){
80100ab6:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100abd:	75 26                	jne    80100ae5 <consputc+0x45>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100abf:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ac6:	e8 a1 6b 00 00       	call   8010766c <uartputc>
80100acb:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100ad2:	e8 95 6b 00 00       	call   8010766c <uartputc>
80100ad7:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100ade:	e8 89 6b 00 00       	call   8010766c <uartputc>
80100ae3:	eb 0b                	jmp    80100af0 <consputc+0x50>
  } else
    uartputc(c);
80100ae5:	8b 45 08             	mov    0x8(%ebp),%eax
80100ae8:	89 04 24             	mov    %eax,(%esp)
80100aeb:	e8 7c 6b 00 00       	call   8010766c <uartputc>
  cgaputc(c);
80100af0:	8b 45 08             	mov    0x8(%ebp),%eax
80100af3:	89 04 24             	mov    %eax,(%esp)
80100af6:	e8 06 fe ff ff       	call   80100901 <cgaputc>
}
80100afb:	c9                   	leave  
80100afc:	c3                   	ret    

80100afd <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
80100afd:	55                   	push   %ebp
80100afe:	89 e5                	mov    %esp,%ebp
80100b00:	83 ec 28             	sub    $0x28,%esp
  int c, doprocdump = 0;
80100b03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&cons.lock);
80100b0a:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100b11:	e8 e9 4d 00 00       	call   801058ff <acquire>
  while((c = getc()) >= 0){
80100b16:	e9 40 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    switch(c){
80100b1b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100b1e:	83 f8 10             	cmp    $0x10,%eax
80100b21:	74 1e                	je     80100b41 <consoleintr+0x44>
80100b23:	83 f8 10             	cmp    $0x10,%eax
80100b26:	7f 0a                	jg     80100b32 <consoleintr+0x35>
80100b28:	83 f8 08             	cmp    $0x8,%eax
80100b2b:	74 6a                	je     80100b97 <consoleintr+0x9a>
80100b2d:	e9 96 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
80100b32:	83 f8 15             	cmp    $0x15,%eax
80100b35:	74 31                	je     80100b68 <consoleintr+0x6b>
80100b37:	83 f8 7f             	cmp    $0x7f,%eax
80100b3a:	74 5b                	je     80100b97 <consoleintr+0x9a>
80100b3c:	e9 87 00 00 00       	jmp    80100bc8 <consoleintr+0xcb>
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
80100b41:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
      break;
80100b48:	e9 0e 01 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
80100b4d:	a1 88 20 11 80       	mov    0x80112088,%eax
80100b52:	83 e8 01             	sub    $0x1,%eax
80100b55:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(BACKSPACE);
80100b5a:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100b61:	e8 3a ff ff ff       	call   80100aa0 <consputc>
80100b66:	eb 01                	jmp    80100b69 <consoleintr+0x6c>
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b68:	90                   	nop
80100b69:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100b6f:	a1 84 20 11 80       	mov    0x80112084,%eax
80100b74:	39 c2                	cmp    %eax,%edx
80100b76:	0f 84 db 00 00 00    	je     80100c57 <consoleintr+0x15a>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100b7c:	a1 88 20 11 80       	mov    0x80112088,%eax
80100b81:	83 e8 01             	sub    $0x1,%eax
80100b84:	83 e0 7f             	and    $0x7f,%eax
80100b87:	0f b6 80 00 20 11 80 	movzbl -0x7feee000(%eax),%eax
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100b8e:	3c 0a                	cmp    $0xa,%al
80100b90:	75 bb                	jne    80100b4d <consoleintr+0x50>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100b92:	e9 c4 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
80100b97:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100b9d:	a1 84 20 11 80       	mov    0x80112084,%eax
80100ba2:	39 c2                	cmp    %eax,%edx
80100ba4:	0f 84 b0 00 00 00    	je     80100c5a <consoleintr+0x15d>
        input.e--;
80100baa:	a1 88 20 11 80       	mov    0x80112088,%eax
80100baf:	83 e8 01             	sub    $0x1,%eax
80100bb2:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(BACKSPACE);
80100bb7:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100bbe:	e8 dd fe ff ff       	call   80100aa0 <consputc>
      }
      break;
80100bc3:	e9 93 00 00 00       	jmp    80100c5b <consoleintr+0x15e>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
80100bc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100bcc:	0f 84 89 00 00 00    	je     80100c5b <consoleintr+0x15e>
80100bd2:	8b 15 88 20 11 80    	mov    0x80112088,%edx
80100bd8:	a1 80 20 11 80       	mov    0x80112080,%eax
80100bdd:	89 d1                	mov    %edx,%ecx
80100bdf:	29 c1                	sub    %eax,%ecx
80100be1:	89 c8                	mov    %ecx,%eax
80100be3:	83 f8 7f             	cmp    $0x7f,%eax
80100be6:	77 73                	ja     80100c5b <consoleintr+0x15e>
        c = (c == '\r') ? '\n' : c;
80100be8:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
80100bec:	74 05                	je     80100bf3 <consoleintr+0xf6>
80100bee:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100bf1:	eb 05                	jmp    80100bf8 <consoleintr+0xfb>
80100bf3:	b8 0a 00 00 00       	mov    $0xa,%eax
80100bf8:	89 45 f0             	mov    %eax,-0x10(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100bfb:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c00:	89 c1                	mov    %eax,%ecx
80100c02:	83 e1 7f             	and    $0x7f,%ecx
80100c05:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100c08:	88 91 00 20 11 80    	mov    %dl,-0x7feee000(%ecx)
80100c0e:	83 c0 01             	add    $0x1,%eax
80100c11:	a3 88 20 11 80       	mov    %eax,0x80112088
        consputc(c);
80100c16:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100c19:	89 04 24             	mov    %eax,(%esp)
80100c1c:	e8 7f fe ff ff       	call   80100aa0 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100c21:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100c25:	74 18                	je     80100c3f <consoleintr+0x142>
80100c27:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100c2b:	74 12                	je     80100c3f <consoleintr+0x142>
80100c2d:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c32:	8b 15 80 20 11 80    	mov    0x80112080,%edx
80100c38:	83 ea 80             	sub    $0xffffff80,%edx
80100c3b:	39 d0                	cmp    %edx,%eax
80100c3d:	75 1c                	jne    80100c5b <consoleintr+0x15e>
          input.w = input.e;
80100c3f:	a1 88 20 11 80       	mov    0x80112088,%eax
80100c44:	a3 84 20 11 80       	mov    %eax,0x80112084
          wakeup(&input.r);
80100c49:	c7 04 24 80 20 11 80 	movl   $0x80112080,(%esp)
80100c50:	e8 a5 4a 00 00       	call   801056fa <wakeup>
80100c55:	eb 04                	jmp    80100c5b <consoleintr+0x15e>
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c57:	90                   	nop
80100c58:	eb 01                	jmp    80100c5b <consoleintr+0x15e>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100c5a:	90                   	nop
consoleintr(int (*getc)(void))
{
  int c, doprocdump = 0;

  acquire(&cons.lock);
  while((c = getc()) >= 0){
80100c5b:	8b 45 08             	mov    0x8(%ebp),%eax
80100c5e:	ff d0                	call   *%eax
80100c60:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100c63:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100c67:	0f 89 ae fe ff ff    	jns    80100b1b <consoleintr+0x1e>
        }
      }
      break;
    }
  }
  release(&cons.lock);
80100c6d:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100c74:	e8 ef 4c 00 00       	call   80105968 <release>
  if(doprocdump) {
80100c79:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100c7d:	74 05                	je     80100c84 <consoleintr+0x187>
    procdump();  // now call procdump() wo. cons.lock held
80100c7f:	e8 1d 4b 00 00       	call   801057a1 <procdump>
  }
}
80100c84:	c9                   	leave  
80100c85:	c3                   	ret    

80100c86 <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
80100c86:	55                   	push   %ebp
80100c87:	89 e5                	mov    %esp,%ebp
80100c89:	83 ec 28             	sub    $0x28,%esp
  uint target;
  int c;

  iunlock(ip);
80100c8c:	8b 45 08             	mov    0x8(%ebp),%eax
80100c8f:	89 04 24             	mov    %eax,(%esp)
80100c92:	e8 31 15 00 00       	call   801021c8 <iunlock>
  target = n;
80100c97:	8b 45 10             	mov    0x10(%ebp),%eax
80100c9a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  acquire(&cons.lock);
80100c9d:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100ca4:	e8 56 4c 00 00       	call   801058ff <acquire>
  while(n > 0){
80100ca9:	e9 a8 00 00 00       	jmp    80100d56 <consoleread+0xd0>
    while(input.r == input.w){
      if(proc->killed){
80100cae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100cb4:	8b 40 24             	mov    0x24(%eax),%eax
80100cb7:	85 c0                	test   %eax,%eax
80100cb9:	74 21                	je     80100cdc <consoleread+0x56>
        release(&cons.lock);
80100cbb:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100cc2:	e8 a1 4c 00 00       	call   80105968 <release>
        ilock(ip);
80100cc7:	8b 45 08             	mov    0x8(%ebp),%eax
80100cca:	89 04 24             	mov    %eax,(%esp)
80100ccd:	e8 9f 13 00 00       	call   80102071 <ilock>
        return -1;
80100cd2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100cd7:	e9 a9 00 00 00       	jmp    80100d85 <consoleread+0xff>
      }
      sleep(&input.r, &cons.lock);
80100cdc:	c7 44 24 04 00 c6 10 	movl   $0x8010c600,0x4(%esp)
80100ce3:	80 
80100ce4:	c7 04 24 80 20 11 80 	movl   $0x80112080,(%esp)
80100ceb:	e8 2d 49 00 00       	call   8010561d <sleep>
80100cf0:	eb 01                	jmp    80100cf3 <consoleread+0x6d>

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
80100cf2:	90                   	nop
80100cf3:	8b 15 80 20 11 80    	mov    0x80112080,%edx
80100cf9:	a1 84 20 11 80       	mov    0x80112084,%eax
80100cfe:	39 c2                	cmp    %eax,%edx
80100d00:	74 ac                	je     80100cae <consoleread+0x28>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100d02:	a1 80 20 11 80       	mov    0x80112080,%eax
80100d07:	89 c2                	mov    %eax,%edx
80100d09:	83 e2 7f             	and    $0x7f,%edx
80100d0c:	0f b6 92 00 20 11 80 	movzbl -0x7feee000(%edx),%edx
80100d13:	0f be d2             	movsbl %dl,%edx
80100d16:	89 55 f4             	mov    %edx,-0xc(%ebp)
80100d19:	83 c0 01             	add    $0x1,%eax
80100d1c:	a3 80 20 11 80       	mov    %eax,0x80112080
    if(c == C('D')){  // EOF
80100d21:	83 7d f4 04          	cmpl   $0x4,-0xc(%ebp)
80100d25:	75 17                	jne    80100d3e <consoleread+0xb8>
      if(n < target){
80100d27:	8b 45 10             	mov    0x10(%ebp),%eax
80100d2a:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80100d2d:	73 2f                	jae    80100d5e <consoleread+0xd8>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100d2f:	a1 80 20 11 80       	mov    0x80112080,%eax
80100d34:	83 e8 01             	sub    $0x1,%eax
80100d37:	a3 80 20 11 80       	mov    %eax,0x80112080
      }
      break;
80100d3c:	eb 24                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
80100d3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100d41:	89 c2                	mov    %eax,%edx
80100d43:	8b 45 0c             	mov    0xc(%ebp),%eax
80100d46:	88 10                	mov    %dl,(%eax)
80100d48:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
    --n;
80100d4c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100d50:	83 7d f4 0a          	cmpl   $0xa,-0xc(%ebp)
80100d54:	74 0b                	je     80100d61 <consoleread+0xdb>
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
80100d56:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100d5a:	7f 96                	jg     80100cf2 <consoleread+0x6c>
80100d5c:	eb 04                	jmp    80100d62 <consoleread+0xdc>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100d5e:	90                   	nop
80100d5f:	eb 01                	jmp    80100d62 <consoleread+0xdc>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100d61:	90                   	nop
  }
  release(&cons.lock);
80100d62:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100d69:	e8 fa 4b 00 00       	call   80105968 <release>
  ilock(ip);
80100d6e:	8b 45 08             	mov    0x8(%ebp),%eax
80100d71:	89 04 24             	mov    %eax,(%esp)
80100d74:	e8 f8 12 00 00       	call   80102071 <ilock>

  return target - n;
80100d79:	8b 45 10             	mov    0x10(%ebp),%eax
80100d7c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100d7f:	89 d1                	mov    %edx,%ecx
80100d81:	29 c1                	sub    %eax,%ecx
80100d83:	89 c8                	mov    %ecx,%eax
}
80100d85:	c9                   	leave  
80100d86:	c3                   	ret    

80100d87 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100d87:	55                   	push   %ebp
80100d88:	89 e5                	mov    %esp,%ebp
80100d8a:	83 ec 28             	sub    $0x28,%esp
  int i;

  iunlock(ip);
80100d8d:	8b 45 08             	mov    0x8(%ebp),%eax
80100d90:	89 04 24             	mov    %eax,(%esp)
80100d93:	e8 30 14 00 00       	call   801021c8 <iunlock>
  acquire(&cons.lock);
80100d98:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100d9f:	e8 5b 4b 00 00       	call   801058ff <acquire>
  for(i = 0; i < n; i++)
80100da4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100dab:	eb 1d                	jmp    80100dca <consolewrite+0x43>
    consputc(buf[i] & 0xff);
80100dad:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100db0:	03 45 0c             	add    0xc(%ebp),%eax
80100db3:	0f b6 00             	movzbl (%eax),%eax
80100db6:	0f be c0             	movsbl %al,%eax
80100db9:	25 ff 00 00 00       	and    $0xff,%eax
80100dbe:	89 04 24             	mov    %eax,(%esp)
80100dc1:	e8 da fc ff ff       	call   80100aa0 <consputc>
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100dc6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100dca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100dcd:	3b 45 10             	cmp    0x10(%ebp),%eax
80100dd0:	7c db                	jl     80100dad <consolewrite+0x26>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100dd2:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100dd9:	e8 8a 4b 00 00       	call   80105968 <release>
  ilock(ip);
80100dde:	8b 45 08             	mov    0x8(%ebp),%eax
80100de1:	89 04 24             	mov    %eax,(%esp)
80100de4:	e8 88 12 00 00       	call   80102071 <ilock>

  return n;
80100de9:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100dec:	c9                   	leave  
80100ded:	c3                   	ret    

80100dee <consoleinit>:

void
consoleinit(void)
{
80100dee:	55                   	push   %ebp
80100def:	89 e5                	mov    %esp,%ebp
80100df1:	83 ec 18             	sub    $0x18,%esp
  initlock(&cons.lock, "console");
80100df4:	c7 44 24 04 8d 90 10 	movl   $0x8010908d,0x4(%esp)
80100dfb:	80 
80100dfc:	c7 04 24 00 c6 10 80 	movl   $0x8010c600,(%esp)
80100e03:	e8 d6 4a 00 00       	call   801058de <initlock>

  devsw[CONSOLE].write = consolewrite;
80100e08:	c7 05 4c 2a 11 80 87 	movl   $0x80100d87,0x80112a4c
80100e0f:	0d 10 80 
  devsw[CONSOLE].read = consoleread;
80100e12:	c7 05 48 2a 11 80 86 	movl   $0x80100c86,0x80112a48
80100e19:	0c 10 80 
  cons.locking = 1;
80100e1c:	c7 05 34 c6 10 80 01 	movl   $0x1,0x8010c634
80100e23:	00 00 00 

  picenable(IRQ_KBD);
80100e26:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e2d:	e8 6b 36 00 00       	call   8010449d <picenable>
  ioapicenable(IRQ_KBD, 0);
80100e32:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100e39:	00 
80100e3a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100e41:	e8 b4 23 00 00       	call   801031fa <ioapicenable>
}
80100e46:	c9                   	leave  
80100e47:	c3                   	ret    

80100e48 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80100e48:	55                   	push   %ebp
80100e49:	89 e5                	mov    %esp,%ebp
80100e4b:	83 ec 14             	sub    $0x14,%esp
80100e4e:	8b 45 08             	mov    0x8(%ebp),%eax
80100e51:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100e55:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80100e59:	89 c2                	mov    %eax,%edx
80100e5b:	ec                   	in     (%dx),%al
80100e5c:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80100e5f:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80100e63:	c9                   	leave  
80100e64:	c3                   	ret    

80100e65 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80100e65:	55                   	push   %ebp
80100e66:	89 e5                	mov    %esp,%ebp
80100e68:	83 ec 08             	sub    $0x8,%esp
80100e6b:	8b 55 08             	mov    0x8(%ebp),%edx
80100e6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e71:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80100e75:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100e78:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100e7c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80100e80:	ee                   	out    %al,(%dx)
}
80100e81:	c9                   	leave  
80100e82:	c3                   	ret    

80100e83 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80100e83:	55                   	push   %ebp
80100e84:	89 e5                	mov    %esp,%ebp
80100e86:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80100e89:	9c                   	pushf  
80100e8a:	58                   	pop    %eax
80100e8b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80100e8e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100e91:	c9                   	leave  
80100e92:	c3                   	ret    

80100e93 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80100e93:	55                   	push   %ebp
80100e94:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80100e96:	fa                   	cli    
}
80100e97:	5d                   	pop    %ebp
80100e98:	c3                   	ret    

80100e99 <sti>:

static inline void
sti(void)
{
80100e99:	55                   	push   %ebp
80100e9a:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80100e9c:	fb                   	sti    
}
80100e9d:	5d                   	pop    %ebp
80100e9e:	c3                   	ret    

80100e9f <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
80100e9f:	55                   	push   %ebp
80100ea0:	89 e5                	mov    %esp,%ebp
80100ea2:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80100ea5:	8b 55 08             	mov    0x8(%ebp),%edx
80100ea8:	8b 45 0c             	mov    0xc(%ebp),%eax
80100eab:	8b 4d 08             	mov    0x8(%ebp),%ecx
80100eae:	f0 87 02             	lock xchg %eax,(%edx)
80100eb1:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80100eb4:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80100eb7:	c9                   	leave  
80100eb8:	c3                   	ret    

80100eb9 <lock>:
// debugf(), we don't have to worry about which CPU we're on. The spinlocks in
// xv6 presumably pay that prize to make their interface simpler?

static void
lock(int *intena)
{
80100eb9:	55                   	push   %ebp
80100eba:	89 e5                	mov    %esp,%ebp
80100ebc:	83 ec 08             	sub    $0x8,%esp
  *intena = readeflags() & FL_IF;
80100ebf:	e8 bf ff ff ff       	call   80100e83 <readeflags>
80100ec4:	89 c2                	mov    %eax,%edx
80100ec6:	81 e2 00 02 00 00    	and    $0x200,%edx
80100ecc:	8b 45 08             	mov    0x8(%ebp),%eax
80100ecf:	89 10                	mov    %edx,(%eax)
  // Migration is possible *right here*, but if so, FL_IF was asserted when
  // we sampled it and is still asserted now. That holds true up until the
  // CLI instruction itself. (Also see TRICKS.)
  cli();
80100ed1:	e8 bd ff ff ff       	call   80100e93 <cli>
  while(xchg(&locked, 1) != 0)
80100ed6:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80100edd:	00 
80100ede:	c7 04 24 3c c6 10 80 	movl   $0x8010c63c,(%esp)
80100ee5:	e8 b5 ff ff ff       	call   80100e9f <xchg>
80100eea:	85 c0                	test   %eax,%eax
80100eec:	75 e8                	jne    80100ed6 <lock+0x1d>
    ;
}
80100eee:	c9                   	leave  
80100eef:	c3                   	ret    

80100ef0 <unlock>:

static void
unlock(int intena)
{
80100ef0:	55                   	push   %ebp
80100ef1:	89 e5                	mov    %esp,%ebp
80100ef3:	83 ec 08             	sub    $0x8,%esp
  xchg(&locked, 0);
80100ef6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100efd:	00 
80100efe:	c7 04 24 3c c6 10 80 	movl   $0x8010c63c,(%esp)
80100f05:	e8 95 ff ff ff       	call   80100e9f <xchg>
  if(intena)
80100f0a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80100f0e:	74 05                	je     80100f15 <unlock+0x25>
    sti();
80100f10:	e8 84 ff ff ff       	call   80100e99 <sti>
}
80100f15:	c9                   	leave  
80100f16:	c3                   	ret    

80100f17 <cantransmit>:

static int
cantransmit(void)
{
80100f17:	55                   	push   %ebp
80100f18:	89 e5                	mov    %esp,%ebp
80100f1a:	83 ec 04             	sub    $0x4,%esp
  return inb(COM2+UART_LINE_STATUS) & UART_TRANSMIT_READY;
80100f1d:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
80100f24:	e8 1f ff ff ff       	call   80100e48 <inb>
80100f29:	0f b6 c0             	movzbl %al,%eax
80100f2c:	83 e0 20             	and    $0x20,%eax
}
80100f2f:	c9                   	leave  
80100f30:	c3                   	ret    

80100f31 <debugputc>:

static void
debugputc(int c)
{
80100f31:	55                   	push   %ebp
80100f32:	89 e5                	mov    %esp,%ebp
80100f34:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
80100f37:	a1 38 c6 10 80       	mov    0x8010c638,%eax
80100f3c:	85 c0                	test   %eax,%eax
80100f3e:	74 40                	je     80100f80 <debugputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f40:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100f47:	eb 10                	jmp    80100f59 <debugputc+0x28>
    microdelay(10);
80100f49:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
80100f50:	e8 26 28 00 00       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80100f55:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100f59:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
80100f5d:	7f 09                	jg     80100f68 <debugputc+0x37>
80100f5f:	e8 b3 ff ff ff       	call   80100f17 <cantransmit>
80100f64:	85 c0                	test   %eax,%eax
80100f66:	74 e1                	je     80100f49 <debugputc+0x18>
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
80100f68:	8b 45 08             	mov    0x8(%ebp),%eax
80100f6b:	0f b6 c0             	movzbl %al,%eax
80100f6e:	89 44 24 04          	mov    %eax,0x4(%esp)
80100f72:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100f79:	e8 e7 fe ff ff       	call   80100e65 <outb>
80100f7e:	eb 01                	jmp    80100f81 <debugputc+0x50>
debugputc(int c)
{
  int i;

  if(!uart)
    return;
80100f80:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM2+UART_TRANSMIT_BUFFER, c);
}
80100f81:	c9                   	leave  
80100f82:	c3                   	ret    

80100f83 <debuginit>:

void
debuginit(void)
{
80100f83:	55                   	push   %ebp
80100f84:	89 e5                	mov    %esp,%ebp
80100f86:	83 ec 08             	sub    $0x8,%esp
  // Turn off the FIFO
  outb(COM2+UART_FIFO_CONTROL, 0);
80100f89:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100f90:	00 
80100f91:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80100f98:	e8 c8 fe ff ff       	call   80100e65 <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM2+UART_LINE_CONTROL, UART_DIVISOR_LATCH);
80100f9d:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
80100fa4:	00 
80100fa5:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fac:	e8 b4 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_LOW, 115200/9600);
80100fb1:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
80100fb8:	00 
80100fb9:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80100fc0:	e8 a0 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_DIVISOR_HIGH, 0);
80100fc5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100fcc:	00 
80100fcd:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80100fd4:	e8 8c fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_LINE_CONTROL, 0x03);   // 8 data bits.
80100fd9:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80100fe0:	00 
80100fe1:	c7 04 24 fb 02 00 00 	movl   $0x2fb,(%esp)
80100fe8:	e8 78 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
80100fed:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100ff4:	00 
80100ff5:	c7 04 24 fc 02 00 00 	movl   $0x2fc,(%esp)
80100ffc:	e8 64 fe ff ff       	call   80100e65 <outb>
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.
80101001:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101008:	00 
80101009:	c7 04 24 f9 02 00 00 	movl   $0x2f9,(%esp)
80101010:	e8 50 fe ff ff       	call   80100e65 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
80101015:	c7 04 24 fd 02 00 00 	movl   $0x2fd,(%esp)
8010101c:	e8 27 fe ff ff       	call   80100e48 <inb>
80101021:	3c ff                	cmp    $0xff,%al
80101023:	74 24                	je     80101049 <debuginit+0xc6>
    return;
  uart = 1;
80101025:	c7 05 38 c6 10 80 01 	movl   $0x1,0x8010c638
8010102c:	00 00 00 

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
8010102f:	c7 04 24 fa 02 00 00 	movl   $0x2fa,(%esp)
80101036:	e8 0d fe ff ff       	call   80100e48 <inb>
  inb(COM2+UART_RECEIVE_BUFFER);
8010103b:	c7 04 24 f8 02 00 00 	movl   $0x2f8,(%esp)
80101042:	e8 01 fe ff ff       	call   80100e48 <inb>
80101047:	eb 01                	jmp    8010104a <debuginit+0xc7>
  outb(COM2+UART_MODEM_CONTROL, 0);     // No RTS/DTR handshake.
  outb(COM2+UART_INTERRUPT_ENABLE, 0);  // Disable interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM2+UART_LINE_STATUS) == 0xFF)
    return;
80101049:	90                   	nop

  // Acknowledge pre-existing interrupt conditions, if any.
  // We don't use any of the interrupt stuff ourselves.
  inb(COM2+UART_INTERRUPT_ID);
  inb(COM2+UART_RECEIVE_BUFFER);
}
8010104a:	c9                   	leave  
8010104b:	c3                   	ret    

8010104c <printint>:
// It's certainly sad that we have to replicate the printf code *again* for
// this module. Alas there doesn't seem to be a nice way to modularize.

static void
printint(int xx, int base, int sign)
{
8010104c:	55                   	push   %ebp
8010104d:	89 e5                	mov    %esp,%ebp
8010104f:	53                   	push   %ebx
80101050:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101053:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101057:	74 19                	je     80101072 <printint+0x26>
80101059:	8b 45 08             	mov    0x8(%ebp),%eax
8010105c:	c1 e8 1f             	shr    $0x1f,%eax
8010105f:	89 45 10             	mov    %eax,0x10(%ebp)
80101062:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101066:	74 0a                	je     80101072 <printint+0x26>
    x = -xx;
80101068:	8b 45 08             	mov    0x8(%ebp),%eax
8010106b:	f7 d8                	neg    %eax
8010106d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = (xx < 0)))
80101070:	eb 06                	jmp    80101078 <printint+0x2c>
    x = -xx;
  else
    x = xx;
80101072:	8b 45 08             	mov    0x8(%ebp),%eax
80101075:	89 45 f4             	mov    %eax,-0xc(%ebp)

  i = 0;
80101078:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  do{
    buf[i++] = digits[x % base];
8010107f:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80101082:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80101085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101088:	ba 00 00 00 00       	mov    $0x0,%edx
8010108d:	f7 f3                	div    %ebx
8010108f:	89 d0                	mov    %edx,%eax
80101091:	0f b6 80 18 a0 10 80 	movzbl -0x7fef5fe8(%eax),%eax
80101098:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
8010109c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  }while((x /= base) != 0);
801010a0:	8b 45 0c             	mov    0xc(%ebp),%eax
801010a3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801010a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801010a9:	ba 00 00 00 00       	mov    $0x0,%edx
801010ae:	f7 75 d4             	divl   -0x2c(%ebp)
801010b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801010b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801010b8:	75 c5                	jne    8010107f <printint+0x33>

  if(sign)
801010ba:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801010be:	74 23                	je     801010e3 <printint+0x97>
    buf[i++] = '-';
801010c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010c3:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)
801010c8:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)

  while(--i >= 0)
801010cc:	eb 16                	jmp    801010e4 <printint+0x98>
    debugputc(buf[i]);
801010ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
801010d1:	0f b6 44 05 e0       	movzbl -0x20(%ebp,%eax,1),%eax
801010d6:	0f be c0             	movsbl %al,%eax
801010d9:	89 04 24             	mov    %eax,(%esp)
801010dc:	e8 50 fe ff ff       	call   80100f31 <debugputc>
801010e1:	eb 01                	jmp    801010e4 <printint+0x98>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801010e3:	90                   	nop
801010e4:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
801010e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801010ec:	79 e0                	jns    801010ce <printint+0x82>
    debugputc(buf[i]);
}
801010ee:	83 c4 44             	add    $0x44,%esp
801010f1:	5b                   	pop    %ebx
801010f2:	5d                   	pop    %ebp
801010f3:	c3                   	ret    

801010f4 <debugf>:

void
debugf(char *fmt, ...)
{
801010f4:	55                   	push   %ebp
801010f5:	89 e5                	mov    %esp,%ebp
801010f7:	83 ec 38             	sub    $0x38,%esp
  int i, c;
  uint *argp;
  char *s;
  int intena;

  if(fmt == 0){
801010fa:	8b 45 08             	mov    0x8(%ebp),%eax
801010fd:	85 c0                	test   %eax,%eax
801010ff:	75 11                	jne    80101112 <debugf+0x1e>
    debugf("debugf: null fmt\n");  // no panic(), no console.c dependency
80101101:	c7 04 24 95 90 10 80 	movl   $0x80109095,(%esp)
80101108:	e8 e7 ff ff ff       	call   801010f4 <debugf>
    return;
8010110d:	e9 6f 01 00 00       	jmp    80101281 <debugf+0x18d>
  }

  lock(&intena);
80101112:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80101115:	89 04 24             	mov    %eax,(%esp)
80101118:	e8 9c fd ff ff       	call   80100eb9 <lock>

  argp = (uint*)(void*)(&fmt + 1);
8010111d:	8d 45 08             	lea    0x8(%ebp),%eax
80101120:	83 c0 04             	add    $0x4,%eax
80101123:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80101126:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010112d:	e9 20 01 00 00       	jmp    80101252 <debugf+0x15e>
    if(c != '%'){
80101132:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
80101136:	74 10                	je     80101148 <debugf+0x54>
      debugputc(c);
80101138:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010113b:	89 04 24             	mov    %eax,(%esp)
8010113e:	e8 ee fd ff ff       	call   80100f31 <debugputc>
      continue;
80101143:	e9 06 01 00 00       	jmp    8010124e <debugf+0x15a>
    }
    c = fmt[++i] & 0xff;
80101148:	8b 55 08             	mov    0x8(%ebp),%edx
8010114b:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
8010114f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101152:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101155:	0f b6 00             	movzbl (%eax),%eax
80101158:	0f be c0             	movsbl %al,%eax
8010115b:	25 ff 00 00 00       	and    $0xff,%eax
80101160:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(c == 0)
80101163:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101167:	0f 84 08 01 00 00    	je     80101275 <debugf+0x181>
      break;
    switch(c){
8010116d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101170:	83 f8 70             	cmp    $0x70,%eax
80101173:	74 4d                	je     801011c2 <debugf+0xce>
80101175:	83 f8 70             	cmp    $0x70,%eax
80101178:	7f 13                	jg     8010118d <debugf+0x99>
8010117a:	83 f8 25             	cmp    $0x25,%eax
8010117d:	0f 84 a6 00 00 00    	je     80101229 <debugf+0x135>
80101183:	83 f8 64             	cmp    $0x64,%eax
80101186:	74 14                	je     8010119c <debugf+0xa8>
80101188:	e9 aa 00 00 00       	jmp    80101237 <debugf+0x143>
8010118d:	83 f8 73             	cmp    $0x73,%eax
80101190:	74 53                	je     801011e5 <debugf+0xf1>
80101192:	83 f8 78             	cmp    $0x78,%eax
80101195:	74 2b                	je     801011c2 <debugf+0xce>
80101197:	e9 9b 00 00 00       	jmp    80101237 <debugf+0x143>
    case 'd':
      printint(*argp++, 10, 1);
8010119c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010119f:	8b 00                	mov    (%eax),%eax
801011a1:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011a5:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
801011ac:	00 
801011ad:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801011b4:	00 
801011b5:	89 04 24             	mov    %eax,(%esp)
801011b8:	e8 8f fe ff ff       	call   8010104c <printint>
      break;
801011bd:	e9 8c 00 00 00       	jmp    8010124e <debugf+0x15a>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
801011c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011c5:	8b 00                	mov    (%eax),%eax
801011c7:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011cb:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801011d2:	00 
801011d3:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
801011da:	00 
801011db:	89 04 24             	mov    %eax,(%esp)
801011de:	e8 69 fe ff ff       	call   8010104c <printint>
      break;
801011e3:	eb 69                	jmp    8010124e <debugf+0x15a>
    case 's':
      if((s = (char*)*argp++) == 0)
801011e5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011e8:	8b 00                	mov    (%eax),%eax
801011ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
801011ed:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801011f1:	0f 94 c0             	sete   %al
801011f4:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801011f8:	84 c0                	test   %al,%al
801011fa:	74 20                	je     8010121c <debugf+0x128>
        s = "(null)";
801011fc:	c7 45 f4 a7 90 10 80 	movl   $0x801090a7,-0xc(%ebp)
      for(; *s; s++)
80101203:	eb 18                	jmp    8010121d <debugf+0x129>
        debugputc(*s);
80101205:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101208:	0f b6 00             	movzbl (%eax),%eax
8010120b:	0f be c0             	movsbl %al,%eax
8010120e:	89 04 24             	mov    %eax,(%esp)
80101211:	e8 1b fd ff ff       	call   80100f31 <debugputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
80101216:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010121a:	eb 01                	jmp    8010121d <debugf+0x129>
8010121c:	90                   	nop
8010121d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101220:	0f b6 00             	movzbl (%eax),%eax
80101223:	84 c0                	test   %al,%al
80101225:	75 de                	jne    80101205 <debugf+0x111>
        debugputc(*s);
      break;
80101227:	eb 25                	jmp    8010124e <debugf+0x15a>
    case '%':
      debugputc('%');
80101229:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
80101230:	e8 fc fc ff ff       	call   80100f31 <debugputc>
      break;
80101235:	eb 17                	jmp    8010124e <debugf+0x15a>
    default:
      // Print unknown % sequence to draw attention.
      debugputc('%');
80101237:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
8010123e:	e8 ee fc ff ff       	call   80100f31 <debugputc>
      debugputc(c);
80101243:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101246:	89 04 24             	mov    %eax,(%esp)
80101249:	e8 e3 fc ff ff       	call   80100f31 <debugputc>
  }

  lock(&intena);

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010124e:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80101252:	8b 55 08             	mov    0x8(%ebp),%edx
80101255:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101258:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010125b:	0f b6 00             	movzbl (%eax),%eax
8010125e:	0f be c0             	movsbl %al,%eax
80101261:	25 ff 00 00 00       	and    $0xff,%eax
80101266:	89 45 ec             	mov    %eax,-0x14(%ebp)
80101269:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010126d:	0f 85 bf fe ff ff    	jne    80101132 <debugf+0x3e>
80101273:	eb 01                	jmp    80101276 <debugf+0x182>
      debugputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80101275:	90                   	nop
      debugputc(c);
      break;
    }
  }

  unlock(intena);
80101276:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101279:	89 04 24             	mov    %eax,(%esp)
8010127c:	e8 6f fc ff ff       	call   80100ef0 <unlock>
}
80101281:	c9                   	leave  
80101282:	c3                   	ret    
	...

80101284 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80101284:	55                   	push   %ebp
80101285:	89 e5                	mov    %esp,%ebp
80101287:	81 ec 38 01 00 00    	sub    $0x138,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
8010128d:	e8 d8 27 00 00       	call   80103a6a <begin_op>
  if((ip = namei(path)) == 0){
80101292:	8b 45 08             	mov    0x8(%ebp),%eax
80101295:	89 04 24             	mov    %eax,(%esp)
80101298:	e8 82 19 00 00       	call   80102c1f <namei>
8010129d:	89 45 ec             	mov    %eax,-0x14(%ebp)
801012a0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801012a4:	75 0f                	jne    801012b5 <exec+0x31>
    end_op();
801012a6:	e8 41 28 00 00       	call   80103aec <end_op>
    return -1;
801012ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801012b0:	e9 e2 03 00 00       	jmp    80101697 <exec+0x413>
  }
  ilock(ip);
801012b5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012b8:	89 04 24             	mov    %eax,(%esp)
801012bb:	e8 b1 0d 00 00       	call   80102071 <ilock>
  pgdir = 0;
801012c0:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
801012c7:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
801012cd:	c7 44 24 0c 34 00 00 	movl   $0x34,0xc(%esp)
801012d4:	00 
801012d5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801012dc:	00 
801012dd:	89 44 24 04          	mov    %eax,0x4(%esp)
801012e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012e4:	89 04 24             	mov    %eax,(%esp)
801012e7:	e8 84 12 00 00       	call   80102570 <readi>
801012ec:	83 f8 33             	cmp    $0x33,%eax
801012ef:	0f 86 57 03 00 00    	jbe    8010164c <exec+0x3c8>
    goto bad;
  if(elf.magic != ELF_MAGIC)
801012f5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
801012fb:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80101300:	0f 85 49 03 00 00    	jne    8010164f <exec+0x3cb>
    goto bad;

  if((pgdir = setupkvm()) == 0)
80101306:	e8 6e 74 00 00       	call   80108779 <setupkvm>
8010130b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010130e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80101312:	0f 84 3a 03 00 00    	je     80101652 <exec+0x3ce>
    goto bad;

  // Load program into memory.
  sz = 0;
80101318:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
8010131f:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
80101326:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
8010132c:	89 45 dc             	mov    %eax,-0x24(%ebp)
8010132f:	e9 ca 00 00 00       	jmp    801013fe <exec+0x17a>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80101334:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101337:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
8010133d:	c7 44 24 0c 20 00 00 	movl   $0x20,0xc(%esp)
80101344:	00 
80101345:	89 54 24 08          	mov    %edx,0x8(%esp)
80101349:	89 44 24 04          	mov    %eax,0x4(%esp)
8010134d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101350:	89 04 24             	mov    %eax,(%esp)
80101353:	e8 18 12 00 00       	call   80102570 <readi>
80101358:	83 f8 20             	cmp    $0x20,%eax
8010135b:	0f 85 f4 02 00 00    	jne    80101655 <exec+0x3d1>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80101361:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80101367:	83 f8 01             	cmp    $0x1,%eax
8010136a:	0f 85 80 00 00 00    	jne    801013f0 <exec+0x16c>
      continue;
    if(ph.memsz < ph.filesz)
80101370:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80101376:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
8010137c:	39 c2                	cmp    %eax,%edx
8010137e:	0f 82 d4 02 00 00    	jb     80101658 <exec+0x3d4>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80101384:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
8010138a:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80101390:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101393:	89 44 24 08          	mov    %eax,0x8(%esp)
80101397:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010139a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010139e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013a1:	89 04 24             	mov    %eax,(%esp)
801013a4:	e8 a9 77 00 00       	call   80108b52 <allocuvm>
801013a9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801013ac:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
801013b0:	0f 84 a5 02 00 00    	je     8010165b <exec+0x3d7>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
801013b6:	8b 8d fc fe ff ff    	mov    -0x104(%ebp),%ecx
801013bc:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
801013c2:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
801013c8:	89 4c 24 10          	mov    %ecx,0x10(%esp)
801013cc:	89 54 24 0c          	mov    %edx,0xc(%esp)
801013d0:	8b 55 ec             	mov    -0x14(%ebp),%edx
801013d3:	89 54 24 08          	mov    %edx,0x8(%esp)
801013d7:	89 44 24 04          	mov    %eax,0x4(%esp)
801013db:	8b 45 f0             	mov    -0x10(%ebp),%eax
801013de:	89 04 24             	mov    %eax,(%esp)
801013e1:	e8 84 76 00 00       	call   80108a6a <loaduvm>
801013e6:	85 c0                	test   %eax,%eax
801013e8:	0f 88 70 02 00 00    	js     8010165e <exec+0x3da>
801013ee:	eb 01                	jmp    801013f1 <exec+0x16d>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
801013f0:	90                   	nop
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
801013f1:	83 45 d8 01          	addl   $0x1,-0x28(%ebp)
801013f5:	8b 45 dc             	mov    -0x24(%ebp),%eax
801013f8:	83 c0 20             	add    $0x20,%eax
801013fb:	89 45 dc             	mov    %eax,-0x24(%ebp)
801013fe:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80101405:	0f b7 c0             	movzwl %ax,%eax
80101408:	3b 45 d8             	cmp    -0x28(%ebp),%eax
8010140b:	0f 8f 23 ff ff ff    	jg     80101334 <exec+0xb0>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80101411:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101414:	89 04 24             	mov    %eax,(%esp)
80101417:	e8 e2 0e 00 00       	call   801022fe <iunlockput>
  end_op();
8010141c:	e8 cb 26 00 00       	call   80103aec <end_op>
  ip = 0;
80101421:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80101428:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010142b:	05 ff 0f 00 00       	add    $0xfff,%eax
80101430:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80101435:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80101438:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010143b:	05 00 20 00 00       	add    $0x2000,%eax
80101440:	89 44 24 08          	mov    %eax,0x8(%esp)
80101444:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101447:	89 44 24 04          	mov    %eax,0x4(%esp)
8010144b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010144e:	89 04 24             	mov    %eax,(%esp)
80101451:	e8 fc 76 00 00       	call   80108b52 <allocuvm>
80101456:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101459:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
8010145d:	0f 84 fe 01 00 00    	je     80101661 <exec+0x3dd>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80101463:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101466:	2d 00 20 00 00       	sub    $0x2000,%eax
8010146b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010146f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101472:	89 04 24             	mov    %eax,(%esp)
80101475:	e8 01 79 00 00       	call   80108d7b <clearpteu>
  sp = sz;
8010147a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010147d:	89 45 e8             	mov    %eax,-0x18(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101480:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80101487:	e9 81 00 00 00       	jmp    8010150d <exec+0x289>
    if(argc >= MAXARG)
8010148c:	83 7d e0 1f          	cmpl   $0x1f,-0x20(%ebp)
80101490:	0f 87 ce 01 00 00    	ja     80101664 <exec+0x3e0>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80101496:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101499:	c1 e0 02             	shl    $0x2,%eax
8010149c:	03 45 0c             	add    0xc(%ebp),%eax
8010149f:	8b 00                	mov    (%eax),%eax
801014a1:	89 04 24             	mov    %eax,(%esp)
801014a4:	e8 3a 49 00 00       	call   80105de3 <strlen>
801014a9:	f7 d0                	not    %eax
801014ab:	03 45 e8             	add    -0x18(%ebp),%eax
801014ae:	83 e0 fc             	and    $0xfffffffc,%eax
801014b1:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
801014b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014b7:	c1 e0 02             	shl    $0x2,%eax
801014ba:	03 45 0c             	add    0xc(%ebp),%eax
801014bd:	8b 00                	mov    (%eax),%eax
801014bf:	89 04 24             	mov    %eax,(%esp)
801014c2:	e8 1c 49 00 00       	call   80105de3 <strlen>
801014c7:	83 c0 01             	add    $0x1,%eax
801014ca:	89 c2                	mov    %eax,%edx
801014cc:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014cf:	c1 e0 02             	shl    $0x2,%eax
801014d2:	03 45 0c             	add    0xc(%ebp),%eax
801014d5:	8b 00                	mov    (%eax),%eax
801014d7:	89 54 24 0c          	mov    %edx,0xc(%esp)
801014db:	89 44 24 08          	mov    %eax,0x8(%esp)
801014df:	8b 45 e8             	mov    -0x18(%ebp),%eax
801014e2:	89 44 24 04          	mov    %eax,0x4(%esp)
801014e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801014e9:	89 04 24             	mov    %eax,(%esp)
801014ec:	e8 42 7a 00 00       	call   80108f33 <copyout>
801014f1:	85 c0                	test   %eax,%eax
801014f3:	0f 88 6e 01 00 00    	js     80101667 <exec+0x3e3>
      goto bad;
    ustack[3+argc] = sp;
801014f9:	8b 45 e0             	mov    -0x20(%ebp),%eax
801014fc:	8d 50 03             	lea    0x3(%eax),%edx
801014ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101502:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80101509:	83 45 e0 01          	addl   $0x1,-0x20(%ebp)
8010150d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101510:	c1 e0 02             	shl    $0x2,%eax
80101513:	03 45 0c             	add    0xc(%ebp),%eax
80101516:	8b 00                	mov    (%eax),%eax
80101518:	85 c0                	test   %eax,%eax
8010151a:	0f 85 6c ff ff ff    	jne    8010148c <exec+0x208>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80101520:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101523:	83 c0 03             	add    $0x3,%eax
80101526:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
8010152d:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80101531:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80101538:	ff ff ff 
  ustack[1] = argc;
8010153b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010153e:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80101544:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101547:	83 c0 01             	add    $0x1,%eax
8010154a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101551:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101554:	29 d0                	sub    %edx,%eax
80101556:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
8010155c:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010155f:	83 c0 04             	add    $0x4,%eax
80101562:	c1 e0 02             	shl    $0x2,%eax
80101565:	29 45 e8             	sub    %eax,-0x18(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80101568:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010156b:	83 c0 04             	add    $0x4,%eax
8010156e:	c1 e0 02             	shl    $0x2,%eax
80101571:	89 44 24 0c          	mov    %eax,0xc(%esp)
80101575:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
8010157b:	89 44 24 08          	mov    %eax,0x8(%esp)
8010157f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101582:	89 44 24 04          	mov    %eax,0x4(%esp)
80101586:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101589:	89 04 24             	mov    %eax,(%esp)
8010158c:	e8 a2 79 00 00       	call   80108f33 <copyout>
80101591:	85 c0                	test   %eax,%eax
80101593:	0f 88 d1 00 00 00    	js     8010166a <exec+0x3e6>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80101599:	8b 45 08             	mov    0x8(%ebp),%eax
8010159c:	89 45 d0             	mov    %eax,-0x30(%ebp)
8010159f:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015a2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801015a5:	eb 17                	jmp    801015be <exec+0x33a>
    if(*s == '/')
801015a7:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015aa:	0f b6 00             	movzbl (%eax),%eax
801015ad:	3c 2f                	cmp    $0x2f,%al
801015af:	75 09                	jne    801015ba <exec+0x336>
      last = s+1;
801015b1:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015b4:	83 c0 01             	add    $0x1,%eax
801015b7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
801015ba:	83 45 d0 01          	addl   $0x1,-0x30(%ebp)
801015be:	8b 45 d0             	mov    -0x30(%ebp),%eax
801015c1:	0f b6 00             	movzbl (%eax),%eax
801015c4:	84 c0                	test   %al,%al
801015c6:	75 df                	jne    801015a7 <exec+0x323>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
801015c8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ce:	8d 50 6c             	lea    0x6c(%eax),%edx
801015d1:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801015d8:	00 
801015d9:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801015dc:	89 44 24 04          	mov    %eax,0x4(%esp)
801015e0:	89 14 24             	mov    %edx,(%esp)
801015e3:	e8 ad 47 00 00       	call   80105d95 <safestrcpy>

  // Commit to the user image.
  oldpgdir = proc->pgdir;
801015e8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015ee:	8b 40 04             	mov    0x4(%eax),%eax
801015f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  proc->pgdir = pgdir;
801015f4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801015fa:	8b 55 f0             	mov    -0x10(%ebp),%edx
801015fd:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80101600:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101606:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101609:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
8010160b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101611:	8b 40 18             	mov    0x18(%eax),%eax
80101614:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
8010161a:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
8010161d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101623:	8b 40 18             	mov    0x18(%eax),%eax
80101626:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101629:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
8010162c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80101632:	89 04 24             	mov    %eax,(%esp)
80101635:	e8 22 72 00 00       	call   8010885c <switchuvm>
  freevm(oldpgdir);
8010163a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010163d:	89 04 24             	mov    %eax,(%esp)
80101640:	e8 ab 76 00 00       	call   80108cf0 <freevm>
  return 0;
80101645:	b8 00 00 00 00       	mov    $0x0,%eax
8010164a:	eb 4b                	jmp    80101697 <exec+0x413>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
8010164c:	90                   	nop
8010164d:	eb 1c                	jmp    8010166b <exec+0x3e7>
  if(elf.magic != ELF_MAGIC)
    goto bad;
8010164f:	90                   	nop
80101650:	eb 19                	jmp    8010166b <exec+0x3e7>

  if((pgdir = setupkvm()) == 0)
    goto bad;
80101652:	90                   	nop
80101653:	eb 16                	jmp    8010166b <exec+0x3e7>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80101655:	90                   	nop
80101656:	eb 13                	jmp    8010166b <exec+0x3e7>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80101658:	90                   	nop
80101659:	eb 10                	jmp    8010166b <exec+0x3e7>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
8010165b:	90                   	nop
8010165c:	eb 0d                	jmp    8010166b <exec+0x3e7>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
8010165e:	90                   	nop
8010165f:	eb 0a                	jmp    8010166b <exec+0x3e7>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80101661:	90                   	nop
80101662:	eb 07                	jmp    8010166b <exec+0x3e7>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80101664:	90                   	nop
80101665:	eb 04                	jmp    8010166b <exec+0x3e7>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80101667:	90                   	nop
80101668:	eb 01                	jmp    8010166b <exec+0x3e7>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
8010166a:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
8010166b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010166f:	74 0b                	je     8010167c <exec+0x3f8>
    freevm(pgdir);
80101671:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101674:	89 04 24             	mov    %eax,(%esp)
80101677:	e8 74 76 00 00       	call   80108cf0 <freevm>
  if(ip){
8010167c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80101680:	74 10                	je     80101692 <exec+0x40e>
    iunlockput(ip);
80101682:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101685:	89 04 24             	mov    %eax,(%esp)
80101688:	e8 71 0c 00 00       	call   801022fe <iunlockput>
    end_op();
8010168d:	e8 5a 24 00 00       	call   80103aec <end_op>
  }
  return -1;
80101692:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101697:	c9                   	leave  
80101698:	c3                   	ret    
80101699:	00 00                	add    %al,(%eax)
	...

8010169c <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
8010169c:	55                   	push   %ebp
8010169d:	89 e5                	mov    %esp,%ebp
8010169f:	83 ec 18             	sub    $0x18,%esp
  initlock(&ftable.lock, "ftable");
801016a2:	c7 44 24 04 ae 90 10 	movl   $0x801090ae,0x4(%esp)
801016a9:	80 
801016aa:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016b1:	e8 28 42 00 00       	call   801058de <initlock>
}
801016b6:	c9                   	leave  
801016b7:	c3                   	ret    

801016b8 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
801016b8:	55                   	push   %ebp
801016b9:	89 e5                	mov    %esp,%ebp
801016bb:	83 ec 28             	sub    $0x28,%esp
  struct file *f;

  acquire(&ftable.lock);
801016be:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016c5:	e8 35 42 00 00       	call   801058ff <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016ca:	c7 45 f4 d4 20 11 80 	movl   $0x801120d4,-0xc(%ebp)
801016d1:	eb 29                	jmp    801016fc <filealloc+0x44>
    if(f->ref == 0){
801016d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016d6:	8b 40 04             	mov    0x4(%eax),%eax
801016d9:	85 c0                	test   %eax,%eax
801016db:	75 1b                	jne    801016f8 <filealloc+0x40>
      f->ref = 1;
801016dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016e0:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
801016e7:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801016ee:	e8 75 42 00 00       	call   80105968 <release>
      return f;
801016f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016f6:	eb 1f                	jmp    80101717 <filealloc+0x5f>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
801016f8:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
801016fc:	b8 34 2a 11 80       	mov    $0x80112a34,%eax
80101701:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80101704:	72 cd                	jb     801016d3 <filealloc+0x1b>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80101706:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
8010170d:	e8 56 42 00 00       	call   80105968 <release>
  return 0;
80101712:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101717:	c9                   	leave  
80101718:	c3                   	ret    

80101719 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80101719:	55                   	push   %ebp
8010171a:	89 e5                	mov    %esp,%ebp
8010171c:	83 ec 18             	sub    $0x18,%esp
  acquire(&ftable.lock);
8010171f:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
80101726:	e8 d4 41 00 00       	call   801058ff <acquire>
  if(f->ref < 1)
8010172b:	8b 45 08             	mov    0x8(%ebp),%eax
8010172e:	8b 40 04             	mov    0x4(%eax),%eax
80101731:	85 c0                	test   %eax,%eax
80101733:	7f 0c                	jg     80101741 <filedup+0x28>
    panic("filedup");
80101735:	c7 04 24 b5 90 10 80 	movl   $0x801090b5,(%esp)
8010173c:	e8 28 f1 ff ff       	call   80100869 <panic>
  f->ref++;
80101741:	8b 45 08             	mov    0x8(%ebp),%eax
80101744:	8b 40 04             	mov    0x4(%eax),%eax
80101747:	8d 50 01             	lea    0x1(%eax),%edx
8010174a:	8b 45 08             	mov    0x8(%ebp),%eax
8010174d:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
80101750:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
80101757:	e8 0c 42 00 00       	call   80105968 <release>
  return f;
8010175c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010175f:	c9                   	leave  
80101760:	c3                   	ret    

80101761 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101761:	55                   	push   %ebp
80101762:	89 e5                	mov    %esp,%ebp
80101764:	83 ec 38             	sub    $0x38,%esp
  struct file ff;

  acquire(&ftable.lock);
80101767:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
8010176e:	e8 8c 41 00 00       	call   801058ff <acquire>
  if(f->ref < 1)
80101773:	8b 45 08             	mov    0x8(%ebp),%eax
80101776:	8b 40 04             	mov    0x4(%eax),%eax
80101779:	85 c0                	test   %eax,%eax
8010177b:	7f 0c                	jg     80101789 <fileclose+0x28>
    panic("fileclose");
8010177d:	c7 04 24 bd 90 10 80 	movl   $0x801090bd,(%esp)
80101784:	e8 e0 f0 ff ff       	call   80100869 <panic>
  if(--f->ref > 0){
80101789:	8b 45 08             	mov    0x8(%ebp),%eax
8010178c:	8b 40 04             	mov    0x4(%eax),%eax
8010178f:	8d 50 ff             	lea    -0x1(%eax),%edx
80101792:	8b 45 08             	mov    0x8(%ebp),%eax
80101795:	89 50 04             	mov    %edx,0x4(%eax)
80101798:	8b 45 08             	mov    0x8(%ebp),%eax
8010179b:	8b 40 04             	mov    0x4(%eax),%eax
8010179e:	85 c0                	test   %eax,%eax
801017a0:	7e 11                	jle    801017b3 <fileclose+0x52>
    release(&ftable.lock);
801017a2:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801017a9:	e8 ba 41 00 00       	call   80105968 <release>
    return;
801017ae:	e9 82 00 00 00       	jmp    80101835 <fileclose+0xd4>
  }
  ff = *f;
801017b3:	8b 45 08             	mov    0x8(%ebp),%eax
801017b6:	8b 10                	mov    (%eax),%edx
801017b8:	89 55 e0             	mov    %edx,-0x20(%ebp)
801017bb:	8b 50 04             	mov    0x4(%eax),%edx
801017be:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801017c1:	8b 50 08             	mov    0x8(%eax),%edx
801017c4:	89 55 e8             	mov    %edx,-0x18(%ebp)
801017c7:	8b 50 0c             	mov    0xc(%eax),%edx
801017ca:	89 55 ec             	mov    %edx,-0x14(%ebp)
801017cd:	8b 50 10             	mov    0x10(%eax),%edx
801017d0:	89 55 f0             	mov    %edx,-0x10(%ebp)
801017d3:	8b 40 14             	mov    0x14(%eax),%eax
801017d6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
801017d9:	8b 45 08             	mov    0x8(%ebp),%eax
801017dc:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
801017e3:	8b 45 08             	mov    0x8(%ebp),%eax
801017e6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
801017ec:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801017f3:	e8 70 41 00 00       	call   80105968 <release>
  
  if(ff.type == FD_PIPE)
801017f8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801017fb:	83 f8 01             	cmp    $0x1,%eax
801017fe:	75 18                	jne    80101818 <fileclose+0xb7>
    pipeclose(ff.pipe, ff.writable);
80101800:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
80101804:	0f be d0             	movsbl %al,%edx
80101807:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010180a:	89 54 24 04          	mov    %edx,0x4(%esp)
8010180e:	89 04 24             	mov    %eax,(%esp)
80101811:	e8 41 2f 00 00       	call   80104757 <pipeclose>
80101816:	eb 1d                	jmp    80101835 <fileclose+0xd4>
  else if(ff.type == FD_INODE){
80101818:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010181b:	83 f8 02             	cmp    $0x2,%eax
8010181e:	75 15                	jne    80101835 <fileclose+0xd4>
    begin_op();
80101820:	e8 45 22 00 00       	call   80103a6a <begin_op>
    iput(ff.ip);
80101825:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101828:	89 04 24             	mov    %eax,(%esp)
8010182b:	e8 fd 09 00 00       	call   8010222d <iput>
    end_op();
80101830:	e8 b7 22 00 00       	call   80103aec <end_op>
  }
}
80101835:	c9                   	leave  
80101836:	c3                   	ret    

80101837 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80101837:	55                   	push   %ebp
80101838:	89 e5                	mov    %esp,%ebp
8010183a:	83 ec 18             	sub    $0x18,%esp
  if(f->type == FD_INODE){
8010183d:	8b 45 08             	mov    0x8(%ebp),%eax
80101840:	8b 00                	mov    (%eax),%eax
80101842:	83 f8 02             	cmp    $0x2,%eax
80101845:	75 38                	jne    8010187f <filestat+0x48>
    ilock(f->ip);
80101847:	8b 45 08             	mov    0x8(%ebp),%eax
8010184a:	8b 40 10             	mov    0x10(%eax),%eax
8010184d:	89 04 24             	mov    %eax,(%esp)
80101850:	e8 1c 08 00 00       	call   80102071 <ilock>
    stati(f->ip, st);
80101855:	8b 45 08             	mov    0x8(%ebp),%eax
80101858:	8b 40 10             	mov    0x10(%eax),%eax
8010185b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010185e:	89 54 24 04          	mov    %edx,0x4(%esp)
80101862:	89 04 24             	mov    %eax,(%esp)
80101865:	e8 c1 0c 00 00       	call   8010252b <stati>
    iunlock(f->ip);
8010186a:	8b 45 08             	mov    0x8(%ebp),%eax
8010186d:	8b 40 10             	mov    0x10(%eax),%eax
80101870:	89 04 24             	mov    %eax,(%esp)
80101873:	e8 50 09 00 00       	call   801021c8 <iunlock>
    return 0;
80101878:	b8 00 00 00 00       	mov    $0x0,%eax
8010187d:	eb 05                	jmp    80101884 <filestat+0x4d>
  }
  return -1;
8010187f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101884:	c9                   	leave  
80101885:	c3                   	ret    

80101886 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101886:	55                   	push   %ebp
80101887:	89 e5                	mov    %esp,%ebp
80101889:	83 ec 28             	sub    $0x28,%esp
  int r;

  if(f->readable == 0)
8010188c:	8b 45 08             	mov    0x8(%ebp),%eax
8010188f:	0f b6 40 08          	movzbl 0x8(%eax),%eax
80101893:	84 c0                	test   %al,%al
80101895:	75 0a                	jne    801018a1 <fileread+0x1b>
    return -1;
80101897:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010189c:	e9 9f 00 00 00       	jmp    80101940 <fileread+0xba>
  if(f->type == FD_PIPE)
801018a1:	8b 45 08             	mov    0x8(%ebp),%eax
801018a4:	8b 00                	mov    (%eax),%eax
801018a6:	83 f8 01             	cmp    $0x1,%eax
801018a9:	75 1e                	jne    801018c9 <fileread+0x43>
    return piperead(f->pipe, addr, n);
801018ab:	8b 45 08             	mov    0x8(%ebp),%eax
801018ae:	8b 40 0c             	mov    0xc(%eax),%eax
801018b1:	8b 55 10             	mov    0x10(%ebp),%edx
801018b4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018b8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018bb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018bf:	89 04 24             	mov    %eax,(%esp)
801018c2:	e8 12 30 00 00       	call   801048d9 <piperead>
801018c7:	eb 77                	jmp    80101940 <fileread+0xba>
  if(f->type == FD_INODE){
801018c9:	8b 45 08             	mov    0x8(%ebp),%eax
801018cc:	8b 00                	mov    (%eax),%eax
801018ce:	83 f8 02             	cmp    $0x2,%eax
801018d1:	75 61                	jne    80101934 <fileread+0xae>
    ilock(f->ip);
801018d3:	8b 45 08             	mov    0x8(%ebp),%eax
801018d6:	8b 40 10             	mov    0x10(%eax),%eax
801018d9:	89 04 24             	mov    %eax,(%esp)
801018dc:	e8 90 07 00 00       	call   80102071 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
801018e1:	8b 4d 10             	mov    0x10(%ebp),%ecx
801018e4:	8b 45 08             	mov    0x8(%ebp),%eax
801018e7:	8b 50 14             	mov    0x14(%eax),%edx
801018ea:	8b 45 08             	mov    0x8(%ebp),%eax
801018ed:	8b 40 10             	mov    0x10(%eax),%eax
801018f0:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801018f4:	89 54 24 08          	mov    %edx,0x8(%esp)
801018f8:	8b 55 0c             	mov    0xc(%ebp),%edx
801018fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801018ff:	89 04 24             	mov    %eax,(%esp)
80101902:	e8 69 0c 00 00       	call   80102570 <readi>
80101907:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010190a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010190e:	7e 11                	jle    80101921 <fileread+0x9b>
      f->off += r;
80101910:	8b 45 08             	mov    0x8(%ebp),%eax
80101913:	8b 50 14             	mov    0x14(%eax),%edx
80101916:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101919:	01 c2                	add    %eax,%edx
8010191b:	8b 45 08             	mov    0x8(%ebp),%eax
8010191e:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
80101921:	8b 45 08             	mov    0x8(%ebp),%eax
80101924:	8b 40 10             	mov    0x10(%eax),%eax
80101927:	89 04 24             	mov    %eax,(%esp)
8010192a:	e8 99 08 00 00       	call   801021c8 <iunlock>
    return r;
8010192f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101932:	eb 0c                	jmp    80101940 <fileread+0xba>
  }
  panic("fileread");
80101934:	c7 04 24 c7 90 10 80 	movl   $0x801090c7,(%esp)
8010193b:	e8 29 ef ff ff       	call   80100869 <panic>
}
80101940:	c9                   	leave  
80101941:	c3                   	ret    

80101942 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101942:	55                   	push   %ebp
80101943:	89 e5                	mov    %esp,%ebp
80101945:	53                   	push   %ebx
80101946:	83 ec 24             	sub    $0x24,%esp
  int r;

  if(f->writable == 0)
80101949:	8b 45 08             	mov    0x8(%ebp),%eax
8010194c:	0f b6 40 09          	movzbl 0x9(%eax),%eax
80101950:	84 c0                	test   %al,%al
80101952:	75 0a                	jne    8010195e <filewrite+0x1c>
    return -1;
80101954:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101959:	e9 23 01 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_PIPE)
8010195e:	8b 45 08             	mov    0x8(%ebp),%eax
80101961:	8b 00                	mov    (%eax),%eax
80101963:	83 f8 01             	cmp    $0x1,%eax
80101966:	75 21                	jne    80101989 <filewrite+0x47>
    return pipewrite(f->pipe, addr, n);
80101968:	8b 45 08             	mov    0x8(%ebp),%eax
8010196b:	8b 40 0c             	mov    0xc(%eax),%eax
8010196e:	8b 55 10             	mov    0x10(%ebp),%edx
80101971:	89 54 24 08          	mov    %edx,0x8(%esp)
80101975:	8b 55 0c             	mov    0xc(%ebp),%edx
80101978:	89 54 24 04          	mov    %edx,0x4(%esp)
8010197c:	89 04 24             	mov    %eax,(%esp)
8010197f:	e8 65 2e 00 00       	call   801047e9 <pipewrite>
80101984:	e9 f8 00 00 00       	jmp    80101a81 <filewrite+0x13f>
  if(f->type == FD_INODE){
80101989:	8b 45 08             	mov    0x8(%ebp),%eax
8010198c:	8b 00                	mov    (%eax),%eax
8010198e:	83 f8 02             	cmp    $0x2,%eax
80101991:	0f 85 de 00 00 00    	jne    80101a75 <filewrite+0x133>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
80101997:	c7 45 ec 00 1a 00 00 	movl   $0x1a00,-0x14(%ebp)
    int i = 0;
8010199e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    while(i < n){
801019a5:	e9 a8 00 00 00       	jmp    80101a52 <filewrite+0x110>
      int n1 = n - i;
801019aa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019ad:	8b 55 10             	mov    0x10(%ebp),%edx
801019b0:	89 d1                	mov    %edx,%ecx
801019b2:	29 c1                	sub    %eax,%ecx
801019b4:	89 c8                	mov    %ecx,%eax
801019b6:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(n1 > max)
801019b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019bc:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801019bf:	7e 06                	jle    801019c7 <filewrite+0x85>
        n1 = max;
801019c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801019c4:	89 45 f4             	mov    %eax,-0xc(%ebp)

      begin_op();
801019c7:	e8 9e 20 00 00       	call   80103a6a <begin_op>
      ilock(f->ip);
801019cc:	8b 45 08             	mov    0x8(%ebp),%eax
801019cf:	8b 40 10             	mov    0x10(%eax),%eax
801019d2:	89 04 24             	mov    %eax,(%esp)
801019d5:	e8 97 06 00 00       	call   80102071 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
801019da:	8b 5d f4             	mov    -0xc(%ebp),%ebx
801019dd:	8b 45 08             	mov    0x8(%ebp),%eax
801019e0:	8b 48 14             	mov    0x14(%eax),%ecx
801019e3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019e6:	89 c2                	mov    %eax,%edx
801019e8:	03 55 0c             	add    0xc(%ebp),%edx
801019eb:	8b 45 08             	mov    0x8(%ebp),%eax
801019ee:	8b 40 10             	mov    0x10(%eax),%eax
801019f1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
801019f5:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801019f9:	89 54 24 04          	mov    %edx,0x4(%esp)
801019fd:	89 04 24             	mov    %eax,(%esp)
80101a00:	e8 d7 0c 00 00       	call   801026dc <writei>
80101a05:	89 45 e8             	mov    %eax,-0x18(%ebp)
80101a08:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a0c:	7e 11                	jle    80101a1f <filewrite+0xdd>
        f->off += r;
80101a0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a11:	8b 50 14             	mov    0x14(%eax),%edx
80101a14:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a17:	01 c2                	add    %eax,%edx
80101a19:	8b 45 08             	mov    0x8(%ebp),%eax
80101a1c:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
80101a1f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a22:	8b 40 10             	mov    0x10(%eax),%eax
80101a25:	89 04 24             	mov    %eax,(%esp)
80101a28:	e8 9b 07 00 00       	call   801021c8 <iunlock>
      end_op();
80101a2d:	e8 ba 20 00 00       	call   80103aec <end_op>

      if(r < 0)
80101a32:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101a36:	78 28                	js     80101a60 <filewrite+0x11e>
        break;
      if(r != n1)
80101a38:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a3b:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80101a3e:	74 0c                	je     80101a4c <filewrite+0x10a>
        panic("short filewrite");
80101a40:	c7 04 24 d0 90 10 80 	movl   $0x801090d0,(%esp)
80101a47:	e8 1d ee ff ff       	call   80100869 <panic>
      i += r;
80101a4c:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101a4f:	01 45 f0             	add    %eax,-0x10(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * BSIZE;
    int i = 0;
    while(i < n){
80101a52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a55:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a58:	0f 8c 4c ff ff ff    	jl     801019aa <filewrite+0x68>
80101a5e:	eb 01                	jmp    80101a61 <filewrite+0x11f>
        f->off += r;
      iunlock(f->ip);
      end_op();

      if(r < 0)
        break;
80101a60:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
80101a61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101a64:	3b 45 10             	cmp    0x10(%ebp),%eax
80101a67:	75 05                	jne    80101a6e <filewrite+0x12c>
80101a69:	8b 45 10             	mov    0x10(%ebp),%eax
80101a6c:	eb 05                	jmp    80101a73 <filewrite+0x131>
80101a6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101a73:	eb 0c                	jmp    80101a81 <filewrite+0x13f>
  }
  panic("filewrite");
80101a75:	c7 04 24 e0 90 10 80 	movl   $0x801090e0,(%esp)
80101a7c:	e8 e8 ed ff ff       	call   80100869 <panic>
}
80101a81:	83 c4 24             	add    $0x24,%esp
80101a84:	5b                   	pop    %ebx
80101a85:	5d                   	pop    %ebp
80101a86:	c3                   	ret    
	...

80101a88 <readsb>:
struct superblock sb;

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
80101a88:	55                   	push   %ebp
80101a89:	89 e5                	mov    %esp,%ebp
80101a8b:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, 1);
80101a8e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a91:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80101a98:	00 
80101a99:	89 04 24             	mov    %eax,(%esp)
80101a9c:	e8 06 e7 ff ff       	call   801001a7 <bread>
80101aa1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
80101aa4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aa7:	83 c0 18             	add    $0x18,%eax
80101aaa:	c7 44 24 08 1c 00 00 	movl   $0x1c,0x8(%esp)
80101ab1:	00 
80101ab2:	89 44 24 04          	mov    %eax,0x4(%esp)
80101ab6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ab9:	89 04 24             	mov    %eax,(%esp)
80101abc:	e8 74 41 00 00       	call   80105c35 <memmove>
  brelse(bp);
80101ac1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ac4:	89 04 24             	mov    %eax,(%esp)
80101ac7:	e8 4c e7 ff ff       	call   80100218 <brelse>
}
80101acc:	c9                   	leave  
80101acd:	c3                   	ret    

80101ace <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
80101ace:	55                   	push   %ebp
80101acf:	89 e5                	mov    %esp,%ebp
80101ad1:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;

  bp = bread(dev, bno);
80101ad4:	8b 55 0c             	mov    0xc(%ebp),%edx
80101ad7:	8b 45 08             	mov    0x8(%ebp),%eax
80101ada:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ade:	89 04 24             	mov    %eax,(%esp)
80101ae1:	e8 c1 e6 ff ff       	call   801001a7 <bread>
80101ae6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
80101ae9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101aec:	83 c0 18             	add    $0x18,%eax
80101aef:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80101af6:	00 
80101af7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101afe:	00 
80101aff:	89 04 24             	mov    %eax,(%esp)
80101b02:	e8 5b 40 00 00       	call   80105b62 <memset>
  log_write(bp);
80101b07:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b0a:	89 04 24             	mov    %eax,(%esp)
80101b0d:	e8 5e 21 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101b12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101b15:	89 04 24             	mov    %eax,(%esp)
80101b18:	e8 fb e6 ff ff       	call   80100218 <brelse>
}
80101b1d:	c9                   	leave  
80101b1e:	c3                   	ret    

80101b1f <balloc>:
// Blocks.

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
80101b1f:	55                   	push   %ebp
80101b20:	89 e5                	mov    %esp,%ebp
80101b22:	53                   	push   %ebx
80101b23:	83 ec 24             	sub    $0x24,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
80101b26:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101b2d:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
80101b34:	e9 16 01 00 00       	jmp    80101c4f <balloc+0x130>
    bp = bread(dev, BBLOCK(b, sb));
80101b39:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101b3c:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
80101b42:	85 c0                	test   %eax,%eax
80101b44:	0f 48 c2             	cmovs  %edx,%eax
80101b47:	c1 f8 0c             	sar    $0xc,%eax
80101b4a:	89 c2                	mov    %eax,%edx
80101b4c:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101b51:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101b54:	89 44 24 04          	mov    %eax,0x4(%esp)
80101b58:	8b 45 08             	mov    0x8(%ebp),%eax
80101b5b:	89 04 24             	mov    %eax,(%esp)
80101b5e:	e8 44 e6 ff ff       	call   801001a7 <bread>
80101b63:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101b66:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80101b6d:	e9 aa 00 00 00       	jmp    80101c1c <balloc+0xfd>
      m = 1 << (bi % 8);
80101b72:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b75:	89 c2                	mov    %eax,%edx
80101b77:	c1 fa 1f             	sar    $0x1f,%edx
80101b7a:	c1 ea 1d             	shr    $0x1d,%edx
80101b7d:	01 d0                	add    %edx,%eax
80101b7f:	83 e0 07             	and    $0x7,%eax
80101b82:	29 d0                	sub    %edx,%eax
80101b84:	ba 01 00 00 00       	mov    $0x1,%edx
80101b89:	89 d3                	mov    %edx,%ebx
80101b8b:	89 c1                	mov    %eax,%ecx
80101b8d:	d3 e3                	shl    %cl,%ebx
80101b8f:	89 d8                	mov    %ebx,%eax
80101b91:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101b94:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101b97:	8d 50 07             	lea    0x7(%eax),%edx
80101b9a:	85 c0                	test   %eax,%eax
80101b9c:	0f 48 c2             	cmovs  %edx,%eax
80101b9f:	c1 f8 03             	sar    $0x3,%eax
80101ba2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101ba5:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101baa:	0f b6 c0             	movzbl %al,%eax
80101bad:	23 45 f0             	and    -0x10(%ebp),%eax
80101bb0:	85 c0                	test   %eax,%eax
80101bb2:	75 64                	jne    80101c18 <balloc+0xf9>
        bp->data[bi/8] |= m;  // Mark block in use.
80101bb4:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bb7:	8d 50 07             	lea    0x7(%eax),%edx
80101bba:	85 c0                	test   %eax,%eax
80101bbc:	0f 48 c2             	cmovs  %edx,%eax
80101bbf:	c1 f8 03             	sar    $0x3,%eax
80101bc2:	89 c2                	mov    %eax,%edx
80101bc4:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101bc7:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101bcc:	89 c1                	mov    %eax,%ecx
80101bce:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101bd1:	09 c8                	or     %ecx,%eax
80101bd3:	89 c1                	mov    %eax,%ecx
80101bd5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bd8:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
        log_write(bp);
80101bdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bdf:	89 04 24             	mov    %eax,(%esp)
80101be2:	e8 89 20 00 00       	call   80103c70 <log_write>
        brelse(bp);
80101be7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101bea:	89 04 24             	mov    %eax,(%esp)
80101bed:	e8 26 e6 ff ff       	call   80100218 <brelse>
        bzero(dev, b + bi);
80101bf2:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101bf5:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101bf8:	01 c2                	add    %eax,%edx
80101bfa:	8b 45 08             	mov    0x8(%ebp),%eax
80101bfd:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c01:	89 04 24             	mov    %eax,(%esp)
80101c04:	e8 c5 fe ff ff       	call   80101ace <bzero>
        return b + bi;
80101c09:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c0c:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c0f:	8d 04 02             	lea    (%edx,%eax,1),%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
80101c12:	83 c4 24             	add    $0x24,%esp
80101c15:	5b                   	pop    %ebx
80101c16:	5d                   	pop    %ebp
80101c17:	c3                   	ret    
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101c18:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101c1c:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%ebp)
80101c23:	7f 18                	jg     80101c3d <balloc+0x11e>
80101c25:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101c28:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c2b:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101c2e:	89 c2                	mov    %eax,%edx
80101c30:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101c35:	39 c2                	cmp    %eax,%edx
80101c37:	0f 82 35 ff ff ff    	jb     80101b72 <balloc+0x53>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
80101c3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101c40:	89 04 24             	mov    %eax,(%esp)
80101c43:	e8 d0 e5 ff ff       	call   80100218 <brelse>
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
80101c48:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
80101c4f:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101c52:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101c57:	39 c2                	cmp    %eax,%edx
80101c59:	0f 82 da fe ff ff    	jb     80101b39 <balloc+0x1a>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
80101c5f:	c7 04 24 ec 90 10 80 	movl   $0x801090ec,(%esp)
80101c66:	e8 fe eb ff ff       	call   80100869 <panic>

80101c6b <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101c6b:	55                   	push   %ebp
80101c6c:	89 e5                	mov    %esp,%ebp
80101c6e:	53                   	push   %ebx
80101c6f:	83 ec 24             	sub    $0x24,%esp
  struct buf *bp;
  int bi, m;

  readsb(dev, &sb);
80101c72:	c7 44 24 04 a0 2a 11 	movl   $0x80112aa0,0x4(%esp)
80101c79:	80 
80101c7a:	8b 45 08             	mov    0x8(%ebp),%eax
80101c7d:	89 04 24             	mov    %eax,(%esp)
80101c80:	e8 03 fe ff ff       	call   80101a88 <readsb>
  bp = bread(dev, BBLOCK(b, sb));
80101c85:	8b 45 0c             	mov    0xc(%ebp),%eax
80101c88:	89 c2                	mov    %eax,%edx
80101c8a:	c1 ea 0c             	shr    $0xc,%edx
80101c8d:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101c92:	01 c2                	add    %eax,%edx
80101c94:	8b 45 08             	mov    0x8(%ebp),%eax
80101c97:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c9b:	89 04 24             	mov    %eax,(%esp)
80101c9e:	e8 04 e5 ff ff       	call   801001a7 <bread>
80101ca3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  bi = b % BPB;
80101ca6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ca9:	25 ff 0f 00 00       	and    $0xfff,%eax
80101cae:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
80101cb1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cb4:	89 c2                	mov    %eax,%edx
80101cb6:	c1 fa 1f             	sar    $0x1f,%edx
80101cb9:	c1 ea 1d             	shr    $0x1d,%edx
80101cbc:	01 d0                	add    %edx,%eax
80101cbe:	83 e0 07             	and    $0x7,%eax
80101cc1:	29 d0                	sub    %edx,%eax
80101cc3:	ba 01 00 00 00       	mov    $0x1,%edx
80101cc8:	89 d3                	mov    %edx,%ebx
80101cca:	89 c1                	mov    %eax,%ecx
80101ccc:	d3 e3                	shl    %cl,%ebx
80101cce:	89 d8                	mov    %ebx,%eax
80101cd0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((bp->data[bi/8] & m) == 0)
80101cd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101cd6:	8d 50 07             	lea    0x7(%eax),%edx
80101cd9:	85 c0                	test   %eax,%eax
80101cdb:	0f 48 c2             	cmovs  %edx,%eax
80101cde:	c1 f8 03             	sar    $0x3,%eax
80101ce1:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101ce4:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
80101ce9:	0f b6 c0             	movzbl %al,%eax
80101cec:	23 45 f4             	and    -0xc(%ebp),%eax
80101cef:	85 c0                	test   %eax,%eax
80101cf1:	75 0c                	jne    80101cff <bfree+0x94>
    panic("freeing free block");
80101cf3:	c7 04 24 02 91 10 80 	movl   $0x80109102,(%esp)
80101cfa:	e8 6a eb ff ff       	call   80100869 <panic>
  bp->data[bi/8] &= ~m;
80101cff:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d02:	8d 50 07             	lea    0x7(%eax),%edx
80101d05:	85 c0                	test   %eax,%eax
80101d07:	0f 48 c2             	cmovs  %edx,%eax
80101d0a:	c1 f8 03             	sar    $0x3,%eax
80101d0d:	89 c2                	mov    %eax,%edx
80101d0f:	8b 4d ec             	mov    -0x14(%ebp),%ecx
80101d12:	0f b6 44 01 18       	movzbl 0x18(%ecx,%eax,1),%eax
80101d17:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80101d1a:	f7 d1                	not    %ecx
80101d1c:	21 c8                	and    %ecx,%eax
80101d1e:	89 c1                	mov    %eax,%ecx
80101d20:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d23:	88 4c 10 18          	mov    %cl,0x18(%eax,%edx,1)
  log_write(bp);
80101d27:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d2a:	89 04 24             	mov    %eax,(%esp)
80101d2d:	e8 3e 1f 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101d32:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d35:	89 04 24             	mov    %eax,(%esp)
80101d38:	e8 db e4 ff ff       	call   80100218 <brelse>
}
80101d3d:	83 c4 24             	add    $0x24,%esp
80101d40:	5b                   	pop    %ebx
80101d41:	5d                   	pop    %ebp
80101d42:	c3                   	ret    

80101d43 <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(int dev)
{
80101d43:	55                   	push   %ebp
80101d44:	89 e5                	mov    %esp,%ebp
80101d46:	57                   	push   %edi
80101d47:	56                   	push   %esi
80101d48:	53                   	push   %ebx
80101d49:	83 ec 4c             	sub    $0x4c,%esp
  initlock(&icache.lock, "icache");
80101d4c:	c7 44 24 04 15 91 10 	movl   $0x80109115,0x4(%esp)
80101d53:	80 
80101d54:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101d5b:	e8 7e 3b 00 00       	call   801058de <initlock>
  readsb(dev, &sb);
80101d60:	c7 44 24 04 a0 2a 11 	movl   $0x80112aa0,0x4(%esp)
80101d67:	80 
80101d68:	8b 45 08             	mov    0x8(%ebp),%eax
80101d6b:	89 04 24             	mov    %eax,(%esp)
80101d6e:	e8 15 fd ff ff       	call   80101a88 <readsb>
  cprintf(
80101d73:	a1 b8 2a 11 80       	mov    0x80112ab8,%eax
80101d78:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101d7b:	8b 3d b4 2a 11 80    	mov    0x80112ab4,%edi
80101d81:	8b 35 b0 2a 11 80    	mov    0x80112ab0,%esi
80101d87:	8b 1d ac 2a 11 80    	mov    0x80112aac,%ebx
80101d8d:	8b 0d a8 2a 11 80    	mov    0x80112aa8,%ecx
80101d93:	8b 15 a4 2a 11 80    	mov    0x80112aa4,%edx
80101d99:	a1 a0 2a 11 80       	mov    0x80112aa0,%eax
80101d9e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80101da1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101da4:	89 44 24 1c          	mov    %eax,0x1c(%esp)
80101da8:	89 7c 24 18          	mov    %edi,0x18(%esp)
80101dac:	89 74 24 14          	mov    %esi,0x14(%esp)
80101db0:	89 5c 24 10          	mov    %ebx,0x10(%esp)
80101db4:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80101db8:	89 54 24 08          	mov    %edx,0x8(%esp)
80101dbc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80101dbf:	89 44 24 04          	mov    %eax,0x4(%esp)
80101dc3:	c7 04 24 1c 91 10 80 	movl   $0x8010911c,(%esp)
80101dca:	e8 fa e8 ff ff       	call   801006c9 <cprintf>
    "sb: size %d nblocks %d ninodes %d nlog %d logstart %d "
    "inodestart %d bmap start %d\n", sb.size, sb.nblocks, sb.ninodes,
    sb.nlog, sb.logstart, sb.inodestart, sb.bmapstart);
}
80101dcf:	83 c4 4c             	add    $0x4c,%esp
80101dd2:	5b                   	pop    %ebx
80101dd3:	5e                   	pop    %esi
80101dd4:	5f                   	pop    %edi
80101dd5:	5d                   	pop    %ebp
80101dd6:	c3                   	ret    

80101dd7 <ialloc>:
//PAGEBREAK!
// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
80101dd7:	55                   	push   %ebp
80101dd8:	89 e5                	mov    %esp,%ebp
80101dda:	83 ec 38             	sub    $0x38,%esp
80101ddd:	8b 45 0c             	mov    0xc(%ebp),%eax
80101de0:	66 89 45 e4          	mov    %ax,-0x1c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101de4:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%ebp)
80101deb:	e9 9f 00 00 00       	jmp    80101e8f <ialloc+0xb8>
    bp = bread(dev, IBLOCK(inum, sb));
80101df0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101df3:	89 c2                	mov    %eax,%edx
80101df5:	c1 ea 03             	shr    $0x3,%edx
80101df8:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
80101dfd:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101e00:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e04:	8b 45 08             	mov    0x8(%ebp),%eax
80101e07:	89 04 24             	mov    %eax,(%esp)
80101e0a:	e8 98 e3 ff ff       	call   801001a7 <bread>
80101e0f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
80101e12:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e15:	83 c0 18             	add    $0x18,%eax
80101e18:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e1b:	83 e2 07             	and    $0x7,%edx
80101e1e:	c1 e2 06             	shl    $0x6,%edx
80101e21:	01 d0                	add    %edx,%eax
80101e23:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(dip->type == 0){  // a free inode
80101e26:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e29:	0f b7 00             	movzwl (%eax),%eax
80101e2c:	66 85 c0             	test   %ax,%ax
80101e2f:	75 4f                	jne    80101e80 <ialloc+0xa9>
      memset(dip, 0, sizeof(*dip));
80101e31:	c7 44 24 08 40 00 00 	movl   $0x40,0x8(%esp)
80101e38:	00 
80101e39:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80101e40:	00 
80101e41:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e44:	89 04 24             	mov    %eax,(%esp)
80101e47:	e8 16 3d 00 00       	call   80105b62 <memset>
      dip->type = type;
80101e4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101e4f:	0f b7 55 e4          	movzwl -0x1c(%ebp),%edx
80101e53:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
80101e56:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e59:	89 04 24             	mov    %eax,(%esp)
80101e5c:	e8 0f 1e 00 00       	call   80103c70 <log_write>
      brelse(bp);
80101e61:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e64:	89 04 24             	mov    %eax,(%esp)
80101e67:	e8 ac e3 ff ff       	call   80100218 <brelse>
      return iget(dev, inum);
80101e6c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101e6f:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e73:	8b 45 08             	mov    0x8(%ebp),%eax
80101e76:	89 04 24             	mov    %eax,(%esp)
80101e79:	e8 ee 00 00 00       	call   80101f6c <iget>
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}
80101e7e:	c9                   	leave  
80101e7f:	c3                   	ret    
      dip->type = type;
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
80101e80:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101e83:	89 04 24             	mov    %eax,(%esp)
80101e86:	e8 8d e3 ff ff       	call   80100218 <brelse>
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101e8b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80101e8f:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101e92:	a1 a8 2a 11 80       	mov    0x80112aa8,%eax
80101e97:	39 c2                	cmp    %eax,%edx
80101e99:	0f 82 51 ff ff ff    	jb     80101df0 <ialloc+0x19>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
80101e9f:	c7 04 24 6f 91 10 80 	movl   $0x8010916f,(%esp)
80101ea6:	e8 be e9 ff ff       	call   80100869 <panic>

80101eab <iupdate>:
}

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
80101eab:	55                   	push   %ebp
80101eac:	89 e5                	mov    %esp,%ebp
80101eae:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101eb1:	8b 45 08             	mov    0x8(%ebp),%eax
80101eb4:	8b 40 04             	mov    0x4(%eax),%eax
80101eb7:	89 c2                	mov    %eax,%edx
80101eb9:	c1 ea 03             	shr    $0x3,%edx
80101ebc:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
80101ec1:	01 c2                	add    %eax,%edx
80101ec3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ec6:	8b 00                	mov    (%eax),%eax
80101ec8:	89 54 24 04          	mov    %edx,0x4(%esp)
80101ecc:	89 04 24             	mov    %eax,(%esp)
80101ecf:	e8 d3 e2 ff ff       	call   801001a7 <bread>
80101ed4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ed7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101eda:	83 c0 18             	add    $0x18,%eax
80101edd:	89 c2                	mov    %eax,%edx
80101edf:	8b 45 08             	mov    0x8(%ebp),%eax
80101ee2:	8b 40 04             	mov    0x4(%eax),%eax
80101ee5:	83 e0 07             	and    $0x7,%eax
80101ee8:	c1 e0 06             	shl    $0x6,%eax
80101eeb:	8d 04 02             	lea    (%edx,%eax,1),%eax
80101eee:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip->type = ip->type;
80101ef1:	8b 45 08             	mov    0x8(%ebp),%eax
80101ef4:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101ef8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101efb:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101efe:	8b 45 08             	mov    0x8(%ebp),%eax
80101f01:	0f b7 50 12          	movzwl 0x12(%eax),%edx
80101f05:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f08:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
80101f0c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f0f:	0f b7 50 14          	movzwl 0x14(%eax),%edx
80101f13:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f16:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
80101f1a:	8b 45 08             	mov    0x8(%ebp),%eax
80101f1d:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101f21:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f24:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
80101f28:	8b 45 08             	mov    0x8(%ebp),%eax
80101f2b:	8b 50 18             	mov    0x18(%eax),%edx
80101f2e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f31:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101f34:	8b 45 08             	mov    0x8(%ebp),%eax
80101f37:	8d 50 1c             	lea    0x1c(%eax),%edx
80101f3a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f3d:	83 c0 0c             	add    $0xc,%eax
80101f40:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80101f47:	00 
80101f48:	89 54 24 04          	mov    %edx,0x4(%esp)
80101f4c:	89 04 24             	mov    %eax,(%esp)
80101f4f:	e8 e1 3c 00 00       	call   80105c35 <memmove>
  log_write(bp);
80101f54:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f57:	89 04 24             	mov    %eax,(%esp)
80101f5a:	e8 11 1d 00 00       	call   80103c70 <log_write>
  brelse(bp);
80101f5f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f62:	89 04 24             	mov    %eax,(%esp)
80101f65:	e8 ae e2 ff ff       	call   80100218 <brelse>
}
80101f6a:	c9                   	leave  
80101f6b:	c3                   	ret    

80101f6c <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101f6c:	55                   	push   %ebp
80101f6d:	89 e5                	mov    %esp,%ebp
80101f6f:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
80101f72:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101f79:	e8 81 39 00 00       	call   801058ff <acquire>

  // Is the inode already cached?
  empty = 0;
80101f7e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101f85:	c7 45 f0 f4 2a 11 80 	movl   $0x80112af4,-0x10(%ebp)
80101f8c:	eb 59                	jmp    80101fe7 <iget+0x7b>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101f8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f91:	8b 40 08             	mov    0x8(%eax),%eax
80101f94:	85 c0                	test   %eax,%eax
80101f96:	7e 35                	jle    80101fcd <iget+0x61>
80101f98:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f9b:	8b 00                	mov    (%eax),%eax
80101f9d:	3b 45 08             	cmp    0x8(%ebp),%eax
80101fa0:	75 2b                	jne    80101fcd <iget+0x61>
80101fa2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fa5:	8b 40 04             	mov    0x4(%eax),%eax
80101fa8:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101fab:	75 20                	jne    80101fcd <iget+0x61>
      ip->ref++;
80101fad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb0:	8b 40 08             	mov    0x8(%eax),%eax
80101fb3:	8d 50 01             	lea    0x1(%eax),%edx
80101fb6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fb9:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101fbc:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80101fc3:	e8 a0 39 00 00       	call   80105968 <release>
      return ip;
80101fc8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fcb:	eb 70                	jmp    8010203d <iget+0xd1>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101fcd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101fd1:	75 10                	jne    80101fe3 <iget+0x77>
80101fd3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fd6:	8b 40 08             	mov    0x8(%eax),%eax
80101fd9:	85 c0                	test   %eax,%eax
80101fdb:	75 06                	jne    80101fe3 <iget+0x77>
      empty = ip;
80101fdd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101fe0:	89 45 f4             	mov    %eax,-0xc(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101fe3:	83 45 f0 50          	addl   $0x50,-0x10(%ebp)
80101fe7:	b8 94 3a 11 80       	mov    $0x80113a94,%eax
80101fec:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80101fef:	72 9d                	jb     80101f8e <iget+0x22>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101ff1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101ff5:	75 0c                	jne    80102003 <iget+0x97>
    panic("iget: no inodes");
80101ff7:	c7 04 24 81 91 10 80 	movl   $0x80109181,(%esp)
80101ffe:	e8 66 e8 ff ff       	call   80100869 <panic>

  ip = empty;
80102003:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102006:	89 45 f0             	mov    %eax,-0x10(%ebp)
  ip->dev = dev;
80102009:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010200c:	8b 55 08             	mov    0x8(%ebp),%edx
8010200f:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
80102011:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102014:	8b 55 0c             	mov    0xc(%ebp),%edx
80102017:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
8010201a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010201d:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
80102024:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102027:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
8010202e:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102035:	e8 2e 39 00 00       	call   80105968 <release>

  return ip;
8010203a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010203d:	c9                   	leave  
8010203e:	c3                   	ret    

8010203f <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
8010203f:	55                   	push   %ebp
80102040:	89 e5                	mov    %esp,%ebp
80102042:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102045:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010204c:	e8 ae 38 00 00       	call   801058ff <acquire>
  ip->ref++;
80102051:	8b 45 08             	mov    0x8(%ebp),%eax
80102054:	8b 40 08             	mov    0x8(%eax),%eax
80102057:	8d 50 01             	lea    0x1(%eax),%edx
8010205a:	8b 45 08             	mov    0x8(%ebp),%eax
8010205d:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80102060:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102067:	e8 fc 38 00 00       	call   80105968 <release>
  return ip;
8010206c:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010206f:	c9                   	leave  
80102070:	c3                   	ret    

80102071 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
80102071:	55                   	push   %ebp
80102072:	89 e5                	mov    %esp,%ebp
80102074:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
80102077:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010207b:	74 0a                	je     80102087 <ilock+0x16>
8010207d:	8b 45 08             	mov    0x8(%ebp),%eax
80102080:	8b 40 08             	mov    0x8(%eax),%eax
80102083:	85 c0                	test   %eax,%eax
80102085:	7f 0c                	jg     80102093 <ilock+0x22>
    panic("ilock");
80102087:	c7 04 24 91 91 10 80 	movl   $0x80109191,(%esp)
8010208e:	e8 d6 e7 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
80102093:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010209a:	e8 60 38 00 00       	call   801058ff <acquire>
  while(ip->flags & I_BUSY)
8010209f:	eb 13                	jmp    801020b4 <ilock+0x43>
    sleep(ip, &icache.lock);
801020a1:	c7 44 24 04 c0 2a 11 	movl   $0x80112ac0,0x4(%esp)
801020a8:	80 
801020a9:	8b 45 08             	mov    0x8(%ebp),%eax
801020ac:	89 04 24             	mov    %eax,(%esp)
801020af:	e8 69 35 00 00       	call   8010561d <sleep>

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
801020b4:	8b 45 08             	mov    0x8(%ebp),%eax
801020b7:	8b 40 0c             	mov    0xc(%eax),%eax
801020ba:	83 e0 01             	and    $0x1,%eax
801020bd:	84 c0                	test   %al,%al
801020bf:	75 e0                	jne    801020a1 <ilock+0x30>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
801020c1:	8b 45 08             	mov    0x8(%ebp),%eax
801020c4:	8b 40 0c             	mov    0xc(%eax),%eax
801020c7:	89 c2                	mov    %eax,%edx
801020c9:	83 ca 01             	or     $0x1,%edx
801020cc:	8b 45 08             	mov    0x8(%ebp),%eax
801020cf:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
801020d2:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801020d9:	e8 8a 38 00 00       	call   80105968 <release>

  if(!(ip->flags & I_VALID)){
801020de:	8b 45 08             	mov    0x8(%ebp),%eax
801020e1:	8b 40 0c             	mov    0xc(%eax),%eax
801020e4:	83 e0 02             	and    $0x2,%eax
801020e7:	85 c0                	test   %eax,%eax
801020e9:	0f 85 d7 00 00 00    	jne    801021c6 <ilock+0x155>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801020ef:	8b 45 08             	mov    0x8(%ebp),%eax
801020f2:	8b 40 04             	mov    0x4(%eax),%eax
801020f5:	89 c2                	mov    %eax,%edx
801020f7:	c1 ea 03             	shr    $0x3,%edx
801020fa:	a1 b4 2a 11 80       	mov    0x80112ab4,%eax
801020ff:	01 c2                	add    %eax,%edx
80102101:	8b 45 08             	mov    0x8(%ebp),%eax
80102104:	8b 00                	mov    (%eax),%eax
80102106:	89 54 24 04          	mov    %edx,0x4(%esp)
8010210a:	89 04 24             	mov    %eax,(%esp)
8010210d:	e8 95 e0 ff ff       	call   801001a7 <bread>
80102112:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80102115:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102118:	83 c0 18             	add    $0x18,%eax
8010211b:	89 c2                	mov    %eax,%edx
8010211d:	8b 45 08             	mov    0x8(%ebp),%eax
80102120:	8b 40 04             	mov    0x4(%eax),%eax
80102123:	83 e0 07             	and    $0x7,%eax
80102126:	c1 e0 06             	shl    $0x6,%eax
80102129:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010212c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    ip->type = dip->type;
8010212f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102132:	0f b7 10             	movzwl (%eax),%edx
80102135:	8b 45 08             	mov    0x8(%ebp),%eax
80102138:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
8010213c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010213f:	0f b7 50 02          	movzwl 0x2(%eax),%edx
80102143:	8b 45 08             	mov    0x8(%ebp),%eax
80102146:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
8010214a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010214d:	0f b7 50 04          	movzwl 0x4(%eax),%edx
80102151:	8b 45 08             	mov    0x8(%ebp),%eax
80102154:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
80102158:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010215b:	0f b7 50 06          	movzwl 0x6(%eax),%edx
8010215f:	8b 45 08             	mov    0x8(%ebp),%eax
80102162:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
80102166:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102169:	8b 50 08             	mov    0x8(%eax),%edx
8010216c:	8b 45 08             	mov    0x8(%ebp),%eax
8010216f:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80102172:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102175:	8d 50 0c             	lea    0xc(%eax),%edx
80102178:	8b 45 08             	mov    0x8(%ebp),%eax
8010217b:	83 c0 1c             	add    $0x1c,%eax
8010217e:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80102185:	00 
80102186:	89 54 24 04          	mov    %edx,0x4(%esp)
8010218a:	89 04 24             	mov    %eax,(%esp)
8010218d:	e8 a3 3a 00 00       	call   80105c35 <memmove>
    brelse(bp);
80102192:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102195:	89 04 24             	mov    %eax,(%esp)
80102198:	e8 7b e0 ff ff       	call   80100218 <brelse>
    ip->flags |= I_VALID;
8010219d:	8b 45 08             	mov    0x8(%ebp),%eax
801021a0:	8b 40 0c             	mov    0xc(%eax),%eax
801021a3:	89 c2                	mov    %eax,%edx
801021a5:	83 ca 02             	or     $0x2,%edx
801021a8:	8b 45 08             	mov    0x8(%ebp),%eax
801021ab:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
801021ae:	8b 45 08             	mov    0x8(%ebp),%eax
801021b1:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801021b5:	66 85 c0             	test   %ax,%ax
801021b8:	75 0c                	jne    801021c6 <ilock+0x155>
      panic("ilock: no type");
801021ba:	c7 04 24 97 91 10 80 	movl   $0x80109197,(%esp)
801021c1:	e8 a3 e6 ff ff       	call   80100869 <panic>
  }
}
801021c6:	c9                   	leave  
801021c7:	c3                   	ret    

801021c8 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
801021c8:	55                   	push   %ebp
801021c9:	89 e5                	mov    %esp,%ebp
801021cb:	83 ec 18             	sub    $0x18,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
801021ce:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801021d2:	74 17                	je     801021eb <iunlock+0x23>
801021d4:	8b 45 08             	mov    0x8(%ebp),%eax
801021d7:	8b 40 0c             	mov    0xc(%eax),%eax
801021da:	83 e0 01             	and    $0x1,%eax
801021dd:	85 c0                	test   %eax,%eax
801021df:	74 0a                	je     801021eb <iunlock+0x23>
801021e1:	8b 45 08             	mov    0x8(%ebp),%eax
801021e4:	8b 40 08             	mov    0x8(%eax),%eax
801021e7:	85 c0                	test   %eax,%eax
801021e9:	7f 0c                	jg     801021f7 <iunlock+0x2f>
    panic("iunlock");
801021eb:	c7 04 24 a6 91 10 80 	movl   $0x801091a6,(%esp)
801021f2:	e8 72 e6 ff ff       	call   80100869 <panic>

  acquire(&icache.lock);
801021f7:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801021fe:	e8 fc 36 00 00       	call   801058ff <acquire>
  ip->flags &= ~I_BUSY;
80102203:	8b 45 08             	mov    0x8(%ebp),%eax
80102206:	8b 40 0c             	mov    0xc(%eax),%eax
80102209:	89 c2                	mov    %eax,%edx
8010220b:	83 e2 fe             	and    $0xfffffffe,%edx
8010220e:	8b 45 08             	mov    0x8(%ebp),%eax
80102211:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80102214:	8b 45 08             	mov    0x8(%ebp),%eax
80102217:	89 04 24             	mov    %eax,(%esp)
8010221a:	e8 db 34 00 00       	call   801056fa <wakeup>
  release(&icache.lock);
8010221f:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
80102226:	e8 3d 37 00 00       	call   80105968 <release>
}
8010222b:	c9                   	leave  
8010222c:	c3                   	ret    

8010222d <iput>:
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void
iput(struct inode *ip)
{
8010222d:	55                   	push   %ebp
8010222e:	89 e5                	mov    %esp,%ebp
80102230:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80102233:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010223a:	e8 c0 36 00 00       	call   801058ff <acquire>
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
8010223f:	8b 45 08             	mov    0x8(%ebp),%eax
80102242:	8b 40 08             	mov    0x8(%eax),%eax
80102245:	83 f8 01             	cmp    $0x1,%eax
80102248:	0f 85 93 00 00 00    	jne    801022e1 <iput+0xb4>
8010224e:	8b 45 08             	mov    0x8(%ebp),%eax
80102251:	8b 40 0c             	mov    0xc(%eax),%eax
80102254:	83 e0 02             	and    $0x2,%eax
80102257:	85 c0                	test   %eax,%eax
80102259:	0f 84 82 00 00 00    	je     801022e1 <iput+0xb4>
8010225f:	8b 45 08             	mov    0x8(%ebp),%eax
80102262:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80102266:	66 85 c0             	test   %ax,%ax
80102269:	75 76                	jne    801022e1 <iput+0xb4>
    // inode has no links and no other references: truncate and free.
    if(ip->flags & I_BUSY)
8010226b:	8b 45 08             	mov    0x8(%ebp),%eax
8010226e:	8b 40 0c             	mov    0xc(%eax),%eax
80102271:	83 e0 01             	and    $0x1,%eax
80102274:	84 c0                	test   %al,%al
80102276:	74 0c                	je     80102284 <iput+0x57>
      panic("iput busy");
80102278:	c7 04 24 ae 91 10 80 	movl   $0x801091ae,(%esp)
8010227f:	e8 e5 e5 ff ff       	call   80100869 <panic>
    ip->flags |= I_BUSY;
80102284:	8b 45 08             	mov    0x8(%ebp),%eax
80102287:	8b 40 0c             	mov    0xc(%eax),%eax
8010228a:	89 c2                	mov    %eax,%edx
8010228c:	83 ca 01             	or     $0x1,%edx
8010228f:	8b 45 08             	mov    0x8(%ebp),%eax
80102292:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80102295:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
8010229c:	e8 c7 36 00 00       	call   80105968 <release>
    itrunc(ip);
801022a1:	8b 45 08             	mov    0x8(%ebp),%eax
801022a4:	89 04 24             	mov    %eax,(%esp)
801022a7:	e8 72 01 00 00       	call   8010241e <itrunc>
    ip->type = 0;
801022ac:	8b 45 08             	mov    0x8(%ebp),%eax
801022af:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
801022b5:	8b 45 08             	mov    0x8(%ebp),%eax
801022b8:	89 04 24             	mov    %eax,(%esp)
801022bb:	e8 eb fb ff ff       	call   80101eab <iupdate>
    acquire(&icache.lock);
801022c0:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801022c7:	e8 33 36 00 00       	call   801058ff <acquire>
    ip->flags = 0;
801022cc:	8b 45 08             	mov    0x8(%ebp),%eax
801022cf:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
801022d6:	8b 45 08             	mov    0x8(%ebp),%eax
801022d9:	89 04 24             	mov    %eax,(%esp)
801022dc:	e8 19 34 00 00       	call   801056fa <wakeup>
  }
  ip->ref--;
801022e1:	8b 45 08             	mov    0x8(%ebp),%eax
801022e4:	8b 40 08             	mov    0x8(%eax),%eax
801022e7:	8d 50 ff             	lea    -0x1(%eax),%edx
801022ea:	8b 45 08             	mov    0x8(%ebp),%eax
801022ed:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801022f0:	c7 04 24 c0 2a 11 80 	movl   $0x80112ac0,(%esp)
801022f7:	e8 6c 36 00 00       	call   80105968 <release>
}
801022fc:	c9                   	leave  
801022fd:	c3                   	ret    

801022fe <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
801022fe:	55                   	push   %ebp
801022ff:	89 e5                	mov    %esp,%ebp
80102301:	83 ec 18             	sub    $0x18,%esp
  iunlock(ip);
80102304:	8b 45 08             	mov    0x8(%ebp),%eax
80102307:	89 04 24             	mov    %eax,(%esp)
8010230a:	e8 b9 fe ff ff       	call   801021c8 <iunlock>
  iput(ip);
8010230f:	8b 45 08             	mov    0x8(%ebp),%eax
80102312:	89 04 24             	mov    %eax,(%esp)
80102315:	e8 13 ff ff ff       	call   8010222d <iput>
}
8010231a:	c9                   	leave  
8010231b:	c3                   	ret    

8010231c <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
8010231c:	55                   	push   %ebp
8010231d:	89 e5                	mov    %esp,%ebp
8010231f:	53                   	push   %ebx
80102320:	83 ec 24             	sub    $0x24,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80102323:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80102327:	77 3e                	ja     80102367 <bmap+0x4b>
    if((addr = ip->addrs[bn]) == 0)
80102329:	8b 55 0c             	mov    0xc(%ebp),%edx
8010232c:	8b 45 08             	mov    0x8(%ebp),%eax
8010232f:	83 c2 04             	add    $0x4,%edx
80102332:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80102336:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102339:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010233d:	75 20                	jne    8010235f <bmap+0x43>
      ip->addrs[bn] = addr = balloc(ip->dev);
8010233f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80102342:	8b 45 08             	mov    0x8(%ebp),%eax
80102345:	8b 00                	mov    (%eax),%eax
80102347:	89 04 24             	mov    %eax,(%esp)
8010234a:	e8 d0 f7 ff ff       	call   80101b1f <balloc>
8010234f:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102352:	8b 45 08             	mov    0x8(%ebp),%eax
80102355:	8d 4b 04             	lea    0x4(%ebx),%ecx
80102358:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010235b:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
8010235f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102362:	e9 b1 00 00 00       	jmp    80102418 <bmap+0xfc>
  }
  bn -= NDIRECT;
80102367:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
8010236b:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
8010236f:	0f 87 97 00 00 00    	ja     8010240c <bmap+0xf0>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80102375:	8b 45 08             	mov    0x8(%ebp),%eax
80102378:	8b 40 4c             	mov    0x4c(%eax),%eax
8010237b:	89 45 ec             	mov    %eax,-0x14(%ebp)
8010237e:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80102382:	75 19                	jne    8010239d <bmap+0x81>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80102384:	8b 45 08             	mov    0x8(%ebp),%eax
80102387:	8b 00                	mov    (%eax),%eax
80102389:	89 04 24             	mov    %eax,(%esp)
8010238c:	e8 8e f7 ff ff       	call   80101b1f <balloc>
80102391:	89 45 ec             	mov    %eax,-0x14(%ebp)
80102394:	8b 45 08             	mov    0x8(%ebp),%eax
80102397:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010239a:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
8010239d:	8b 45 08             	mov    0x8(%ebp),%eax
801023a0:	8b 00                	mov    (%eax),%eax
801023a2:	8b 55 ec             	mov    -0x14(%ebp),%edx
801023a5:	89 54 24 04          	mov    %edx,0x4(%esp)
801023a9:	89 04 24             	mov    %eax,(%esp)
801023ac:	e8 f6 dd ff ff       	call   801001a7 <bread>
801023b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    a = (uint*)bp->data;
801023b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023b7:	83 c0 18             	add    $0x18,%eax
801023ba:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((addr = a[bn]) == 0){
801023bd:	8b 45 0c             	mov    0xc(%ebp),%eax
801023c0:	c1 e0 02             	shl    $0x2,%eax
801023c3:	03 45 f0             	add    -0x10(%ebp),%eax
801023c6:	8b 00                	mov    (%eax),%eax
801023c8:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023cb:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801023cf:	75 2b                	jne    801023fc <bmap+0xe0>
      a[bn] = addr = balloc(ip->dev);
801023d1:	8b 45 0c             	mov    0xc(%ebp),%eax
801023d4:	c1 e0 02             	shl    $0x2,%eax
801023d7:	89 c3                	mov    %eax,%ebx
801023d9:	03 5d f0             	add    -0x10(%ebp),%ebx
801023dc:	8b 45 08             	mov    0x8(%ebp),%eax
801023df:	8b 00                	mov    (%eax),%eax
801023e1:	89 04 24             	mov    %eax,(%esp)
801023e4:	e8 36 f7 ff ff       	call   80101b1f <balloc>
801023e9:	89 45 ec             	mov    %eax,-0x14(%ebp)
801023ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
801023ef:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
801023f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023f4:	89 04 24             	mov    %eax,(%esp)
801023f7:	e8 74 18 00 00       	call   80103c70 <log_write>
    }
    brelse(bp);
801023fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023ff:	89 04 24             	mov    %eax,(%esp)
80102402:	e8 11 de ff ff       	call   80100218 <brelse>
    return addr;
80102407:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010240a:	eb 0c                	jmp    80102418 <bmap+0xfc>
  }

  panic("bmap: out of range");
8010240c:	c7 04 24 b8 91 10 80 	movl   $0x801091b8,(%esp)
80102413:	e8 51 e4 ff ff       	call   80100869 <panic>
}
80102418:	83 c4 24             	add    $0x24,%esp
8010241b:	5b                   	pop    %ebx
8010241c:	5d                   	pop    %ebp
8010241d:	c3                   	ret    

8010241e <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
8010241e:	55                   	push   %ebp
8010241f:	89 e5                	mov    %esp,%ebp
80102421:	83 ec 28             	sub    $0x28,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80102424:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010242b:	eb 44                	jmp    80102471 <itrunc+0x53>
    if(ip->addrs[i]){
8010242d:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102430:	8b 45 08             	mov    0x8(%ebp),%eax
80102433:	83 c2 04             	add    $0x4,%edx
80102436:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
8010243a:	85 c0                	test   %eax,%eax
8010243c:	74 2f                	je     8010246d <itrunc+0x4f>
      bfree(ip->dev, ip->addrs[i]);
8010243e:	8b 55 e8             	mov    -0x18(%ebp),%edx
80102441:	8b 45 08             	mov    0x8(%ebp),%eax
80102444:	83 c2 04             	add    $0x4,%edx
80102447:	8b 54 90 0c          	mov    0xc(%eax,%edx,4),%edx
8010244b:	8b 45 08             	mov    0x8(%ebp),%eax
8010244e:	8b 00                	mov    (%eax),%eax
80102450:	89 54 24 04          	mov    %edx,0x4(%esp)
80102454:	89 04 24             	mov    %eax,(%esp)
80102457:	e8 0f f8 ff ff       	call   80101c6b <bfree>
      ip->addrs[i] = 0;
8010245c:	8b 55 e8             	mov    -0x18(%ebp),%edx
8010245f:	8b 45 08             	mov    0x8(%ebp),%eax
80102462:	83 c2 04             	add    $0x4,%edx
80102465:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
8010246c:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
8010246d:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
80102471:	83 7d e8 0b          	cmpl   $0xb,-0x18(%ebp)
80102475:	7e b6                	jle    8010242d <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }

  if(ip->addrs[NDIRECT]){
80102477:	8b 45 08             	mov    0x8(%ebp),%eax
8010247a:	8b 40 4c             	mov    0x4c(%eax),%eax
8010247d:	85 c0                	test   %eax,%eax
8010247f:	0f 84 8f 00 00 00    	je     80102514 <itrunc+0xf6>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80102485:	8b 45 08             	mov    0x8(%ebp),%eax
80102488:	8b 50 4c             	mov    0x4c(%eax),%edx
8010248b:	8b 45 08             	mov    0x8(%ebp),%eax
8010248e:	8b 00                	mov    (%eax),%eax
80102490:	89 54 24 04          	mov    %edx,0x4(%esp)
80102494:	89 04 24             	mov    %eax,(%esp)
80102497:	e8 0b dd ff ff       	call   801001a7 <bread>
8010249c:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
8010249f:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024a2:	83 c0 18             	add    $0x18,%eax
801024a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for(j = 0; j < NINDIRECT; j++){
801024a8:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801024af:	eb 2f                	jmp    801024e0 <itrunc+0xc2>
      if(a[j])
801024b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024b4:	c1 e0 02             	shl    $0x2,%eax
801024b7:	03 45 f4             	add    -0xc(%ebp),%eax
801024ba:	8b 00                	mov    (%eax),%eax
801024bc:	85 c0                	test   %eax,%eax
801024be:	74 1c                	je     801024dc <itrunc+0xbe>
        bfree(ip->dev, a[j]);
801024c0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024c3:	c1 e0 02             	shl    $0x2,%eax
801024c6:	03 45 f4             	add    -0xc(%ebp),%eax
801024c9:	8b 10                	mov    (%eax),%edx
801024cb:	8b 45 08             	mov    0x8(%ebp),%eax
801024ce:	8b 00                	mov    (%eax),%eax
801024d0:	89 54 24 04          	mov    %edx,0x4(%esp)
801024d4:	89 04 24             	mov    %eax,(%esp)
801024d7:	e8 8f f7 ff ff       	call   80101c6b <bfree>
  }

  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
801024dc:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801024e0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801024e3:	83 f8 7f             	cmp    $0x7f,%eax
801024e6:	76 c9                	jbe    801024b1 <itrunc+0x93>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
801024e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801024eb:	89 04 24             	mov    %eax,(%esp)
801024ee:	e8 25 dd ff ff       	call   80100218 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801024f3:	8b 45 08             	mov    0x8(%ebp),%eax
801024f6:	8b 50 4c             	mov    0x4c(%eax),%edx
801024f9:	8b 45 08             	mov    0x8(%ebp),%eax
801024fc:	8b 00                	mov    (%eax),%eax
801024fe:	89 54 24 04          	mov    %edx,0x4(%esp)
80102502:	89 04 24             	mov    %eax,(%esp)
80102505:	e8 61 f7 ff ff       	call   80101c6b <bfree>
    ip->addrs[NDIRECT] = 0;
8010250a:	8b 45 08             	mov    0x8(%ebp),%eax
8010250d:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80102514:	8b 45 08             	mov    0x8(%ebp),%eax
80102517:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
8010251e:	8b 45 08             	mov    0x8(%ebp),%eax
80102521:	89 04 24             	mov    %eax,(%esp)
80102524:	e8 82 f9 ff ff       	call   80101eab <iupdate>
}
80102529:	c9                   	leave  
8010252a:	c3                   	ret    

8010252b <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
8010252b:	55                   	push   %ebp
8010252c:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
8010252e:	8b 45 08             	mov    0x8(%ebp),%eax
80102531:	8b 00                	mov    (%eax),%eax
80102533:	89 c2                	mov    %eax,%edx
80102535:	8b 45 0c             	mov    0xc(%ebp),%eax
80102538:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
8010253b:	8b 45 08             	mov    0x8(%ebp),%eax
8010253e:	8b 50 04             	mov    0x4(%eax),%edx
80102541:	8b 45 0c             	mov    0xc(%ebp),%eax
80102544:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80102547:	8b 45 08             	mov    0x8(%ebp),%eax
8010254a:	0f b7 50 10          	movzwl 0x10(%eax),%edx
8010254e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102551:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80102554:	8b 45 08             	mov    0x8(%ebp),%eax
80102557:	0f b7 50 16          	movzwl 0x16(%eax),%edx
8010255b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010255e:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80102562:	8b 45 08             	mov    0x8(%ebp),%eax
80102565:	8b 50 18             	mov    0x18(%eax),%edx
80102568:	8b 45 0c             	mov    0xc(%ebp),%eax
8010256b:	89 50 10             	mov    %edx,0x10(%eax)
}
8010256e:	5d                   	pop    %ebp
8010256f:	c3                   	ret    

80102570 <readi>:

//PAGEBREAK!
// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80102570:	55                   	push   %ebp
80102571:	89 e5                	mov    %esp,%ebp
80102573:	53                   	push   %ebx
80102574:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80102577:	8b 45 08             	mov    0x8(%ebp),%eax
8010257a:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010257e:	66 83 f8 03          	cmp    $0x3,%ax
80102582:	75 60                	jne    801025e4 <readi+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80102584:	8b 45 08             	mov    0x8(%ebp),%eax
80102587:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010258b:	66 85 c0             	test   %ax,%ax
8010258e:	78 20                	js     801025b0 <readi+0x40>
80102590:	8b 45 08             	mov    0x8(%ebp),%eax
80102593:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102597:	66 83 f8 09          	cmp    $0x9,%ax
8010259b:	7f 13                	jg     801025b0 <readi+0x40>
8010259d:	8b 45 08             	mov    0x8(%ebp),%eax
801025a0:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025a4:	98                   	cwtl   
801025a5:	8b 04 c5 40 2a 11 80 	mov    -0x7feed5c0(,%eax,8),%eax
801025ac:	85 c0                	test   %eax,%eax
801025ae:	75 0a                	jne    801025ba <readi+0x4a>
      return -1;
801025b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801025b5:	e9 1c 01 00 00       	jmp    801026d6 <readi+0x166>
    return devsw[ip->major].read(ip, dst, n);
801025ba:	8b 45 08             	mov    0x8(%ebp),%eax
801025bd:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801025c1:	98                   	cwtl   
801025c2:	8b 14 c5 40 2a 11 80 	mov    -0x7feed5c0(,%eax,8),%edx
801025c9:	8b 45 14             	mov    0x14(%ebp),%eax
801025cc:	89 44 24 08          	mov    %eax,0x8(%esp)
801025d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801025d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801025d7:	8b 45 08             	mov    0x8(%ebp),%eax
801025da:	89 04 24             	mov    %eax,(%esp)
801025dd:	ff d2                	call   *%edx
801025df:	e9 f2 00 00 00       	jmp    801026d6 <readi+0x166>
  }

  if(off > ip->size || off + n < off)
801025e4:	8b 45 08             	mov    0x8(%ebp),%eax
801025e7:	8b 40 18             	mov    0x18(%eax),%eax
801025ea:	3b 45 10             	cmp    0x10(%ebp),%eax
801025ed:	72 0e                	jb     801025fd <readi+0x8d>
801025ef:	8b 45 14             	mov    0x14(%ebp),%eax
801025f2:	8b 55 10             	mov    0x10(%ebp),%edx
801025f5:	8d 04 02             	lea    (%edx,%eax,1),%eax
801025f8:	3b 45 10             	cmp    0x10(%ebp),%eax
801025fb:	73 0a                	jae    80102607 <readi+0x97>
    return -1;
801025fd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102602:	e9 cf 00 00 00       	jmp    801026d6 <readi+0x166>
  if(off + n > ip->size)
80102607:	8b 45 14             	mov    0x14(%ebp),%eax
8010260a:	8b 55 10             	mov    0x10(%ebp),%edx
8010260d:	01 c2                	add    %eax,%edx
8010260f:	8b 45 08             	mov    0x8(%ebp),%eax
80102612:	8b 40 18             	mov    0x18(%eax),%eax
80102615:	39 c2                	cmp    %eax,%edx
80102617:	76 0c                	jbe    80102625 <readi+0xb5>
    n = ip->size - off;
80102619:	8b 45 08             	mov    0x8(%ebp),%eax
8010261c:	8b 40 18             	mov    0x18(%eax),%eax
8010261f:	2b 45 10             	sub    0x10(%ebp),%eax
80102622:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102625:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
8010262c:	e9 96 00 00 00       	jmp    801026c7 <readi+0x157>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102631:	8b 45 10             	mov    0x10(%ebp),%eax
80102634:	c1 e8 09             	shr    $0x9,%eax
80102637:	89 44 24 04          	mov    %eax,0x4(%esp)
8010263b:	8b 45 08             	mov    0x8(%ebp),%eax
8010263e:	89 04 24             	mov    %eax,(%esp)
80102641:	e8 d6 fc ff ff       	call   8010231c <bmap>
80102646:	8b 55 08             	mov    0x8(%ebp),%edx
80102649:	8b 12                	mov    (%edx),%edx
8010264b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010264f:	89 14 24             	mov    %edx,(%esp)
80102652:	e8 50 db ff ff       	call   801001a7 <bread>
80102657:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010265a:	8b 45 10             	mov    0x10(%ebp),%eax
8010265d:	89 c2                	mov    %eax,%edx
8010265f:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80102665:	b8 00 02 00 00       	mov    $0x200,%eax
8010266a:	89 c1                	mov    %eax,%ecx
8010266c:	29 d1                	sub    %edx,%ecx
8010266e:	89 ca                	mov    %ecx,%edx
80102670:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102673:	8b 4d 14             	mov    0x14(%ebp),%ecx
80102676:	89 cb                	mov    %ecx,%ebx
80102678:	29 c3                	sub    %eax,%ebx
8010267a:	89 d8                	mov    %ebx,%eax
8010267c:	39 c2                	cmp    %eax,%edx
8010267e:	0f 46 c2             	cmovbe %edx,%eax
80102681:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
80102684:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102687:	8d 50 18             	lea    0x18(%eax),%edx
8010268a:	8b 45 10             	mov    0x10(%ebp),%eax
8010268d:	25 ff 01 00 00       	and    $0x1ff,%eax
80102692:	01 c2                	add    %eax,%edx
80102694:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102697:	89 44 24 08          	mov    %eax,0x8(%esp)
8010269b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010269f:	8b 45 0c             	mov    0xc(%ebp),%eax
801026a2:	89 04 24             	mov    %eax,(%esp)
801026a5:	e8 8b 35 00 00       	call   80105c35 <memmove>
    brelse(bp);
801026aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801026ad:	89 04 24             	mov    %eax,(%esp)
801026b0:	e8 63 db ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801026b5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026b8:	01 45 ec             	add    %eax,-0x14(%ebp)
801026bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026be:	01 45 10             	add    %eax,0x10(%ebp)
801026c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801026c4:	01 45 0c             	add    %eax,0xc(%ebp)
801026c7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801026ca:	3b 45 14             	cmp    0x14(%ebp),%eax
801026cd:	0f 82 5e ff ff ff    	jb     80102631 <readi+0xc1>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
801026d3:	8b 45 14             	mov    0x14(%ebp),%eax
}
801026d6:	83 c4 24             	add    $0x24,%esp
801026d9:	5b                   	pop    %ebx
801026da:	5d                   	pop    %ebp
801026db:	c3                   	ret    

801026dc <writei>:

// PAGEBREAK!
// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
801026dc:	55                   	push   %ebp
801026dd:	89 e5                	mov    %esp,%ebp
801026df:	53                   	push   %ebx
801026e0:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
801026e3:	8b 45 08             	mov    0x8(%ebp),%eax
801026e6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801026ea:	66 83 f8 03          	cmp    $0x3,%ax
801026ee:	75 60                	jne    80102750 <writei+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801026f0:	8b 45 08             	mov    0x8(%ebp),%eax
801026f3:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801026f7:	66 85 c0             	test   %ax,%ax
801026fa:	78 20                	js     8010271c <writei+0x40>
801026fc:	8b 45 08             	mov    0x8(%ebp),%eax
801026ff:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102703:	66 83 f8 09          	cmp    $0x9,%ax
80102707:	7f 13                	jg     8010271c <writei+0x40>
80102709:	8b 45 08             	mov    0x8(%ebp),%eax
8010270c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102710:	98                   	cwtl   
80102711:	8b 04 c5 44 2a 11 80 	mov    -0x7feed5bc(,%eax,8),%eax
80102718:	85 c0                	test   %eax,%eax
8010271a:	75 0a                	jne    80102726 <writei+0x4a>
      return -1;
8010271c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102721:	e9 48 01 00 00       	jmp    8010286e <writei+0x192>
    return devsw[ip->major].write(ip, src, n);
80102726:	8b 45 08             	mov    0x8(%ebp),%eax
80102729:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010272d:	98                   	cwtl   
8010272e:	8b 14 c5 44 2a 11 80 	mov    -0x7feed5bc(,%eax,8),%edx
80102735:	8b 45 14             	mov    0x14(%ebp),%eax
80102738:	89 44 24 08          	mov    %eax,0x8(%esp)
8010273c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010273f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102743:	8b 45 08             	mov    0x8(%ebp),%eax
80102746:	89 04 24             	mov    %eax,(%esp)
80102749:	ff d2                	call   *%edx
8010274b:	e9 1e 01 00 00       	jmp    8010286e <writei+0x192>
  }

  if(off > ip->size || off + n < off)
80102750:	8b 45 08             	mov    0x8(%ebp),%eax
80102753:	8b 40 18             	mov    0x18(%eax),%eax
80102756:	3b 45 10             	cmp    0x10(%ebp),%eax
80102759:	72 0e                	jb     80102769 <writei+0x8d>
8010275b:	8b 45 14             	mov    0x14(%ebp),%eax
8010275e:	8b 55 10             	mov    0x10(%ebp),%edx
80102761:	8d 04 02             	lea    (%edx,%eax,1),%eax
80102764:	3b 45 10             	cmp    0x10(%ebp),%eax
80102767:	73 0a                	jae    80102773 <writei+0x97>
    return -1;
80102769:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010276e:	e9 fb 00 00 00       	jmp    8010286e <writei+0x192>
  if(off + n > MAXFILE*BSIZE)
80102773:	8b 45 14             	mov    0x14(%ebp),%eax
80102776:	8b 55 10             	mov    0x10(%ebp),%edx
80102779:	8d 04 02             	lea    (%edx,%eax,1),%eax
8010277c:	3d 00 18 01 00       	cmp    $0x11800,%eax
80102781:	76 0a                	jbe    8010278d <writei+0xb1>
    return -1;
80102783:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102788:	e9 e1 00 00 00       	jmp    8010286e <writei+0x192>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
8010278d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80102794:	e9 a1 00 00 00       	jmp    8010283a <writei+0x15e>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102799:	8b 45 10             	mov    0x10(%ebp),%eax
8010279c:	c1 e8 09             	shr    $0x9,%eax
8010279f:	89 44 24 04          	mov    %eax,0x4(%esp)
801027a3:	8b 45 08             	mov    0x8(%ebp),%eax
801027a6:	89 04 24             	mov    %eax,(%esp)
801027a9:	e8 6e fb ff ff       	call   8010231c <bmap>
801027ae:	8b 55 08             	mov    0x8(%ebp),%edx
801027b1:	8b 12                	mov    (%edx),%edx
801027b3:	89 44 24 04          	mov    %eax,0x4(%esp)
801027b7:	89 14 24             	mov    %edx,(%esp)
801027ba:	e8 e8 d9 ff ff       	call   801001a7 <bread>
801027bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801027c2:	8b 45 10             	mov    0x10(%ebp),%eax
801027c5:	89 c2                	mov    %eax,%edx
801027c7:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
801027cd:	b8 00 02 00 00       	mov    $0x200,%eax
801027d2:	89 c1                	mov    %eax,%ecx
801027d4:	29 d1                	sub    %edx,%ecx
801027d6:	89 ca                	mov    %ecx,%edx
801027d8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801027db:	8b 4d 14             	mov    0x14(%ebp),%ecx
801027de:	89 cb                	mov    %ecx,%ebx
801027e0:	29 c3                	sub    %eax,%ebx
801027e2:	89 d8                	mov    %ebx,%eax
801027e4:	39 c2                	cmp    %eax,%edx
801027e6:	0f 46 c2             	cmovbe %edx,%eax
801027e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
801027ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027ef:	8d 50 18             	lea    0x18(%eax),%edx
801027f2:	8b 45 10             	mov    0x10(%ebp),%eax
801027f5:	25 ff 01 00 00       	and    $0x1ff,%eax
801027fa:	01 c2                	add    %eax,%edx
801027fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801027ff:	89 44 24 08          	mov    %eax,0x8(%esp)
80102803:	8b 45 0c             	mov    0xc(%ebp),%eax
80102806:	89 44 24 04          	mov    %eax,0x4(%esp)
8010280a:	89 14 24             	mov    %edx,(%esp)
8010280d:	e8 23 34 00 00       	call   80105c35 <memmove>
    log_write(bp);
80102812:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102815:	89 04 24             	mov    %eax,(%esp)
80102818:	e8 53 14 00 00       	call   80103c70 <log_write>
    brelse(bp);
8010281d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102820:	89 04 24             	mov    %eax,(%esp)
80102823:	e8 f0 d9 ff ff       	call   80100218 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102828:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010282b:	01 45 ec             	add    %eax,-0x14(%ebp)
8010282e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102831:	01 45 10             	add    %eax,0x10(%ebp)
80102834:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102837:	01 45 0c             	add    %eax,0xc(%ebp)
8010283a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010283d:	3b 45 14             	cmp    0x14(%ebp),%eax
80102840:	0f 82 53 ff ff ff    	jb     80102799 <writei+0xbd>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
80102846:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010284a:	74 1f                	je     8010286b <writei+0x18f>
8010284c:	8b 45 08             	mov    0x8(%ebp),%eax
8010284f:	8b 40 18             	mov    0x18(%eax),%eax
80102852:	3b 45 10             	cmp    0x10(%ebp),%eax
80102855:	73 14                	jae    8010286b <writei+0x18f>
    ip->size = off;
80102857:	8b 45 08             	mov    0x8(%ebp),%eax
8010285a:	8b 55 10             	mov    0x10(%ebp),%edx
8010285d:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
80102860:	8b 45 08             	mov    0x8(%ebp),%eax
80102863:	89 04 24             	mov    %eax,(%esp)
80102866:	e8 40 f6 ff ff       	call   80101eab <iupdate>
  }
  return n;
8010286b:	8b 45 14             	mov    0x14(%ebp),%eax
}
8010286e:	83 c4 24             	add    $0x24,%esp
80102871:	5b                   	pop    %ebx
80102872:	5d                   	pop    %ebp
80102873:	c3                   	ret    

80102874 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80102874:	55                   	push   %ebp
80102875:	89 e5                	mov    %esp,%ebp
80102877:	83 ec 18             	sub    $0x18,%esp
  return strncmp(s, t, DIRSIZ);
8010287a:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102881:	00 
80102882:	8b 45 0c             	mov    0xc(%ebp),%eax
80102885:	89 44 24 04          	mov    %eax,0x4(%esp)
80102889:	8b 45 08             	mov    0x8(%ebp),%eax
8010288c:	89 04 24             	mov    %eax,(%esp)
8010288f:	e8 49 34 00 00       	call   80105cdd <strncmp>
}
80102894:	c9                   	leave  
80102895:	c3                   	ret    

80102896 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102896:	55                   	push   %ebp
80102897:	89 e5                	mov    %esp,%ebp
80102899:	83 ec 38             	sub    $0x38,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
8010289c:	8b 45 08             	mov    0x8(%ebp),%eax
8010289f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801028a3:	66 83 f8 01          	cmp    $0x1,%ax
801028a7:	74 0c                	je     801028b5 <dirlookup+0x1f>
    panic("dirlookup not DIR");
801028a9:	c7 04 24 cb 91 10 80 	movl   $0x801091cb,(%esp)
801028b0:	e8 b4 df ff ff       	call   80100869 <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
801028b5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801028bc:	e9 87 00 00 00       	jmp    80102948 <dirlookup+0xb2>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801028c1:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028c4:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801028cb:	00 
801028cc:	8b 55 f0             	mov    -0x10(%ebp),%edx
801028cf:	89 54 24 08          	mov    %edx,0x8(%esp)
801028d3:	89 44 24 04          	mov    %eax,0x4(%esp)
801028d7:	8b 45 08             	mov    0x8(%ebp),%eax
801028da:	89 04 24             	mov    %eax,(%esp)
801028dd:	e8 8e fc ff ff       	call   80102570 <readi>
801028e2:	83 f8 10             	cmp    $0x10,%eax
801028e5:	74 0c                	je     801028f3 <dirlookup+0x5d>
      panic("dirlink read");
801028e7:	c7 04 24 dd 91 10 80 	movl   $0x801091dd,(%esp)
801028ee:	e8 76 df ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801028f3:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801028f7:	66 85 c0             	test   %ax,%ax
801028fa:	74 47                	je     80102943 <dirlookup+0xad>
      continue;
    if(namecmp(name, de.name) == 0){
801028fc:	8d 45 e0             	lea    -0x20(%ebp),%eax
801028ff:	83 c0 02             	add    $0x2,%eax
80102902:	89 44 24 04          	mov    %eax,0x4(%esp)
80102906:	8b 45 0c             	mov    0xc(%ebp),%eax
80102909:	89 04 24             	mov    %eax,(%esp)
8010290c:	e8 63 ff ff ff       	call   80102874 <namecmp>
80102911:	85 c0                	test   %eax,%eax
80102913:	75 2f                	jne    80102944 <dirlookup+0xae>
      // entry matches path element
      if(poff)
80102915:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80102919:	74 08                	je     80102923 <dirlookup+0x8d>
        *poff = off;
8010291b:	8b 45 10             	mov    0x10(%ebp),%eax
8010291e:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102921:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
80102923:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102927:	0f b7 c0             	movzwl %ax,%eax
8010292a:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return iget(dp->dev, inum);
8010292d:	8b 45 08             	mov    0x8(%ebp),%eax
80102930:	8b 00                	mov    (%eax),%eax
80102932:	8b 55 f4             	mov    -0xc(%ebp),%edx
80102935:	89 54 24 04          	mov    %edx,0x4(%esp)
80102939:	89 04 24             	mov    %eax,(%esp)
8010293c:	e8 2b f6 ff ff       	call   80101f6c <iget>
80102941:	eb 19                	jmp    8010295c <dirlookup+0xc6>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
80102943:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80102944:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
80102948:	8b 45 08             	mov    0x8(%ebp),%eax
8010294b:	8b 40 18             	mov    0x18(%eax),%eax
8010294e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80102951:	0f 87 6a ff ff ff    	ja     801028c1 <dirlookup+0x2b>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
80102957:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010295c:	c9                   	leave  
8010295d:	c3                   	ret    

8010295e <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
8010295e:	55                   	push   %ebp
8010295f:	89 e5                	mov    %esp,%ebp
80102961:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
80102964:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
8010296b:	00 
8010296c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010296f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102973:	8b 45 08             	mov    0x8(%ebp),%eax
80102976:	89 04 24             	mov    %eax,(%esp)
80102979:	e8 18 ff ff ff       	call   80102896 <dirlookup>
8010297e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102981:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102985:	74 15                	je     8010299c <dirlink+0x3e>
    iput(ip);
80102987:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010298a:	89 04 24             	mov    %eax,(%esp)
8010298d:	e8 9b f8 ff ff       	call   8010222d <iput>
    return -1;
80102992:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102997:	e9 b8 00 00 00       	jmp    80102a54 <dirlink+0xf6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010299c:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801029a3:	eb 44                	jmp    801029e9 <dirlink+0x8b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801029a5:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029a8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801029ab:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801029b2:	00 
801029b3:	89 54 24 08          	mov    %edx,0x8(%esp)
801029b7:	89 44 24 04          	mov    %eax,0x4(%esp)
801029bb:	8b 45 08             	mov    0x8(%ebp),%eax
801029be:	89 04 24             	mov    %eax,(%esp)
801029c1:	e8 aa fb ff ff       	call   80102570 <readi>
801029c6:	83 f8 10             	cmp    $0x10,%eax
801029c9:	74 0c                	je     801029d7 <dirlink+0x79>
      panic("dirlink read");
801029cb:	c7 04 24 dd 91 10 80 	movl   $0x801091dd,(%esp)
801029d2:	e8 92 de ff ff       	call   80100869 <panic>
    if(de.inum == 0)
801029d7:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801029db:	66 85 c0             	test   %ax,%ax
801029de:	74 18                	je     801029f8 <dirlink+0x9a>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
801029e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801029e3:	83 c0 10             	add    $0x10,%eax
801029e6:	89 45 f0             	mov    %eax,-0x10(%ebp)
801029e9:	8b 55 f0             	mov    -0x10(%ebp),%edx
801029ec:	8b 45 08             	mov    0x8(%ebp),%eax
801029ef:	8b 40 18             	mov    0x18(%eax),%eax
801029f2:	39 c2                	cmp    %eax,%edx
801029f4:	72 af                	jb     801029a5 <dirlink+0x47>
801029f6:	eb 01                	jmp    801029f9 <dirlink+0x9b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
801029f8:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
801029f9:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102a00:	00 
80102a01:	8b 45 0c             	mov    0xc(%ebp),%eax
80102a04:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a08:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a0b:	83 c0 02             	add    $0x2,%eax
80102a0e:	89 04 24             	mov    %eax,(%esp)
80102a11:	e8 1f 33 00 00       	call   80105d35 <strncpy>
  de.inum = inum;
80102a16:	8b 45 10             	mov    0x10(%ebp),%eax
80102a19:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102a1d:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102a20:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102a23:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80102a2a:	00 
80102a2b:	89 54 24 08          	mov    %edx,0x8(%esp)
80102a2f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a33:	8b 45 08             	mov    0x8(%ebp),%eax
80102a36:	89 04 24             	mov    %eax,(%esp)
80102a39:	e8 9e fc ff ff       	call   801026dc <writei>
80102a3e:	83 f8 10             	cmp    $0x10,%eax
80102a41:	74 0c                	je     80102a4f <dirlink+0xf1>
    panic("dirlink");
80102a43:	c7 04 24 ea 91 10 80 	movl   $0x801091ea,(%esp)
80102a4a:	e8 1a de ff ff       	call   80100869 <panic>

  return 0;
80102a4f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102a54:	c9                   	leave  
80102a55:	c3                   	ret    

80102a56 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80102a56:	55                   	push   %ebp
80102a57:	89 e5                	mov    %esp,%ebp
80102a59:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int len;

  while(*path == '/')
80102a5c:	eb 04                	jmp    80102a62 <skipelem+0xc>
    path++;
80102a5e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
80102a62:	8b 45 08             	mov    0x8(%ebp),%eax
80102a65:	0f b6 00             	movzbl (%eax),%eax
80102a68:	3c 2f                	cmp    $0x2f,%al
80102a6a:	74 f2                	je     80102a5e <skipelem+0x8>
    path++;
  if(*path == 0)
80102a6c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a6f:	0f b6 00             	movzbl (%eax),%eax
80102a72:	84 c0                	test   %al,%al
80102a74:	75 0a                	jne    80102a80 <skipelem+0x2a>
    return 0;
80102a76:	b8 00 00 00 00       	mov    $0x0,%eax
80102a7b:	e9 86 00 00 00       	jmp    80102b06 <skipelem+0xb0>
  s = path;
80102a80:	8b 45 08             	mov    0x8(%ebp),%eax
80102a83:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(*path != '/' && *path != 0)
80102a86:	eb 04                	jmp    80102a8c <skipelem+0x36>
    path++;
80102a88:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102a8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a8f:	0f b6 00             	movzbl (%eax),%eax
80102a92:	3c 2f                	cmp    $0x2f,%al
80102a94:	74 0a                	je     80102aa0 <skipelem+0x4a>
80102a96:	8b 45 08             	mov    0x8(%ebp),%eax
80102a99:	0f b6 00             	movzbl (%eax),%eax
80102a9c:	84 c0                	test   %al,%al
80102a9e:	75 e8                	jne    80102a88 <skipelem+0x32>
    path++;
  len = path - s;
80102aa0:	8b 55 08             	mov    0x8(%ebp),%edx
80102aa3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102aa6:	89 d1                	mov    %edx,%ecx
80102aa8:	29 c1                	sub    %eax,%ecx
80102aaa:	89 c8                	mov    %ecx,%eax
80102aac:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(len >= DIRSIZ)
80102aaf:	83 7d f4 0d          	cmpl   $0xd,-0xc(%ebp)
80102ab3:	7e 1c                	jle    80102ad1 <skipelem+0x7b>
    memmove(name, s, DIRSIZ);
80102ab5:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102abc:	00 
80102abd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102ac0:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ac4:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ac7:	89 04 24             	mov    %eax,(%esp)
80102aca:	e8 66 31 00 00       	call   80105c35 <memmove>
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102acf:	eb 28                	jmp    80102af9 <skipelem+0xa3>
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
80102ad1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ad4:	89 44 24 08          	mov    %eax,0x8(%esp)
80102ad8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102adb:	89 44 24 04          	mov    %eax,0x4(%esp)
80102adf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ae2:	89 04 24             	mov    %eax,(%esp)
80102ae5:	e8 4b 31 00 00       	call   80105c35 <memmove>
    name[len] = 0;
80102aea:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102aed:	03 45 0c             	add    0xc(%ebp),%eax
80102af0:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
80102af3:	eb 04                	jmp    80102af9 <skipelem+0xa3>
    path++;
80102af5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102af9:	8b 45 08             	mov    0x8(%ebp),%eax
80102afc:	0f b6 00             	movzbl (%eax),%eax
80102aff:	3c 2f                	cmp    $0x2f,%al
80102b01:	74 f2                	je     80102af5 <skipelem+0x9f>
    path++;
  return path;
80102b03:	8b 45 08             	mov    0x8(%ebp),%eax
}
80102b06:	c9                   	leave  
80102b07:	c3                   	ret    

80102b08 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80102b08:	55                   	push   %ebp
80102b09:	89 e5                	mov    %esp,%ebp
80102b0b:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *next;

  if(*path == '/')
80102b0e:	8b 45 08             	mov    0x8(%ebp),%eax
80102b11:	0f b6 00             	movzbl (%eax),%eax
80102b14:	3c 2f                	cmp    $0x2f,%al
80102b16:	75 1c                	jne    80102b34 <namex+0x2c>
    ip = iget(ROOTDEV, ROOTINO);
80102b18:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102b1f:	00 
80102b20:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102b27:	e8 40 f4 ff ff       	call   80101f6c <iget>
80102b2c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102b2f:	e9 af 00 00 00       	jmp    80102be3 <namex+0xdb>
  struct inode *ip, *next;

  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);
80102b34:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80102b3a:	8b 40 68             	mov    0x68(%eax),%eax
80102b3d:	89 04 24             	mov    %eax,(%esp)
80102b40:	e8 fa f4 ff ff       	call   8010203f <idup>
80102b45:	89 45 f0             	mov    %eax,-0x10(%ebp)

  while((path = skipelem(path, name)) != 0){
80102b48:	e9 96 00 00 00       	jmp    80102be3 <namex+0xdb>
    ilock(ip);
80102b4d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b50:	89 04 24             	mov    %eax,(%esp)
80102b53:	e8 19 f5 ff ff       	call   80102071 <ilock>
    if(ip->type != T_DIR){
80102b58:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b5b:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80102b5f:	66 83 f8 01          	cmp    $0x1,%ax
80102b63:	74 15                	je     80102b7a <namex+0x72>
      iunlockput(ip);
80102b65:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b68:	89 04 24             	mov    %eax,(%esp)
80102b6b:	e8 8e f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102b70:	b8 00 00 00 00       	mov    $0x0,%eax
80102b75:	e9 a3 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if(nameiparent && *path == '\0'){
80102b7a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102b7e:	74 1d                	je     80102b9d <namex+0x95>
80102b80:	8b 45 08             	mov    0x8(%ebp),%eax
80102b83:	0f b6 00             	movzbl (%eax),%eax
80102b86:	84 c0                	test   %al,%al
80102b88:	75 13                	jne    80102b9d <namex+0x95>
      // Stop one level early.
      iunlock(ip);
80102b8a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b8d:	89 04 24             	mov    %eax,(%esp)
80102b90:	e8 33 f6 ff ff       	call   801021c8 <iunlock>
      return ip;
80102b95:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102b98:	e9 80 00 00 00       	jmp    80102c1d <namex+0x115>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102b9d:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80102ba4:	00 
80102ba5:	8b 45 10             	mov    0x10(%ebp),%eax
80102ba8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bac:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102baf:	89 04 24             	mov    %eax,(%esp)
80102bb2:	e8 df fc ff ff       	call   80102896 <dirlookup>
80102bb7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102bba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102bbe:	75 12                	jne    80102bd2 <namex+0xca>
      iunlockput(ip);
80102bc0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bc3:	89 04 24             	mov    %eax,(%esp)
80102bc6:	e8 33 f7 ff ff       	call   801022fe <iunlockput>
      return 0;
80102bcb:	b8 00 00 00 00       	mov    $0x0,%eax
80102bd0:	eb 4b                	jmp    80102c1d <namex+0x115>
    }
    iunlockput(ip);
80102bd2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102bd5:	89 04 24             	mov    %eax,(%esp)
80102bd8:	e8 21 f7 ff ff       	call   801022fe <iunlockput>
    ip = next;
80102bdd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102be0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102be3:	8b 45 10             	mov    0x10(%ebp),%eax
80102be6:	89 44 24 04          	mov    %eax,0x4(%esp)
80102bea:	8b 45 08             	mov    0x8(%ebp),%eax
80102bed:	89 04 24             	mov    %eax,(%esp)
80102bf0:	e8 61 fe ff ff       	call   80102a56 <skipelem>
80102bf5:	89 45 08             	mov    %eax,0x8(%ebp)
80102bf8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102bfc:	0f 85 4b ff ff ff    	jne    80102b4d <namex+0x45>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80102c02:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102c06:	74 12                	je     80102c1a <namex+0x112>
    iput(ip);
80102c08:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102c0b:	89 04 24             	mov    %eax,(%esp)
80102c0e:	e8 1a f6 ff ff       	call   8010222d <iput>
    return 0;
80102c13:	b8 00 00 00 00       	mov    $0x0,%eax
80102c18:	eb 03                	jmp    80102c1d <namex+0x115>
  }
  return ip;
80102c1a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80102c1d:	c9                   	leave  
80102c1e:	c3                   	ret    

80102c1f <namei>:

struct inode*
namei(char *path)
{
80102c1f:	55                   	push   %ebp
80102c20:	89 e5                	mov    %esp,%ebp
80102c22:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
80102c25:	8d 45 ea             	lea    -0x16(%ebp),%eax
80102c28:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c2c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102c33:	00 
80102c34:	8b 45 08             	mov    0x8(%ebp),%eax
80102c37:	89 04 24             	mov    %eax,(%esp)
80102c3a:	e8 c9 fe ff ff       	call   80102b08 <namex>
}
80102c3f:	c9                   	leave  
80102c40:	c3                   	ret    

80102c41 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102c41:	55                   	push   %ebp
80102c42:	89 e5                	mov    %esp,%ebp
80102c44:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 1, name);
80102c47:	8b 45 0c             	mov    0xc(%ebp),%eax
80102c4a:	89 44 24 08          	mov    %eax,0x8(%esp)
80102c4e:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102c55:	00 
80102c56:	8b 45 08             	mov    0x8(%ebp),%eax
80102c59:	89 04 24             	mov    %eax,(%esp)
80102c5c:	e8 a7 fe ff ff       	call   80102b08 <namex>
}
80102c61:	c9                   	leave  
80102c62:	c3                   	ret    
	...

80102c64 <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80102c64:	55                   	push   %ebp
80102c65:	89 e5                	mov    %esp,%ebp
80102c67:	83 ec 14             	sub    $0x14,%esp
80102c6a:	8b 45 08             	mov    0x8(%ebp),%eax
80102c6d:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c71:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102c75:	89 c2                	mov    %eax,%edx
80102c77:	ec                   	in     (%dx),%al
80102c78:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102c7b:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102c7f:	c9                   	leave  
80102c80:	c3                   	ret    

80102c81 <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
80102c81:	55                   	push   %ebp
80102c82:	89 e5                	mov    %esp,%ebp
80102c84:	57                   	push   %edi
80102c85:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
80102c86:	8b 55 08             	mov    0x8(%ebp),%edx
80102c89:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102c8c:	8b 45 10             	mov    0x10(%ebp),%eax
80102c8f:	89 cb                	mov    %ecx,%ebx
80102c91:	89 df                	mov    %ebx,%edi
80102c93:	89 c1                	mov    %eax,%ecx
80102c95:	fc                   	cld    
80102c96:	f3 6d                	rep insl (%dx),%es:(%edi)
80102c98:	89 c8                	mov    %ecx,%eax
80102c9a:	89 fb                	mov    %edi,%ebx
80102c9c:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102c9f:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
80102ca2:	5b                   	pop    %ebx
80102ca3:	5f                   	pop    %edi
80102ca4:	5d                   	pop    %ebp
80102ca5:	c3                   	ret    

80102ca6 <outb>:

static inline void
outb(ushort port, uchar data)
{
80102ca6:	55                   	push   %ebp
80102ca7:	89 e5                	mov    %esp,%ebp
80102ca9:	83 ec 08             	sub    $0x8,%esp
80102cac:	8b 55 08             	mov    0x8(%ebp),%edx
80102caf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102cb2:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102cb6:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cb9:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102cbd:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102cc1:	ee                   	out    %al,(%dx)
}
80102cc2:	c9                   	leave  
80102cc3:	c3                   	ret    

80102cc4 <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
80102cc4:	55                   	push   %ebp
80102cc5:	89 e5                	mov    %esp,%ebp
80102cc7:	56                   	push   %esi
80102cc8:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102cc9:	8b 55 08             	mov    0x8(%ebp),%edx
80102ccc:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102ccf:	8b 45 10             	mov    0x10(%ebp),%eax
80102cd2:	89 cb                	mov    %ecx,%ebx
80102cd4:	89 de                	mov    %ebx,%esi
80102cd6:	89 c1                	mov    %eax,%ecx
80102cd8:	fc                   	cld    
80102cd9:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102cdb:	89 c8                	mov    %ecx,%eax
80102cdd:	89 f3                	mov    %esi,%ebx
80102cdf:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102ce2:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
80102ce5:	5b                   	pop    %ebx
80102ce6:	5e                   	pop    %esi
80102ce7:	5d                   	pop    %ebp
80102ce8:	c3                   	ret    

80102ce9 <diskready>:
static int havedisk1;
static void idestart(struct buf*);

static int
diskready(int *status)
{
80102ce9:	55                   	push   %ebp
80102cea:	89 e5                	mov    %esp,%ebp
80102cec:	83 ec 04             	sub    $0x4,%esp
  *status = inb(IDE_DATA_PRIMARY+IDE_REG_STATUS);
80102cef:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102cf6:	e8 69 ff ff ff       	call   80102c64 <inb>
80102cfb:	0f b6 d0             	movzbl %al,%edx
80102cfe:	8b 45 08             	mov    0x8(%ebp),%eax
80102d01:	89 10                	mov    %edx,(%eax)
  return (*status & (IDE_BSY|IDE_DRDY)) == IDE_DRDY;
80102d03:	8b 45 08             	mov    0x8(%ebp),%eax
80102d06:	8b 00                	mov    (%eax),%eax
80102d08:	25 c0 00 00 00       	and    $0xc0,%eax
80102d0d:	83 f8 40             	cmp    $0x40,%eax
80102d10:	0f 94 c0             	sete   %al
80102d13:	0f b6 c0             	movzbl %al,%eax
}
80102d16:	c9                   	leave  
80102d17:	c3                   	ret    

80102d18 <idewait>:

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102d18:	55                   	push   %ebp
80102d19:	89 e5                	mov    %esp,%ebp
80102d1b:	83 ec 14             	sub    $0x14,%esp
  int r;

  while(!diskready(&r))
80102d1e:	8d 45 fc             	lea    -0x4(%ebp),%eax
80102d21:	89 04 24             	mov    %eax,(%esp)
80102d24:	e8 c0 ff ff ff       	call   80102ce9 <diskready>
80102d29:	85 c0                	test   %eax,%eax
80102d2b:	74 f1                	je     80102d1e <idewait+0x6>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
80102d2d:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102d31:	74 11                	je     80102d44 <idewait+0x2c>
80102d33:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102d36:	83 e0 21             	and    $0x21,%eax
80102d39:	85 c0                	test   %eax,%eax
80102d3b:	74 07                	je     80102d44 <idewait+0x2c>
    return -1;
80102d3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d42:	eb 05                	jmp    80102d49 <idewait+0x31>
  return 0;
80102d44:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102d49:	c9                   	leave  
80102d4a:	c3                   	ret    

80102d4b <ideinit>:

void
ideinit(void)
{
80102d4b:	55                   	push   %ebp
80102d4c:	89 e5                	mov    %esp,%ebp
80102d4e:	83 ec 28             	sub    $0x28,%esp
  int i;

  initlock(&idelock, "ide");
80102d51:	c7 44 24 04 f2 91 10 	movl   $0x801091f2,0x4(%esp)
80102d58:	80 
80102d59:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102d60:	e8 79 2b 00 00       	call   801058de <initlock>
  picenable(IRQ_IDE);
80102d65:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d6c:	e8 2c 17 00 00       	call   8010449d <picenable>
  ioapicenable(IRQ_IDE, ncpu - 1);
80102d71:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80102d76:	83 e8 01             	sub    $0x1,%eax
80102d79:	89 44 24 04          	mov    %eax,0x4(%esp)
80102d7d:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
80102d84:	e8 71 04 00 00       	call   801031fa <ioapicenable>
  idewait(0);
80102d89:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102d90:	e8 83 ff ff ff       	call   80102d18 <idewait>

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
80102d95:	c7 44 24 04 f0 00 00 	movl   $0xf0,0x4(%esp)
80102d9c:	00 
80102d9d:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102da4:	e8 fd fe ff ff       	call   80102ca6 <outb>
  for(i=0; i<1000; i++){
80102da9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102db0:	eb 20                	jmp    80102dd2 <ideinit+0x87>
    if(inb(IDE_DATA_PRIMARY+IDE_REG_STATUS) != 0){
80102db2:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102db9:	e8 a6 fe ff ff       	call   80102c64 <inb>
80102dbe:	84 c0                	test   %al,%al
80102dc0:	74 0c                	je     80102dce <ideinit+0x83>
      havedisk1 = 1;
80102dc2:	c7 05 78 c6 10 80 01 	movl   $0x1,0x8010c678
80102dc9:	00 00 00 
      break;
80102dcc:	eb 0d                	jmp    80102ddb <ideinit+0x90>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);

  // Check if disk 1 is present
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (1<<4));
  for(i=0; i<1000; i++){
80102dce:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102dd2:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
80102dd9:	7e d7                	jle    80102db2 <ideinit+0x67>
      break;
    }
  }

  // Switch back to disk 0.
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK, IDE_DISK_LBA | (0<<4));
80102ddb:	c7 44 24 04 e0 00 00 	movl   $0xe0,0x4(%esp)
80102de2:	00 
80102de3:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102dea:	e8 b7 fe ff ff       	call   80102ca6 <outb>
}
80102def:	c9                   	leave  
80102df0:	c3                   	ret    

80102df1 <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102df1:	55                   	push   %ebp
80102df2:	89 e5                	mov    %esp,%ebp
80102df4:	83 ec 28             	sub    $0x28,%esp
  if(b == 0)
80102df7:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102dfb:	75 0c                	jne    80102e09 <idestart+0x18>
    panic("idestart");
80102dfd:	c7 04 24 f6 91 10 80 	movl   $0x801091f6,(%esp)
80102e04:	e8 60 da ff ff       	call   80100869 <panic>
  if(b->blockno >= FSSIZE)
80102e09:	8b 45 08             	mov    0x8(%ebp),%eax
80102e0c:	8b 40 08             	mov    0x8(%eax),%eax
80102e0f:	3d e7 03 00 00       	cmp    $0x3e7,%eax
80102e14:	76 0c                	jbe    80102e22 <idestart+0x31>
    panic("incorrect blockno");
80102e16:	c7 04 24 ff 91 10 80 	movl   $0x801091ff,(%esp)
80102e1d:	e8 47 da ff ff       	call   80100869 <panic>
  int sector_per_block =  BSIZE/SECTOR_SIZE;
80102e22:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
  int sector = b->blockno * sector_per_block;
80102e29:	8b 45 08             	mov    0x8(%ebp),%eax
80102e2c:	8b 50 08             	mov    0x8(%eax),%edx
80102e2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e32:	0f af c2             	imul   %edx,%eax
80102e35:	89 45 f4             	mov    %eax,-0xc(%ebp)

  if (sector_per_block > 7) panic("idestart");
80102e38:	83 7d f0 07          	cmpl   $0x7,-0x10(%ebp)
80102e3c:	7e 0c                	jle    80102e4a <idestart+0x59>
80102e3e:	c7 04 24 f6 91 10 80 	movl   $0x801091f6,(%esp)
80102e45:	e8 1f da ff ff       	call   80100869 <panic>

  idewait(0);
80102e4a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80102e51:	e8 c2 fe ff ff       	call   80102d18 <idewait>
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
80102e56:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102e5d:	00 
80102e5e:	c7 04 24 f6 03 00 00 	movl   $0x3f6,(%esp)
80102e65:	e8 3c fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
80102e6a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102e6d:	0f b6 c0             	movzbl %al,%eax
80102e70:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e74:	c7 04 24 f2 01 00 00 	movl   $0x1f2,(%esp)
80102e7b:	e8 26 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
80102e80:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e83:	0f b6 c0             	movzbl %al,%eax
80102e86:	89 44 24 04          	mov    %eax,0x4(%esp)
80102e8a:	c7 04 24 f3 01 00 00 	movl   $0x1f3,(%esp)
80102e91:	e8 10 fe ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
80102e96:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102e99:	c1 f8 08             	sar    $0x8,%eax
80102e9c:	0f b6 c0             	movzbl %al,%eax
80102e9f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ea3:	c7 04 24 f4 01 00 00 	movl   $0x1f4,(%esp)
80102eaa:	e8 f7 fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
80102eaf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102eb2:	c1 f8 10             	sar    $0x10,%eax
80102eb5:	0f b6 c0             	movzbl %al,%eax
80102eb8:	89 44 24 04          	mov    %eax,0x4(%esp)
80102ebc:	c7 04 24 f5 01 00 00 	movl   $0x1f5,(%esp)
80102ec3:	e8 de fd ff ff       	call   80102ca6 <outb>
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102ec8:	8b 45 08             	mov    0x8(%ebp),%eax
80102ecb:	8b 40 04             	mov    0x4(%eax),%eax
80102ece:	83 e0 01             	and    $0x1,%eax
80102ed1:	89 c2                	mov    %eax,%edx
80102ed3:	c1 e2 04             	shl    $0x4,%edx
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80102ed6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ed9:	c1 f8 18             	sar    $0x18,%eax
  outb(IDE_CTRL_PRIMARY+IDE_REG_CTRL, 0);  // generate interrupt
  outb(IDE_DATA_PRIMARY+IDE_REG_SECTORS, sector_per_block);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA0, sector & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA1, (sector >> 8) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_LBA2, (sector >> 16) & 0xff);
  outb(IDE_DATA_PRIMARY+IDE_REG_DISK,
80102edc:	83 e0 0f             	and    $0xf,%eax
80102edf:	09 d0                	or     %edx,%eax
80102ee1:	83 c8 e0             	or     $0xffffffe0,%eax
80102ee4:	0f b6 c0             	movzbl %al,%eax
80102ee7:	89 44 24 04          	mov    %eax,0x4(%esp)
80102eeb:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102ef2:	e8 af fd ff ff       	call   80102ca6 <outb>
    IDE_DISK_LBA | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
80102ef7:	8b 45 08             	mov    0x8(%ebp),%eax
80102efa:	8b 00                	mov    (%eax),%eax
80102efc:	83 e0 04             	and    $0x4,%eax
80102eff:	85 c0                	test   %eax,%eax
80102f01:	74 34                	je     80102f37 <idestart+0x146>
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_WRITE);
80102f03:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
80102f0a:	00 
80102f0b:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f12:	e8 8f fd ff ff       	call   80102ca6 <outb>
    outsl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102f17:	8b 45 08             	mov    0x8(%ebp),%eax
80102f1a:	83 c0 18             	add    $0x18,%eax
80102f1d:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102f24:	00 
80102f25:	89 44 24 04          	mov    %eax,0x4(%esp)
80102f29:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102f30:	e8 8f fd ff ff       	call   80102cc4 <outsl>
80102f35:	eb 14                	jmp    80102f4b <idestart+0x15a>
  } else {
    outb(IDE_DATA_PRIMARY+IDE_REG_COMMAND, IDE_CMD_READ);
80102f37:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80102f3e:	00 
80102f3f:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102f46:	e8 5b fd ff ff       	call   80102ca6 <outb>
  }
}
80102f4b:	c9                   	leave  
80102f4c:	c3                   	ret    

80102f4d <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102f4d:	55                   	push   %ebp
80102f4e:	89 e5                	mov    %esp,%ebp
80102f50:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102f53:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102f5a:	e8 a0 29 00 00       	call   801058ff <acquire>
  if((b = idequeue) == 0){
80102f5f:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102f64:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102f67:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102f6b:	75 11                	jne    80102f7e <ideintr+0x31>
    release(&idelock);
80102f6d:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80102f74:	e8 ef 29 00 00       	call   80105968 <release>
    // cprintf("spurious IDE interrupt\n");
    return;
80102f79:	e9 90 00 00 00       	jmp    8010300e <ideintr+0xc1>
  }
  idequeue = b->qnext;
80102f7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f81:	8b 40 14             	mov    0x14(%eax),%eax
80102f84:	a3 74 c6 10 80       	mov    %eax,0x8010c674

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102f89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102f8c:	8b 00                	mov    (%eax),%eax
80102f8e:	83 e0 04             	and    $0x4,%eax
80102f91:	85 c0                	test   %eax,%eax
80102f93:	75 2e                	jne    80102fc3 <ideintr+0x76>
80102f95:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102f9c:	e8 77 fd ff ff       	call   80102d18 <idewait>
80102fa1:	85 c0                	test   %eax,%eax
80102fa3:	78 1e                	js     80102fc3 <ideintr+0x76>
    insl(IDE_DATA_PRIMARY+IDE_REG_DATA, b->data, BSIZE/4);
80102fa5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fa8:	83 c0 18             	add    $0x18,%eax
80102fab:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102fb2:	00 
80102fb3:	89 44 24 04          	mov    %eax,0x4(%esp)
80102fb7:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102fbe:	e8 be fc ff ff       	call   80102c81 <insl>

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80102fc3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fc6:	8b 00                	mov    (%eax),%eax
80102fc8:	89 c2                	mov    %eax,%edx
80102fca:	83 ca 02             	or     $0x2,%edx
80102fcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd0:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102fd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fd5:	8b 00                	mov    (%eax),%eax
80102fd7:	89 c2                	mov    %eax,%edx
80102fd9:	83 e2 fb             	and    $0xfffffffb,%edx
80102fdc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fdf:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102fe1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102fe4:	89 04 24             	mov    %eax,(%esp)
80102fe7:	e8 0e 27 00 00       	call   801056fa <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102fec:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102ff1:	85 c0                	test   %eax,%eax
80102ff3:	74 0d                	je     80103002 <ideintr+0xb5>
    idestart(idequeue);
80102ff5:	a1 74 c6 10 80       	mov    0x8010c674,%eax
80102ffa:	89 04 24             	mov    %eax,(%esp)
80102ffd:	e8 ef fd ff ff       	call   80102df1 <idestart>

  release(&idelock);
80103002:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
80103009:	e8 5a 29 00 00       	call   80105968 <release>
}
8010300e:	c9                   	leave  
8010300f:	c3                   	ret    

80103010 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80103010:	55                   	push   %ebp
80103011:	89 e5                	mov    %esp,%ebp
80103013:	83 ec 28             	sub    $0x28,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
80103016:	8b 45 08             	mov    0x8(%ebp),%eax
80103019:	8b 00                	mov    (%eax),%eax
8010301b:	83 e0 01             	and    $0x1,%eax
8010301e:	85 c0                	test   %eax,%eax
80103020:	75 0c                	jne    8010302e <iderw+0x1e>
    panic("iderw: buf not busy");
80103022:	c7 04 24 11 92 10 80 	movl   $0x80109211,(%esp)
80103029:	e8 3b d8 ff ff       	call   80100869 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
8010302e:	8b 45 08             	mov    0x8(%ebp),%eax
80103031:	8b 00                	mov    (%eax),%eax
80103033:	83 e0 06             	and    $0x6,%eax
80103036:	83 f8 02             	cmp    $0x2,%eax
80103039:	75 0c                	jne    80103047 <iderw+0x37>
    panic("iderw: nothing to do");
8010303b:	c7 04 24 25 92 10 80 	movl   $0x80109225,(%esp)
80103042:	e8 22 d8 ff ff       	call   80100869 <panic>
  if(b->dev != 0 && !havedisk1)
80103047:	8b 45 08             	mov    0x8(%ebp),%eax
8010304a:	8b 40 04             	mov    0x4(%eax),%eax
8010304d:	85 c0                	test   %eax,%eax
8010304f:	74 15                	je     80103066 <iderw+0x56>
80103051:	a1 78 c6 10 80       	mov    0x8010c678,%eax
80103056:	85 c0                	test   %eax,%eax
80103058:	75 0c                	jne    80103066 <iderw+0x56>
    panic("iderw: ide disk 1 not present");
8010305a:	c7 04 24 3a 92 10 80 	movl   $0x8010923a,(%esp)
80103061:	e8 03 d8 ff ff       	call   80100869 <panic>

  acquire(&idelock);  //DOC:acquire-lock
80103066:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
8010306d:	e8 8d 28 00 00       	call   801058ff <acquire>

  // Append b to idequeue.
  b->qnext = 0;
80103072:	8b 45 08             	mov    0x8(%ebp),%eax
80103075:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010307c:	c7 45 f4 74 c6 10 80 	movl   $0x8010c674,-0xc(%ebp)
80103083:	eb 0b                	jmp    80103090 <iderw+0x80>
80103085:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103088:	8b 00                	mov    (%eax),%eax
8010308a:	83 c0 14             	add    $0x14,%eax
8010308d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103090:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103093:	8b 00                	mov    (%eax),%eax
80103095:	85 c0                	test   %eax,%eax
80103097:	75 ec                	jne    80103085 <iderw+0x75>
    ;
  *pp = b;
80103099:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010309c:	8b 55 08             	mov    0x8(%ebp),%edx
8010309f:	89 10                	mov    %edx,(%eax)

  // Start disk if necessary.
  if(idequeue == b)
801030a1:	a1 74 c6 10 80       	mov    0x8010c674,%eax
801030a6:	3b 45 08             	cmp    0x8(%ebp),%eax
801030a9:	75 22                	jne    801030cd <iderw+0xbd>
    idestart(b);
801030ab:	8b 45 08             	mov    0x8(%ebp),%eax
801030ae:	89 04 24             	mov    %eax,(%esp)
801030b1:	e8 3b fd ff ff       	call   80102df1 <idestart>

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030b6:	eb 16                	jmp    801030ce <iderw+0xbe>
    sleep(b, &idelock);
801030b8:	c7 44 24 04 40 c6 10 	movl   $0x8010c640,0x4(%esp)
801030bf:	80 
801030c0:	8b 45 08             	mov    0x8(%ebp),%eax
801030c3:	89 04 24             	mov    %eax,(%esp)
801030c6:	e8 52 25 00 00       	call   8010561d <sleep>
801030cb:	eb 01                	jmp    801030ce <iderw+0xbe>
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801030cd:	90                   	nop
801030ce:	8b 45 08             	mov    0x8(%ebp),%eax
801030d1:	8b 00                	mov    (%eax),%eax
801030d3:	83 e0 06             	and    $0x6,%eax
801030d6:	83 f8 02             	cmp    $0x2,%eax
801030d9:	75 dd                	jne    801030b8 <iderw+0xa8>
    sleep(b, &idelock);
  }

  release(&idelock);
801030db:	c7 04 24 40 c6 10 80 	movl   $0x8010c640,(%esp)
801030e2:	e8 81 28 00 00       	call   80105968 <release>
}
801030e7:	c9                   	leave  
801030e8:	c3                   	ret    
801030e9:	00 00                	add    %al,(%eax)
	...

801030ec <ioapicread>:
  uint data;    // offset  10
};

static uint
ioapicread(int reg)
{
801030ec:	55                   	push   %ebp
801030ed:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
801030ef:	a1 94 3a 11 80       	mov    0x80113a94,%eax
801030f4:	8b 55 08             	mov    0x8(%ebp),%edx
801030f7:	81 e2 ff 00 00 00    	and    $0xff,%edx
801030fd:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
801030ff:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103104:	8b 40 10             	mov    0x10(%eax),%eax
}
80103107:	5d                   	pop    %ebp
80103108:	c3                   	ret    

80103109 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80103109:	55                   	push   %ebp
8010310a:	89 e5                	mov    %esp,%ebp
  ioapic->reg = (reg & 0xFF);  // bits 0..7, see datasheet
8010310c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103111:	8b 55 08             	mov    0x8(%ebp),%edx
80103114:	81 e2 ff 00 00 00    	and    $0xff,%edx
8010311a:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
8010311c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103121:	8b 55 0c             	mov    0xc(%ebp),%edx
80103124:	89 50 10             	mov    %edx,0x10(%eax)
}
80103127:	5d                   	pop    %ebp
80103128:	c3                   	ret    

80103129 <ioapicinit>:

void
ioapicinit(void)
{
80103129:	55                   	push   %ebp
8010312a:	89 e5                	mov    %esp,%ebp
8010312c:	83 ec 28             	sub    $0x28,%esp
  int i, id;

  if(!ismp)
8010312f:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103134:	85 c0                	test   %eax,%eax
80103136:	0f 84 bb 00 00 00    	je     801031f7 <ioapicinit+0xce>
    return;

  if(ioapic == 0) {
8010313c:	a1 94 3a 11 80       	mov    0x80113a94,%eax
80103141:	85 c0                	test   %eax,%eax
80103143:	75 16                	jne    8010315b <ioapicinit+0x32>
    ioapic = (volatile struct ioapic*)IOAPIC;
80103145:	c7 05 94 3a 11 80 00 	movl   $0xfec00000,0x80113a94
8010314c:	00 c0 fe 
    cprintf("ioapicinit: falling back to default ioapic address\n");
8010314f:	c7 04 24 58 92 10 80 	movl   $0x80109258,(%esp)
80103156:	e8 6e d5 ff ff       	call   801006c9 <cprintf>
  }
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF; // bits 16..23, see datasheet
8010315b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80103162:	e8 85 ff ff ff       	call   801030ec <ioapicread>
80103167:	c1 e8 10             	shr    $0x10,%eax
8010316a:	25 ff 00 00 00       	and    $0xff,%eax
8010316f:	a3 7c c6 10 80       	mov    %eax,0x8010c67c
  id = (ioapicread(REG_ID) >> 24) & 0x0F;  // bits 24..27, see datasheet
80103174:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010317b:	e8 6c ff ff ff       	call   801030ec <ioapicread>
80103180:	c1 e8 18             	shr    $0x18,%eax
80103183:	83 e0 0f             	and    $0xf,%eax
80103186:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(id != ioapicid)
80103189:	0f b6 05 c0 3b 11 80 	movzbl 0x80113bc0,%eax
80103190:	0f b6 c0             	movzbl %al,%eax
80103193:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103196:	74 0c                	je     801031a4 <ioapicinit+0x7b>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80103198:	c7 04 24 8c 92 10 80 	movl   $0x8010928c,(%esp)
8010319f:	e8 25 d5 ff ff       	call   801006c9 <cprintf>

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031a4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801031ab:	eb 3e                	jmp    801031eb <ioapicinit+0xc2>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801031ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031b0:	83 c0 20             	add    $0x20,%eax
801031b3:	0d 00 00 01 00       	or     $0x10000,%eax
801031b8:	8b 55 f0             	mov    -0x10(%ebp),%edx
801031bb:	83 c2 08             	add    $0x8,%edx
801031be:	01 d2                	add    %edx,%edx
801031c0:	89 44 24 04          	mov    %eax,0x4(%esp)
801031c4:	89 14 24             	mov    %edx,(%esp)
801031c7:	e8 3d ff ff ff       	call   80103109 <ioapicwrite>
    ioapicwrite(REG_TABLE+2*i+1, 0);
801031cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031cf:	83 c0 08             	add    $0x8,%eax
801031d2:	01 c0                	add    %eax,%eax
801031d4:	83 c0 01             	add    $0x1,%eax
801031d7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801031de:	00 
801031df:	89 04 24             	mov    %eax,(%esp)
801031e2:	e8 22 ff ff ff       	call   80103109 <ioapicwrite>
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801031e7:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801031eb:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
801031f0:	39 45 f0             	cmp    %eax,-0x10(%ebp)
801031f3:	7e b8                	jle    801031ad <ioapicinit+0x84>
801031f5:	eb 01                	jmp    801031f8 <ioapicinit+0xcf>
ioapicinit(void)
{
  int i, id;

  if(!ismp)
    return;
801031f7:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
801031f8:	c9                   	leave  
801031f9:	c3                   	ret    

801031fa <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
801031fa:	55                   	push   %ebp
801031fb:	89 e5                	mov    %esp,%ebp
801031fd:	83 ec 18             	sub    $0x18,%esp
  if(!ismp)
80103200:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103205:	85 c0                	test   %eax,%eax
80103207:	74 63                	je     8010326c <ioapicenable+0x72>
    return;

  if(irq > maxintr)
80103209:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
8010320e:	39 45 08             	cmp    %eax,0x8(%ebp)
80103211:	7e 1c                	jle    8010322f <ioapicenable+0x35>
    cprintf("ioapicenable: no irq %d, maximum is %d\n", irq, maxintr);
80103213:	a1 7c c6 10 80       	mov    0x8010c67c,%eax
80103218:	89 44 24 08          	mov    %eax,0x8(%esp)
8010321c:	8b 45 08             	mov    0x8(%ebp),%eax
8010321f:	89 44 24 04          	mov    %eax,0x4(%esp)
80103223:	c7 04 24 c0 92 10 80 	movl   $0x801092c0,(%esp)
8010322a:	e8 9a d4 ff ff       	call   801006c9 <cprintf>

  cpunum = cpunum & 0x0F; // used as APIC id below, 4 bits, see datasheet
8010322f:	83 65 0c 0f          	andl   $0xf,0xc(%ebp)

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80103233:	8b 45 08             	mov    0x8(%ebp),%eax
80103236:	83 c0 20             	add    $0x20,%eax
80103239:	8b 55 08             	mov    0x8(%ebp),%edx
8010323c:	83 c2 08             	add    $0x8,%edx
8010323f:	01 d2                	add    %edx,%edx
80103241:	89 44 24 04          	mov    %eax,0x4(%esp)
80103245:	89 14 24             	mov    %edx,(%esp)
80103248:	e8 bc fe ff ff       	call   80103109 <ioapicwrite>
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
8010324d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103250:	c1 e0 18             	shl    $0x18,%eax
80103253:	8b 55 08             	mov    0x8(%ebp),%edx
80103256:	83 c2 08             	add    $0x8,%edx
80103259:	01 d2                	add    %edx,%edx
8010325b:	83 c2 01             	add    $0x1,%edx
8010325e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103262:	89 14 24             	mov    %edx,(%esp)
80103265:	e8 9f fe ff ff       	call   80103109 <ioapicwrite>
8010326a:	eb 01                	jmp    8010326d <ioapicenable+0x73>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
8010326c:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24); // bits 56..59, see datasheet
}
8010326d:	c9                   	leave  
8010326e:	c3                   	ret    
	...

80103270 <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80103270:	55                   	push   %ebp
80103271:	89 e5                	mov    %esp,%ebp
80103273:	83 ec 18             	sub    $0x18,%esp
  initlock(&kmem.lock, "kmem");
80103276:	c7 44 24 04 e8 92 10 	movl   $0x801092e8,0x4(%esp)
8010327d:	80 
8010327e:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103285:	e8 54 26 00 00       	call   801058de <initlock>
  kmem.use_lock = 0;
8010328a:	c7 05 d4 3a 11 80 00 	movl   $0x0,0x80113ad4
80103291:	00 00 00 
  freerange(vstart, vend);
80103294:	8b 45 0c             	mov    0xc(%ebp),%eax
80103297:	89 44 24 04          	mov    %eax,0x4(%esp)
8010329b:	8b 45 08             	mov    0x8(%ebp),%eax
8010329e:	89 04 24             	mov    %eax,(%esp)
801032a1:	e8 26 00 00 00       	call   801032cc <freerange>
}
801032a6:	c9                   	leave  
801032a7:	c3                   	ret    

801032a8 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
801032a8:	55                   	push   %ebp
801032a9:	89 e5                	mov    %esp,%ebp
801032ab:	83 ec 18             	sub    $0x18,%esp
  freerange(vstart, vend);
801032ae:	8b 45 0c             	mov    0xc(%ebp),%eax
801032b1:	89 44 24 04          	mov    %eax,0x4(%esp)
801032b5:	8b 45 08             	mov    0x8(%ebp),%eax
801032b8:	89 04 24             	mov    %eax,(%esp)
801032bb:	e8 0c 00 00 00       	call   801032cc <freerange>
  kmem.use_lock = 1;
801032c0:	c7 05 d4 3a 11 80 01 	movl   $0x1,0x80113ad4
801032c7:	00 00 00 
}
801032ca:	c9                   	leave  
801032cb:	c3                   	ret    

801032cc <freerange>:

void
freerange(void *vstart, void *vend)
{
801032cc:	55                   	push   %ebp
801032cd:	89 e5                	mov    %esp,%ebp
801032cf:	83 ec 28             	sub    $0x28,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
801032d2:	8b 45 08             	mov    0x8(%ebp),%eax
801032d5:	05 ff 0f 00 00       	add    $0xfff,%eax
801032da:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801032df:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032e2:	eb 12                	jmp    801032f6 <freerange+0x2a>
    kfree(p);
801032e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032e7:	89 04 24             	mov    %eax,(%esp)
801032ea:	e8 19 00 00 00       	call   80103308 <kfree>
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801032ef:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801032f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801032f9:	8d 90 00 10 00 00    	lea    0x1000(%eax),%edx
801032ff:	8b 45 0c             	mov    0xc(%ebp),%eax
80103302:	39 c2                	cmp    %eax,%edx
80103304:	76 de                	jbe    801032e4 <freerange+0x18>
    kfree(p);
}
80103306:	c9                   	leave  
80103307:	c3                   	ret    

80103308 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80103308:	55                   	push   %ebp
80103309:	89 e5                	mov    %esp,%ebp
8010330b:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
8010330e:	8b 45 08             	mov    0x8(%ebp),%eax
80103311:	25 ff 0f 00 00       	and    $0xfff,%eax
80103316:	85 c0                	test   %eax,%eax
80103318:	75 18                	jne    80103332 <kfree+0x2a>
8010331a:	81 7d 08 78 64 11 80 	cmpl   $0x80116478,0x8(%ebp)
80103321:	72 0f                	jb     80103332 <kfree+0x2a>
80103323:	8b 45 08             	mov    0x8(%ebp),%eax
80103326:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010332b:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80103330:	76 0c                	jbe    8010333e <kfree+0x36>
    panic("kfree");
80103332:	c7 04 24 ed 92 10 80 	movl   $0x801092ed,(%esp)
80103339:	e8 2b d5 ff ff       	call   80100869 <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
8010333e:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80103345:	00 
80103346:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010334d:	00 
8010334e:	8b 45 08             	mov    0x8(%ebp),%eax
80103351:	89 04 24             	mov    %eax,(%esp)
80103354:	e8 09 28 00 00       	call   80105b62 <memset>

  if(kmem.use_lock)
80103359:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
8010335e:	85 c0                	test   %eax,%eax
80103360:	74 0c                	je     8010336e <kfree+0x66>
    acquire(&kmem.lock);
80103362:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103369:	e8 91 25 00 00       	call   801058ff <acquire>
  r = (struct run*)v;
8010336e:	8b 45 08             	mov    0x8(%ebp),%eax
80103371:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80103374:	8b 15 d8 3a 11 80    	mov    0x80113ad8,%edx
8010337a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010337d:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
8010337f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103382:	a3 d8 3a 11 80       	mov    %eax,0x80113ad8
  if(kmem.use_lock)
80103387:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
8010338c:	85 c0                	test   %eax,%eax
8010338e:	74 0c                	je     8010339c <kfree+0x94>
    release(&kmem.lock);
80103390:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
80103397:	e8 cc 25 00 00       	call   80105968 <release>
}
8010339c:	c9                   	leave  
8010339d:	c3                   	ret    

8010339e <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
8010339e:	55                   	push   %ebp
8010339f:	89 e5                	mov    %esp,%ebp
801033a1:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if(kmem.use_lock)
801033a4:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
801033a9:	85 c0                	test   %eax,%eax
801033ab:	74 0c                	je     801033b9 <kalloc+0x1b>
    acquire(&kmem.lock);
801033ad:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
801033b4:	e8 46 25 00 00       	call   801058ff <acquire>
  r = kmem.freelist;
801033b9:	a1 d8 3a 11 80       	mov    0x80113ad8,%eax
801033be:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
801033c1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801033c5:	74 0a                	je     801033d1 <kalloc+0x33>
    kmem.freelist = r->next;
801033c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801033ca:	8b 00                	mov    (%eax),%eax
801033cc:	a3 d8 3a 11 80       	mov    %eax,0x80113ad8
  if(kmem.use_lock)
801033d1:	a1 d4 3a 11 80       	mov    0x80113ad4,%eax
801033d6:	85 c0                	test   %eax,%eax
801033d8:	74 0c                	je     801033e6 <kalloc+0x48>
    release(&kmem.lock);
801033da:	c7 04 24 a0 3a 11 80 	movl   $0x80113aa0,(%esp)
801033e1:	e8 82 25 00 00       	call   80105968 <release>
  return (char*)r;
801033e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801033e9:	c9                   	leave  
801033ea:	c3                   	ret    
	...

801033ec <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801033ec:	55                   	push   %ebp
801033ed:	89 e5                	mov    %esp,%ebp
801033ef:	83 ec 14             	sub    $0x14,%esp
801033f2:	8b 45 08             	mov    0x8(%ebp),%eax
801033f5:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801033f9:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801033fd:	89 c2                	mov    %eax,%edx
801033ff:	ec                   	in     (%dx),%al
80103400:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103403:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103407:	c9                   	leave  
80103408:	c3                   	ret    

80103409 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80103409:	55                   	push   %ebp
8010340a:	89 e5                	mov    %esp,%ebp
8010340c:	83 ec 14             	sub    $0x14,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
8010340f:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103416:	e8 d1 ff ff ff       	call   801033ec <inb>
8010341b:	0f b6 c0             	movzbl %al,%eax
8010341e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80103421:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103424:	83 e0 01             	and    $0x1,%eax
80103427:	85 c0                	test   %eax,%eax
80103429:	75 0a                	jne    80103435 <kbdgetc+0x2c>
    return -1;
8010342b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103430:	e9 20 01 00 00       	jmp    80103555 <kbdgetc+0x14c>
  data = inb(KBDATAP);
80103435:	c7 04 24 60 00 00 00 	movl   $0x60,(%esp)
8010343c:	e8 ab ff ff ff       	call   801033ec <inb>
80103441:	0f b6 c0             	movzbl %al,%eax
80103444:	89 45 f8             	mov    %eax,-0x8(%ebp)

  if(data == 0xE0){
80103447:	81 7d f8 e0 00 00 00 	cmpl   $0xe0,-0x8(%ebp)
8010344e:	75 17                	jne    80103467 <kbdgetc+0x5e>
    shift |= E0ESC;
80103450:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103455:	83 c8 40             	or     $0x40,%eax
80103458:	a3 80 c6 10 80       	mov    %eax,0x8010c680
    return 0;
8010345d:	b8 00 00 00 00       	mov    $0x0,%eax
80103462:	e9 ee 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(data & 0x80){
80103467:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010346a:	25 80 00 00 00       	and    $0x80,%eax
8010346f:	85 c0                	test   %eax,%eax
80103471:	74 44                	je     801034b7 <kbdgetc+0xae>
    // Key released
    data = ((shift & E0ESC) ? data : data & 0x7F);
80103473:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103478:	83 e0 40             	and    $0x40,%eax
8010347b:	85 c0                	test   %eax,%eax
8010347d:	75 08                	jne    80103487 <kbdgetc+0x7e>
8010347f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103482:	83 e0 7f             	and    $0x7f,%eax
80103485:	eb 03                	jmp    8010348a <kbdgetc+0x81>
80103487:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010348a:	89 45 f8             	mov    %eax,-0x8(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
8010348d:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103490:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
80103497:	83 c8 40             	or     $0x40,%eax
8010349a:	0f b6 c0             	movzbl %al,%eax
8010349d:	f7 d0                	not    %eax
8010349f:	89 c2                	mov    %eax,%edx
801034a1:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034a6:	21 d0                	and    %edx,%eax
801034a8:	a3 80 c6 10 80       	mov    %eax,0x8010c680
    return 0;
801034ad:	b8 00 00 00 00       	mov    $0x0,%eax
801034b2:	e9 9e 00 00 00       	jmp    80103555 <kbdgetc+0x14c>
  } else if(shift & E0ESC){
801034b7:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034bc:	83 e0 40             	and    $0x40,%eax
801034bf:	85 c0                	test   %eax,%eax
801034c1:	74 14                	je     801034d7 <kbdgetc+0xce>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
801034c3:	81 4d f8 80 00 00 00 	orl    $0x80,-0x8(%ebp)
    shift &= ~E0ESC;
801034ca:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034cf:	83 e0 bf             	and    $0xffffffbf,%eax
801034d2:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  }

  shift |= shiftcode[data];
801034d7:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034da:	0f b6 80 40 a0 10 80 	movzbl -0x7fef5fc0(%eax),%eax
801034e1:	0f b6 d0             	movzbl %al,%edx
801034e4:	a1 80 c6 10 80       	mov    0x8010c680,%eax
801034e9:	09 d0                	or     %edx,%eax
801034eb:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  shift ^= togglecode[data];
801034f0:	8b 45 f8             	mov    -0x8(%ebp),%eax
801034f3:	0f b6 80 40 a1 10 80 	movzbl -0x7fef5ec0(%eax),%eax
801034fa:	0f b6 d0             	movzbl %al,%edx
801034fd:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103502:	31 d0                	xor    %edx,%eax
80103504:	a3 80 c6 10 80       	mov    %eax,0x8010c680
  c = charcode[shift & (CTL | SHIFT)][data];
80103509:	a1 80 c6 10 80       	mov    0x8010c680,%eax
8010350e:	83 e0 03             	and    $0x3,%eax
80103511:	8b 04 85 40 a5 10 80 	mov    -0x7fef5ac0(,%eax,4),%eax
80103518:	03 45 f8             	add    -0x8(%ebp),%eax
8010351b:	0f b6 00             	movzbl (%eax),%eax
8010351e:	0f b6 c0             	movzbl %al,%eax
80103521:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(shift & CAPSLOCK){
80103524:	a1 80 c6 10 80       	mov    0x8010c680,%eax
80103529:	83 e0 08             	and    $0x8,%eax
8010352c:	85 c0                	test   %eax,%eax
8010352e:	74 22                	je     80103552 <kbdgetc+0x149>
    if('a' <= c && c <= 'z')
80103530:	83 7d fc 60          	cmpl   $0x60,-0x4(%ebp)
80103534:	76 0c                	jbe    80103542 <kbdgetc+0x139>
80103536:	83 7d fc 7a          	cmpl   $0x7a,-0x4(%ebp)
8010353a:	77 06                	ja     80103542 <kbdgetc+0x139>
      c += 'A' - 'a';
8010353c:	83 6d fc 20          	subl   $0x20,-0x4(%ebp)

  shift |= shiftcode[data];
  shift ^= togglecode[data];
  c = charcode[shift & (CTL | SHIFT)][data];
  if(shift & CAPSLOCK){
    if('a' <= c && c <= 'z')
80103540:	eb 10                	jmp    80103552 <kbdgetc+0x149>
      c += 'A' - 'a';
    else if('A' <= c && c <= 'Z')
80103542:	83 7d fc 40          	cmpl   $0x40,-0x4(%ebp)
80103546:	76 0a                	jbe    80103552 <kbdgetc+0x149>
80103548:	83 7d fc 5a          	cmpl   $0x5a,-0x4(%ebp)
8010354c:	77 04                	ja     80103552 <kbdgetc+0x149>
      c += 'a' - 'A';
8010354e:	83 45 fc 20          	addl   $0x20,-0x4(%ebp)
  }
  return c;
80103552:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103555:	c9                   	leave  
80103556:	c3                   	ret    

80103557 <kbdintr>:

void
kbdintr(void)
{
80103557:	55                   	push   %ebp
80103558:	89 e5                	mov    %esp,%ebp
8010355a:	83 ec 18             	sub    $0x18,%esp
  consoleintr(kbdgetc);
8010355d:	c7 04 24 09 34 10 80 	movl   $0x80103409,(%esp)
80103564:	e8 94 d5 ff ff       	call   80100afd <consoleintr>
}
80103569:	c9                   	leave  
8010356a:	c3                   	ret    
	...

8010356c <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
8010356c:	55                   	push   %ebp
8010356d:	89 e5                	mov    %esp,%ebp
8010356f:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103572:	9c                   	pushf  
80103573:	58                   	pop    %eax
80103574:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80103577:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010357a:	c9                   	leave  
8010357b:	c3                   	ret    

8010357c <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
8010357c:	55                   	push   %ebp
8010357d:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
8010357f:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103584:	8b 55 08             	mov    0x8(%ebp),%edx
80103587:	c1 e2 02             	shl    $0x2,%edx
8010358a:	8d 14 10             	lea    (%eax,%edx,1),%edx
8010358d:	8b 45 0c             	mov    0xc(%ebp),%eax
80103590:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80103592:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103597:	83 c0 20             	add    $0x20,%eax
8010359a:	8b 00                	mov    (%eax),%eax
}
8010359c:	5d                   	pop    %ebp
8010359d:	c3                   	ret    

8010359e <lapicinit>:
//PAGEBREAK!

void
lapicinit(void)
{
8010359e:	55                   	push   %ebp
8010359f:	89 e5                	mov    %esp,%ebp
801035a1:	83 ec 08             	sub    $0x8,%esp
  if(!lapic)
801035a4:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
801035a9:	85 c0                	test   %eax,%eax
801035ab:	0f 84 46 01 00 00    	je     801036f7 <lapicinit+0x159>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
801035b1:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
801035b8:	00 
801035b9:	c7 04 24 3c 00 00 00 	movl   $0x3c,(%esp)
801035c0:	e8 b7 ff ff ff       	call   8010357c <lapicw>

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
801035c5:	c7 44 24 04 0b 00 00 	movl   $0xb,0x4(%esp)
801035cc:	00 
801035cd:	c7 04 24 f8 00 00 00 	movl   $0xf8,(%esp)
801035d4:	e8 a3 ff ff ff       	call   8010357c <lapicw>
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
801035d9:	c7 44 24 04 20 00 02 	movl   $0x20020,0x4(%esp)
801035e0:	00 
801035e1:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801035e8:	e8 8f ff ff ff       	call   8010357c <lapicw>
  lapicw(TICR, 10000000);
801035ed:	c7 44 24 04 80 96 98 	movl   $0x989680,0x4(%esp)
801035f4:	00 
801035f5:	c7 04 24 e0 00 00 00 	movl   $0xe0,(%esp)
801035fc:	e8 7b ff ff ff       	call   8010357c <lapicw>

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80103601:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103608:	00 
80103609:	c7 04 24 d4 00 00 00 	movl   $0xd4,(%esp)
80103610:	e8 67 ff ff ff       	call   8010357c <lapicw>
  lapicw(LINT1, MASKED);
80103615:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
8010361c:	00 
8010361d:	c7 04 24 d8 00 00 00 	movl   $0xd8,(%esp)
80103624:	e8 53 ff ff ff       	call   8010357c <lapicw>

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80103629:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
8010362e:	83 c0 30             	add    $0x30,%eax
80103631:	8b 00                	mov    (%eax),%eax
80103633:	c1 e8 10             	shr    $0x10,%eax
80103636:	25 ff 00 00 00       	and    $0xff,%eax
8010363b:	83 f8 03             	cmp    $0x3,%eax
8010363e:	76 14                	jbe    80103654 <lapicinit+0xb6>
    lapicw(PCINT, MASKED);
80103640:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103647:	00 
80103648:	c7 04 24 d0 00 00 00 	movl   $0xd0,(%esp)
8010364f:	e8 28 ff ff ff       	call   8010357c <lapicw>

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80103654:	c7 44 24 04 33 00 00 	movl   $0x33,0x4(%esp)
8010365b:	00 
8010365c:	c7 04 24 dc 00 00 00 	movl   $0xdc,(%esp)
80103663:	e8 14 ff ff ff       	call   8010357c <lapicw>

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80103668:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010366f:	00 
80103670:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103677:	e8 00 ff ff ff       	call   8010357c <lapicw>
  lapicw(ESR, 0);
8010367c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103683:	00 
80103684:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
8010368b:	e8 ec fe ff ff       	call   8010357c <lapicw>

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80103690:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80103697:	00 
80103698:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
8010369f:	e8 d8 fe ff ff       	call   8010357c <lapicw>

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
801036a4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036ab:	00 
801036ac:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801036b3:	e8 c4 fe ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, BCAST | INIT | LEVEL);
801036b8:	c7 44 24 04 00 85 08 	movl   $0x88500,0x4(%esp)
801036bf:	00 
801036c0:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801036c7:	e8 b0 fe ff ff       	call   8010357c <lapicw>
  while(lapic[ICRLO] & DELIVS)
801036cc:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
801036d1:	05 00 03 00 00       	add    $0x300,%eax
801036d6:	8b 00                	mov    (%eax),%eax
801036d8:	25 00 10 00 00       	and    $0x1000,%eax
801036dd:	85 c0                	test   %eax,%eax
801036df:	75 eb                	jne    801036cc <lapicinit+0x12e>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
801036e1:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801036e8:	00 
801036e9:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801036f0:	e8 87 fe ff ff       	call   8010357c <lapicw>
801036f5:	eb 01                	jmp    801036f8 <lapicinit+0x15a>

void
lapicinit(void)
{
  if(!lapic)
    return;
801036f7:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
801036f8:	c9                   	leave  
801036f9:	c3                   	ret    

801036fa <cpunum>:

int
cpunum(void)
{
801036fa:	55                   	push   %ebp
801036fb:	89 e5                	mov    %esp,%ebp
801036fd:	83 ec 18             	sub    $0x18,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80103700:	e8 67 fe ff ff       	call   8010356c <readeflags>
80103705:	25 00 02 00 00       	and    $0x200,%eax
8010370a:	85 c0                	test   %eax,%eax
8010370c:	74 29                	je     80103737 <cpunum+0x3d>
    static int n;
    if(n++ == 0)
8010370e:	a1 84 c6 10 80       	mov    0x8010c684,%eax
80103713:	85 c0                	test   %eax,%eax
80103715:	0f 94 c2             	sete   %dl
80103718:	83 c0 01             	add    $0x1,%eax
8010371b:	a3 84 c6 10 80       	mov    %eax,0x8010c684
80103720:	84 d2                	test   %dl,%dl
80103722:	74 13                	je     80103737 <cpunum+0x3d>
      cprintf("cpu called from %x with interrupts enabled\n",
80103724:	8b 45 04             	mov    0x4(%ebp),%eax
80103727:	89 44 24 04          	mov    %eax,0x4(%esp)
8010372b:	c7 04 24 f4 92 10 80 	movl   $0x801092f4,(%esp)
80103732:	e8 92 cf ff ff       	call   801006c9 <cprintf>
        __builtin_return_address(0));
  }

  if(lapic)
80103737:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
8010373c:	85 c0                	test   %eax,%eax
8010373e:	74 0f                	je     8010374f <cpunum+0x55>
    return lapic[ID]>>24;
80103740:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103745:	83 c0 20             	add    $0x20,%eax
80103748:	8b 00                	mov    (%eax),%eax
8010374a:	c1 e8 18             	shr    $0x18,%eax
8010374d:	eb 05                	jmp    80103754 <cpunum+0x5a>
  return 0;
8010374f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80103754:	c9                   	leave  
80103755:	c3                   	ret    

80103756 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
80103756:	55                   	push   %ebp
80103757:	89 e5                	mov    %esp,%ebp
80103759:	83 ec 08             	sub    $0x8,%esp
  if(lapic)
8010375c:	a1 dc 3a 11 80       	mov    0x80113adc,%eax
80103761:	85 c0                	test   %eax,%eax
80103763:	74 14                	je     80103779 <lapiceoi+0x23>
    lapicw(EOI, 0);
80103765:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010376c:	00 
8010376d:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
80103774:	e8 03 fe ff ff       	call   8010357c <lapicw>
}
80103779:	c9                   	leave  
8010377a:	c3                   	ret    

8010377b <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
8010377b:	55                   	push   %ebp
8010377c:	89 e5                	mov    %esp,%ebp
}
8010377e:	5d                   	pop    %ebp
8010377f:	c3                   	ret    

80103780 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80103780:	55                   	push   %ebp
80103781:	89 e5                	mov    %esp,%ebp
80103783:	83 ec 38             	sub    $0x38,%esp
80103786:	8b 45 08             	mov    0x8(%ebp),%eax
80103789:	88 45 e4             	mov    %al,-0x1c(%ebp)
  ushort *wrv;

  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  cmoswrite(CMOS_SHUTDOWN_STAT, CMOS_JMP_DWORD_NO_EOI);
8010378c:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80103793:	00 
80103794:	c7 04 24 0f 00 00 00 	movl   $0xf,(%esp)
8010379b:	e8 9a cb ff ff       	call   8010033a <cmoswrite>
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
801037a0:	c7 45 f4 67 04 00 80 	movl   $0x80000467,-0xc(%ebp)
  wrv[0] = 0;
801037a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037aa:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
801037af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037b2:	8d 50 02             	lea    0x2(%eax),%edx
801037b5:	8b 45 0c             	mov    0xc(%ebp),%eax
801037b8:	c1 e8 04             	shr    $0x4,%eax
801037bb:	66 89 02             	mov    %ax,(%edx)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
801037be:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
801037c2:	c1 e0 18             	shl    $0x18,%eax
801037c5:	89 44 24 04          	mov    %eax,0x4(%esp)
801037c9:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
801037d0:	e8 a7 fd ff ff       	call   8010357c <lapicw>
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
801037d5:	c7 44 24 04 00 c5 00 	movl   $0xc500,0x4(%esp)
801037dc:	00 
801037dd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
801037e4:	e8 93 fd ff ff       	call   8010357c <lapicw>
  microdelay(200);
801037e9:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
801037f0:	e8 86 ff ff ff       	call   8010377b <microdelay>
  lapicw(ICRLO, INIT | LEVEL);
801037f5:	c7 44 24 04 00 85 00 	movl   $0x8500,0x4(%esp)
801037fc:	00 
801037fd:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103804:	e8 73 fd ff ff       	call   8010357c <lapicw>
  microdelay(100);    // should be 10ms, but too slow in Bochs!
80103809:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103810:	e8 66 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103815:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010381c:	eb 40                	jmp    8010385e <lapicstartap+0xde>
    lapicw(ICRHI, apicid<<24);
8010381e:	0f b6 45 e4          	movzbl -0x1c(%ebp),%eax
80103822:	c1 e0 18             	shl    $0x18,%eax
80103825:	89 44 24 04          	mov    %eax,0x4(%esp)
80103829:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80103830:	e8 47 fd ff ff       	call   8010357c <lapicw>
    lapicw(ICRLO, STARTUP | (addr>>12));
80103835:	8b 45 0c             	mov    0xc(%ebp),%eax
80103838:	c1 e8 0c             	shr    $0xc,%eax
8010383b:	80 cc 06             	or     $0x6,%ah
8010383e:	89 44 24 04          	mov    %eax,0x4(%esp)
80103842:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103849:	e8 2e fd ff ff       	call   8010357c <lapicw>
    microdelay(200);
8010384e:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80103855:	e8 21 ff ff ff       	call   8010377b <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
8010385a:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
8010385e:	83 7d f0 01          	cmpl   $0x1,-0x10(%ebp)
80103862:	7e ba                	jle    8010381e <lapicstartap+0x9e>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
80103864:	c9                   	leave  
80103865:	c3                   	ret    
	...

80103868 <initlog>:
static void recover_from_log(void);
static void commit();

void
initlog(int dev)
{
80103868:	55                   	push   %ebp
80103869:	89 e5                	mov    %esp,%ebp
8010386b:	83 ec 38             	sub    $0x38,%esp
  if (sizeof(struct logheader) >= BSIZE)
8010386e:	90                   	nop
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
8010386f:	c7 44 24 04 20 93 10 	movl   $0x80109320,0x4(%esp)
80103876:	80 
80103877:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
8010387e:	e8 5b 20 00 00       	call   801058de <initlock>
  readsb(dev, &sb);
80103883:	8d 45 dc             	lea    -0x24(%ebp),%eax
80103886:	89 44 24 04          	mov    %eax,0x4(%esp)
8010388a:	8b 45 08             	mov    0x8(%ebp),%eax
8010388d:	89 04 24             	mov    %eax,(%esp)
80103890:	e8 f3 e1 ff ff       	call   80101a88 <readsb>
  log.start = sb.logstart;
80103895:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103898:	a3 14 3b 11 80       	mov    %eax,0x80113b14
  log.size = sb.nlog;
8010389d:	8b 45 e8             	mov    -0x18(%ebp),%eax
801038a0:	a3 18 3b 11 80       	mov    %eax,0x80113b18
  log.dev = dev;
801038a5:	8b 45 08             	mov    0x8(%ebp),%eax
801038a8:	a3 24 3b 11 80       	mov    %eax,0x80113b24
  recover_from_log();
801038ad:	e8 97 01 00 00       	call   80103a49 <recover_from_log>
}
801038b2:	c9                   	leave  
801038b3:	c3                   	ret    

801038b4 <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
801038b4:	55                   	push   %ebp
801038b5:	89 e5                	mov    %esp,%ebp
801038b7:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801038ba:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
801038c1:	e9 89 00 00 00       	jmp    8010394f <install_trans+0x9b>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
801038c6:	a1 14 3b 11 80       	mov    0x80113b14,%eax
801038cb:	03 45 ec             	add    -0x14(%ebp),%eax
801038ce:	83 c0 01             	add    $0x1,%eax
801038d1:	89 c2                	mov    %eax,%edx
801038d3:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801038d8:	89 54 24 04          	mov    %edx,0x4(%esp)
801038dc:	89 04 24             	mov    %eax,(%esp)
801038df:	e8 c3 c8 ff ff       	call   801001a7 <bread>
801038e4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
801038e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
801038ea:	83 c0 10             	add    $0x10,%eax
801038ed:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
801038f4:	89 c2                	mov    %eax,%edx
801038f6:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801038fb:	89 54 24 04          	mov    %edx,0x4(%esp)
801038ff:	89 04 24             	mov    %eax,(%esp)
80103902:	e8 a0 c8 ff ff       	call   801001a7 <bread>
80103907:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
8010390a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010390d:	8d 50 18             	lea    0x18(%eax),%edx
80103910:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103913:	83 c0 18             	add    $0x18,%eax
80103916:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
8010391d:	00 
8010391e:	89 54 24 04          	mov    %edx,0x4(%esp)
80103922:	89 04 24             	mov    %eax,(%esp)
80103925:	e8 0b 23 00 00       	call   80105c35 <memmove>
    bwrite(dbuf);  // write dst to disk
8010392a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010392d:	89 04 24             	mov    %eax,(%esp)
80103930:	e8 a9 c8 ff ff       	call   801001de <bwrite>
    brelse(lbuf); 
80103935:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103938:	89 04 24             	mov    %eax,(%esp)
8010393b:	e8 d8 c8 ff ff       	call   80100218 <brelse>
    brelse(dbuf);
80103940:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103943:	89 04 24             	mov    %eax,(%esp)
80103946:	e8 cd c8 ff ff       	call   80100218 <brelse>
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
8010394b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
8010394f:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103954:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103957:	0f 8f 69 ff ff ff    	jg     801038c6 <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
8010395d:	c9                   	leave  
8010395e:	c3                   	ret    

8010395f <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
8010395f:	55                   	push   %ebp
80103960:	89 e5                	mov    %esp,%ebp
80103962:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
80103965:	a1 14 3b 11 80       	mov    0x80113b14,%eax
8010396a:	89 c2                	mov    %eax,%edx
8010396c:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103971:	89 54 24 04          	mov    %edx,0x4(%esp)
80103975:	89 04 24             	mov    %eax,(%esp)
80103978:	e8 2a c8 ff ff       	call   801001a7 <bread>
8010397d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
80103980:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103983:	83 c0 18             	add    $0x18,%eax
80103986:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  log.lh.n = lh->n;
80103989:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010398c:	8b 00                	mov    (%eax),%eax
8010398e:	a3 28 3b 11 80       	mov    %eax,0x80113b28
  for (i = 0; i < log.lh.n; i++) {
80103993:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010399a:	eb 1b                	jmp    801039b7 <read_head+0x58>
    log.lh.block[i] = lh->block[i];
8010399c:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010399f:	8b 55 f4             	mov    -0xc(%ebp),%edx
801039a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801039a5:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
801039a9:	8d 51 10             	lea    0x10(%ecx),%edx
801039ac:	89 04 95 ec 3a 11 80 	mov    %eax,-0x7feec514(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
801039b3:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801039b7:	a1 28 3b 11 80       	mov    0x80113b28,%eax
801039bc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801039bf:	7f db                	jg     8010399c <read_head+0x3d>
    log.lh.block[i] = lh->block[i];
  }
  brelse(buf);
801039c1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039c4:	89 04 24             	mov    %eax,(%esp)
801039c7:	e8 4c c8 ff ff       	call   80100218 <brelse>
}
801039cc:	c9                   	leave  
801039cd:	c3                   	ret    

801039ce <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
801039ce:	55                   	push   %ebp
801039cf:	89 e5                	mov    %esp,%ebp
801039d1:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
801039d4:	a1 14 3b 11 80       	mov    0x80113b14,%eax
801039d9:	89 c2                	mov    %eax,%edx
801039db:	a1 24 3b 11 80       	mov    0x80113b24,%eax
801039e0:	89 54 24 04          	mov    %edx,0x4(%esp)
801039e4:	89 04 24             	mov    %eax,(%esp)
801039e7:	e8 bb c7 ff ff       	call   801001a7 <bread>
801039ec:	89 45 ec             	mov    %eax,-0x14(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
801039ef:	8b 45 ec             	mov    -0x14(%ebp),%eax
801039f2:	83 c0 18             	add    $0x18,%eax
801039f5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  int i;
  hb->n = log.lh.n;
801039f8:	8b 15 28 3b 11 80    	mov    0x80113b28,%edx
801039fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a01:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
80103a03:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103a0a:	eb 1b                	jmp    80103a27 <write_head+0x59>
    hb->block[i] = log.lh.block[i];
80103a0c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103a0f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a12:	83 c0 10             	add    $0x10,%eax
80103a15:	8b 0c 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%ecx
80103a1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a1f:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80103a23:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103a27:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103a2c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103a2f:	7f db                	jg     80103a0c <write_head+0x3e>
    hb->block[i] = log.lh.block[i];
  }
  bwrite(buf);
80103a31:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a34:	89 04 24             	mov    %eax,(%esp)
80103a37:	e8 a2 c7 ff ff       	call   801001de <bwrite>
  brelse(buf);
80103a3c:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103a3f:	89 04 24             	mov    %eax,(%esp)
80103a42:	e8 d1 c7 ff ff       	call   80100218 <brelse>
}
80103a47:	c9                   	leave  
80103a48:	c3                   	ret    

80103a49 <recover_from_log>:

static void
recover_from_log(void)
{
80103a49:	55                   	push   %ebp
80103a4a:	89 e5                	mov    %esp,%ebp
80103a4c:	83 ec 08             	sub    $0x8,%esp
  read_head();      
80103a4f:	e8 0b ff ff ff       	call   8010395f <read_head>
  install_trans(); // if committed, copy from log to disk
80103a54:	e8 5b fe ff ff       	call   801038b4 <install_trans>
  log.lh.n = 0;
80103a59:	c7 05 28 3b 11 80 00 	movl   $0x0,0x80113b28
80103a60:	00 00 00 
  write_head(); // clear the log
80103a63:	e8 66 ff ff ff       	call   801039ce <write_head>
}
80103a68:	c9                   	leave  
80103a69:	c3                   	ret    

80103a6a <begin_op>:

// called at the start of each FS system call.
void
begin_op(void)
{
80103a6a:	55                   	push   %ebp
80103a6b:	89 e5                	mov    %esp,%ebp
80103a6d:	83 ec 18             	sub    $0x18,%esp
  acquire(&log.lock);
80103a70:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103a77:	e8 83 1e 00 00       	call   801058ff <acquire>
  while(1){
    if(log.committing){
80103a7c:	a1 20 3b 11 80       	mov    0x80113b20,%eax
80103a81:	85 c0                	test   %eax,%eax
80103a83:	74 16                	je     80103a9b <begin_op+0x31>
      sleep(&log, &log.lock);
80103a85:	c7 44 24 04 e0 3a 11 	movl   $0x80113ae0,0x4(%esp)
80103a8c:	80 
80103a8d:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103a94:	e8 84 1b 00 00       	call   8010561d <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103a99:	eb e1                	jmp    80103a7c <begin_op+0x12>
{
  acquire(&log.lock);
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80103a9b:	8b 0d 28 3b 11 80    	mov    0x80113b28,%ecx
80103aa1:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103aa6:	8d 50 01             	lea    0x1(%eax),%edx
80103aa9:	89 d0                	mov    %edx,%eax
80103aab:	c1 e0 02             	shl    $0x2,%eax
80103aae:	01 d0                	add    %edx,%eax
80103ab0:	01 c0                	add    %eax,%eax
80103ab2:	8d 04 01             	lea    (%ecx,%eax,1),%eax
80103ab5:	83 f8 1e             	cmp    $0x1e,%eax
80103ab8:	7e 16                	jle    80103ad0 <begin_op+0x66>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
80103aba:	c7 44 24 04 e0 3a 11 	movl   $0x80113ae0,0x4(%esp)
80103ac1:	80 
80103ac2:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103ac9:	e8 4f 1b 00 00       	call   8010561d <sleep>
    } else {
      log.outstanding += 1;
      release(&log.lock);
      break;
    }
  }
80103ace:	eb ac                	jmp    80103a7c <begin_op+0x12>
      sleep(&log, &log.lock);
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
80103ad0:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103ad5:	83 c0 01             	add    $0x1,%eax
80103ad8:	a3 1c 3b 11 80       	mov    %eax,0x80113b1c
      release(&log.lock);
80103add:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103ae4:	e8 7f 1e 00 00       	call   80105968 <release>
      break;
80103ae9:	90                   	nop
    }
  }
}
80103aea:	c9                   	leave  
80103aeb:	c3                   	ret    

80103aec <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103aec:	55                   	push   %ebp
80103aed:	89 e5                	mov    %esp,%ebp
80103aef:	83 ec 28             	sub    $0x28,%esp
  int do_commit = 0;
80103af2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&log.lock);
80103af9:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b00:	e8 fa 1d 00 00       	call   801058ff <acquire>
  log.outstanding -= 1;
80103b05:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103b0a:	83 e8 01             	sub    $0x1,%eax
80103b0d:	a3 1c 3b 11 80       	mov    %eax,0x80113b1c
  if(log.committing)
80103b12:	a1 20 3b 11 80       	mov    0x80113b20,%eax
80103b17:	85 c0                	test   %eax,%eax
80103b19:	74 0c                	je     80103b27 <end_op+0x3b>
    panic("log.committing");
80103b1b:	c7 04 24 24 93 10 80 	movl   $0x80109324,(%esp)
80103b22:	e8 42 cd ff ff       	call   80100869 <panic>
  if(log.outstanding == 0){
80103b27:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103b2c:	85 c0                	test   %eax,%eax
80103b2e:	75 13                	jne    80103b43 <end_op+0x57>
    do_commit = 1;
80103b30:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    log.committing = 1;
80103b37:	c7 05 20 3b 11 80 01 	movl   $0x1,0x80113b20
80103b3e:	00 00 00 
80103b41:	eb 0c                	jmp    80103b4f <end_op+0x63>
  } else {
    // begin_op() may be waiting for log space.
    wakeup(&log);
80103b43:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b4a:	e8 ab 1b 00 00       	call   801056fa <wakeup>
  }
  release(&log.lock);
80103b4f:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b56:	e8 0d 1e 00 00       	call   80105968 <release>

  if(do_commit){
80103b5b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103b5f:	74 33                	je     80103b94 <end_op+0xa8>
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
80103b61:	e8 db 00 00 00       	call   80103c41 <commit>
    acquire(&log.lock);
80103b66:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b6d:	e8 8d 1d 00 00       	call   801058ff <acquire>
    log.committing = 0;
80103b72:	c7 05 20 3b 11 80 00 	movl   $0x0,0x80113b20
80103b79:	00 00 00 
    wakeup(&log);
80103b7c:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b83:	e8 72 1b 00 00       	call   801056fa <wakeup>
    release(&log.lock);
80103b88:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103b8f:	e8 d4 1d 00 00       	call   80105968 <release>
  }
}
80103b94:	c9                   	leave  
80103b95:	c3                   	ret    

80103b96 <write_log>:

// Copy modified blocks from cache to log.
static void 
write_log(void)
{
80103b96:	55                   	push   %ebp
80103b97:	89 e5                	mov    %esp,%ebp
80103b99:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103b9c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80103ba3:	e9 89 00 00 00       	jmp    80103c31 <write_log+0x9b>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80103ba8:	a1 14 3b 11 80       	mov    0x80113b14,%eax
80103bad:	03 45 ec             	add    -0x14(%ebp),%eax
80103bb0:	83 c0 01             	add    $0x1,%eax
80103bb3:	89 c2                	mov    %eax,%edx
80103bb5:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103bba:	89 54 24 04          	mov    %edx,0x4(%esp)
80103bbe:	89 04 24             	mov    %eax,(%esp)
80103bc1:	e8 e1 c5 ff ff       	call   801001a7 <bread>
80103bc6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80103bc9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103bcc:	83 c0 10             	add    $0x10,%eax
80103bcf:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
80103bd6:	89 c2                	mov    %eax,%edx
80103bd8:	a1 24 3b 11 80       	mov    0x80113b24,%eax
80103bdd:	89 54 24 04          	mov    %edx,0x4(%esp)
80103be1:	89 04 24             	mov    %eax,(%esp)
80103be4:	e8 be c5 ff ff       	call   801001a7 <bread>
80103be9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    memmove(to->data, from->data, BSIZE);
80103bec:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103bef:	8d 50 18             	lea    0x18(%eax),%edx
80103bf2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103bf5:	83 c0 18             	add    $0x18,%eax
80103bf8:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80103bff:	00 
80103c00:	89 54 24 04          	mov    %edx,0x4(%esp)
80103c04:	89 04 24             	mov    %eax,(%esp)
80103c07:	e8 29 20 00 00       	call   80105c35 <memmove>
    bwrite(to);  // write the log
80103c0c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c0f:	89 04 24             	mov    %eax,(%esp)
80103c12:	e8 c7 c5 ff ff       	call   801001de <bwrite>
    brelse(from); 
80103c17:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c1a:	89 04 24             	mov    %eax,(%esp)
80103c1d:	e8 f6 c5 ff ff       	call   80100218 <brelse>
    brelse(to);
80103c22:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103c25:	89 04 24             	mov    %eax,(%esp)
80103c28:	e8 eb c5 ff ff       	call   80100218 <brelse>
static void 
write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103c2d:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80103c31:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c36:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103c39:	0f 8f 69 ff ff ff    	jg     80103ba8 <write_log+0x12>
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from); 
    brelse(to);
  }
}
80103c3f:	c9                   	leave  
80103c40:	c3                   	ret    

80103c41 <commit>:

static void
commit()
{
80103c41:	55                   	push   %ebp
80103c42:	89 e5                	mov    %esp,%ebp
80103c44:	83 ec 08             	sub    $0x8,%esp
  if (log.lh.n > 0) {
80103c47:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c4c:	85 c0                	test   %eax,%eax
80103c4e:	7e 1e                	jle    80103c6e <commit+0x2d>
    write_log();     // Write modified blocks from cache to log
80103c50:	e8 41 ff ff ff       	call   80103b96 <write_log>
    write_head();    // Write header to disk -- the real commit
80103c55:	e8 74 fd ff ff       	call   801039ce <write_head>
    install_trans(); // Now install writes to home locations
80103c5a:	e8 55 fc ff ff       	call   801038b4 <install_trans>
    log.lh.n = 0; 
80103c5f:	c7 05 28 3b 11 80 00 	movl   $0x0,0x80113b28
80103c66:	00 00 00 
    write_head();    // Erase the transaction from the log
80103c69:	e8 60 fd ff ff       	call   801039ce <write_head>
  }
}
80103c6e:	c9                   	leave  
80103c6f:	c3                   	ret    

80103c70 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103c70:	55                   	push   %ebp
80103c71:	89 e5                	mov    %esp,%ebp
80103c73:	83 ec 28             	sub    $0x28,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103c76:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c7b:	83 f8 1d             	cmp    $0x1d,%eax
80103c7e:	7f 12                	jg     80103c92 <log_write+0x22>
80103c80:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103c85:	8b 15 18 3b 11 80    	mov    0x80113b18,%edx
80103c8b:	83 ea 01             	sub    $0x1,%edx
80103c8e:	39 d0                	cmp    %edx,%eax
80103c90:	7c 0c                	jl     80103c9e <log_write+0x2e>
    panic("too big a transaction");
80103c92:	c7 04 24 33 93 10 80 	movl   $0x80109333,(%esp)
80103c99:	e8 cb cb ff ff       	call   80100869 <panic>
  if (log.outstanding < 1)
80103c9e:	a1 1c 3b 11 80       	mov    0x80113b1c,%eax
80103ca3:	85 c0                	test   %eax,%eax
80103ca5:	7f 0c                	jg     80103cb3 <log_write+0x43>
    panic("log_write outside of trans");
80103ca7:	c7 04 24 49 93 10 80 	movl   $0x80109349,(%esp)
80103cae:	e8 b6 cb ff ff       	call   80100869 <panic>

  acquire(&log.lock);
80103cb3:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103cba:	e8 40 1c 00 00       	call   801058ff <acquire>
  for (i = 0; i < log.lh.n; i++) {
80103cbf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103cc6:	eb 1d                	jmp    80103ce5 <log_write+0x75>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103cc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ccb:	83 c0 10             	add    $0x10,%eax
80103cce:	8b 04 85 ec 3a 11 80 	mov    -0x7feec514(,%eax,4),%eax
80103cd5:	89 c2                	mov    %eax,%edx
80103cd7:	8b 45 08             	mov    0x8(%ebp),%eax
80103cda:	8b 40 08             	mov    0x8(%eax),%eax
80103cdd:	39 c2                	cmp    %eax,%edx
80103cdf:	74 10                	je     80103cf1 <log_write+0x81>
    panic("too big a transaction");
  if (log.outstanding < 1)
    panic("log_write outside of trans");

  acquire(&log.lock);
  for (i = 0; i < log.lh.n; i++) {
80103ce1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103ce5:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103cea:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103ced:	7f d9                	jg     80103cc8 <log_write+0x58>
80103cef:	eb 01                	jmp    80103cf2 <log_write+0x82>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
      break;
80103cf1:	90                   	nop
  }
  log.lh.block[i] = b->blockno;
80103cf2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103cf5:	8b 45 08             	mov    0x8(%ebp),%eax
80103cf8:	8b 40 08             	mov    0x8(%eax),%eax
80103cfb:	83 c2 10             	add    $0x10,%edx
80103cfe:	89 04 95 ec 3a 11 80 	mov    %eax,-0x7feec514(,%edx,4)
  if (i == log.lh.n)
80103d05:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103d0a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103d0d:	75 0d                	jne    80103d1c <log_write+0xac>
    log.lh.n++;
80103d0f:	a1 28 3b 11 80       	mov    0x80113b28,%eax
80103d14:	83 c0 01             	add    $0x1,%eax
80103d17:	a3 28 3b 11 80       	mov    %eax,0x80113b28
  b->flags |= B_DIRTY; // prevent eviction
80103d1c:	8b 45 08             	mov    0x8(%ebp),%eax
80103d1f:	8b 00                	mov    (%eax),%eax
80103d21:	89 c2                	mov    %eax,%edx
80103d23:	83 ca 04             	or     $0x4,%edx
80103d26:	8b 45 08             	mov    0x8(%ebp),%eax
80103d29:	89 10                	mov    %edx,(%eax)
  release(&log.lock);
80103d2b:	c7 04 24 e0 3a 11 80 	movl   $0x80113ae0,(%esp)
80103d32:	e8 31 1c 00 00       	call   80105968 <release>
}
80103d37:	c9                   	leave  
80103d38:	c3                   	ret    
80103d39:	00 00                	add    %al,(%eax)
	...

80103d3c <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103d3c:	55                   	push   %ebp
80103d3d:	89 e5                	mov    %esp,%ebp
80103d3f:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103d42:	8b 55 08             	mov    0x8(%ebp),%edx
80103d45:	8b 45 0c             	mov    0xc(%ebp),%eax
80103d48:	8b 4d 08             	mov    0x8(%ebp),%ecx
80103d4b:	f0 87 02             	lock xchg %eax,(%edx)
80103d4e:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80103d51:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103d54:	c9                   	leave  
80103d55:	c3                   	ret    

80103d56 <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
80103d56:	55                   	push   %ebp
80103d57:	89 e5                	mov    %esp,%ebp
80103d59:	83 e4 f0             	and    $0xfffffff0,%esp
80103d5c:	83 ec 10             	sub    $0x10,%esp
  debuginit();     // enable debug output first
80103d5f:	e8 1f d2 ff ff       	call   80100f83 <debuginit>
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80103d64:	c7 44 24 04 00 00 40 	movl   $0x80400000,0x4(%esp)
80103d6b:	80 
80103d6c:	c7 04 24 78 64 11 80 	movl   $0x80116478,(%esp)
80103d73:	e8 f8 f4 ff ff       	call   80103270 <kinit1>
  kvmalloc();      // kernel page table
80103d78:	e8 99 4a 00 00       	call   80108816 <kvmalloc>
  mpinit();        // collect info about this machine
80103d7d:	e8 0f 04 00 00       	call   80104191 <mpinit>
  lapicinit();
80103d82:	e8 17 f8 ff ff       	call   8010359e <lapicinit>
  seginit();       // set up segments
80103d87:	e8 53 44 00 00       	call   801081df <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
80103d8c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103d92:	0f b6 00             	movzbl (%eax),%eax
80103d95:	0f b6 c0             	movzbl %al,%eax
80103d98:	89 44 24 04          	mov    %eax,0x4(%esp)
80103d9c:	c7 04 24 64 93 10 80 	movl   $0x80109364,(%esp)
80103da3:	e8 21 c9 ff ff       	call   801006c9 <cprintf>
  picinit();       // interrupt controller
80103da8:	e8 25 07 00 00       	call   801044d2 <picinit>
  ioapicinit();    // another interrupt controller
80103dad:	e8 77 f3 ff ff       	call   80103129 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
80103db2:	e8 37 d0 ff ff       	call   80100dee <consoleinit>
  uartinit();      // serial port
80103db7:	e8 6b 37 00 00       	call   80107527 <uartinit>
  pinit();         // process table
80103dbc:	e8 21 0c 00 00       	call   801049e2 <pinit>
  tvinit();        // trap vectors
80103dc1:	e8 1e 32 00 00       	call   80106fe4 <tvinit>
  binit();         // buffer cache
80103dc6:	e8 69 c2 ff ff       	call   80100034 <binit>
  fileinit();      // file table
80103dcb:	e8 cc d8 ff ff       	call   8010169c <fileinit>
  ideinit();       // disk
80103dd0:	e8 76 ef ff ff       	call   80102d4b <ideinit>
  if(!ismp)
80103dd5:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
80103dda:	85 c0                	test   %eax,%eax
80103ddc:	75 05                	jne    80103de3 <main+0x8d>
    timerinit();   // uniprocessor timer
80103dde:	e8 43 31 00 00       	call   80106f26 <timerinit>
  startothers();   // start other processors
80103de3:	e8 7f 00 00 00       	call   80103e67 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103de8:	c7 44 24 04 00 00 00 	movl   $0x8e000000,0x4(%esp)
80103def:	8e 
80103df0:	c7 04 24 00 00 40 80 	movl   $0x80400000,(%esp)
80103df7:	e8 ac f4 ff ff       	call   801032a8 <kinit2>
  userinit();      // first user process
80103dfc:	e8 00 0d 00 00       	call   80104b01 <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
80103e01:	e8 1a 00 00 00       	call   80103e20 <mpmain>

80103e06 <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
80103e06:	55                   	push   %ebp
80103e07:	89 e5                	mov    %esp,%ebp
80103e09:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103e0c:	e8 31 4a 00 00       	call   80108842 <switchkvm>
  seginit();
80103e11:	e8 c9 43 00 00       	call   801081df <seginit>
  lapicinit();
80103e16:	e8 83 f7 ff ff       	call   8010359e <lapicinit>
  mpmain();
80103e1b:	e8 00 00 00 00       	call   80103e20 <mpmain>

80103e20 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103e20:	55                   	push   %ebp
80103e21:	89 e5                	mov    %esp,%ebp
80103e23:	83 ec 18             	sub    $0x18,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103e26:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e2c:	0f b6 00             	movzbl (%eax),%eax
80103e2f:	0f b6 c0             	movzbl %al,%eax
80103e32:	89 44 24 04          	mov    %eax,0x4(%esp)
80103e36:	c7 04 24 7b 93 10 80 	movl   $0x8010937b,(%esp)
80103e3d:	e8 87 c8 ff ff       	call   801006c9 <cprintf>
  idtinit();       // load idt register
80103e42:	e8 17 33 00 00       	call   8010715e <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103e47:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103e4d:	05 a8 00 00 00       	add    $0xa8,%eax
80103e52:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80103e59:	00 
80103e5a:	89 04 24             	mov    %eax,(%esp)
80103e5d:	e8 da fe ff ff       	call   80103d3c <xchg>
  scheduler();     // start running processes
80103e62:	e8 f0 12 00 00       	call   80105157 <scheduler>

80103e67 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
80103e67:	55                   	push   %ebp
80103e68:	89 e5                	mov    %esp,%ebp
80103e6a:	83 ec 28             	sub    $0x28,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
80103e6d:	c7 45 ec 00 70 00 80 	movl   $0x80007000,-0x14(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103e74:	b8 86 00 00 00       	mov    $0x86,%eax
80103e79:	89 44 24 08          	mov    %eax,0x8(%esp)
80103e7d:	c7 44 24 04 4c c5 10 	movl   $0x8010c54c,0x4(%esp)
80103e84:	80 
80103e85:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103e88:	89 04 24             	mov    %eax,(%esp)
80103e8b:	e8 a5 1d 00 00       	call   80105c35 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
80103e90:	c7 45 f0 e0 3b 11 80 	movl   $0x80113be0,-0x10(%ebp)
80103e97:	e9 93 00 00 00       	jmp    80103f2f <startothers+0xc8>
    if(c == cpus+cpunum())  // We've started already.
80103e9c:	e8 59 f8 ff ff       	call   801036fa <cpunum>
80103ea1:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103ea7:	05 e0 3b 11 80       	add    $0x80113be0,%eax
80103eac:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103eaf:	74 76                	je     80103f27 <startothers+0xc0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    if((stack = kalloc()) == 0)
80103eb1:	e8 e8 f4 ff ff       	call   8010339e <kalloc>
80103eb6:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103eb9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103ebd:	75 0c                	jne    80103ecb <startothers+0x64>
      panic("startothers: failed to allocate stack");
80103ebf:	c7 04 24 8c 93 10 80 	movl   $0x8010938c,(%esp)
80103ec6:	e8 9e c9 ff ff       	call   80100869 <panic>
    *(void**)(code-4) = stack + KSTACKSIZE;
80103ecb:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103ece:	83 e8 04             	sub    $0x4,%eax
80103ed1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103ed4:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103eda:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103edc:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103edf:	83 e8 08             	sub    $0x8,%eax
80103ee2:	c7 00 06 3e 10 80    	movl   $0x80103e06,(%eax)
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80103ee8:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103eeb:	8d 50 f4             	lea    -0xc(%eax),%edx
80103eee:	b8 00 b0 10 80       	mov    $0x8010b000,%eax
80103ef3:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103ef8:	89 02                	mov    %eax,(%edx)

    lapicstartap(c->id, V2P(code));
80103efa:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103efd:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80103f03:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f06:	0f b6 00             	movzbl (%eax),%eax
80103f09:	0f b6 c0             	movzbl %al,%eax
80103f0c:	89 54 24 04          	mov    %edx,0x4(%esp)
80103f10:	89 04 24             	mov    %eax,(%esp)
80103f13:	e8 68 f8 ff ff       	call   80103780 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103f18:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103f1b:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
80103f21:	85 c0                	test   %eax,%eax
80103f23:	74 f3                	je     80103f18 <startothers+0xb1>
80103f25:	eb 01                	jmp    80103f28 <startothers+0xc1>
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103f27:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103f28:	81 45 f0 bc 00 00 00 	addl   $0xbc,-0x10(%ebp)
80103f2f:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80103f34:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103f3a:	05 e0 3b 11 80       	add    $0x80113be0,%eax
80103f3f:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80103f42:	0f 87 54 ff ff ff    	ja     80103e9c <startothers+0x35>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103f48:	c9                   	leave  
80103f49:	c3                   	ret    
	...

80103f4c <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
80103f4c:	55                   	push   %ebp
80103f4d:	89 e5                	mov    %esp,%ebp
80103f4f:	83 ec 14             	sub    $0x14,%esp
80103f52:	8b 45 08             	mov    0x8(%ebp),%eax
80103f55:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103f59:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80103f5d:	89 c2                	mov    %eax,%edx
80103f5f:	ec                   	in     (%dx),%al
80103f60:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103f63:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103f67:	c9                   	leave  
80103f68:	c3                   	ret    

80103f69 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103f69:	55                   	push   %ebp
80103f6a:	89 e5                	mov    %esp,%ebp
80103f6c:	83 ec 08             	sub    $0x8,%esp
80103f6f:	8b 55 08             	mov    0x8(%ebp),%edx
80103f72:	8b 45 0c             	mov    0xc(%ebp),%eax
80103f75:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103f79:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103f7c:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103f80:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103f84:	ee                   	out    %al,(%dx)
}
80103f85:	c9                   	leave  
80103f86:	c3                   	ret    

80103f87 <sum>:
int ncpu;
uchar ioapicid;

static uchar
sum(uchar *addr, int len)
{
80103f87:	55                   	push   %ebp
80103f88:	89 e5                	mov    %esp,%ebp
80103f8a:	83 ec 10             	sub    $0x10,%esp
  int i, sum;

  sum = 0;
80103f8d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  for(i=0; i<len; i++)
80103f94:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80103f9b:	eb 13                	jmp    80103fb0 <sum+0x29>
    sum += addr[i];
80103f9d:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fa0:	03 45 08             	add    0x8(%ebp),%eax
80103fa3:	0f b6 00             	movzbl (%eax),%eax
80103fa6:	0f b6 c0             	movzbl %al,%eax
80103fa9:	01 45 fc             	add    %eax,-0x4(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;

  sum = 0;
  for(i=0; i<len; i++)
80103fac:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80103fb0:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103fb3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103fb6:	7c e5                	jl     80103f9d <sum+0x16>
    sum += addr[i];
  return sum;
80103fb8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80103fbb:	c9                   	leave  
80103fbc:	c3                   	ret    

80103fbd <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103fbd:	55                   	push   %ebp
80103fbe:	89 e5                	mov    %esp,%ebp
80103fc0:	83 ec 28             	sub    $0x28,%esp
  uchar *e, *p, *addr;

  addr = P2V(a);
80103fc3:	8b 45 08             	mov    0x8(%ebp),%eax
80103fc6:	2d 00 00 00 80       	sub    $0x80000000,%eax
80103fcb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  e = addr+len;
80103fce:	8b 45 0c             	mov    0xc(%ebp),%eax
80103fd1:	03 45 f4             	add    -0xc(%ebp),%eax
80103fd4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103fd7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103fda:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103fdd:	eb 3f                	jmp    8010401e <mpsearch1+0x61>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103fdf:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
80103fe6:	00 
80103fe7:	c7 44 24 04 b4 93 10 	movl   $0x801093b4,0x4(%esp)
80103fee:	80 
80103fef:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ff2:	89 04 24             	mov    %eax,(%esp)
80103ff5:	e8 df 1b 00 00       	call   80105bd9 <memcmp>
80103ffa:	85 c0                	test   %eax,%eax
80103ffc:	75 1c                	jne    8010401a <mpsearch1+0x5d>
80103ffe:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
80104005:	00 
80104006:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104009:	89 04 24             	mov    %eax,(%esp)
8010400c:	e8 76 ff ff ff       	call   80103f87 <sum>
80104011:	84 c0                	test   %al,%al
80104013:	75 05                	jne    8010401a <mpsearch1+0x5d>
      return (struct mp*)p;
80104015:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104018:	eb 11                	jmp    8010402b <mpsearch1+0x6e>
{
  uchar *e, *p, *addr;

  addr = P2V(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
8010401a:	83 45 f0 10          	addl   $0x10,-0x10(%ebp)
8010401e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104021:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80104024:	72 b9                	jb     80103fdf <mpsearch1+0x22>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
80104026:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010402b:	c9                   	leave  
8010402c:	c3                   	ret    

8010402d <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xF0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
8010402d:	55                   	push   %ebp
8010402e:	89 e5                	mov    %esp,%ebp
80104030:	83 ec 28             	sub    $0x28,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
80104033:	c7 45 ec 00 04 00 80 	movl   $0x80000400,-0x14(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
8010403a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010403d:	83 c0 0f             	add    $0xf,%eax
80104040:	0f b6 00             	movzbl (%eax),%eax
80104043:	0f b6 c0             	movzbl %al,%eax
80104046:	89 c2                	mov    %eax,%edx
80104048:	c1 e2 08             	shl    $0x8,%edx
8010404b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010404e:	83 c0 0e             	add    $0xe,%eax
80104051:	0f b6 00             	movzbl (%eax),%eax
80104054:	0f b6 c0             	movzbl %al,%eax
80104057:	09 d0                	or     %edx,%eax
80104059:	c1 e0 04             	shl    $0x4,%eax
8010405c:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010405f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80104063:	74 21                	je     80104086 <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
80104065:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
8010406c:	00 
8010406d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104070:	89 04 24             	mov    %eax,(%esp)
80104073:	e8 45 ff ff ff       	call   80103fbd <mpsearch1>
80104078:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010407b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010407f:	74 50                	je     801040d1 <mpsearch+0xa4>
      return mp;
80104081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104084:	eb 5f                	jmp    801040e5 <mpsearch+0xb8>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80104086:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104089:	83 c0 14             	add    $0x14,%eax
8010408c:	0f b6 00             	movzbl (%eax),%eax
8010408f:	0f b6 c0             	movzbl %al,%eax
80104092:	89 c2                	mov    %eax,%edx
80104094:	c1 e2 08             	shl    $0x8,%edx
80104097:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010409a:	83 c0 13             	add    $0x13,%eax
8010409d:	0f b6 00             	movzbl (%eax),%eax
801040a0:	0f b6 c0             	movzbl %al,%eax
801040a3:	09 d0                	or     %edx,%eax
801040a5:	c1 e0 0a             	shl    $0xa,%eax
801040a8:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
801040ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
801040ae:	2d 00 04 00 00       	sub    $0x400,%eax
801040b3:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
801040ba:	00 
801040bb:	89 04 24             	mov    %eax,(%esp)
801040be:	e8 fa fe ff ff       	call   80103fbd <mpsearch1>
801040c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040c6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040ca:	74 05                	je     801040d1 <mpsearch+0xa4>
      return mp;
801040cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040cf:	eb 14                	jmp    801040e5 <mpsearch+0xb8>
  }
  return mpsearch1(0xF0000, 0x10000);
801040d1:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
801040d8:	00 
801040d9:	c7 04 24 00 00 0f 00 	movl   $0xf0000,(%esp)
801040e0:	e8 d8 fe ff ff       	call   80103fbd <mpsearch1>
}
801040e5:	c9                   	leave  
801040e6:	c3                   	ret    

801040e7 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
801040e7:	55                   	push   %ebp
801040e8:	89 e5                	mov    %esp,%ebp
801040ea:	83 ec 28             	sub    $0x28,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801040ed:	e8 3b ff ff ff       	call   8010402d <mpsearch>
801040f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
801040f5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801040f9:	74 0a                	je     80104105 <mpconfig+0x1e>
801040fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040fe:	8b 40 04             	mov    0x4(%eax),%eax
80104101:	85 c0                	test   %eax,%eax
80104103:	75 0a                	jne    8010410f <mpconfig+0x28>
    return 0;
80104105:	b8 00 00 00 00       	mov    $0x0,%eax
8010410a:	e9 80 00 00 00       	jmp    8010418f <mpconfig+0xa8>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
8010410f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104112:	8b 40 04             	mov    0x4(%eax),%eax
80104115:	2d 00 00 00 80       	sub    $0x80000000,%eax
8010411a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
8010411d:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
80104124:	00 
80104125:	c7 44 24 04 b9 93 10 	movl   $0x801093b9,0x4(%esp)
8010412c:	80 
8010412d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104130:	89 04 24             	mov    %eax,(%esp)
80104133:	e8 a1 1a 00 00       	call   80105bd9 <memcmp>
80104138:	85 c0                	test   %eax,%eax
8010413a:	74 07                	je     80104143 <mpconfig+0x5c>
    return 0;
8010413c:	b8 00 00 00 00       	mov    $0x0,%eax
80104141:	eb 4c                	jmp    8010418f <mpconfig+0xa8>
  if(conf->specrev != 1 && conf->specrev != 4)
80104143:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104146:	0f b6 40 06          	movzbl 0x6(%eax),%eax
8010414a:	3c 01                	cmp    $0x1,%al
8010414c:	74 12                	je     80104160 <mpconfig+0x79>
8010414e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104151:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80104155:	3c 04                	cmp    $0x4,%al
80104157:	74 07                	je     80104160 <mpconfig+0x79>
    return 0;
80104159:	b8 00 00 00 00       	mov    $0x0,%eax
8010415e:	eb 2f                	jmp    8010418f <mpconfig+0xa8>
  if(sum((uchar*)conf, conf->length) != 0)
80104160:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104163:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80104167:	0f b7 d0             	movzwl %ax,%edx
8010416a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010416d:	89 54 24 04          	mov    %edx,0x4(%esp)
80104171:	89 04 24             	mov    %eax,(%esp)
80104174:	e8 0e fe ff ff       	call   80103f87 <sum>
80104179:	84 c0                	test   %al,%al
8010417b:	74 07                	je     80104184 <mpconfig+0x9d>
    return 0;
8010417d:	b8 00 00 00 00       	mov    $0x0,%eax
80104182:	eb 0b                	jmp    8010418f <mpconfig+0xa8>
  *pmp = mp;
80104184:	8b 45 08             	mov    0x8(%ebp),%eax
80104187:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010418a:	89 10                	mov    %edx,(%eax)
  return conf;
8010418c:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010418f:	c9                   	leave  
80104190:	c3                   	ret    

80104191 <mpinit>:

void
mpinit(void)
{
80104191:	55                   	push   %ebp
80104192:	89 e5                	mov    %esp,%ebp
80104194:	83 ec 38             	sub    $0x38,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
80104197:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010419a:	89 04 24             	mov    %eax,(%esp)
8010419d:	e8 45 ff ff ff       	call   801040e7 <mpconfig>
801041a2:	89 45 ec             	mov    %eax,-0x14(%ebp)
801041a5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801041a9:	0f 84 80 02 00 00    	je     8010442f <mpinit+0x29e>
    return;
  ismp = 1;
801041af:	c7 05 c4 3b 11 80 01 	movl   $0x1,0x80113bc4
801041b6:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
801041b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041bc:	8b 40 24             	mov    0x24(%eax),%eax
801041bf:	a3 dc 3a 11 80       	mov    %eax,0x80113adc
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801041c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041c7:	83 c0 2c             	add    $0x2c,%eax
801041ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801041cd:	8b 55 ec             	mov    -0x14(%ebp),%edx
801041d0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801041d3:	0f b7 40 04          	movzwl 0x4(%eax),%eax
801041d7:	0f b7 c0             	movzwl %ax,%eax
801041da:	8d 04 02             	lea    (%edx,%eax,1),%eax
801041dd:	89 45 e8             	mov    %eax,-0x18(%ebp)
801041e0:	e9 cb 01 00 00       	jmp    801043b0 <mpinit+0x21f>
    switch(*p){
801041e5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801041e8:	0f b6 00             	movzbl (%eax),%eax
801041eb:	0f b6 c0             	movzbl %al,%eax
801041ee:	83 f8 04             	cmp    $0x4,%eax
801041f1:	0f 87 96 01 00 00    	ja     8010438d <mpinit+0x1fc>
801041f7:	8b 04 85 cc 94 10 80 	mov    -0x7fef6b34(,%eax,4),%eax
801041fe:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80104200:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104203:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(!(proc->flags & MP_PROC_ENABLED)){
80104206:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104209:	0f b6 40 03          	movzbl 0x3(%eax),%eax
8010420d:	0f b6 c0             	movzbl %al,%eax
80104210:	83 e0 01             	and    $0x1,%eax
80104213:	85 c0                	test   %eax,%eax
80104215:	75 23                	jne    8010423a <mpinit+0xa9>
        cprintf("mpinit: ignoring cpu %d, not enabled\n", proc->apicid);
80104217:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010421a:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010421e:	0f b6 c0             	movzbl %al,%eax
80104221:	89 44 24 04          	mov    %eax,0x4(%esp)
80104225:	c7 04 24 c0 93 10 80 	movl   $0x801093c0,(%esp)
8010422c:	e8 98 c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
80104231:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
80104235:	e9 76 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      }
      if(!(proc->feature & MP_PROC_APIC)){
8010423a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010423d:	8b 40 08             	mov    0x8(%eax),%eax
80104240:	25 00 02 00 00       	and    $0x200,%eax
80104245:	85 c0                	test   %eax,%eax
80104247:	75 23                	jne    8010426c <mpinit+0xdb>
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
80104249:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010424c:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104250:	0f b6 c0             	movzbl %al,%eax
80104253:	89 44 24 04          	mov    %eax,0x4(%esp)
80104257:	c7 04 24 e8 93 10 80 	movl   $0x801093e8,(%esp)
8010425e:	e8 66 c4 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpproc);
80104263:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
80104267:	e9 44 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      }
      if(ncpu >= NCPU){
8010426c:	a1 c0 41 11 80       	mov    0x801141c0,%eax
80104271:	83 f8 07             	cmp    $0x7,%eax
80104274:	7e 2b                	jle    801042a1 <mpinit+0x110>
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
          NCPU, proc->apicid);
80104276:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104279:	0f b6 40 01          	movzbl 0x1(%eax),%eax
        cprintf("mpinit: cpu %d has no working APIC, ignored\n", proc->apicid);
        p += sizeof(struct mpproc);
        continue;
      }
      if(ncpu >= NCPU){
        cprintf("mpinit: more than %d cpus, ignoring cpu%d\n",
8010427d:	0f b6 c0             	movzbl %al,%eax
80104280:	89 44 24 08          	mov    %eax,0x8(%esp)
80104284:	c7 44 24 04 08 00 00 	movl   $0x8,0x4(%esp)
8010428b:	00 
8010428c:	c7 04 24 18 94 10 80 	movl   $0x80109418,(%esp)
80104293:	e8 31 c4 ff ff       	call   801006c9 <cprintf>
          NCPU, proc->apicid);
        p += sizeof(struct mpproc);
80104298:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
        continue;
8010429c:	e9 0f 01 00 00       	jmp    801043b0 <mpinit+0x21f>
      // says that they DON'T have to be. As long as QEMU is used to simulate
      // multiple SOCKETS everything seems fine, but as soon as we add CORES
      // the code here breaks (and fails to identify them). The confusion
      // between the id we assign here and the actual APIC id continues in
      // ioapic.c for example. A rewrite is in order.
      if(ncpu != proc->apicid){
801042a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042a4:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042a8:	0f b6 d0             	movzbl %al,%edx
801042ab:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042b0:	39 c2                	cmp    %eax,%edx
801042b2:	74 2d                	je     801042e1 <mpinit+0x150>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
801042b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801042b7:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801042bb:	0f b6 d0             	movzbl %al,%edx
801042be:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042c3:	89 54 24 08          	mov    %edx,0x8(%esp)
801042c7:	89 44 24 04          	mov    %eax,0x4(%esp)
801042cb:	c7 04 24 43 94 10 80 	movl   $0x80109443,(%esp)
801042d2:	e8 f2 c3 ff ff       	call   801006c9 <cprintf>
        ismp = 0;
801042d7:	c7 05 c4 3b 11 80 00 	movl   $0x0,0x80113bc4
801042de:	00 00 00 
      }
      cpus[ncpu].id = ncpu;
801042e1:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042e6:	8b 15 c0 41 11 80    	mov    0x801141c0,%edx
801042ec:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
801042f2:	88 90 e0 3b 11 80    	mov    %dl,-0x7feec420(%eax)
      ncpu++;
801042f8:	a1 c0 41 11 80       	mov    0x801141c0,%eax
801042fd:	83 c0 01             	add    $0x1,%eax
80104300:	a3 c0 41 11 80       	mov    %eax,0x801141c0
      p += sizeof(struct mpproc);
80104305:	83 45 e4 14          	addl   $0x14,-0x1c(%ebp)
      continue;
80104309:	e9 a2 00 00 00       	jmp    801043b0 <mpinit+0x21f>
    case MPIOAPIC:
      mioapic = (struct mpioapic*)p;
8010430e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104311:	89 45 f4             	mov    %eax,-0xc(%ebp)
      if(!(mioapic->flags & MP_APIC_ENABLED)){
80104314:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104317:	0f b6 40 03          	movzbl 0x3(%eax),%eax
8010431b:	0f b6 c0             	movzbl %al,%eax
8010431e:	83 e0 01             	and    $0x1,%eax
80104321:	85 c0                	test   %eax,%eax
80104323:	75 20                	jne    80104345 <mpinit+0x1b4>
        cprintf("mpinit: ioapic %d disabled, ignored\n", mioapic->apicid);
80104325:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104328:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010432c:	0f b6 c0             	movzbl %al,%eax
8010432f:	89 44 24 04          	mov    %eax,0x4(%esp)
80104333:	c7 04 24 60 94 10 80 	movl   $0x80109460,(%esp)
8010433a:	e8 8a c3 ff ff       	call   801006c9 <cprintf>
        p += sizeof(struct mpioapic);
8010433f:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
        continue;
80104343:	eb 6b                	jmp    801043b0 <mpinit+0x21f>
      }
      if(ioapic == 0){
80104345:	a1 94 3a 11 80       	mov    0x80113a94,%eax
8010434a:	85 c0                	test   %eax,%eax
8010434c:	75 19                	jne    80104367 <mpinit+0x1d6>
        ioapic = (volatile struct ioapic*)mioapic->addr;
8010434e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104351:	8b 40 04             	mov    0x4(%eax),%eax
80104354:	a3 94 3a 11 80       	mov    %eax,0x80113a94
        ioapicid = mioapic->apicid;
80104359:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010435c:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80104360:	a2 c0 3b 11 80       	mov    %al,0x80113bc0
80104365:	eb 1a                	jmp    80104381 <mpinit+0x1f0>
      } else {
        cprintf("mpinit: ignored extra ioapic %d\n", mioapic->apicid);
80104367:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010436a:	0f b6 40 01          	movzbl 0x1(%eax),%eax
8010436e:	0f b6 c0             	movzbl %al,%eax
80104371:	89 44 24 04          	mov    %eax,0x4(%esp)
80104375:	c7 04 24 88 94 10 80 	movl   $0x80109488,(%esp)
8010437c:	e8 48 c3 ff ff       	call   801006c9 <cprintf>
      }
      p += sizeof(struct mpioapic);
80104381:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
80104385:	eb 29                	jmp    801043b0 <mpinit+0x21f>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80104387:	83 45 e4 08          	addl   $0x8,-0x1c(%ebp)
      continue;
8010438b:	eb 23                	jmp    801043b0 <mpinit+0x21f>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
8010438d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104390:	0f b6 00             	movzbl (%eax),%eax
80104393:	0f b6 c0             	movzbl %al,%eax
80104396:	89 44 24 04          	mov    %eax,0x4(%esp)
8010439a:	c7 04 24 ac 94 10 80 	movl   $0x801094ac,(%esp)
801043a1:	e8 23 c3 ff ff       	call   801006c9 <cprintf>
      ismp = 0;
801043a6:	c7 05 c4 3b 11 80 00 	movl   $0x0,0x80113bc4
801043ad:	00 00 00 

  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801043b0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801043b3:	3b 45 e8             	cmp    -0x18(%ebp),%eax
801043b6:	0f 82 29 fe ff ff    	jb     801041e5 <mpinit+0x54>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
801043bc:	a1 c4 3b 11 80       	mov    0x80113bc4,%eax
801043c1:	85 c0                	test   %eax,%eax
801043c3:	75 27                	jne    801043ec <mpinit+0x25b>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
801043c5:	c7 05 c0 41 11 80 01 	movl   $0x1,0x801141c0
801043cc:	00 00 00 
    lapic = 0;
801043cf:	c7 05 dc 3a 11 80 00 	movl   $0x0,0x80113adc
801043d6:	00 00 00 
    ioapic = 0;
801043d9:	c7 05 94 3a 11 80 00 	movl   $0x0,0x80113a94
801043e0:	00 00 00 
    ioapicid = 0;
801043e3:	c6 05 c0 3b 11 80 00 	movb   $0x0,0x80113bc0
    return;
801043ea:	eb 44                	jmp    80104430 <mpinit+0x29f>
  }

  if(mp->imcrp){
801043ec:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043ef:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
801043f3:	84 c0                	test   %al,%al
801043f5:	74 39                	je     80104430 <mpinit+0x29f>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
801043f7:	c7 44 24 04 70 00 00 	movl   $0x70,0x4(%esp)
801043fe:	00 
801043ff:	c7 04 24 22 00 00 00 	movl   $0x22,(%esp)
80104406:	e8 5e fb ff ff       	call   80103f69 <outb>
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
8010440b:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80104412:	e8 35 fb ff ff       	call   80103f4c <inb>
80104417:	83 c8 01             	or     $0x1,%eax
8010441a:	0f b6 c0             	movzbl %al,%eax
8010441d:	89 44 24 04          	mov    %eax,0x4(%esp)
80104421:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80104428:	e8 3c fb ff ff       	call   80103f69 <outb>
8010442d:	eb 01                	jmp    80104430 <mpinit+0x29f>
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *mioapic;

  if((conf = mpconfig(&mp)) == 0)
    return;
8010442f:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80104430:	c9                   	leave  
80104431:	c3                   	ret    
	...

80104434 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80104434:	55                   	push   %ebp
80104435:	89 e5                	mov    %esp,%ebp
80104437:	83 ec 08             	sub    $0x8,%esp
8010443a:	8b 55 08             	mov    0x8(%ebp),%edx
8010443d:	8b 45 0c             	mov    0xc(%ebp),%eax
80104440:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80104444:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80104447:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
8010444b:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010444f:	ee                   	out    %al,(%dx)
}
80104450:	c9                   	leave  
80104451:	c3                   	ret    

80104452 <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
80104452:	55                   	push   %ebp
80104453:	89 e5                	mov    %esp,%ebp
80104455:	83 ec 0c             	sub    $0xc,%esp
80104458:	8b 45 08             	mov    0x8(%ebp),%eax
8010445b:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
8010445f:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80104463:	66 a3 00 c0 10 80    	mov    %ax,0x8010c000
  outb(IO_PIC1+1, mask);
80104469:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
8010446d:	0f b6 c0             	movzbl %al,%eax
80104470:	89 44 24 04          	mov    %eax,0x4(%esp)
80104474:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010447b:	e8 b4 ff ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, mask >> 8);
80104480:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80104484:	66 c1 e8 08          	shr    $0x8,%ax
80104488:	0f b6 c0             	movzbl %al,%eax
8010448b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010448f:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104496:	e8 99 ff ff ff       	call   80104434 <outb>
}
8010449b:	c9                   	leave  
8010449c:	c3                   	ret    

8010449d <picenable>:

void
picenable(int irq)
{
8010449d:	55                   	push   %ebp
8010449e:	89 e5                	mov    %esp,%ebp
801044a0:	53                   	push   %ebx
801044a1:	83 ec 04             	sub    $0x4,%esp
  picsetmask(irqmask & ~(1<<irq));
801044a4:	8b 45 08             	mov    0x8(%ebp),%eax
801044a7:	ba 01 00 00 00       	mov    $0x1,%edx
801044ac:	89 d3                	mov    %edx,%ebx
801044ae:	89 c1                	mov    %eax,%ecx
801044b0:	d3 e3                	shl    %cl,%ebx
801044b2:	89 d8                	mov    %ebx,%eax
801044b4:	89 c2                	mov    %eax,%edx
801044b6:	f7 d2                	not    %edx
801044b8:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801044bf:	21 d0                	and    %edx,%eax
801044c1:	0f b7 c0             	movzwl %ax,%eax
801044c4:	89 04 24             	mov    %eax,(%esp)
801044c7:	e8 86 ff ff ff       	call   80104452 <picsetmask>
}
801044cc:	83 c4 04             	add    $0x4,%esp
801044cf:	5b                   	pop    %ebx
801044d0:	5d                   	pop    %ebp
801044d1:	c3                   	ret    

801044d2 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
801044d2:	55                   	push   %ebp
801044d3:	89 e5                	mov    %esp,%ebp
801044d5:	83 ec 08             	sub    $0x8,%esp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
801044d8:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
801044df:	00 
801044e0:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
801044e7:	e8 48 ff ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, 0xFF);
801044ec:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
801044f3:	00 
801044f4:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
801044fb:	e8 34 ff ff ff       	call   80104434 <outb>

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
80104500:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80104507:	00 
80104508:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
8010450f:	e8 20 ff ff ff       	call   80104434 <outb>

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
80104514:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
8010451b:	00 
8010451c:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80104523:	e8 0c ff ff ff       	call   80104434 <outb>

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80104528:	c7 44 24 04 04 00 00 	movl   $0x4,0x4(%esp)
8010452f:	00 
80104530:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80104537:	e8 f8 fe ff ff       	call   80104434 <outb>
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
8010453c:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80104543:	00 
80104544:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
8010454b:	e8 e4 fe ff ff       	call   80104434 <outb>

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
80104550:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80104557:	00 
80104558:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
8010455f:	e8 d0 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
80104564:	c7 44 24 04 28 00 00 	movl   $0x28,0x4(%esp)
8010456b:	00 
8010456c:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104573:	e8 bc fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
80104578:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
8010457f:	00 
80104580:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80104587:	e8 a8 fe ff ff       	call   80104434 <outb>
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
8010458c:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80104593:	00 
80104594:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
8010459b:	e8 94 fe ff ff       	call   80104434 <outb>

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
801045a0:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045a7:	00 
801045a8:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045af:	e8 80 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC1, 0x0a);             // read IRR by default
801045b4:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801045bb:	00 
801045bc:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801045c3:	e8 6c fe ff ff       	call   80104434 <outb>

  outb(IO_PIC2, 0x68);             // OCW3
801045c8:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
801045cf:	00 
801045d0:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
801045d7:	e8 58 fe ff ff       	call   80104434 <outb>
  outb(IO_PIC2, 0x0a);             // OCW3
801045dc:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
801045e3:	00 
801045e4:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
801045eb:	e8 44 fe ff ff       	call   80104434 <outb>

  if(irqmask != 0xFFFF)
801045f0:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801045f7:	66 83 f8 ff          	cmp    $0xffffffff,%ax
801045fb:	74 12                	je     8010460f <picinit+0x13d>
    picsetmask(irqmask);
801045fd:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
80104604:	0f b7 c0             	movzwl %ax,%eax
80104607:	89 04 24             	mov    %eax,(%esp)
8010460a:	e8 43 fe ff ff       	call   80104452 <picsetmask>
}
8010460f:	c9                   	leave  
80104610:	c3                   	ret    
80104611:	00 00                	add    %al,(%eax)
	...

80104614 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80104614:	55                   	push   %ebp
80104615:	89 e5                	mov    %esp,%ebp
80104617:	83 ec 28             	sub    $0x28,%esp
  struct pipe *p;

  p = 0;
8010461a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
80104621:	8b 45 0c             	mov    0xc(%ebp),%eax
80104624:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
8010462a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010462d:	8b 10                	mov    (%eax),%edx
8010462f:	8b 45 08             	mov    0x8(%ebp),%eax
80104632:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
80104634:	e8 7f d0 ff ff       	call   801016b8 <filealloc>
80104639:	8b 55 08             	mov    0x8(%ebp),%edx
8010463c:	89 02                	mov    %eax,(%edx)
8010463e:	8b 45 08             	mov    0x8(%ebp),%eax
80104641:	8b 00                	mov    (%eax),%eax
80104643:	85 c0                	test   %eax,%eax
80104645:	0f 84 c8 00 00 00    	je     80104713 <pipealloc+0xff>
8010464b:	e8 68 d0 ff ff       	call   801016b8 <filealloc>
80104650:	8b 55 0c             	mov    0xc(%ebp),%edx
80104653:	89 02                	mov    %eax,(%edx)
80104655:	8b 45 0c             	mov    0xc(%ebp),%eax
80104658:	8b 00                	mov    (%eax),%eax
8010465a:	85 c0                	test   %eax,%eax
8010465c:	0f 84 b1 00 00 00    	je     80104713 <pipealloc+0xff>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80104662:	e8 37 ed ff ff       	call   8010339e <kalloc>
80104667:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010466a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010466e:	0f 84 9e 00 00 00    	je     80104712 <pipealloc+0xfe>
    goto bad;
  p->readopen = 1;
80104674:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104677:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
8010467e:	00 00 00 
  p->writeopen = 1;
80104681:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104684:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
8010468b:	00 00 00 
  p->nwrite = 0;
8010468e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104691:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80104698:	00 00 00 
  p->nread = 0;
8010469b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010469e:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801046a5:	00 00 00 
  initlock(&p->lock, "pipe");
801046a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046ab:	c7 44 24 04 e0 94 10 	movl   $0x801094e0,0x4(%esp)
801046b2:	80 
801046b3:	89 04 24             	mov    %eax,(%esp)
801046b6:	e8 23 12 00 00       	call   801058de <initlock>
  (*f0)->type = FD_PIPE;
801046bb:	8b 45 08             	mov    0x8(%ebp),%eax
801046be:	8b 00                	mov    (%eax),%eax
801046c0:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
801046c6:	8b 45 08             	mov    0x8(%ebp),%eax
801046c9:	8b 00                	mov    (%eax),%eax
801046cb:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801046cf:	8b 45 08             	mov    0x8(%ebp),%eax
801046d2:	8b 00                	mov    (%eax),%eax
801046d4:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801046d8:	8b 45 08             	mov    0x8(%ebp),%eax
801046db:	8b 00                	mov    (%eax),%eax
801046dd:	8b 55 f4             	mov    -0xc(%ebp),%edx
801046e0:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
801046e3:	8b 45 0c             	mov    0xc(%ebp),%eax
801046e6:	8b 00                	mov    (%eax),%eax
801046e8:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801046ee:	8b 45 0c             	mov    0xc(%ebp),%eax
801046f1:	8b 00                	mov    (%eax),%eax
801046f3:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801046f7:	8b 45 0c             	mov    0xc(%ebp),%eax
801046fa:	8b 00                	mov    (%eax),%eax
801046fc:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80104700:	8b 45 0c             	mov    0xc(%ebp),%eax
80104703:	8b 00                	mov    (%eax),%eax
80104705:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104708:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
8010470b:	b8 00 00 00 00       	mov    $0x0,%eax
80104710:	eb 43                	jmp    80104755 <pipealloc+0x141>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
80104712:	90                   	nop
  (*f1)->pipe = p;
  return 0;

//PAGEBREAK: 20
 bad:
  if(p)
80104713:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104717:	74 0b                	je     80104724 <pipealloc+0x110>
    kfree((char*)p);
80104719:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010471c:	89 04 24             	mov    %eax,(%esp)
8010471f:	e8 e4 eb ff ff       	call   80103308 <kfree>
  if(*f0)
80104724:	8b 45 08             	mov    0x8(%ebp),%eax
80104727:	8b 00                	mov    (%eax),%eax
80104729:	85 c0                	test   %eax,%eax
8010472b:	74 0d                	je     8010473a <pipealloc+0x126>
    fileclose(*f0);
8010472d:	8b 45 08             	mov    0x8(%ebp),%eax
80104730:	8b 00                	mov    (%eax),%eax
80104732:	89 04 24             	mov    %eax,(%esp)
80104735:	e8 27 d0 ff ff       	call   80101761 <fileclose>
  if(*f1)
8010473a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010473d:	8b 00                	mov    (%eax),%eax
8010473f:	85 c0                	test   %eax,%eax
80104741:	74 0d                	je     80104750 <pipealloc+0x13c>
    fileclose(*f1);
80104743:	8b 45 0c             	mov    0xc(%ebp),%eax
80104746:	8b 00                	mov    (%eax),%eax
80104748:	89 04 24             	mov    %eax,(%esp)
8010474b:	e8 11 d0 ff ff       	call   80101761 <fileclose>
  return -1;
80104750:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104755:	c9                   	leave  
80104756:	c3                   	ret    

80104757 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80104757:	55                   	push   %ebp
80104758:	89 e5                	mov    %esp,%ebp
8010475a:	83 ec 18             	sub    $0x18,%esp
  acquire(&p->lock);
8010475d:	8b 45 08             	mov    0x8(%ebp),%eax
80104760:	89 04 24             	mov    %eax,(%esp)
80104763:	e8 97 11 00 00       	call   801058ff <acquire>
  if(writable){
80104768:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010476c:	74 1f                	je     8010478d <pipeclose+0x36>
    p->writeopen = 0;
8010476e:	8b 45 08             	mov    0x8(%ebp),%eax
80104771:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
80104778:	00 00 00 
    wakeup(&p->nread);
8010477b:	8b 45 08             	mov    0x8(%ebp),%eax
8010477e:	05 34 02 00 00       	add    $0x234,%eax
80104783:	89 04 24             	mov    %eax,(%esp)
80104786:	e8 6f 0f 00 00       	call   801056fa <wakeup>
8010478b:	eb 1d                	jmp    801047aa <pipeclose+0x53>
  } else {
    p->readopen = 0;
8010478d:	8b 45 08             	mov    0x8(%ebp),%eax
80104790:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
80104797:	00 00 00 
    wakeup(&p->nwrite);
8010479a:	8b 45 08             	mov    0x8(%ebp),%eax
8010479d:	05 38 02 00 00       	add    $0x238,%eax
801047a2:	89 04 24             	mov    %eax,(%esp)
801047a5:	e8 50 0f 00 00       	call   801056fa <wakeup>
  }
  if(p->readopen == 0 && p->writeopen == 0){
801047aa:	8b 45 08             	mov    0x8(%ebp),%eax
801047ad:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
801047b3:	85 c0                	test   %eax,%eax
801047b5:	75 25                	jne    801047dc <pipeclose+0x85>
801047b7:	8b 45 08             	mov    0x8(%ebp),%eax
801047ba:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
801047c0:	85 c0                	test   %eax,%eax
801047c2:	75 18                	jne    801047dc <pipeclose+0x85>
    release(&p->lock);
801047c4:	8b 45 08             	mov    0x8(%ebp),%eax
801047c7:	89 04 24             	mov    %eax,(%esp)
801047ca:	e8 99 11 00 00       	call   80105968 <release>
    kfree((char*)p);
801047cf:	8b 45 08             	mov    0x8(%ebp),%eax
801047d2:	89 04 24             	mov    %eax,(%esp)
801047d5:	e8 2e eb ff ff       	call   80103308 <kfree>
    wakeup(&p->nread);
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
801047da:	eb 0b                	jmp    801047e7 <pipeclose+0x90>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
801047dc:	8b 45 08             	mov    0x8(%ebp),%eax
801047df:	89 04 24             	mov    %eax,(%esp)
801047e2:	e8 81 11 00 00       	call   80105968 <release>
}
801047e7:	c9                   	leave  
801047e8:	c3                   	ret    

801047e9 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
801047e9:	55                   	push   %ebp
801047ea:	89 e5                	mov    %esp,%ebp
801047ec:	53                   	push   %ebx
801047ed:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
801047f0:	8b 45 08             	mov    0x8(%ebp),%eax
801047f3:	89 04 24             	mov    %eax,(%esp)
801047f6:	e8 04 11 00 00       	call   801058ff <acquire>
  for(i = 0; i < n; i++){
801047fb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104802:	e9 a6 00 00 00       	jmp    801048ad <pipewrite+0xc4>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
80104807:	8b 45 08             	mov    0x8(%ebp),%eax
8010480a:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80104810:	85 c0                	test   %eax,%eax
80104812:	74 0d                	je     80104821 <pipewrite+0x38>
80104814:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010481a:	8b 40 24             	mov    0x24(%eax),%eax
8010481d:	85 c0                	test   %eax,%eax
8010481f:	74 15                	je     80104836 <pipewrite+0x4d>
        release(&p->lock);
80104821:	8b 45 08             	mov    0x8(%ebp),%eax
80104824:	89 04 24             	mov    %eax,(%esp)
80104827:	e8 3c 11 00 00       	call   80105968 <release>
        return -1;
8010482c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104831:	e9 9d 00 00 00       	jmp    801048d3 <pipewrite+0xea>
      }
      wakeup(&p->nread);
80104836:	8b 45 08             	mov    0x8(%ebp),%eax
80104839:	05 34 02 00 00       	add    $0x234,%eax
8010483e:	89 04 24             	mov    %eax,(%esp)
80104841:	e8 b4 0e 00 00       	call   801056fa <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80104846:	8b 45 08             	mov    0x8(%ebp),%eax
80104849:	8b 55 08             	mov    0x8(%ebp),%edx
8010484c:	81 c2 38 02 00 00    	add    $0x238,%edx
80104852:	89 44 24 04          	mov    %eax,0x4(%esp)
80104856:	89 14 24             	mov    %edx,(%esp)
80104859:	e8 bf 0d 00 00       	call   8010561d <sleep>
8010485e:	eb 01                	jmp    80104861 <pipewrite+0x78>
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80104860:	90                   	nop
80104861:	8b 45 08             	mov    0x8(%ebp),%eax
80104864:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
8010486a:	8b 45 08             	mov    0x8(%ebp),%eax
8010486d:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
80104873:	05 00 02 00 00       	add    $0x200,%eax
80104878:	39 c2                	cmp    %eax,%edx
8010487a:	74 8b                	je     80104807 <pipewrite+0x1e>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
8010487c:	8b 45 08             	mov    0x8(%ebp),%eax
8010487f:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104885:	89 c3                	mov    %eax,%ebx
80104887:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
8010488d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104890:	03 55 0c             	add    0xc(%ebp),%edx
80104893:	0f b6 0a             	movzbl (%edx),%ecx
80104896:	8b 55 08             	mov    0x8(%ebp),%edx
80104899:	88 4c 1a 34          	mov    %cl,0x34(%edx,%ebx,1)
8010489d:	8d 50 01             	lea    0x1(%eax),%edx
801048a0:	8b 45 08             	mov    0x8(%ebp),%eax
801048a3:	89 90 38 02 00 00    	mov    %edx,0x238(%eax)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
801048a9:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801048ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048b0:	3b 45 10             	cmp    0x10(%ebp),%eax
801048b3:	7c ab                	jl     80104860 <pipewrite+0x77>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801048b5:	8b 45 08             	mov    0x8(%ebp),%eax
801048b8:	05 34 02 00 00       	add    $0x234,%eax
801048bd:	89 04 24             	mov    %eax,(%esp)
801048c0:	e8 35 0e 00 00       	call   801056fa <wakeup>
  release(&p->lock);
801048c5:	8b 45 08             	mov    0x8(%ebp),%eax
801048c8:	89 04 24             	mov    %eax,(%esp)
801048cb:	e8 98 10 00 00       	call   80105968 <release>
  return n;
801048d0:	8b 45 10             	mov    0x10(%ebp),%eax
}
801048d3:	83 c4 24             	add    $0x24,%esp
801048d6:	5b                   	pop    %ebx
801048d7:	5d                   	pop    %ebp
801048d8:	c3                   	ret    

801048d9 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801048d9:	55                   	push   %ebp
801048da:	89 e5                	mov    %esp,%ebp
801048dc:	53                   	push   %ebx
801048dd:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
801048e0:	8b 45 08             	mov    0x8(%ebp),%eax
801048e3:	89 04 24             	mov    %eax,(%esp)
801048e6:	e8 14 10 00 00       	call   801058ff <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801048eb:	eb 3a                	jmp    80104927 <piperead+0x4e>
    if(proc->killed){
801048ed:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801048f3:	8b 40 24             	mov    0x24(%eax),%eax
801048f6:	85 c0                	test   %eax,%eax
801048f8:	74 15                	je     8010490f <piperead+0x36>
      release(&p->lock);
801048fa:	8b 45 08             	mov    0x8(%ebp),%eax
801048fd:	89 04 24             	mov    %eax,(%esp)
80104900:	e8 63 10 00 00       	call   80105968 <release>
      return -1;
80104905:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010490a:	e9 b6 00 00 00       	jmp    801049c5 <piperead+0xec>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
8010490f:	8b 45 08             	mov    0x8(%ebp),%eax
80104912:	8b 55 08             	mov    0x8(%ebp),%edx
80104915:	81 c2 34 02 00 00    	add    $0x234,%edx
8010491b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010491f:	89 14 24             	mov    %edx,(%esp)
80104922:	e8 f6 0c 00 00       	call   8010561d <sleep>
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80104927:	8b 45 08             	mov    0x8(%ebp),%eax
8010492a:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104930:	8b 45 08             	mov    0x8(%ebp),%eax
80104933:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104939:	39 c2                	cmp    %eax,%edx
8010493b:	75 0d                	jne    8010494a <piperead+0x71>
8010493d:	8b 45 08             	mov    0x8(%ebp),%eax
80104940:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80104946:	85 c0                	test   %eax,%eax
80104948:	75 a3                	jne    801048ed <piperead+0x14>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
8010494a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104951:	eb 49                	jmp    8010499c <piperead+0xc3>
    if(p->nread == p->nwrite)
80104953:	8b 45 08             	mov    0x8(%ebp),%eax
80104956:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
8010495c:	8b 45 08             	mov    0x8(%ebp),%eax
8010495f:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104965:	39 c2                	cmp    %eax,%edx
80104967:	74 3d                	je     801049a6 <piperead+0xcd>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80104969:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010496c:	89 c2                	mov    %eax,%edx
8010496e:	03 55 0c             	add    0xc(%ebp),%edx
80104971:	8b 45 08             	mov    0x8(%ebp),%eax
80104974:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010497a:	89 c3                	mov    %eax,%ebx
8010497c:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
80104982:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104985:	0f b6 4c 19 34       	movzbl 0x34(%ecx,%ebx,1),%ecx
8010498a:	88 0a                	mov    %cl,(%edx)
8010498c:	8d 50 01             	lea    0x1(%eax),%edx
8010498f:	8b 45 08             	mov    0x8(%ebp),%eax
80104992:	89 90 34 02 00 00    	mov    %edx,0x234(%eax)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80104998:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010499c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010499f:	3b 45 10             	cmp    0x10(%ebp),%eax
801049a2:	7c af                	jl     80104953 <piperead+0x7a>
801049a4:	eb 01                	jmp    801049a7 <piperead+0xce>
    if(p->nread == p->nwrite)
      break;
801049a6:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801049a7:	8b 45 08             	mov    0x8(%ebp),%eax
801049aa:	05 38 02 00 00       	add    $0x238,%eax
801049af:	89 04 24             	mov    %eax,(%esp)
801049b2:	e8 43 0d 00 00       	call   801056fa <wakeup>
  release(&p->lock);
801049b7:	8b 45 08             	mov    0x8(%ebp),%eax
801049ba:	89 04 24             	mov    %eax,(%esp)
801049bd:	e8 a6 0f 00 00       	call   80105968 <release>
  return i;
801049c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801049c5:	83 c4 24             	add    $0x24,%esp
801049c8:	5b                   	pop    %ebx
801049c9:	5d                   	pop    %ebp
801049ca:	c3                   	ret    
	...

801049cc <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801049cc:	55                   	push   %ebp
801049cd:	89 e5                	mov    %esp,%ebp
801049cf:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801049d2:	9c                   	pushf  
801049d3:	58                   	pop    %eax
801049d4:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801049d7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801049da:	c9                   	leave  
801049db:	c3                   	ret    

801049dc <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
801049dc:	55                   	push   %ebp
801049dd:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801049df:	fb                   	sti    
}
801049e0:	5d                   	pop    %ebp
801049e1:	c3                   	ret    

801049e2 <pinit>:

static void wakeup1(void *chan);

void
pinit(void)
{
801049e2:	55                   	push   %ebp
801049e3:	89 e5                	mov    %esp,%ebp
801049e5:	83 ec 18             	sub    $0x18,%esp
  initlock(&ptable.lock, "ptable");
801049e8:	c7 44 24 04 e5 94 10 	movl   $0x801094e5,0x4(%esp)
801049ef:	80 
801049f0:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801049f7:	e8 e2 0e 00 00       	call   801058de <initlock>
}
801049fc:	c9                   	leave  
801049fd:	c3                   	ret    

801049fe <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
801049fe:	55                   	push   %ebp
801049ff:	89 e5                	mov    %esp,%ebp
80104a01:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
80104a04:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a0b:	e8 ef 0e 00 00       	call   801058ff <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a10:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
80104a17:	eb 11                	jmp    80104a2a <allocproc+0x2c>
    if(p->state == UNUSED)
80104a19:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a1c:	8b 40 0c             	mov    0xc(%eax),%eax
80104a1f:	85 c0                	test   %eax,%eax
80104a21:	74 27                	je     80104a4a <allocproc+0x4c>
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a23:	81 45 f0 88 00 00 00 	addl   $0x88,-0x10(%ebp)
80104a2a:	b8 14 64 11 80       	mov    $0x80116414,%eax
80104a2f:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104a32:	72 e5                	jb     80104a19 <allocproc+0x1b>
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
80104a34:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a3b:	e8 28 0f 00 00       	call   80105968 <release>
  return 0;
80104a40:	b8 00 00 00 00       	mov    $0x0,%eax
80104a45:	e9 b5 00 00 00       	jmp    80104aff <allocproc+0x101>
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
80104a4a:	90                   	nop
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
80104a4b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a4e:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  p->pid = nextpid++;
80104a55:	a1 04 c0 10 80       	mov    0x8010c004,%eax
80104a5a:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104a5d:	89 42 10             	mov    %eax,0x10(%edx)
80104a60:	83 c0 01             	add    $0x1,%eax
80104a63:	a3 04 c0 10 80       	mov    %eax,0x8010c004
  release(&ptable.lock);
80104a68:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104a6f:	e8 f4 0e 00 00       	call   80105968 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80104a74:	e8 25 e9 ff ff       	call   8010339e <kalloc>
80104a79:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104a7c:	89 42 08             	mov    %eax,0x8(%edx)
80104a7f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a82:	8b 40 08             	mov    0x8(%eax),%eax
80104a85:	85 c0                	test   %eax,%eax
80104a87:	75 11                	jne    80104a9a <allocproc+0x9c>
    p->state = UNUSED;
80104a89:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a8c:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return 0;
80104a93:	b8 00 00 00 00       	mov    $0x0,%eax
80104a98:	eb 65                	jmp    80104aff <allocproc+0x101>
  }
  sp = p->kstack + KSTACKSIZE;
80104a9a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a9d:	8b 40 08             	mov    0x8(%eax),%eax
80104aa0:	05 00 10 00 00       	add    $0x1000,%eax
80104aa5:	89 45 f4             	mov    %eax,-0xc(%ebp)

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80104aa8:	83 6d f4 4c          	subl   $0x4c,-0xc(%ebp)
  p->tf = (struct trapframe*)sp;
80104aac:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104aaf:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ab2:	89 50 18             	mov    %edx,0x18(%eax)

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
80104ab5:	83 6d f4 04          	subl   $0x4,-0xc(%ebp)
  *(uint*)sp = (uint)trapret;
80104ab9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104abc:	ba 98 6f 10 80       	mov    $0x80106f98,%edx
80104ac1:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104ac3:	83 6d f4 14          	subl   $0x14,-0xc(%ebp)
  p->context = (struct context*)sp;
80104ac7:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104aca:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104acd:	89 50 1c             	mov    %edx,0x1c(%eax)
  memset(p->context, 0, sizeof *p->context);
80104ad0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ad3:	8b 40 1c             	mov    0x1c(%eax),%eax
80104ad6:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80104add:	00 
80104ade:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104ae5:	00 
80104ae6:	89 04 24             	mov    %eax,(%esp)
80104ae9:	e8 74 10 00 00       	call   80105b62 <memset>
  p->context->eip = (uint)forkret;
80104aee:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104af1:	8b 40 1c             	mov    0x1c(%eax),%eax
80104af4:	ba de 55 10 80       	mov    $0x801055de,%edx
80104af9:	89 50 10             	mov    %edx,0x10(%eax)
  return p;
80104afc:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80104aff:	c9                   	leave  
80104b00:	c3                   	ret    

80104b01 <userinit>:

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
80104b01:	55                   	push   %ebp
80104b02:	89 e5                	mov    %esp,%ebp
80104b04:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
80104b07:	e8 f2 fe ff ff       	call   801049fe <allocproc>
80104b0c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  initproc = p;
80104b0f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b12:	a3 98 c6 10 80       	mov    %eax,0x8010c698
  if((p->pgdir = setupkvm()) == 0)
80104b17:	e8 5d 3c 00 00       	call   80108779 <setupkvm>
80104b1c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104b1f:	89 42 04             	mov    %eax,0x4(%edx)
80104b22:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b25:	8b 40 04             	mov    0x4(%eax),%eax
80104b28:	85 c0                	test   %eax,%eax
80104b2a:	75 0c                	jne    80104b38 <userinit+0x37>
    panic("userinit: out of memory?");
80104b2c:	c7 04 24 ec 94 10 80 	movl   $0x801094ec,(%esp)
80104b33:	e8 31 bd ff ff       	call   80100869 <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80104b38:	ba 2c 00 00 00       	mov    $0x2c,%edx
80104b3d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b40:	8b 40 04             	mov    0x4(%eax),%eax
80104b43:	89 54 24 08          	mov    %edx,0x8(%esp)
80104b47:	c7 44 24 04 20 c5 10 	movl   $0x8010c520,0x4(%esp)
80104b4e:	80 
80104b4f:	89 04 24             	mov    %eax,(%esp)
80104b52:	e8 69 3e 00 00       	call   801089c0 <inituvm>
  p->sz = PGSIZE;
80104b57:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b5a:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
80104b60:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b63:	8b 40 18             	mov    0x18(%eax),%eax
80104b66:	c7 44 24 08 4c 00 00 	movl   $0x4c,0x8(%esp)
80104b6d:	00 
80104b6e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104b75:	00 
80104b76:	89 04 24             	mov    %eax,(%esp)
80104b79:	e8 e4 0f 00 00       	call   80105b62 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80104b7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b81:	8b 40 18             	mov    0x18(%eax),%eax
80104b84:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80104b8a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b8d:	8b 40 18             	mov    0x18(%eax),%eax
80104b90:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
80104b96:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b99:	8b 40 18             	mov    0x18(%eax),%eax
80104b9c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104b9f:	8b 52 18             	mov    0x18(%edx),%edx
80104ba2:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104ba6:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80104baa:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bad:	8b 40 18             	mov    0x18(%eax),%eax
80104bb0:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104bb3:	8b 52 18             	mov    0x18(%edx),%edx
80104bb6:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104bba:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80104bbe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bc1:	8b 40 18             	mov    0x18(%eax),%eax
80104bc4:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80104bcb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bce:	8b 40 18             	mov    0x18(%eax),%eax
80104bd1:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104bd8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104bdb:	8b 40 18             	mov    0x18(%eax),%eax
80104bde:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
80104be5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104be8:	83 c0 6c             	add    $0x6c,%eax
80104beb:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104bf2:	00 
80104bf3:	c7 44 24 04 05 95 10 	movl   $0x80109505,0x4(%esp)
80104bfa:	80 
80104bfb:	89 04 24             	mov    %eax,(%esp)
80104bfe:	e8 92 11 00 00       	call   80105d95 <safestrcpy>
  p->cwd = namei("/");
80104c03:	c7 04 24 0e 95 10 80 	movl   $0x8010950e,(%esp)
80104c0a:	e8 10 e0 ff ff       	call   80102c1f <namei>
80104c0f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c12:	89 42 68             	mov    %eax,0x68(%edx)

  p->state = RUNNABLE;
80104c15:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c18:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
}
80104c1f:	c9                   	leave  
80104c20:	c3                   	ret    

80104c21 <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
80104c21:	55                   	push   %ebp
80104c22:	89 e5                	mov    %esp,%ebp
80104c24:	83 ec 28             	sub    $0x28,%esp
  uint sz;

  sz = proc->sz;
80104c27:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c2d:	8b 00                	mov    (%eax),%eax
80104c2f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
80104c32:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c36:	7e 34                	jle    80104c6c <growproc+0x4b>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c38:	8b 45 08             	mov    0x8(%ebp),%eax
80104c3b:	89 c2                	mov    %eax,%edx
80104c3d:	03 55 f4             	add    -0xc(%ebp),%edx
80104c40:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c46:	8b 40 04             	mov    0x4(%eax),%eax
80104c49:	89 54 24 08          	mov    %edx,0x8(%esp)
80104c4d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c50:	89 54 24 04          	mov    %edx,0x4(%esp)
80104c54:	89 04 24             	mov    %eax,(%esp)
80104c57:	e8 f6 3e 00 00       	call   80108b52 <allocuvm>
80104c5c:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c5f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c63:	75 41                	jne    80104ca6 <growproc+0x85>
      return -1;
80104c65:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c6a:	eb 58                	jmp    80104cc4 <growproc+0xa3>
  } else if(n < 0){
80104c6c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c70:	79 34                	jns    80104ca6 <growproc+0x85>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c72:	8b 45 08             	mov    0x8(%ebp),%eax
80104c75:	89 c2                	mov    %eax,%edx
80104c77:	03 55 f4             	add    -0xc(%ebp),%edx
80104c7a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c80:	8b 40 04             	mov    0x4(%eax),%eax
80104c83:	89 54 24 08          	mov    %edx,0x8(%esp)
80104c87:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c8a:	89 54 24 04          	mov    %edx,0x4(%esp)
80104c8e:	89 04 24             	mov    %eax,(%esp)
80104c91:	e8 a4 3f 00 00       	call   80108c3a <deallocuvm>
80104c96:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c99:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c9d:	75 07                	jne    80104ca6 <growproc+0x85>
      return -1;
80104c9f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ca4:	eb 1e                	jmp    80104cc4 <growproc+0xa3>
  }
  proc->sz = sz;
80104ca6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cac:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104caf:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
80104cb1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cb7:	89 04 24             	mov    %eax,(%esp)
80104cba:	e8 9d 3b 00 00       	call   8010885c <switchuvm>
  return 0;
80104cbf:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104cc4:	c9                   	leave  
80104cc5:	c3                   	ret    

80104cc6 <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
80104cc6:	55                   	push   %ebp
80104cc7:	89 e5                	mov    %esp,%ebp
80104cc9:	57                   	push   %edi
80104cca:	56                   	push   %esi
80104ccb:	53                   	push   %ebx
80104ccc:	83 ec 2c             	sub    $0x2c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
80104ccf:	e8 2a fd ff ff       	call   801049fe <allocproc>
80104cd4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80104cd7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80104cdb:	75 0a                	jne    80104ce7 <fork+0x21>
    return -1;
80104cdd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ce2:	e9 6c 01 00 00       	jmp    80104e53 <fork+0x18d>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
80104ce7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ced:	8b 10                	mov    (%eax),%edx
80104cef:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cf5:	8b 40 04             	mov    0x4(%eax),%eax
80104cf8:	89 54 24 04          	mov    %edx,0x4(%esp)
80104cfc:	89 04 24             	mov    %eax,(%esp)
80104cff:	e8 bd 40 00 00       	call   80108dc1 <copyuvm>
80104d04:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104d07:	89 42 04             	mov    %eax,0x4(%edx)
80104d0a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d0d:	8b 40 04             	mov    0x4(%eax),%eax
80104d10:	85 c0                	test   %eax,%eax
80104d12:	75 2c                	jne    80104d40 <fork+0x7a>
    kfree(np->kstack);
80104d14:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d17:	8b 40 08             	mov    0x8(%eax),%eax
80104d1a:	89 04 24             	mov    %eax,(%esp)
80104d1d:	e8 e6 e5 ff ff       	call   80103308 <kfree>
    np->kstack = 0;
80104d22:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d25:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
    np->state = UNUSED;
80104d2c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d2f:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return -1;
80104d36:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d3b:	e9 13 01 00 00       	jmp    80104e53 <fork+0x18d>
  }
  np->sz = proc->sz;
80104d40:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d46:	8b 10                	mov    (%eax),%edx
80104d48:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d4b:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104d4d:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104d54:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d57:	89 50 14             	mov    %edx,0x14(%eax)
  *np->tf = *proc->tf;
80104d5a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d5d:	8b 50 18             	mov    0x18(%eax),%edx
80104d60:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d66:	8b 40 18             	mov    0x18(%eax),%eax
80104d69:	89 c3                	mov    %eax,%ebx
80104d6b:	b8 13 00 00 00       	mov    $0x13,%eax
80104d70:	89 d7                	mov    %edx,%edi
80104d72:	89 de                	mov    %ebx,%esi
80104d74:	89 c1                	mov    %eax,%ecx
80104d76:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104d78:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104d7b:	8b 40 18             	mov    0x18(%eax),%eax
80104d7e:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104d85:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
80104d8c:	eb 3d                	jmp    80104dcb <fork+0x105>
    if(proc->ofile[i])
80104d8e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d94:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104d97:	83 c2 08             	add    $0x8,%edx
80104d9a:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104d9e:	85 c0                	test   %eax,%eax
80104da0:	74 25                	je     80104dc7 <fork+0x101>
      np->ofile[i] = filedup(proc->ofile[i]);
80104da2:	8b 5d dc             	mov    -0x24(%ebp),%ebx
80104da5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dab:	8b 55 dc             	mov    -0x24(%ebp),%edx
80104dae:	83 c2 08             	add    $0x8,%edx
80104db1:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104db5:	89 04 24             	mov    %eax,(%esp)
80104db8:	e8 5c c9 ff ff       	call   80101719 <filedup>
80104dbd:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104dc0:	8d 4b 08             	lea    0x8(%ebx),%ecx
80104dc3:	89 44 8a 08          	mov    %eax,0x8(%edx,%ecx,4)
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104dc7:	83 45 dc 01          	addl   $0x1,-0x24(%ebp)
80104dcb:	83 7d dc 0f          	cmpl   $0xf,-0x24(%ebp)
80104dcf:	7e bd                	jle    80104d8e <fork+0xc8>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
80104dd1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dd7:	8b 40 68             	mov    0x68(%eax),%eax
80104dda:	89 04 24             	mov    %eax,(%esp)
80104ddd:	e8 5d d2 ff ff       	call   8010203f <idup>
80104de2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104de5:	89 42 68             	mov    %eax,0x68(%edx)

  safestrcpy(np->name, proc->name, sizeof(proc->name));
80104de8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dee:	8d 50 6c             	lea    0x6c(%eax),%edx
80104df1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104df4:	83 c0 6c             	add    $0x6c,%eax
80104df7:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80104dfe:	00 
80104dff:	89 54 24 04          	mov    %edx,0x4(%esp)
80104e03:	89 04 24             	mov    %eax,(%esp)
80104e06:	e8 8a 0f 00 00       	call   80105d95 <safestrcpy>

  pid = np->pid;
80104e0b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e0e:	8b 40 10             	mov    0x10(%eax),%eax
80104e11:	89 45 e0             	mov    %eax,-0x20(%ebp)

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
80104e14:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104e1b:	e8 df 0a 00 00       	call   801058ff <acquire>
  np->state = RUNNABLE;
80104e20:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e23:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  //cprintf("pid %d tickstart %d\n", np->pid, ticks);
  np->tickstart = ticks;
80104e2a:	a1 74 64 11 80       	mov    0x80116474,%eax
80104e2f:	89 c2                	mov    %eax,%edx
80104e31:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e34:	89 50 7c             	mov    %edx,0x7c(%eax)
  np->tickfirstrun = 0;
80104e37:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104e3a:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80104e41:	00 00 00 
  release(&ptable.lock);
80104e44:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104e4b:	e8 18 0b 00 00       	call   80105968 <release>

  return pid;
80104e50:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80104e53:	83 c4 2c             	add    $0x2c,%esp
80104e56:	5b                   	pop    %ebx
80104e57:	5e                   	pop    %esi
80104e58:	5f                   	pop    %edi
80104e59:	5d                   	pop    %ebp
80104e5a:	c3                   	ret    

80104e5b <exit>:
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
80104e5b:	55                   	push   %ebp
80104e5c:	89 e5                	mov    %esp,%ebp
80104e5e:	53                   	push   %ebx
80104e5f:	83 ec 24             	sub    $0x24,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
80104e62:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104e69:	a1 98 c6 10 80       	mov    0x8010c698,%eax
80104e6e:	39 c2                	cmp    %eax,%edx
80104e70:	75 0c                	jne    80104e7e <exit+0x23>
    panic("init exiting");
80104e72:	c7 04 24 10 95 10 80 	movl   $0x80109510,(%esp)
80104e79:	e8 eb b9 ff ff       	call   80100869 <panic>

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104e7e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104e85:	eb 44                	jmp    80104ecb <exit+0x70>
    if(proc->ofile[fd]){
80104e87:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e8d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104e90:	83 c2 08             	add    $0x8,%edx
80104e93:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104e97:	85 c0                	test   %eax,%eax
80104e99:	74 2c                	je     80104ec7 <exit+0x6c>
      fileclose(proc->ofile[fd]);
80104e9b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ea1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ea4:	83 c2 08             	add    $0x8,%edx
80104ea7:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104eab:	89 04 24             	mov    %eax,(%esp)
80104eae:	e8 ae c8 ff ff       	call   80101761 <fileclose>
      proc->ofile[fd] = 0;
80104eb3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104eb9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ebc:	83 c2 08             	add    $0x8,%edx
80104ebf:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80104ec6:	00 

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104ec7:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104ecb:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104ecf:	7e b6                	jle    80104e87 <exit+0x2c>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
80104ed1:	e8 94 eb ff ff       	call   80103a6a <begin_op>
  iput(proc->cwd);
80104ed6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104edc:	8b 40 68             	mov    0x68(%eax),%eax
80104edf:	89 04 24             	mov    %eax,(%esp)
80104ee2:	e8 46 d3 ff ff       	call   8010222d <iput>
  end_op();
80104ee7:	e8 00 ec ff ff       	call   80103aec <end_op>
  proc->cwd = 0;
80104eec:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ef2:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)

  acquire(&ptable.lock);
80104ef9:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80104f00:	e8 fa 09 00 00       	call   801058ff <acquire>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104f05:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f0b:	8b 40 14             	mov    0x14(%eax),%eax
80104f0e:	89 04 24             	mov    %eax,(%esp)
80104f11:	e8 a2 07 00 00       	call   801056b8 <wakeup1>

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f16:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
80104f1d:	eb 3b                	jmp    80104f5a <exit+0xff>
    if(p->parent == proc){
80104f1f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f22:	8b 50 14             	mov    0x14(%eax),%edx
80104f25:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f2b:	39 c2                	cmp    %eax,%edx
80104f2d:	75 24                	jne    80104f53 <exit+0xf8>
      p->parent = initproc;
80104f2f:	8b 15 98 c6 10 80    	mov    0x8010c698,%edx
80104f35:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f38:	89 50 14             	mov    %edx,0x14(%eax)
      if(p->state == ZOMBIE)
80104f3b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104f3e:	8b 40 0c             	mov    0xc(%eax),%eax
80104f41:	83 f8 05             	cmp    $0x5,%eax
80104f44:	75 0d                	jne    80104f53 <exit+0xf8>
        wakeup1(initproc);
80104f46:	a1 98 c6 10 80       	mov    0x8010c698,%eax
80104f4b:	89 04 24             	mov    %eax,(%esp)
80104f4e:	e8 65 07 00 00       	call   801056b8 <wakeup1>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f53:	81 45 f0 88 00 00 00 	addl   $0x88,-0x10(%ebp)
80104f5a:	b8 14 64 11 80       	mov    $0x80116414,%eax
80104f5f:	39 45 f0             	cmp    %eax,-0x10(%ebp)
80104f62:	72 bb                	jb     80104f1f <exit+0xc4>
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
80104f64:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f6a:	c7 40 0c 05 00 00 00 	movl   $0x5,0xc(%eax)
  if(capturing && proc->tickfirstrun > 0) { // may still be 0 if not capturing when proc started
80104f71:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
80104f76:	85 c0                	test   %eax,%eax
80104f78:	74 6a                	je     80104fe4 <exit+0x189>
80104f7a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f80:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
80104f86:	85 c0                	test   %eax,%eax
80104f88:	7e 5a                	jle    80104fe4 <exit+0x189>
      cs.proc_exit_count++;
80104f8a:	a1 24 64 11 80       	mov    0x80116424,%eax
80104f8f:	83 c0 01             	add    $0x1,%eax
80104f92:	a3 24 64 11 80       	mov    %eax,0x80116424
      cs.proc_tick_count += (ticks - proc->tickstart);
80104f97:	a1 28 64 11 80       	mov    0x80116428,%eax
80104f9c:	89 c2                	mov    %eax,%edx
80104f9e:	8b 0d 74 64 11 80    	mov    0x80116474,%ecx
80104fa4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104faa:	8b 40 7c             	mov    0x7c(%eax),%eax
80104fad:	89 cb                	mov    %ecx,%ebx
80104faf:	29 c3                	sub    %eax,%ebx
80104fb1:	89 d8                	mov    %ebx,%eax
80104fb3:	8d 04 02             	lea    (%edx,%eax,1),%eax
80104fb6:	a3 28 64 11 80       	mov    %eax,0x80116428
      cs.proc_firstrun_count += (proc->tickfirstrun - proc->tickstart);
80104fbb:	8b 15 2c 64 11 80    	mov    0x8011642c,%edx
80104fc1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fc7:	8b 88 80 00 00 00    	mov    0x80(%eax),%ecx
80104fcd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fd3:	8b 40 7c             	mov    0x7c(%eax),%eax
80104fd6:	89 cb                	mov    %ecx,%ebx
80104fd8:	29 c3                	sub    %eax,%ebx
80104fda:	89 d8                	mov    %ebx,%eax
80104fdc:	8d 04 02             	lea    (%edx,%eax,1),%eax
80104fdf:	a3 2c 64 11 80       	mov    %eax,0x8011642c
  }
  sched();
80104fe4:	e8 11 05 00 00       	call   801054fa <sched>
  panic("zombie exit");
80104fe9:	c7 04 24 1d 95 10 80 	movl   $0x8010951d,(%esp)
80104ff0:	e8 74 b8 ff ff       	call   80100869 <panic>

80104ff5 <wait>:

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
80104ff5:	55                   	push   %ebp
80104ff6:	89 e5                	mov    %esp,%ebp
80104ff8:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80104ffb:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105002:	e8 f8 08 00 00       	call   801058ff <acquire>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
80105007:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010500e:	c7 45 ec 14 42 11 80 	movl   $0x80114214,-0x14(%ebp)
80105015:	e9 9d 00 00 00       	jmp    801050b7 <wait+0xc2>
      if(p->parent != proc)
8010501a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010501d:	8b 50 14             	mov    0x14(%eax),%edx
80105020:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105026:	39 c2                	cmp    %eax,%edx
80105028:	0f 85 81 00 00 00    	jne    801050af <wait+0xba>
        continue;
      havekids = 1;
8010502e:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      if(p->state == ZOMBIE){
80105035:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105038:	8b 40 0c             	mov    0xc(%eax),%eax
8010503b:	83 f8 05             	cmp    $0x5,%eax
8010503e:	75 70                	jne    801050b0 <wait+0xbb>
        // Found one.
        pid = p->pid;
80105040:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105043:	8b 40 10             	mov    0x10(%eax),%eax
80105046:	89 45 f4             	mov    %eax,-0xc(%ebp)
        kfree(p->kstack);
80105049:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010504c:	8b 40 08             	mov    0x8(%eax),%eax
8010504f:	89 04 24             	mov    %eax,(%esp)
80105052:	e8 b1 e2 ff ff       	call   80103308 <kfree>
        p->kstack = 0;
80105057:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010505a:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80105061:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105064:	8b 40 04             	mov    0x4(%eax),%eax
80105067:	89 04 24             	mov    %eax,(%esp)
8010506a:	e8 81 3c 00 00       	call   80108cf0 <freevm>
        p->state = UNUSED;
8010506f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105072:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
        p->pid = 0;
80105079:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010507c:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
80105083:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105086:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
        p->name[0] = 0;
8010508d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105090:	c6 40 6c 00          	movb   $0x0,0x6c(%eax)
        p->killed = 0;
80105094:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105097:	c7 40 24 00 00 00 00 	movl   $0x0,0x24(%eax)
        release(&ptable.lock);
8010509e:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801050a5:	e8 be 08 00 00       	call   80105968 <release>
        return pid;
801050aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801050ad:	eb 57                	jmp    80105106 <wait+0x111>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
801050af:	90                   	nop

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801050b0:	81 45 ec 88 00 00 00 	addl   $0x88,-0x14(%ebp)
801050b7:	b8 14 64 11 80       	mov    $0x80116414,%eax
801050bc:	39 45 ec             	cmp    %eax,-0x14(%ebp)
801050bf:	0f 82 55 ff ff ff    	jb     8010501a <wait+0x25>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
801050c5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801050c9:	74 0d                	je     801050d8 <wait+0xe3>
801050cb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050d1:	8b 40 24             	mov    0x24(%eax),%eax
801050d4:	85 c0                	test   %eax,%eax
801050d6:	74 13                	je     801050eb <wait+0xf6>
      release(&ptable.lock);
801050d8:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801050df:	e8 84 08 00 00       	call   80105968 <release>
      return -1;
801050e4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050e9:	eb 1b                	jmp    80105106 <wait+0x111>
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
801050eb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050f1:	c7 44 24 04 e0 41 11 	movl   $0x801141e0,0x4(%esp)
801050f8:	80 
801050f9:	89 04 24             	mov    %eax,(%esp)
801050fc:	e8 1c 05 00 00       	call   8010561d <sleep>
  }
80105101:	e9 01 ff ff ff       	jmp    80105007 <wait+0x12>
}
80105106:	c9                   	leave  
80105107:	c3                   	ret    

80105108 <switch_scheduler>:
//  - eventually that process transfers control
//      via swtch back to the scheduler

int sched_type = SCHED_ROUND_ROBIN;

void switch_scheduler(int new_sched_type) {
80105108:	55                   	push   %ebp
80105109:	89 e5                	mov    %esp,%ebp
    // check for a valid scheduler type
    if(new_sched_type >= SCHED_ROUND_ROBIN && new_sched_type <= SCHED_MLFQ) {
8010510b:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010510f:	78 0e                	js     8010511f <switch_scheduler+0x17>
80105111:	83 7d 08 03          	cmpl   $0x3,0x8(%ebp)
80105115:	7f 08                	jg     8010511f <switch_scheduler+0x17>
        sched_type = new_sched_type;
80105117:	8b 45 08             	mov    0x8(%ebp),%eax
8010511a:	a3 88 c6 10 80       	mov    %eax,0x8010c688
    }
}
8010511f:	5d                   	pop    %ebp
80105120:	c3                   	ret    

80105121 <sys_switch_scheduler>:

int sys_switch_scheduler(void)
{
80105121:	55                   	push   %ebp
80105122:	89 e5                	mov    %esp,%ebp
80105124:	83 ec 28             	sub    $0x28,%esp
    int new_sched_type;
    if(argint(0, &new_sched_type) < 0)
80105127:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010512a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010512e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105135:	e8 84 0d 00 00       	call   80105ebe <argint>
8010513a:	85 c0                	test   %eax,%eax
8010513c:	79 07                	jns    80105145 <sys_switch_scheduler+0x24>
      return -1;
8010513e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105143:	eb 10                	jmp    80105155 <sys_switch_scheduler+0x34>
    switch_scheduler(new_sched_type);
80105145:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105148:	89 04 24             	mov    %eax,(%esp)
8010514b:	e8 b8 ff ff ff       	call   80105108 <switch_scheduler>
    return 0;
80105150:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105155:	c9                   	leave  
80105156:	c3                   	ret    

80105157 <scheduler>:
int lastprocidx = 0;
int lastpriority = 0;
int mostrecent = 0;
void
scheduler(void)
{
80105157:	55                   	push   %ebp
80105158:	89 e5                	mov    %esp,%ebp
8010515a:	83 ec 38             	sub    $0x38,%esp
  int idx;
  int i;

  for(;;){
    // Enable interrupts on this processor.
    sti();
8010515d:	e8 7a f8 ff ff       	call   801049dc <sti>

    acquire(&ptable.lock);
80105162:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105169:	e8 91 07 00 00       	call   801058ff <acquire>
    p = 0;
8010516e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    if(sched_type == SCHED_ROUND_ROBIN) {
80105175:	a1 88 c6 10 80       	mov    0x8010c688,%eax
8010517a:	85 c0                	test   %eax,%eax
8010517c:	0f 85 81 00 00 00    	jne    80105203 <scheduler+0xac>
        // round-robin is reimplemented because I changed the nesting of the
        // outer infinite for() and the if's inside, to support different
        // schedulers
        idx = (lastprocidx + 1) % NPROC;
80105182:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
80105187:	8d 50 01             	lea    0x1(%eax),%edx
8010518a:	89 d0                	mov    %edx,%eax
8010518c:	c1 f8 1f             	sar    $0x1f,%eax
8010518f:	c1 e8 1a             	shr    $0x1a,%eax
80105192:	01 c2                	add    %eax,%edx
80105194:	83 e2 3f             	and    $0x3f,%edx
80105197:	89 d1                	mov    %edx,%ecx
80105199:	29 c1                	sub    %eax,%ecx
8010519b:	89 c8                	mov    %ecx,%eax
8010519d:	89 45 ec             	mov    %eax,-0x14(%ebp)
801051a0:	eb 01                	jmp    801051a3 <scheduler+0x4c>
            idx = (idx + 1) % NPROC;
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
                p = 0;
                break;
            }
        }
801051a2:	90                   	nop
        // round-robin is reimplemented because I changed the nesting of the
        // outer infinite for() and the if's inside, to support different
        // schedulers
        idx = (lastprocidx + 1) % NPROC;
        while(1) {
            p = &ptable.proc[idx];
801051a3:	8b 45 ec             	mov    -0x14(%ebp),%eax
801051a6:	c1 e0 03             	shl    $0x3,%eax
801051a9:	89 c2                	mov    %eax,%edx
801051ab:	c1 e2 04             	shl    $0x4,%edx
801051ae:	01 d0                	add    %edx,%eax
801051b0:	05 14 42 11 80       	add    $0x80114214,%eax
801051b5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            if(p->state == RUNNABLE) {
801051b8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801051bb:	8b 40 0c             	mov    0xc(%eax),%eax
801051be:	83 f8 03             	cmp    $0x3,%eax
801051c1:	75 0a                	jne    801051cd <scheduler+0x76>
                lastprocidx = idx;
801051c3:	8b 45 ec             	mov    -0x14(%ebp),%eax
801051c6:	a3 8c c6 10 80       	mov    %eax,0x8010c68c
                break;
801051cb:	eb 31                	jmp    801051fe <scheduler+0xa7>
            }
            idx = (idx + 1) % NPROC;
801051cd:	8b 45 ec             	mov    -0x14(%ebp),%eax
801051d0:	8d 50 01             	lea    0x1(%eax),%edx
801051d3:	89 d0                	mov    %edx,%eax
801051d5:	c1 f8 1f             	sar    $0x1f,%eax
801051d8:	c1 e8 1a             	shr    $0x1a,%eax
801051db:	01 c2                	add    %eax,%edx
801051dd:	83 e2 3f             	and    $0x3f,%edx
801051e0:	89 d1                	mov    %edx,%ecx
801051e2:	29 c1                	sub    %eax,%ecx
801051e4:	89 c8                	mov    %ecx,%eax
801051e6:	89 45 ec             	mov    %eax,-0x14(%ebp)
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
801051e9:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
801051ee:	83 c0 01             	add    $0x1,%eax
801051f1:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801051f4:	75 ac                	jne    801051a2 <scheduler+0x4b>
                p = 0;
801051f6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
                break;
801051fd:	90                   	nop
801051fe:	e9 32 02 00 00       	jmp    80105435 <scheduler+0x2de>
            }
        }
    }
    else if(sched_type == SCHED_FIFO) {
80105203:	a1 88 c6 10 80       	mov    0x8010c688,%eax
80105208:	83 f8 01             	cmp    $0x1,%eax
8010520b:	75 6a                	jne    80105277 <scheduler+0x120>
	current = &ptable.proc[0];
8010520d:	c7 45 e8 14 42 11 80 	movl   $0x80114214,-0x18(%ebp)
    	for(i = 0; i < NPROC; i++){
80105214:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010521b:	eb 49                	jmp    80105266 <scheduler+0x10f>
		p = &ptable.proc[i];
8010521d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105220:	c1 e0 03             	shl    $0x3,%eax
80105223:	89 c2                	mov    %eax,%edx
80105225:	c1 e2 04             	shl    $0x4,%edx
80105228:	01 d0                	add    %edx,%eax
8010522a:	05 14 42 11 80       	add    $0x80114214,%eax
8010522f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		if(p->state == RUNNABLE){
80105232:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105235:	8b 40 0c             	mov    0xc(%eax),%eax
80105238:	83 f8 03             	cmp    $0x3,%eax
8010523b:	75 25                	jne    80105262 <scheduler+0x10b>
			if(p->tickstart < current->tickstart)
8010523d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105240:	8b 50 7c             	mov    0x7c(%eax),%edx
80105243:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105246:	8b 40 7c             	mov    0x7c(%eax),%eax
80105249:	39 c2                	cmp    %eax,%edx
8010524b:	7d 15                	jge    80105262 <scheduler+0x10b>
				current = &ptable.proc[i];
8010524d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105250:	c1 e0 03             	shl    $0x3,%eax
80105253:	89 c2                	mov    %eax,%edx
80105255:	c1 e2 04             	shl    $0x4,%edx
80105258:	01 d0                	add    %edx,%eax
8010525a:	05 14 42 11 80       	add    $0x80114214,%eax
8010525f:	89 45 e8             	mov    %eax,-0x18(%ebp)
            }
        }
    }
    else if(sched_type == SCHED_FIFO) {
	current = &ptable.proc[0];
    	for(i = 0; i < NPROC; i++){
80105262:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80105266:	83 7d f0 3f          	cmpl   $0x3f,-0x10(%ebp)
8010526a:	7e b1                	jle    8010521d <scheduler+0xc6>
		if(p->state == RUNNABLE){
			if(p->tickstart < current->tickstart)
				current = &ptable.proc[i];
		}
	}
	p = current;
8010526c:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010526f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80105272:	e9 be 01 00 00       	jmp    80105435 <scheduler+0x2de>
    }
    else if(sched_type == SCHED_LIFO) {
80105277:	a1 88 c6 10 80       	mov    0x8010c688,%eax
8010527c:	83 f8 02             	cmp    $0x2,%eax
8010527f:	75 64                	jne    801052e5 <scheduler+0x18e>
	struct proc *current;
	current = &ptable.proc[0];
80105281:	c7 45 f4 14 42 11 80 	movl   $0x80114214,-0xc(%ebp)
	for(i = 0; i < NPROC; i++){
80105288:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010528f:	eb 49                	jmp    801052da <scheduler+0x183>
		p = &ptable.proc[i];
80105291:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105294:	c1 e0 03             	shl    $0x3,%eax
80105297:	89 c2                	mov    %eax,%edx
80105299:	c1 e2 04             	shl    $0x4,%edx
8010529c:	01 d0                	add    %edx,%eax
8010529e:	05 14 42 11 80       	add    $0x80114214,%eax
801052a3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		if(p->state == RUNNABLE){
801052a6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801052a9:	8b 40 0c             	mov    0xc(%eax),%eax
801052ac:	83 f8 03             	cmp    $0x3,%eax
801052af:	75 25                	jne    801052d6 <scheduler+0x17f>
			if(p->tickstart > current->tickstart)
801052b1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801052b4:	8b 50 7c             	mov    0x7c(%eax),%edx
801052b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801052ba:	8b 40 7c             	mov    0x7c(%eax),%eax
801052bd:	39 c2                	cmp    %eax,%edx
801052bf:	7e 15                	jle    801052d6 <scheduler+0x17f>
				current = &ptable.proc[i];
801052c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801052c4:	c1 e0 03             	shl    $0x3,%eax
801052c7:	89 c2                	mov    %eax,%edx
801052c9:	c1 e2 04             	shl    $0x4,%edx
801052cc:	01 d0                	add    %edx,%eax
801052ce:	05 14 42 11 80       	add    $0x80114214,%eax
801052d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
	p = current;
    }
    else if(sched_type == SCHED_LIFO) {
	struct proc *current;
	current = &ptable.proc[0];
	for(i = 0; i < NPROC; i++){
801052d6:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801052da:	83 7d f0 3f          	cmpl   $0x3f,-0x10(%ebp)
801052de:	7e b1                	jle    80105291 <scheduler+0x13a>
801052e0:	e9 50 01 00 00       	jmp    80105435 <scheduler+0x2de>
			if(p->tickstart > current->tickstart)
				current = &ptable.proc[i];
		}
	}
    }
    else if(sched_type == SCHED_MLFQ) {
801052e5:	a1 88 c6 10 80       	mov    0x8010c688,%eax
801052ea:	83 f8 03             	cmp    $0x3,%eax
801052ed:	0f 85 42 01 00 00    	jne    80105435 <scheduler+0x2de>
        // - rule 1: if priority(a) > priority(b), a runs (on different queues)
	// - rule 2: if priority(a) = priority(b), a and b run in round robin (on same queue)
        // - rule 3: when job is created, has highest priority
        // - rule 4: when a job uses up its time allotment, its priority is reduced by 1
        // - rule 5: after some fixed time period, all jobs move to highest priority
	for(i = 0; i < NPROC; i++){
801052f3:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801052fa:	eb 65                	jmp    80105361 <scheduler+0x20a>
		current = &ptable.proc[i];
801052fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801052ff:	c1 e0 03             	shl    $0x3,%eax
80105302:	89 c2                	mov    %eax,%edx
80105304:	c1 e2 04             	shl    $0x4,%edx
80105307:	01 d0                	add    %edx,%eax
80105309:	05 14 42 11 80       	add    $0x80114214,%eax
8010530e:	89 45 e8             	mov    %eax,-0x18(%ebp)
		if(current->state == RUNNABLE){
80105311:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105314:	8b 40 0c             	mov    0xc(%eax),%eax
80105317:	83 f8 03             	cmp    $0x3,%eax
			if(current->priority > current->priority)
				current = &ptable.proc[i];
		}
		if(current->tickstart > mostrecent)
8010531a:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010531d:	8b 50 7c             	mov    0x7c(%eax),%edx
80105320:	a1 94 c6 10 80       	mov    0x8010c694,%eax
80105325:	39 c2                	cmp    %eax,%edx
80105327:	7e 0b                	jle    80105334 <scheduler+0x1dd>
			mostrecent = current->tickstart;
80105329:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010532c:	8b 40 7c             	mov    0x7c(%eax),%eax
8010532f:	a3 94 c6 10 80       	mov    %eax,0x8010c694
		if((ticks - current->tickfirstrun) > 100000)
80105334:	8b 15 74 64 11 80    	mov    0x80116474,%edx
8010533a:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010533d:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
80105343:	89 d1                	mov    %edx,%ecx
80105345:	29 c1                	sub    %eax,%ecx
80105347:	89 c8                	mov    %ecx,%eax
80105349:	3d a0 86 01 00       	cmp    $0x186a0,%eax
8010534e:	76 0d                	jbe    8010535d <scheduler+0x206>
			current->priority = 4;
80105350:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105353:	c7 80 84 00 00 00 04 	movl   $0x4,0x84(%eax)
8010535a:	00 00 00 
        // - rule 1: if priority(a) > priority(b), a runs (on different queues)
	// - rule 2: if priority(a) = priority(b), a and b run in round robin (on same queue)
        // - rule 3: when job is created, has highest priority
        // - rule 4: when a job uses up its time allotment, its priority is reduced by 1
        // - rule 5: after some fixed time period, all jobs move to highest priority
	for(i = 0; i < NPROC; i++){
8010535d:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80105361:	83 7d f0 3f          	cmpl   $0x3f,-0x10(%ebp)
80105365:	7e 95                	jle    801052fc <scheduler+0x1a5>
		if(current->tickstart > mostrecent)
			mostrecent = current->tickstart;
		if((ticks - current->tickfirstrun) > 100000)
			current->priority = 4;
	}
	idx = (lastprocidx + 1) % NPROC;
80105367:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
8010536c:	8d 50 01             	lea    0x1(%eax),%edx
8010536f:	89 d0                	mov    %edx,%eax
80105371:	c1 f8 1f             	sar    $0x1f,%eax
80105374:	c1 e8 1a             	shr    $0x1a,%eax
80105377:	01 c2                	add    %eax,%edx
80105379:	83 e2 3f             	and    $0x3f,%edx
8010537c:	89 d1                	mov    %edx,%ecx
8010537e:	29 c1                	sub    %eax,%ecx
80105380:	89 c8                	mov    %ecx,%eax
80105382:	89 45 ec             	mov    %eax,-0x14(%ebp)
80105385:	eb 01                	jmp    80105388 <scheduler+0x231>
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
                p = 0;
		lastpriority = 0;
                break;
            }
        }
80105387:	90                   	nop
		if((ticks - current->tickfirstrun) > 100000)
			current->priority = 4;
	}
	idx = (lastprocidx + 1) % NPROC;
        while(1) {
            p = &ptable.proc[idx];
80105388:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010538b:	c1 e0 03             	shl    $0x3,%eax
8010538e:	89 c2                	mov    %eax,%edx
80105390:	c1 e2 04             	shl    $0x4,%edx
80105393:	01 d0                	add    %edx,%eax
80105395:	05 14 42 11 80       	add    $0x80114214,%eax
8010539a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            if(p->state == RUNNABLE && p->priority >= lastpriority) {
8010539d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053a0:	8b 40 0c             	mov    0xc(%eax),%eax
801053a3:	83 f8 03             	cmp    $0x3,%eax
801053a6:	75 4c                	jne    801053f4 <scheduler+0x29d>
801053a8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053ab:	8b 90 84 00 00 00    	mov    0x84(%eax),%edx
801053b1:	a1 90 c6 10 80       	mov    0x8010c690,%eax
801053b6:	39 c2                	cmp    %eax,%edx
801053b8:	7c 3a                	jl     801053f4 <scheduler+0x29d>
                lastprocidx = idx;
801053ba:	8b 45 ec             	mov    -0x14(%ebp),%eax
801053bd:	a3 8c c6 10 80       	mov    %eax,0x8010c68c
		lastpriority = p->priority;
801053c2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053c5:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
801053cb:	a3 90 c6 10 80       	mov    %eax,0x8010c690
		if(p->priority > 0)
801053d0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053d3:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
801053d9:	85 c0                	test   %eax,%eax
801053db:	7e 57                	jle    80105434 <scheduler+0x2dd>
			p->priority = p->priority - 1;
801053dd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053e0:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
801053e6:	8d 50 ff             	lea    -0x1(%eax),%edx
801053e9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801053ec:	89 90 84 00 00 00    	mov    %edx,0x84(%eax)
                break;
801053f2:	eb 41                	jmp    80105435 <scheduler+0x2de>
            }
            idx = (idx + 1) % NPROC;
801053f4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801053f7:	8d 50 01             	lea    0x1(%eax),%edx
801053fa:	89 d0                	mov    %edx,%eax
801053fc:	c1 f8 1f             	sar    $0x1f,%eax
801053ff:	c1 e8 1a             	shr    $0x1a,%eax
80105402:	01 c2                	add    %eax,%edx
80105404:	83 e2 3f             	and    $0x3f,%edx
80105407:	89 d1                	mov    %edx,%ecx
80105409:	29 c1                	sub    %eax,%ecx
8010540b:	89 c8                	mov    %ecx,%eax
8010540d:	89 45 ec             	mov    %eax,-0x14(%ebp)
            if(idx == (lastprocidx+1)) { // ran out of procs to check, nothing runnable
80105410:	a1 8c c6 10 80       	mov    0x8010c68c,%eax
80105415:	83 c0 01             	add    $0x1,%eax
80105418:	3b 45 ec             	cmp    -0x14(%ebp),%eax
8010541b:	0f 85 66 ff ff ff    	jne    80105387 <scheduler+0x230>
                p = 0;
80105421:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
		lastpriority = 0;
80105428:	c7 05 90 c6 10 80 00 	movl   $0x0,0x8010c690
8010542f:	00 00 00 
                break;
80105432:	eb 01                	jmp    80105435 <scheduler+0x2de>
            if(p->state == RUNNABLE && p->priority >= lastpriority) {
                lastprocidx = idx;
		lastpriority = p->priority;
		if(p->priority > 0)
			p->priority = p->priority - 1;
                break;
80105434:	90                   	nop
            }
        }
    }

    // if schedulers are broken and p == 0, try again to find a proc
    if(p == 0) {
80105435:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80105439:	75 28                	jne    80105463 <scheduler+0x30c>
        for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
8010543b:	c7 45 e4 14 42 11 80 	movl   $0x80114214,-0x1c(%ebp)
80105442:	eb 12                	jmp    80105456 <scheduler+0x2ff>
            if(p->state == RUNNABLE) break;
80105444:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105447:	8b 40 0c             	mov    0xc(%eax),%eax
8010544a:	83 f8 03             	cmp    $0x3,%eax
8010544d:	74 13                	je     80105462 <scheduler+0x30b>
        }
    }

    // if schedulers are broken and p == 0, try again to find a proc
    if(p == 0) {
        for(p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
8010544f:	81 45 e4 88 00 00 00 	addl   $0x88,-0x1c(%ebp)
80105456:	b8 14 64 11 80       	mov    $0x80116414,%eax
8010545b:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
8010545e:	72 e4                	jb     80105444 <scheduler+0x2ed>
80105460:	eb 01                	jmp    80105463 <scheduler+0x30c>
            if(p->state == RUNNABLE) break;
80105462:	90                   	nop
        }
    }

    // Now, p should be set to the process that we will switch to;
    // or, if no such process was found, p will be bogus and ignored
    if(p >= ptable.proc && p < &ptable.proc[NPROC]) {
80105463:	81 7d e4 14 42 11 80 	cmpl   $0x80114214,-0x1c(%ebp)
8010546a:	72 7d                	jb     801054e9 <scheduler+0x392>
8010546c:	b8 14 64 11 80       	mov    $0x80116414,%eax
80105471:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
80105474:	73 73                	jae    801054e9 <scheduler+0x392>
        // Switch to chosen process.  It is the process's job
        // to release ptable.lock and then reacquire it
        // before jumping back to us.
        proc = p;
80105476:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105479:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
        //cprintf("switching to %d\n", proc->pid);
        switchuvm(p);
8010547f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105482:	89 04 24             	mov    %eax,(%esp)
80105485:	e8 d2 33 00 00       	call   8010885c <switchuvm>
        p->state = RUNNING;
8010548a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010548d:	c7 40 0c 04 00 00 00 	movl   $0x4,0xc(%eax)
        if(capturing && p->tickfirstrun == 0)
80105494:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
80105499:	85 c0                	test   %eax,%eax
8010549b:	74 1d                	je     801054ba <scheduler+0x363>
8010549d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801054a0:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
801054a6:	85 c0                	test   %eax,%eax
801054a8:	75 10                	jne    801054ba <scheduler+0x363>
            p->tickfirstrun = ticks;
801054aa:	a1 74 64 11 80       	mov    0x80116474,%eax
801054af:	89 c2                	mov    %eax,%edx
801054b1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801054b4:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)
        swtch(&cpu->scheduler, proc->context);
801054ba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801054c0:	8b 40 1c             	mov    0x1c(%eax),%eax
801054c3:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801054ca:	83 c2 04             	add    $0x4,%edx
801054cd:	89 44 24 04          	mov    %eax,0x4(%esp)
801054d1:	89 14 24             	mov    %edx,(%esp)
801054d4:	e8 2f 09 00 00       	call   80105e08 <swtch>
        switchkvm();
801054d9:	e8 64 33 00 00       	call   80108842 <switchkvm>

        // Process is done running for now.
        // It should have changed its p->state before coming back.
        proc = 0;
801054de:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801054e5:	00 00 00 00 
    }

    release(&ptable.lock);
801054e9:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801054f0:	e8 73 04 00 00       	call   80105968 <release>

  }
801054f5:	e9 63 fc ff ff       	jmp    8010515d <scheduler+0x6>

801054fa <sched>:

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
801054fa:	55                   	push   %ebp
801054fb:	89 e5                	mov    %esp,%ebp
801054fd:	83 ec 28             	sub    $0x28,%esp
  int intena;

  if(!holding(&ptable.lock))
80105500:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105507:	e8 27 05 00 00       	call   80105a33 <holding>
8010550c:	85 c0                	test   %eax,%eax
8010550e:	75 0c                	jne    8010551c <sched+0x22>
    panic("sched ptable.lock");
80105510:	c7 04 24 29 95 10 80 	movl   $0x80109529,(%esp)
80105517:	e8 4d b3 ff ff       	call   80100869 <panic>
  if(cpu->ncli != 1)
8010551c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105522:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105528:	83 f8 01             	cmp    $0x1,%eax
8010552b:	74 0c                	je     80105539 <sched+0x3f>
    panic("sched locks");
8010552d:	c7 04 24 3b 95 10 80 	movl   $0x8010953b,(%esp)
80105534:	e8 30 b3 ff ff       	call   80100869 <panic>
  if(proc->state == RUNNING)
80105539:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010553f:	8b 40 0c             	mov    0xc(%eax),%eax
80105542:	83 f8 04             	cmp    $0x4,%eax
80105545:	75 0c                	jne    80105553 <sched+0x59>
    panic("sched running");
80105547:	c7 04 24 47 95 10 80 	movl   $0x80109547,(%esp)
8010554e:	e8 16 b3 ff ff       	call   80100869 <panic>
  if(readeflags()&FL_IF)
80105553:	e8 74 f4 ff ff       	call   801049cc <readeflags>
80105558:	25 00 02 00 00       	and    $0x200,%eax
8010555d:	85 c0                	test   %eax,%eax
8010555f:	74 0c                	je     8010556d <sched+0x73>
    panic("sched interruptible");
80105561:	c7 04 24 55 95 10 80 	movl   $0x80109555,(%esp)
80105568:	e8 fc b2 ff ff       	call   80100869 <panic>
  intena = cpu->intena;
8010556d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105573:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105579:	89 45 f4             	mov    %eax,-0xc(%ebp)
  swtch(&proc->context, cpu->scheduler);
8010557c:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105582:	8b 40 04             	mov    0x4(%eax),%eax
80105585:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
8010558c:	83 c2 1c             	add    $0x1c,%edx
8010558f:	89 44 24 04          	mov    %eax,0x4(%esp)
80105593:	89 14 24             	mov    %edx,(%esp)
80105596:	e8 6d 08 00 00       	call   80105e08 <swtch>
  cpu->intena = intena;
8010559b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801055a1:	8b 55 f4             	mov    -0xc(%ebp),%edx
801055a4:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801055aa:	c9                   	leave  
801055ab:	c3                   	ret    

801055ac <yield>:

// Give up the CPU for one scheduling round.
void
yield(void)
{
801055ac:	55                   	push   %ebp
801055ad:	89 e5                	mov    %esp,%ebp
801055af:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801055b2:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801055b9:	e8 41 03 00 00       	call   801058ff <acquire>
  proc->state = RUNNABLE;
801055be:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801055c4:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
801055cb:	e8 2a ff ff ff       	call   801054fa <sched>
  release(&ptable.lock);
801055d0:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801055d7:	e8 8c 03 00 00       	call   80105968 <release>
}
801055dc:	c9                   	leave  
801055dd:	c3                   	ret    

801055de <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
801055de:	55                   	push   %ebp
801055df:	89 e5                	mov    %esp,%ebp
801055e1:	83 ec 18             	sub    $0x18,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
801055e4:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801055eb:	e8 78 03 00 00       	call   80105968 <release>

  if (first) {
801055f0:	a1 20 c0 10 80       	mov    0x8010c020,%eax
801055f5:	85 c0                	test   %eax,%eax
801055f7:	74 22                	je     8010561b <forkret+0x3d>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
801055f9:	c7 05 20 c0 10 80 00 	movl   $0x0,0x8010c020
80105600:	00 00 00 
    iinit(ROOTDEV);
80105603:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010560a:	e8 34 c7 ff ff       	call   80101d43 <iinit>
    initlog(ROOTDEV);
8010560f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105616:	e8 4d e2 ff ff       	call   80103868 <initlog>
  }

  // Return to "caller", actually trapret (see allocproc).
}
8010561b:	c9                   	leave  
8010561c:	c3                   	ret    

8010561d <sleep>:

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
8010561d:	55                   	push   %ebp
8010561e:	89 e5                	mov    %esp,%ebp
80105620:	83 ec 18             	sub    $0x18,%esp
  if(proc == 0)
80105623:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105629:	85 c0                	test   %eax,%eax
8010562b:	75 0c                	jne    80105639 <sleep+0x1c>
    panic("sleep");
8010562d:	c7 04 24 69 95 10 80 	movl   $0x80109569,(%esp)
80105634:	e8 30 b2 ff ff       	call   80100869 <panic>

  if(lk == 0)
80105639:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010563d:	75 0c                	jne    8010564b <sleep+0x2e>
    panic("sleep without lk");
8010563f:	c7 04 24 6f 95 10 80 	movl   $0x8010956f,(%esp)
80105646:	e8 1e b2 ff ff       	call   80100869 <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
8010564b:	81 7d 0c e0 41 11 80 	cmpl   $0x801141e0,0xc(%ebp)
80105652:	74 17                	je     8010566b <sleep+0x4e>
    acquire(&ptable.lock);  //DOC: sleeplock1
80105654:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
8010565b:	e8 9f 02 00 00       	call   801058ff <acquire>
    release(lk);
80105660:	8b 45 0c             	mov    0xc(%ebp),%eax
80105663:	89 04 24             	mov    %eax,(%esp)
80105666:	e8 fd 02 00 00       	call   80105968 <release>
  }

  // Go to sleep.
  proc->chan = chan;
8010566b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105671:	8b 55 08             	mov    0x8(%ebp),%edx
80105674:	89 50 20             	mov    %edx,0x20(%eax)
  proc->state = SLEEPING;
80105677:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010567d:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
80105684:	e8 71 fe ff ff       	call   801054fa <sched>

  // Tidy up.
  proc->chan = 0;
80105689:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010568f:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
80105696:	81 7d 0c e0 41 11 80 	cmpl   $0x801141e0,0xc(%ebp)
8010569d:	74 17                	je     801056b6 <sleep+0x99>
    release(&ptable.lock);
8010569f:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
801056a6:	e8 bd 02 00 00       	call   80105968 <release>
    acquire(lk);
801056ab:	8b 45 0c             	mov    0xc(%ebp),%eax
801056ae:	89 04 24             	mov    %eax,(%esp)
801056b1:	e8 49 02 00 00       	call   801058ff <acquire>
  }
}
801056b6:	c9                   	leave  
801056b7:	c3                   	ret    

801056b8 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
801056b8:	55                   	push   %ebp
801056b9:	89 e5                	mov    %esp,%ebp
801056bb:	83 ec 10             	sub    $0x10,%esp
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801056be:	c7 45 fc 14 42 11 80 	movl   $0x80114214,-0x4(%ebp)
801056c5:	eb 27                	jmp    801056ee <wakeup1+0x36>
    if(p->state == SLEEPING && p->chan == chan)
801056c7:	8b 45 fc             	mov    -0x4(%ebp),%eax
801056ca:	8b 40 0c             	mov    0xc(%eax),%eax
801056cd:	83 f8 02             	cmp    $0x2,%eax
801056d0:	75 15                	jne    801056e7 <wakeup1+0x2f>
801056d2:	8b 45 fc             	mov    -0x4(%ebp),%eax
801056d5:	8b 40 20             	mov    0x20(%eax),%eax
801056d8:	3b 45 08             	cmp    0x8(%ebp),%eax
801056db:	75 0a                	jne    801056e7 <wakeup1+0x2f>
      p->state = RUNNABLE;
801056dd:	8b 45 fc             	mov    -0x4(%ebp),%eax
801056e0:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801056e7:	81 45 fc 88 00 00 00 	addl   $0x88,-0x4(%ebp)
801056ee:	b8 14 64 11 80       	mov    $0x80116414,%eax
801056f3:	39 45 fc             	cmp    %eax,-0x4(%ebp)
801056f6:	72 cf                	jb     801056c7 <wakeup1+0xf>
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
801056f8:	c9                   	leave  
801056f9:	c3                   	ret    

801056fa <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801056fa:	55                   	push   %ebp
801056fb:	89 e5                	mov    %esp,%ebp
801056fd:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);
80105700:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105707:	e8 f3 01 00 00       	call   801058ff <acquire>
  wakeup1(chan);
8010570c:	8b 45 08             	mov    0x8(%ebp),%eax
8010570f:	89 04 24             	mov    %eax,(%esp)
80105712:	e8 a1 ff ff ff       	call   801056b8 <wakeup1>
  release(&ptable.lock);
80105717:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
8010571e:	e8 45 02 00 00       	call   80105968 <release>
}
80105723:	c9                   	leave  
80105724:	c3                   	ret    

80105725 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80105725:	55                   	push   %ebp
80105726:	89 e5                	mov    %esp,%ebp
80105728:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  acquire(&ptable.lock);
8010572b:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105732:	e8 c8 01 00 00       	call   801058ff <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105737:	c7 45 f4 14 42 11 80 	movl   $0x80114214,-0xc(%ebp)
8010573e:	eb 44                	jmp    80105784 <kill+0x5f>
    if(p->pid == pid){
80105740:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105743:	8b 40 10             	mov    0x10(%eax),%eax
80105746:	3b 45 08             	cmp    0x8(%ebp),%eax
80105749:	75 32                	jne    8010577d <kill+0x58>
      p->killed = 1;
8010574b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010574e:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80105755:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105758:	8b 40 0c             	mov    0xc(%eax),%eax
8010575b:	83 f8 02             	cmp    $0x2,%eax
8010575e:	75 0a                	jne    8010576a <kill+0x45>
        p->state = RUNNABLE;
80105760:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105763:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
8010576a:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105771:	e8 f2 01 00 00       	call   80105968 <release>
      return 0;
80105776:	b8 00 00 00 00       	mov    $0x0,%eax
8010577b:	eb 22                	jmp    8010579f <kill+0x7a>
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010577d:	81 45 f4 88 00 00 00 	addl   $0x88,-0xc(%ebp)
80105784:	b8 14 64 11 80       	mov    $0x80116414,%eax
80105789:	39 45 f4             	cmp    %eax,-0xc(%ebp)
8010578c:	72 b2                	jb     80105740 <kill+0x1b>
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
8010578e:	c7 04 24 e0 41 11 80 	movl   $0x801141e0,(%esp)
80105795:	e8 ce 01 00 00       	call   80105968 <release>
  return -1;
8010579a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010579f:	c9                   	leave  
801057a0:	c3                   	ret    

801057a1 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801057a1:	55                   	push   %ebp
801057a2:	89 e5                	mov    %esp,%ebp
801057a4:	83 ec 58             	sub    $0x58,%esp
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801057a7:	c7 45 f0 14 42 11 80 	movl   $0x80114214,-0x10(%ebp)
801057ae:	e9 e3 00 00 00       	jmp    80105896 <procdump+0xf5>
    if(p->state == UNUSED)
801057b3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057b6:	8b 40 0c             	mov    0xc(%eax),%eax
801057b9:	85 c0                	test   %eax,%eax
801057bb:	0f 84 cd 00 00 00    	je     8010588e <procdump+0xed>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801057c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057c4:	8b 40 0c             	mov    0xc(%eax),%eax
801057c7:	83 f8 05             	cmp    $0x5,%eax
801057ca:	77 23                	ja     801057ef <procdump+0x4e>
801057cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057cf:	8b 40 0c             	mov    0xc(%eax),%eax
801057d2:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801057d9:	85 c0                	test   %eax,%eax
801057db:	74 12                	je     801057ef <procdump+0x4e>
      state = states[p->state];
801057dd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057e0:	8b 40 0c             	mov    0xc(%eax),%eax
801057e3:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801057ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801057ed:	eb 07                	jmp    801057f6 <procdump+0x55>
      state = states[p->state];
    else
      state = "???";
801057ef:	c7 45 f4 80 95 10 80 	movl   $0x80109580,-0xc(%ebp)
    cprintf("%d %s %s", p->pid, state, p->name);
801057f6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057f9:	8d 50 6c             	lea    0x6c(%eax),%edx
801057fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801057ff:	8b 40 10             	mov    0x10(%eax),%eax
80105802:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105806:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105809:	89 54 24 08          	mov    %edx,0x8(%esp)
8010580d:	89 44 24 04          	mov    %eax,0x4(%esp)
80105811:	c7 04 24 84 95 10 80 	movl   $0x80109584,(%esp)
80105818:	e8 ac ae ff ff       	call   801006c9 <cprintf>
    if(p->state == SLEEPING){
8010581d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105820:	8b 40 0c             	mov    0xc(%eax),%eax
80105823:	83 f8 02             	cmp    $0x2,%eax
80105826:	75 58                	jne    80105880 <procdump+0xdf>
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
80105828:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010582b:	8b 40 1c             	mov    0x1c(%eax),%eax
8010582e:	8b 40 0c             	mov    0xc(%eax),%eax
80105831:	83 c0 08             	add    $0x8,%eax
80105834:	8d 55 c4             	lea    -0x3c(%ebp),%edx
80105837:	89 54 24 08          	mov    %edx,0x8(%esp)
8010583b:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80105842:	00 
80105843:	89 04 24             	mov    %eax,(%esp)
80105846:	e8 6c 01 00 00       	call   801059b7 <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
8010584b:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80105852:	eb 1b                	jmp    8010586f <procdump+0xce>
        cprintf(" %p", pc[i]);
80105854:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105857:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
8010585b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010585f:	c7 04 24 8d 95 10 80 	movl   $0x8010958d,(%esp)
80105866:	e8 5e ae ff ff       	call   801006c9 <cprintf>
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, NELEM(pc), pc);
      for(i=0; i<10 && pc[i] != 0; i++)
8010586b:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
8010586f:	83 7d ec 09          	cmpl   $0x9,-0x14(%ebp)
80105873:	7f 0b                	jg     80105880 <procdump+0xdf>
80105875:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105878:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
8010587c:	85 c0                	test   %eax,%eax
8010587e:	75 d4                	jne    80105854 <procdump+0xb3>
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80105880:	c7 04 24 91 95 10 80 	movl   $0x80109591,(%esp)
80105887:	e8 3d ae ff ff       	call   801006c9 <cprintf>
8010588c:	eb 01                	jmp    8010588f <procdump+0xee>
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
8010588e:	90                   	nop
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010588f:	81 45 f0 88 00 00 00 	addl   $0x88,-0x10(%ebp)
80105896:	b8 14 64 11 80       	mov    $0x80116414,%eax
8010589b:	39 45 f0             	cmp    %eax,-0x10(%ebp)
8010589e:	0f 82 0f ff ff ff    	jb     801057b3 <procdump+0x12>
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
801058a4:	c9                   	leave  
801058a5:	c3                   	ret    
	...

801058a8 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801058a8:	55                   	push   %ebp
801058a9:	89 e5                	mov    %esp,%ebp
801058ab:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801058ae:	9c                   	pushf  
801058af:	58                   	pop    %eax
801058b0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801058b3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801058b6:	c9                   	leave  
801058b7:	c3                   	ret    

801058b8 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
801058b8:	55                   	push   %ebp
801058b9:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
801058bb:	fa                   	cli    
}
801058bc:	5d                   	pop    %ebp
801058bd:	c3                   	ret    

801058be <sti>:

static inline void
sti(void)
{
801058be:	55                   	push   %ebp
801058bf:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801058c1:	fb                   	sti    
}
801058c2:	5d                   	pop    %ebp
801058c3:	c3                   	ret    

801058c4 <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
801058c4:	55                   	push   %ebp
801058c5:	89 e5                	mov    %esp,%ebp
801058c7:	83 ec 10             	sub    $0x10,%esp
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
801058ca:	8b 55 08             	mov    0x8(%ebp),%edx
801058cd:	8b 45 0c             	mov    0xc(%ebp),%eax
801058d0:	8b 4d 08             	mov    0x8(%ebp),%ecx
801058d3:	f0 87 02             	lock xchg %eax,(%edx)
801058d6:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
801058d9:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801058dc:	c9                   	leave  
801058dd:	c3                   	ret    

801058de <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
801058de:	55                   	push   %ebp
801058df:	89 e5                	mov    %esp,%ebp
  lk->name = name;
801058e1:	8b 45 08             	mov    0x8(%ebp),%eax
801058e4:	8b 55 0c             	mov    0xc(%ebp),%edx
801058e7:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
801058ea:	8b 45 08             	mov    0x8(%ebp),%eax
801058ed:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
801058f3:	8b 45 08             	mov    0x8(%ebp),%eax
801058f6:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
801058fd:	5d                   	pop    %ebp
801058fe:	c3                   	ret    

801058ff <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
801058ff:	55                   	push   %ebp
80105900:	89 e5                	mov    %esp,%ebp
80105902:	83 ec 18             	sub    $0x18,%esp
  pushcli(); // disable interrupts to avoid deadlock.
80105905:	e8 53 01 00 00       	call   80105a5d <pushcli>
  if(holding(lk))
8010590a:	8b 45 08             	mov    0x8(%ebp),%eax
8010590d:	89 04 24             	mov    %eax,(%esp)
80105910:	e8 1e 01 00 00       	call   80105a33 <holding>
80105915:	85 c0                	test   %eax,%eax
80105917:	74 0c                	je     80105925 <acquire+0x26>
    panic("acquire");
80105919:	c7 04 24 bd 95 10 80 	movl   $0x801095bd,(%esp)
80105920:	e8 44 af ff ff       	call   80100869 <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
80105925:	8b 45 08             	mov    0x8(%ebp),%eax
80105928:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010592f:	00 
80105930:	89 04 24             	mov    %eax,(%esp)
80105933:	e8 8c ff ff ff       	call   801058c4 <xchg>
80105938:	85 c0                	test   %eax,%eax
8010593a:	75 e9                	jne    80105925 <acquire+0x26>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
8010593c:	8b 45 08             	mov    0x8(%ebp),%eax
8010593f:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80105946:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, NELEM(lk->pcs), lk->pcs);
80105949:	8b 45 08             	mov    0x8(%ebp),%eax
8010594c:	83 c0 0c             	add    $0xc,%eax
8010594f:	89 44 24 08          	mov    %eax,0x8(%esp)
80105953:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
8010595a:	00 
8010595b:	8d 45 08             	lea    0x8(%ebp),%eax
8010595e:	89 04 24             	mov    %eax,(%esp)
80105961:	e8 51 00 00 00       	call   801059b7 <getcallerpcs>
}
80105966:	c9                   	leave  
80105967:	c3                   	ret    

80105968 <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
80105968:	55                   	push   %ebp
80105969:	89 e5                	mov    %esp,%ebp
8010596b:	83 ec 18             	sub    $0x18,%esp
  if(!holding(lk))
8010596e:	8b 45 08             	mov    0x8(%ebp),%eax
80105971:	89 04 24             	mov    %eax,(%esp)
80105974:	e8 ba 00 00 00       	call   80105a33 <holding>
80105979:	85 c0                	test   %eax,%eax
8010597b:	75 0c                	jne    80105989 <release+0x21>
    panic("release");
8010597d:	c7 04 24 c5 95 10 80 	movl   $0x801095c5,(%esp)
80105984:	e8 e0 ae ff ff       	call   80100869 <panic>

  lk->pcs[0] = 0;
80105989:	8b 45 08             	mov    0x8(%ebp),%eax
8010598c:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
80105993:	8b 45 08             	mov    0x8(%ebp),%eax
80105996:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
8010599d:	8b 45 08             	mov    0x8(%ebp),%eax
801059a0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801059a7:	00 
801059a8:	89 04 24             	mov    %eax,(%esp)
801059ab:	e8 14 ff ff ff       	call   801058c4 <xchg>

  popcli();
801059b0:	e8 f0 00 00 00       	call   80105aa5 <popcli>
}
801059b5:	c9                   	leave  
801059b6:	c3                   	ret    

801059b7 <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint n, uint pcs[])
{
801059b7:	55                   	push   %ebp
801059b8:	89 e5                	mov    %esp,%ebp
801059ba:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
801059bd:	8b 45 08             	mov    0x8(%ebp),%eax
801059c0:	83 e8 08             	sub    $0x8,%eax
801059c3:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(i = 0; i < n; i++){
801059c6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
801059cd:	eb 34                	jmp    80105a03 <getcallerpcs+0x4c>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801059cf:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
801059d3:	74 4d                	je     80105a22 <getcallerpcs+0x6b>
801059d5:	81 7d f8 ff ff ff 7f 	cmpl   $0x7fffffff,-0x8(%ebp)
801059dc:	76 47                	jbe    80105a25 <getcallerpcs+0x6e>
801059de:	83 7d f8 ff          	cmpl   $0xffffffff,-0x8(%ebp)
801059e2:	74 44                	je     80105a28 <getcallerpcs+0x71>
      break;
    pcs[i] = ebp[1];     // saved %eip
801059e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
801059e7:	c1 e0 02             	shl    $0x2,%eax
801059ea:	03 45 10             	add    0x10(%ebp),%eax
801059ed:	8b 55 f8             	mov    -0x8(%ebp),%edx
801059f0:	83 c2 04             	add    $0x4,%edx
801059f3:	8b 12                	mov    (%edx),%edx
801059f5:	89 10                	mov    %edx,(%eax)
    ebp = (uint*)ebp[0]; // saved %ebp
801059f7:	8b 45 f8             	mov    -0x8(%ebp),%eax
801059fa:	8b 00                	mov    (%eax),%eax
801059fc:	89 45 f8             	mov    %eax,-0x8(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < n; i++){
801059ff:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105a03:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a06:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105a09:	72 c4                	jb     801059cf <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105a0b:	eb 1c                	jmp    80105a29 <getcallerpcs+0x72>
    pcs[i] = 0;
80105a0d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a10:	c1 e0 02             	shl    $0x2,%eax
80105a13:	03 45 10             	add    0x10(%ebp),%eax
80105a16:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < n; i++)
80105a1c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105a20:	eb 07                	jmp    80105a29 <getcallerpcs+0x72>
80105a22:	90                   	nop
80105a23:	eb 04                	jmp    80105a29 <getcallerpcs+0x72>
80105a25:	90                   	nop
80105a26:	eb 01                	jmp    80105a29 <getcallerpcs+0x72>
80105a28:	90                   	nop
80105a29:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105a2c:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105a2f:	72 dc                	jb     80105a0d <getcallerpcs+0x56>
    pcs[i] = 0;
}
80105a31:	c9                   	leave  
80105a32:	c3                   	ret    

80105a33 <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
80105a33:	55                   	push   %ebp
80105a34:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
80105a36:	8b 45 08             	mov    0x8(%ebp),%eax
80105a39:	8b 00                	mov    (%eax),%eax
80105a3b:	85 c0                	test   %eax,%eax
80105a3d:	74 17                	je     80105a56 <holding+0x23>
80105a3f:	8b 45 08             	mov    0x8(%ebp),%eax
80105a42:	8b 50 08             	mov    0x8(%eax),%edx
80105a45:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105a4b:	39 c2                	cmp    %eax,%edx
80105a4d:	75 07                	jne    80105a56 <holding+0x23>
80105a4f:	b8 01 00 00 00       	mov    $0x1,%eax
80105a54:	eb 05                	jmp    80105a5b <holding+0x28>
80105a56:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105a5b:	5d                   	pop    %ebp
80105a5c:	c3                   	ret    

80105a5d <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80105a5d:	55                   	push   %ebp
80105a5e:	89 e5                	mov    %esp,%ebp
80105a60:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
80105a63:	e8 40 fe ff ff       	call   801058a8 <readeflags>
80105a68:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
80105a6b:	e8 48 fe ff ff       	call   801058b8 <cli>
  if(cpu->ncli++ == 0)
80105a70:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105a76:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80105a7c:	85 d2                	test   %edx,%edx
80105a7e:	0f 94 c1             	sete   %cl
80105a81:	83 c2 01             	add    $0x1,%edx
80105a84:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80105a8a:	84 c9                	test   %cl,%cl
80105a8c:	74 15                	je     80105aa3 <pushcli+0x46>
    cpu->intena = eflags & FL_IF;
80105a8e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105a94:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105a97:	81 e2 00 02 00 00    	and    $0x200,%edx
80105a9d:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
80105aa3:	c9                   	leave  
80105aa4:	c3                   	ret    

80105aa5 <popcli>:

void
popcli(void)
{
80105aa5:	55                   	push   %ebp
80105aa6:	89 e5                	mov    %esp,%ebp
80105aa8:	83 ec 18             	sub    $0x18,%esp
  if(readeflags()&FL_IF)
80105aab:	e8 f8 fd ff ff       	call   801058a8 <readeflags>
80105ab0:	25 00 02 00 00       	and    $0x200,%eax
80105ab5:	85 c0                	test   %eax,%eax
80105ab7:	74 0c                	je     80105ac5 <popcli+0x20>
    panic("popcli - interruptible");
80105ab9:	c7 04 24 cd 95 10 80 	movl   $0x801095cd,(%esp)
80105ac0:	e8 a4 ad ff ff       	call   80100869 <panic>
  if(--cpu->ncli < 0)
80105ac5:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105acb:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80105ad1:	83 ea 01             	sub    $0x1,%edx
80105ad4:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80105ada:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105ae0:	85 c0                	test   %eax,%eax
80105ae2:	79 0c                	jns    80105af0 <popcli+0x4b>
    panic("popcli");
80105ae4:	c7 04 24 e4 95 10 80 	movl   $0x801095e4,(%esp)
80105aeb:	e8 79 ad ff ff       	call   80100869 <panic>
  if(cpu->ncli == 0 && cpu->intena)
80105af0:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105af6:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105afc:	85 c0                	test   %eax,%eax
80105afe:	75 15                	jne    80105b15 <popcli+0x70>
80105b00:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105b06:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105b0c:	85 c0                	test   %eax,%eax
80105b0e:	74 05                	je     80105b15 <popcli+0x70>
    sti();
80105b10:	e8 a9 fd ff ff       	call   801058be <sti>
}
80105b15:	c9                   	leave  
80105b16:	c3                   	ret    
	...

80105b18 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80105b18:	55                   	push   %ebp
80105b19:	89 e5                	mov    %esp,%ebp
80105b1b:	57                   	push   %edi
80105b1c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80105b1d:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105b20:	8b 55 10             	mov    0x10(%ebp),%edx
80105b23:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b26:	89 cb                	mov    %ecx,%ebx
80105b28:	89 df                	mov    %ebx,%edi
80105b2a:	89 d1                	mov    %edx,%ecx
80105b2c:	fc                   	cld    
80105b2d:	f3 aa                	rep stos %al,%es:(%edi)
80105b2f:	89 ca                	mov    %ecx,%edx
80105b31:	89 fb                	mov    %edi,%ebx
80105b33:	89 5d 08             	mov    %ebx,0x8(%ebp)
80105b36:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105b39:	5b                   	pop    %ebx
80105b3a:	5f                   	pop    %edi
80105b3b:	5d                   	pop    %ebp
80105b3c:	c3                   	ret    

80105b3d <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80105b3d:	55                   	push   %ebp
80105b3e:	89 e5                	mov    %esp,%ebp
80105b40:	57                   	push   %edi
80105b41:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
80105b42:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105b45:	8b 55 10             	mov    0x10(%ebp),%edx
80105b48:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b4b:	89 cb                	mov    %ecx,%ebx
80105b4d:	89 df                	mov    %ebx,%edi
80105b4f:	89 d1                	mov    %edx,%ecx
80105b51:	fc                   	cld    
80105b52:	f3 ab                	rep stos %eax,%es:(%edi)
80105b54:	89 ca                	mov    %ecx,%edx
80105b56:	89 fb                	mov    %edi,%ebx
80105b58:	89 5d 08             	mov    %ebx,0x8(%ebp)
80105b5b:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105b5e:	5b                   	pop    %ebx
80105b5f:	5f                   	pop    %edi
80105b60:	5d                   	pop    %ebp
80105b61:	c3                   	ret    

80105b62 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80105b62:	55                   	push   %ebp
80105b63:	89 e5                	mov    %esp,%ebp
80105b65:	83 ec 0c             	sub    $0xc,%esp
  if ((int)dst%4 == 0 && n%4 == 0){
80105b68:	8b 45 08             	mov    0x8(%ebp),%eax
80105b6b:	83 e0 03             	and    $0x3,%eax
80105b6e:	85 c0                	test   %eax,%eax
80105b70:	75 49                	jne    80105bbb <memset+0x59>
80105b72:	8b 45 10             	mov    0x10(%ebp),%eax
80105b75:	83 e0 03             	and    $0x3,%eax
80105b78:	85 c0                	test   %eax,%eax
80105b7a:	75 3f                	jne    80105bbb <memset+0x59>
    c &= 0xFF;
80105b7c:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80105b83:	8b 45 10             	mov    0x10(%ebp),%eax
80105b86:	c1 e8 02             	shr    $0x2,%eax
80105b89:	89 c2                	mov    %eax,%edx
80105b8b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b8e:	89 c1                	mov    %eax,%ecx
80105b90:	c1 e1 18             	shl    $0x18,%ecx
80105b93:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b96:	c1 e0 10             	shl    $0x10,%eax
80105b99:	09 c1                	or     %eax,%ecx
80105b9b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b9e:	c1 e0 08             	shl    $0x8,%eax
80105ba1:	09 c8                	or     %ecx,%eax
80105ba3:	0b 45 0c             	or     0xc(%ebp),%eax
80105ba6:	89 54 24 08          	mov    %edx,0x8(%esp)
80105baa:	89 44 24 04          	mov    %eax,0x4(%esp)
80105bae:	8b 45 08             	mov    0x8(%ebp),%eax
80105bb1:	89 04 24             	mov    %eax,(%esp)
80105bb4:	e8 84 ff ff ff       	call   80105b3d <stosl>
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
  if ((int)dst%4 == 0 && n%4 == 0){
80105bb9:	eb 19                	jmp    80105bd4 <memset+0x72>
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
80105bbb:	8b 45 10             	mov    0x10(%ebp),%eax
80105bbe:	89 44 24 08          	mov    %eax,0x8(%esp)
80105bc2:	8b 45 0c             	mov    0xc(%ebp),%eax
80105bc5:	89 44 24 04          	mov    %eax,0x4(%esp)
80105bc9:	8b 45 08             	mov    0x8(%ebp),%eax
80105bcc:	89 04 24             	mov    %eax,(%esp)
80105bcf:	e8 44 ff ff ff       	call   80105b18 <stosb>
  return dst;
80105bd4:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105bd7:	c9                   	leave  
80105bd8:	c3                   	ret    

80105bd9 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80105bd9:	55                   	push   %ebp
80105bda:	89 e5                	mov    %esp,%ebp
80105bdc:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
80105bdf:	8b 45 08             	mov    0x8(%ebp),%eax
80105be2:	89 45 f8             	mov    %eax,-0x8(%ebp)
  s2 = v2;
80105be5:	8b 45 0c             	mov    0xc(%ebp),%eax
80105be8:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0){
80105beb:	eb 32                	jmp    80105c1f <memcmp+0x46>
    if(*s1 != *s2)
80105bed:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105bf0:	0f b6 10             	movzbl (%eax),%edx
80105bf3:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105bf6:	0f b6 00             	movzbl (%eax),%eax
80105bf9:	38 c2                	cmp    %al,%dl
80105bfb:	74 1a                	je     80105c17 <memcmp+0x3e>
      return *s1 - *s2;
80105bfd:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c00:	0f b6 00             	movzbl (%eax),%eax
80105c03:	0f b6 d0             	movzbl %al,%edx
80105c06:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c09:	0f b6 00             	movzbl (%eax),%eax
80105c0c:	0f b6 c0             	movzbl %al,%eax
80105c0f:	89 d1                	mov    %edx,%ecx
80105c11:	29 c1                	sub    %eax,%ecx
80105c13:	89 c8                	mov    %ecx,%eax
80105c15:	eb 1c                	jmp    80105c33 <memcmp+0x5a>
    s1++, s2++;
80105c17:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105c1b:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80105c1f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105c23:	0f 95 c0             	setne  %al
80105c26:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105c2a:	84 c0                	test   %al,%al
80105c2c:	75 bf                	jne    80105bed <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80105c2e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105c33:	c9                   	leave  
80105c34:	c3                   	ret    

80105c35 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80105c35:	55                   	push   %ebp
80105c36:	89 e5                	mov    %esp,%ebp
80105c38:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80105c3b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c3e:	89 45 f8             	mov    %eax,-0x8(%ebp)
  d = dst;
80105c41:	8b 45 08             	mov    0x8(%ebp),%eax
80105c44:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(s < d && s + n > d){
80105c47:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c4a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105c4d:	73 55                	jae    80105ca4 <memmove+0x6f>
80105c4f:	8b 45 10             	mov    0x10(%ebp),%eax
80105c52:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105c55:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105c58:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105c5b:	76 4a                	jbe    80105ca7 <memmove+0x72>
    s += n;
80105c5d:	8b 45 10             	mov    0x10(%ebp),%eax
80105c60:	01 45 f8             	add    %eax,-0x8(%ebp)
    d += n;
80105c63:	8b 45 10             	mov    0x10(%ebp),%eax
80105c66:	01 45 fc             	add    %eax,-0x4(%ebp)
    while(n-- > 0)
80105c69:	eb 13                	jmp    80105c7e <memmove+0x49>
      *--d = *--s;
80105c6b:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80105c6f:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
80105c73:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c76:	0f b6 10             	movzbl (%eax),%edx
80105c79:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c7c:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80105c7e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105c82:	0f 95 c0             	setne  %al
80105c85:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105c89:	84 c0                	test   %al,%al
80105c8b:	75 de                	jne    80105c6b <memmove+0x36>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80105c8d:	eb 28                	jmp    80105cb7 <memmove+0x82>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80105c8f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c92:	0f b6 10             	movzbl (%eax),%edx
80105c95:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c98:	88 10                	mov    %dl,(%eax)
80105c9a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105c9e:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105ca2:	eb 04                	jmp    80105ca8 <memmove+0x73>
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
80105ca4:	90                   	nop
80105ca5:	eb 01                	jmp    80105ca8 <memmove+0x73>
80105ca7:	90                   	nop
80105ca8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105cac:	0f 95 c0             	setne  %al
80105caf:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105cb3:	84 c0                	test   %al,%al
80105cb5:	75 d8                	jne    80105c8f <memmove+0x5a>
      *d++ = *s++;

  return dst;
80105cb7:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105cba:	c9                   	leave  
80105cbb:	c3                   	ret    

80105cbc <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80105cbc:	55                   	push   %ebp
80105cbd:	89 e5                	mov    %esp,%ebp
80105cbf:	83 ec 0c             	sub    $0xc,%esp
  return memmove(dst, src, n);
80105cc2:	8b 45 10             	mov    0x10(%ebp),%eax
80105cc5:	89 44 24 08          	mov    %eax,0x8(%esp)
80105cc9:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ccc:	89 44 24 04          	mov    %eax,0x4(%esp)
80105cd0:	8b 45 08             	mov    0x8(%ebp),%eax
80105cd3:	89 04 24             	mov    %eax,(%esp)
80105cd6:	e8 5a ff ff ff       	call   80105c35 <memmove>
}
80105cdb:	c9                   	leave  
80105cdc:	c3                   	ret    

80105cdd <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80105cdd:	55                   	push   %ebp
80105cde:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
80105ce0:	eb 0c                	jmp    80105cee <strncmp+0x11>
    n--, p++, q++;
80105ce2:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105ce6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105cea:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
80105cee:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105cf2:	74 1a                	je     80105d0e <strncmp+0x31>
80105cf4:	8b 45 08             	mov    0x8(%ebp),%eax
80105cf7:	0f b6 00             	movzbl (%eax),%eax
80105cfa:	84 c0                	test   %al,%al
80105cfc:	74 10                	je     80105d0e <strncmp+0x31>
80105cfe:	8b 45 08             	mov    0x8(%ebp),%eax
80105d01:	0f b6 10             	movzbl (%eax),%edx
80105d04:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d07:	0f b6 00             	movzbl (%eax),%eax
80105d0a:	38 c2                	cmp    %al,%dl
80105d0c:	74 d4                	je     80105ce2 <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
80105d0e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105d12:	75 07                	jne    80105d1b <strncmp+0x3e>
    return 0;
80105d14:	b8 00 00 00 00       	mov    $0x0,%eax
80105d19:	eb 18                	jmp    80105d33 <strncmp+0x56>
  return (uchar)*p - (uchar)*q;
80105d1b:	8b 45 08             	mov    0x8(%ebp),%eax
80105d1e:	0f b6 00             	movzbl (%eax),%eax
80105d21:	0f b6 d0             	movzbl %al,%edx
80105d24:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d27:	0f b6 00             	movzbl (%eax),%eax
80105d2a:	0f b6 c0             	movzbl %al,%eax
80105d2d:	89 d1                	mov    %edx,%ecx
80105d2f:	29 c1                	sub    %eax,%ecx
80105d31:	89 c8                	mov    %ecx,%eax
}
80105d33:	5d                   	pop    %ebp
80105d34:	c3                   	ret    

80105d35 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80105d35:	55                   	push   %ebp
80105d36:	89 e5                	mov    %esp,%ebp
80105d38:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105d3b:	8b 45 08             	mov    0x8(%ebp),%eax
80105d3e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
80105d41:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105d45:	0f 9f c0             	setg   %al
80105d48:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105d4c:	84 c0                	test   %al,%al
80105d4e:	74 30                	je     80105d80 <strncpy+0x4b>
80105d50:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d53:	0f b6 10             	movzbl (%eax),%edx
80105d56:	8b 45 08             	mov    0x8(%ebp),%eax
80105d59:	88 10                	mov    %dl,(%eax)
80105d5b:	8b 45 08             	mov    0x8(%ebp),%eax
80105d5e:	0f b6 00             	movzbl (%eax),%eax
80105d61:	84 c0                	test   %al,%al
80105d63:	0f 95 c0             	setne  %al
80105d66:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105d6a:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
80105d6e:	84 c0                	test   %al,%al
80105d70:	75 cf                	jne    80105d41 <strncpy+0xc>
    ;
  while(n-- > 0)
80105d72:	eb 0d                	jmp    80105d81 <strncpy+0x4c>
    *s++ = 0;
80105d74:	8b 45 08             	mov    0x8(%ebp),%eax
80105d77:	c6 00 00             	movb   $0x0,(%eax)
80105d7a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105d7e:	eb 01                	jmp    80105d81 <strncpy+0x4c>
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
80105d80:	90                   	nop
80105d81:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105d85:	0f 9f c0             	setg   %al
80105d88:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105d8c:	84 c0                	test   %al,%al
80105d8e:	75 e4                	jne    80105d74 <strncpy+0x3f>
    *s++ = 0;
  return os;
80105d90:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105d93:	c9                   	leave  
80105d94:	c3                   	ret    

80105d95 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80105d95:	55                   	push   %ebp
80105d96:	89 e5                	mov    %esp,%ebp
80105d98:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105d9b:	8b 45 08             	mov    0x8(%ebp),%eax
80105d9e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
80105da1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105da5:	7f 05                	jg     80105dac <safestrcpy+0x17>
    return os;
80105da7:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105daa:	eb 35                	jmp    80105de1 <safestrcpy+0x4c>
  while(--n > 0 && (*s++ = *t++) != 0)
80105dac:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105db0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105db4:	7e 22                	jle    80105dd8 <safestrcpy+0x43>
80105db6:	8b 45 0c             	mov    0xc(%ebp),%eax
80105db9:	0f b6 10             	movzbl (%eax),%edx
80105dbc:	8b 45 08             	mov    0x8(%ebp),%eax
80105dbf:	88 10                	mov    %dl,(%eax)
80105dc1:	8b 45 08             	mov    0x8(%ebp),%eax
80105dc4:	0f b6 00             	movzbl (%eax),%eax
80105dc7:	84 c0                	test   %al,%al
80105dc9:	0f 95 c0             	setne  %al
80105dcc:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105dd0:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
80105dd4:	84 c0                	test   %al,%al
80105dd6:	75 d4                	jne    80105dac <safestrcpy+0x17>
    ;
  *s = 0;
80105dd8:	8b 45 08             	mov    0x8(%ebp),%eax
80105ddb:	c6 00 00             	movb   $0x0,(%eax)
  return os;
80105dde:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105de1:	c9                   	leave  
80105de2:	c3                   	ret    

80105de3 <strlen>:

int
strlen(const char *s)
{
80105de3:	55                   	push   %ebp
80105de4:	89 e5                	mov    %esp,%ebp
80105de6:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
80105de9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80105df0:	eb 04                	jmp    80105df6 <strlen+0x13>
80105df2:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105df6:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105df9:	03 45 08             	add    0x8(%ebp),%eax
80105dfc:	0f b6 00             	movzbl (%eax),%eax
80105dff:	84 c0                	test   %al,%al
80105e01:	75 ef                	jne    80105df2 <strlen+0xf>
    ;
  return n;
80105e03:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105e06:	c9                   	leave  
80105e07:	c3                   	ret    

80105e08 <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
80105e08:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80105e0c:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80105e10:	55                   	push   %ebp
  pushl %ebx
80105e11:	53                   	push   %ebx
  pushl %esi
80105e12:	56                   	push   %esi
  pushl %edi
80105e13:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80105e14:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80105e16:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
80105e18:	5f                   	pop    %edi
  popl %esi
80105e19:	5e                   	pop    %esi
  popl %ebx
80105e1a:	5b                   	pop    %ebx
  popl %ebp
80105e1b:	5d                   	pop    %ebp
  ret
80105e1c:	c3                   	ret    
80105e1d:	00 00                	add    %al,(%eax)
	...

80105e20 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80105e20:	55                   	push   %ebp
80105e21:	89 e5                	mov    %esp,%ebp
  if(addr >= proc->sz || addr+4 > proc->sz)
80105e23:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e29:	8b 00                	mov    (%eax),%eax
80105e2b:	3b 45 08             	cmp    0x8(%ebp),%eax
80105e2e:	76 12                	jbe    80105e42 <fetchint+0x22>
80105e30:	8b 45 08             	mov    0x8(%ebp),%eax
80105e33:	8d 50 04             	lea    0x4(%eax),%edx
80105e36:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e3c:	8b 00                	mov    (%eax),%eax
80105e3e:	39 c2                	cmp    %eax,%edx
80105e40:	76 07                	jbe    80105e49 <fetchint+0x29>
    return -1;
80105e42:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e47:	eb 0f                	jmp    80105e58 <fetchint+0x38>
  *ip = *(int*)(addr);
80105e49:	8b 45 08             	mov    0x8(%ebp),%eax
80105e4c:	8b 10                	mov    (%eax),%edx
80105e4e:	8b 45 0c             	mov    0xc(%ebp),%eax
80105e51:	89 10                	mov    %edx,(%eax)
  return 0;
80105e53:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105e58:	5d                   	pop    %ebp
80105e59:	c3                   	ret    

80105e5a <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80105e5a:	55                   	push   %ebp
80105e5b:	89 e5                	mov    %esp,%ebp
80105e5d:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= proc->sz)
80105e60:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e66:	8b 00                	mov    (%eax),%eax
80105e68:	3b 45 08             	cmp    0x8(%ebp),%eax
80105e6b:	77 07                	ja     80105e74 <fetchstr+0x1a>
    return -1;
80105e6d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e72:	eb 48                	jmp    80105ebc <fetchstr+0x62>
  *pp = (char*)addr;
80105e74:	8b 55 08             	mov    0x8(%ebp),%edx
80105e77:	8b 45 0c             	mov    0xc(%ebp),%eax
80105e7a:	89 10                	mov    %edx,(%eax)
  ep = (char*)proc->sz;
80105e7c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105e82:	8b 00                	mov    (%eax),%eax
80105e84:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(s = *pp; s < ep; s++)
80105e87:	8b 45 0c             	mov    0xc(%ebp),%eax
80105e8a:	8b 00                	mov    (%eax),%eax
80105e8c:	89 45 f8             	mov    %eax,-0x8(%ebp)
80105e8f:	eb 1e                	jmp    80105eaf <fetchstr+0x55>
    if(*s == 0)
80105e91:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105e94:	0f b6 00             	movzbl (%eax),%eax
80105e97:	84 c0                	test   %al,%al
80105e99:	75 10                	jne    80105eab <fetchstr+0x51>
      return s - *pp;
80105e9b:	8b 55 f8             	mov    -0x8(%ebp),%edx
80105e9e:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ea1:	8b 00                	mov    (%eax),%eax
80105ea3:	89 d1                	mov    %edx,%ecx
80105ea5:	29 c1                	sub    %eax,%ecx
80105ea7:	89 c8                	mov    %ecx,%eax
80105ea9:	eb 11                	jmp    80105ebc <fetchstr+0x62>

  if(addr >= proc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)proc->sz;
  for(s = *pp; s < ep; s++)
80105eab:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105eaf:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105eb2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
80105eb5:	72 da                	jb     80105e91 <fetchstr+0x37>
    if(*s == 0)
      return s - *pp;
  return -1;
80105eb7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105ebc:	c9                   	leave  
80105ebd:	c3                   	ret    

80105ebe <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80105ebe:	55                   	push   %ebp
80105ebf:	89 e5                	mov    %esp,%ebp
80105ec1:	83 ec 08             	sub    $0x8,%esp
  return fetchint(proc->tf->esp + 4 + 4*n, ip);
80105ec4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105eca:	8b 40 18             	mov    0x18(%eax),%eax
80105ecd:	8b 50 44             	mov    0x44(%eax),%edx
80105ed0:	8b 45 08             	mov    0x8(%ebp),%eax
80105ed3:	c1 e0 02             	shl    $0x2,%eax
80105ed6:	8d 04 02             	lea    (%edx,%eax,1),%eax
80105ed9:	8d 50 04             	lea    0x4(%eax),%edx
80105edc:	8b 45 0c             	mov    0xc(%ebp),%eax
80105edf:	89 44 24 04          	mov    %eax,0x4(%esp)
80105ee3:	89 14 24             	mov    %edx,(%esp)
80105ee6:	e8 35 ff ff ff       	call   80105e20 <fetchint>
}
80105eeb:	c9                   	leave  
80105eec:	c3                   	ret    

80105eed <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80105eed:	55                   	push   %ebp
80105eee:	89 e5                	mov    %esp,%ebp
80105ef0:	83 ec 18             	sub    $0x18,%esp
  int i;

  if(argint(n, &i) < 0)
80105ef3:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105ef6:	89 44 24 04          	mov    %eax,0x4(%esp)
80105efa:	8b 45 08             	mov    0x8(%ebp),%eax
80105efd:	89 04 24             	mov    %eax,(%esp)
80105f00:	e8 b9 ff ff ff       	call   80105ebe <argint>
80105f05:	85 c0                	test   %eax,%eax
80105f07:	79 07                	jns    80105f10 <argptr+0x23>
    return -1;
80105f09:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f0e:	eb 3d                	jmp    80105f4d <argptr+0x60>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
80105f10:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105f13:	89 c2                	mov    %eax,%edx
80105f15:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105f1b:	8b 00                	mov    (%eax),%eax
80105f1d:	39 c2                	cmp    %eax,%edx
80105f1f:	73 16                	jae    80105f37 <argptr+0x4a>
80105f21:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105f24:	89 c2                	mov    %eax,%edx
80105f26:	8b 45 10             	mov    0x10(%ebp),%eax
80105f29:	01 c2                	add    %eax,%edx
80105f2b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105f31:	8b 00                	mov    (%eax),%eax
80105f33:	39 c2                	cmp    %eax,%edx
80105f35:	76 07                	jbe    80105f3e <argptr+0x51>
    return -1;
80105f37:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f3c:	eb 0f                	jmp    80105f4d <argptr+0x60>
  *pp = (char*)i;
80105f3e:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105f41:	89 c2                	mov    %eax,%edx
80105f43:	8b 45 0c             	mov    0xc(%ebp),%eax
80105f46:	89 10                	mov    %edx,(%eax)
  return 0;
80105f48:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105f4d:	c9                   	leave  
80105f4e:	c3                   	ret    

80105f4f <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80105f4f:	55                   	push   %ebp
80105f50:	89 e5                	mov    %esp,%ebp
80105f52:	83 ec 18             	sub    $0x18,%esp
  int addr;
  if(argint(n, &addr) < 0)
80105f55:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105f58:	89 44 24 04          	mov    %eax,0x4(%esp)
80105f5c:	8b 45 08             	mov    0x8(%ebp),%eax
80105f5f:	89 04 24             	mov    %eax,(%esp)
80105f62:	e8 57 ff ff ff       	call   80105ebe <argint>
80105f67:	85 c0                	test   %eax,%eax
80105f69:	79 07                	jns    80105f72 <argstr+0x23>
    return -1;
80105f6b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f70:	eb 12                	jmp    80105f84 <argstr+0x35>
  return fetchstr(addr, pp);
80105f72:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105f75:	8b 55 0c             	mov    0xc(%ebp),%edx
80105f78:	89 54 24 04          	mov    %edx,0x4(%esp)
80105f7c:	89 04 24             	mov    %eax,(%esp)
80105f7f:	e8 d6 fe ff ff       	call   80105e5a <fetchstr>
}
80105f84:	c9                   	leave  
80105f85:	c3                   	ret    

80105f86 <syscall>:
  [SYS_switch_scheduler] = sys_switch_scheduler,
};

void
syscall(void)
{
80105f86:	55                   	push   %ebp
80105f87:	89 e5                	mov    %esp,%ebp
80105f89:	53                   	push   %ebx
80105f8a:	83 ec 24             	sub    $0x24,%esp
  int num;

  num = proc->tf->eax;
80105f8d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105f93:	8b 40 18             	mov    0x18(%eax),%eax
80105f96:	8b 40 1c             	mov    0x1c(%eax),%eax
80105f99:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105f9c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105fa0:	7e 30                	jle    80105fd2 <syscall+0x4c>
80105fa2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fa5:	83 f8 20             	cmp    $0x20,%eax
80105fa8:	77 28                	ja     80105fd2 <syscall+0x4c>
80105faa:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fad:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105fb4:	85 c0                	test   %eax,%eax
80105fb6:	74 1a                	je     80105fd2 <syscall+0x4c>
    proc->tf->eax = syscalls[num]();
80105fb8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105fbe:	8b 58 18             	mov    0x18(%eax),%ebx
80105fc1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fc4:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
80105fcb:	ff d0                	call   *%eax
80105fcd:	89 43 1c             	mov    %eax,0x1c(%ebx)
syscall(void)
{
  int num;

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80105fd0:	eb 3d                	jmp    8010600f <syscall+0x89>
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
80105fd2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105fd8:	8d 48 6c             	lea    0x6c(%eax),%ecx
            proc->pid, proc->name, num);
80105fdb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax

  num = proc->tf->eax;
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
80105fe1:	8b 40 10             	mov    0x10(%eax),%eax
80105fe4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105fe7:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105feb:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105fef:	89 44 24 04          	mov    %eax,0x4(%esp)
80105ff3:	c7 04 24 eb 95 10 80 	movl   $0x801095eb,(%esp)
80105ffa:	e8 ca a6 ff ff       	call   801006c9 <cprintf>
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
80105fff:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106005:	8b 40 18             	mov    0x18(%eax),%eax
80106008:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
8010600f:	83 c4 24             	add    $0x24,%esp
80106012:	5b                   	pop    %ebx
80106013:	5d                   	pop    %ebp
80106014:	c3                   	ret    
80106015:	00 00                	add    %al,(%eax)
	...

80106018 <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
80106018:	55                   	push   %ebp
80106019:	89 e5                	mov    %esp,%ebp
8010601b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
8010601e:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106021:	89 44 24 04          	mov    %eax,0x4(%esp)
80106025:	8b 45 08             	mov    0x8(%ebp),%eax
80106028:	89 04 24             	mov    %eax,(%esp)
8010602b:	e8 8e fe ff ff       	call   80105ebe <argint>
80106030:	85 c0                	test   %eax,%eax
80106032:	79 07                	jns    8010603b <argfd+0x23>
    return -1;
80106034:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106039:	eb 50                	jmp    8010608b <argfd+0x73>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
8010603b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010603e:	85 c0                	test   %eax,%eax
80106040:	78 21                	js     80106063 <argfd+0x4b>
80106042:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106045:	83 f8 0f             	cmp    $0xf,%eax
80106048:	7f 19                	jg     80106063 <argfd+0x4b>
8010604a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106050:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106053:	83 c2 08             	add    $0x8,%edx
80106056:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
8010605a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010605d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106061:	75 07                	jne    8010606a <argfd+0x52>
    return -1;
80106063:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106068:	eb 21                	jmp    8010608b <argfd+0x73>
  if(pfd)
8010606a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010606e:	74 08                	je     80106078 <argfd+0x60>
    *pfd = fd;
80106070:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106073:	8b 45 0c             	mov    0xc(%ebp),%eax
80106076:	89 10                	mov    %edx,(%eax)
  if(pf)
80106078:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010607c:	74 08                	je     80106086 <argfd+0x6e>
    *pf = f;
8010607e:	8b 45 10             	mov    0x10(%ebp),%eax
80106081:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106084:	89 10                	mov    %edx,(%eax)
  return 0;
80106086:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010608b:	c9                   	leave  
8010608c:	c3                   	ret    

8010608d <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
8010608d:	55                   	push   %ebp
8010608e:	89 e5                	mov    %esp,%ebp
80106090:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80106093:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010609a:	eb 30                	jmp    801060cc <fdalloc+0x3f>
    if(proc->ofile[fd] == 0){
8010609c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801060a2:	8b 55 fc             	mov    -0x4(%ebp),%edx
801060a5:	83 c2 08             	add    $0x8,%edx
801060a8:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
801060ac:	85 c0                	test   %eax,%eax
801060ae:	75 18                	jne    801060c8 <fdalloc+0x3b>
      proc->ofile[fd] = f;
801060b0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801060b6:	8b 55 fc             	mov    -0x4(%ebp),%edx
801060b9:	8d 4a 08             	lea    0x8(%edx),%ecx
801060bc:	8b 55 08             	mov    0x8(%ebp),%edx
801060bf:	89 54 88 08          	mov    %edx,0x8(%eax,%ecx,4)
      return fd;
801060c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
801060c6:	eb 0f                	jmp    801060d7 <fdalloc+0x4a>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
801060c8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801060cc:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
801060d0:	7e ca                	jle    8010609c <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
801060d2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801060d7:	c9                   	leave  
801060d8:	c3                   	ret    

801060d9 <sys_dup>:

int
sys_dup(void)
{
801060d9:	55                   	push   %ebp
801060da:	89 e5                	mov    %esp,%ebp
801060dc:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
801060df:	8d 45 f0             	lea    -0x10(%ebp),%eax
801060e2:	89 44 24 08          	mov    %eax,0x8(%esp)
801060e6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801060ed:	00 
801060ee:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801060f5:	e8 1e ff ff ff       	call   80106018 <argfd>
801060fa:	85 c0                	test   %eax,%eax
801060fc:	79 07                	jns    80106105 <sys_dup+0x2c>
    return -1;
801060fe:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106103:	eb 29                	jmp    8010612e <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
80106105:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106108:	89 04 24             	mov    %eax,(%esp)
8010610b:	e8 7d ff ff ff       	call   8010608d <fdalloc>
80106110:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106113:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106117:	79 07                	jns    80106120 <sys_dup+0x47>
    return -1;
80106119:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010611e:	eb 0e                	jmp    8010612e <sys_dup+0x55>
  filedup(f);
80106120:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106123:	89 04 24             	mov    %eax,(%esp)
80106126:	e8 ee b5 ff ff       	call   80101719 <filedup>
  return fd;
8010612b:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
8010612e:	c9                   	leave  
8010612f:	c3                   	ret    

80106130 <sys_read>:

int
sys_read(void)
{
80106130:	55                   	push   %ebp
80106131:	89 e5                	mov    %esp,%ebp
80106133:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80106136:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106139:	89 44 24 08          	mov    %eax,0x8(%esp)
8010613d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80106144:	00 
80106145:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010614c:	e8 c7 fe ff ff       	call   80106018 <argfd>
80106151:	85 c0                	test   %eax,%eax
80106153:	78 35                	js     8010618a <sys_read+0x5a>
80106155:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106158:	89 44 24 04          	mov    %eax,0x4(%esp)
8010615c:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80106163:	e8 56 fd ff ff       	call   80105ebe <argint>
80106168:	85 c0                	test   %eax,%eax
8010616a:	78 1e                	js     8010618a <sys_read+0x5a>
8010616c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010616f:	89 44 24 08          	mov    %eax,0x8(%esp)
80106173:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106176:	89 44 24 04          	mov    %eax,0x4(%esp)
8010617a:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106181:	e8 67 fd ff ff       	call   80105eed <argptr>
80106186:	85 c0                	test   %eax,%eax
80106188:	79 07                	jns    80106191 <sys_read+0x61>
    return -1;
8010618a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010618f:	eb 19                	jmp    801061aa <sys_read+0x7a>
  return fileread(f, p, n);
80106191:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80106194:	8b 55 ec             	mov    -0x14(%ebp),%edx
80106197:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010619a:	89 4c 24 08          	mov    %ecx,0x8(%esp)
8010619e:	89 54 24 04          	mov    %edx,0x4(%esp)
801061a2:	89 04 24             	mov    %eax,(%esp)
801061a5:	e8 dc b6 ff ff       	call   80101886 <fileread>
}
801061aa:	c9                   	leave  
801061ab:	c3                   	ret    

801061ac <sys_write>:

int
sys_write(void)
{
801061ac:	55                   	push   %ebp
801061ad:	89 e5                	mov    %esp,%ebp
801061af:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
801061b2:	8d 45 f4             	lea    -0xc(%ebp),%eax
801061b5:	89 44 24 08          	mov    %eax,0x8(%esp)
801061b9:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801061c0:	00 
801061c1:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801061c8:	e8 4b fe ff ff       	call   80106018 <argfd>
801061cd:	85 c0                	test   %eax,%eax
801061cf:	78 35                	js     80106206 <sys_write+0x5a>
801061d1:	8d 45 f0             	lea    -0x10(%ebp),%eax
801061d4:	89 44 24 04          	mov    %eax,0x4(%esp)
801061d8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801061df:	e8 da fc ff ff       	call   80105ebe <argint>
801061e4:	85 c0                	test   %eax,%eax
801061e6:	78 1e                	js     80106206 <sys_write+0x5a>
801061e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801061eb:	89 44 24 08          	mov    %eax,0x8(%esp)
801061ef:	8d 45 ec             	lea    -0x14(%ebp),%eax
801061f2:	89 44 24 04          	mov    %eax,0x4(%esp)
801061f6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801061fd:	e8 eb fc ff ff       	call   80105eed <argptr>
80106202:	85 c0                	test   %eax,%eax
80106204:	79 07                	jns    8010620d <sys_write+0x61>
    return -1;
80106206:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010620b:	eb 19                	jmp    80106226 <sys_write+0x7a>
  return filewrite(f, p, n);
8010620d:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80106210:	8b 55 ec             	mov    -0x14(%ebp),%edx
80106213:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106216:	89 4c 24 08          	mov    %ecx,0x8(%esp)
8010621a:	89 54 24 04          	mov    %edx,0x4(%esp)
8010621e:	89 04 24             	mov    %eax,(%esp)
80106221:	e8 1c b7 ff ff       	call   80101942 <filewrite>
}
80106226:	c9                   	leave  
80106227:	c3                   	ret    

80106228 <sys_close>:

int
sys_close(void)
{
80106228:	55                   	push   %ebp
80106229:	89 e5                	mov    %esp,%ebp
8010622b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
8010622e:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106231:	89 44 24 08          	mov    %eax,0x8(%esp)
80106235:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106238:	89 44 24 04          	mov    %eax,0x4(%esp)
8010623c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106243:	e8 d0 fd ff ff       	call   80106018 <argfd>
80106248:	85 c0                	test   %eax,%eax
8010624a:	79 07                	jns    80106253 <sys_close+0x2b>
    return -1;
8010624c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106251:	eb 24                	jmp    80106277 <sys_close+0x4f>
  proc->ofile[fd] = 0;
80106253:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106259:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010625c:	83 c2 08             	add    $0x8,%edx
8010625f:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106266:	00 
  fileclose(f);
80106267:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010626a:	89 04 24             	mov    %eax,(%esp)
8010626d:	e8 ef b4 ff ff       	call   80101761 <fileclose>
  return 0;
80106272:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106277:	c9                   	leave  
80106278:	c3                   	ret    

80106279 <sys_fstat>:

int
sys_fstat(void)
{
80106279:	55                   	push   %ebp
8010627a:	89 e5                	mov    %esp,%ebp
8010627c:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  struct stat *st;

  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
8010627f:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106282:	89 44 24 08          	mov    %eax,0x8(%esp)
80106286:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010628d:	00 
8010628e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106295:	e8 7e fd ff ff       	call   80106018 <argfd>
8010629a:	85 c0                	test   %eax,%eax
8010629c:	78 1f                	js     801062bd <sys_fstat+0x44>
8010629e:	8d 45 f0             	lea    -0x10(%ebp),%eax
801062a1:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
801062a8:	00 
801062a9:	89 44 24 04          	mov    %eax,0x4(%esp)
801062ad:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801062b4:	e8 34 fc ff ff       	call   80105eed <argptr>
801062b9:	85 c0                	test   %eax,%eax
801062bb:	79 07                	jns    801062c4 <sys_fstat+0x4b>
    return -1;
801062bd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801062c2:	eb 12                	jmp    801062d6 <sys_fstat+0x5d>
  return filestat(f, st);
801062c4:	8b 55 f0             	mov    -0x10(%ebp),%edx
801062c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801062ca:	89 54 24 04          	mov    %edx,0x4(%esp)
801062ce:	89 04 24             	mov    %eax,(%esp)
801062d1:	e8 61 b5 ff ff       	call   80101837 <filestat>
}
801062d6:	c9                   	leave  
801062d7:	c3                   	ret    

801062d8 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
801062d8:	55                   	push   %ebp
801062d9:	89 e5                	mov    %esp,%ebp
801062db:	83 ec 38             	sub    $0x38,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
801062de:	8d 45 d8             	lea    -0x28(%ebp),%eax
801062e1:	89 44 24 04          	mov    %eax,0x4(%esp)
801062e5:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801062ec:	e8 5e fc ff ff       	call   80105f4f <argstr>
801062f1:	85 c0                	test   %eax,%eax
801062f3:	78 17                	js     8010630c <sys_link+0x34>
801062f5:	8d 45 dc             	lea    -0x24(%ebp),%eax
801062f8:	89 44 24 04          	mov    %eax,0x4(%esp)
801062fc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106303:	e8 47 fc ff ff       	call   80105f4f <argstr>
80106308:	85 c0                	test   %eax,%eax
8010630a:	79 0a                	jns    80106316 <sys_link+0x3e>
    return -1;
8010630c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106311:	e9 41 01 00 00       	jmp    80106457 <sys_link+0x17f>

  begin_op();
80106316:	e8 4f d7 ff ff       	call   80103a6a <begin_op>
  if((ip = namei(old)) == 0){
8010631b:	8b 45 d8             	mov    -0x28(%ebp),%eax
8010631e:	89 04 24             	mov    %eax,(%esp)
80106321:	e8 f9 c8 ff ff       	call   80102c1f <namei>
80106326:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106329:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010632d:	75 0f                	jne    8010633e <sys_link+0x66>
    end_op();
8010632f:	e8 b8 d7 ff ff       	call   80103aec <end_op>
    return -1;
80106334:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106339:	e9 19 01 00 00       	jmp    80106457 <sys_link+0x17f>
  }

  ilock(ip);
8010633e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106341:	89 04 24             	mov    %eax,(%esp)
80106344:	e8 28 bd ff ff       	call   80102071 <ilock>
  if(ip->type == T_DIR){
80106349:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010634c:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106350:	66 83 f8 01          	cmp    $0x1,%ax
80106354:	75 1a                	jne    80106370 <sys_link+0x98>
    iunlockput(ip);
80106356:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106359:	89 04 24             	mov    %eax,(%esp)
8010635c:	e8 9d bf ff ff       	call   801022fe <iunlockput>
    end_op();
80106361:	e8 86 d7 ff ff       	call   80103aec <end_op>
    return -1;
80106366:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010636b:	e9 e7 00 00 00       	jmp    80106457 <sys_link+0x17f>
  }

  ip->nlink++;
80106370:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106373:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106377:	8d 50 01             	lea    0x1(%eax),%edx
8010637a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010637d:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106381:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106384:	89 04 24             	mov    %eax,(%esp)
80106387:	e8 1f bb ff ff       	call   80101eab <iupdate>
  iunlock(ip);
8010638c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010638f:	89 04 24             	mov    %eax,(%esp)
80106392:	e8 31 be ff ff       	call   801021c8 <iunlock>

  if((dp = nameiparent(new, name)) == 0)
80106397:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010639a:	8d 55 e2             	lea    -0x1e(%ebp),%edx
8010639d:	89 54 24 04          	mov    %edx,0x4(%esp)
801063a1:	89 04 24             	mov    %eax,(%esp)
801063a4:	e8 98 c8 ff ff       	call   80102c41 <nameiparent>
801063a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801063ac:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801063b0:	74 68                	je     8010641a <sys_link+0x142>
    goto bad;
  ilock(dp);
801063b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063b5:	89 04 24             	mov    %eax,(%esp)
801063b8:	e8 b4 bc ff ff       	call   80102071 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
801063bd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063c0:	8b 10                	mov    (%eax),%edx
801063c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063c5:	8b 00                	mov    (%eax),%eax
801063c7:	39 c2                	cmp    %eax,%edx
801063c9:	75 20                	jne    801063eb <sys_link+0x113>
801063cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063ce:	8b 40 04             	mov    0x4(%eax),%eax
801063d1:	89 44 24 08          	mov    %eax,0x8(%esp)
801063d5:	8d 45 e2             	lea    -0x1e(%ebp),%eax
801063d8:	89 44 24 04          	mov    %eax,0x4(%esp)
801063dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063df:	89 04 24             	mov    %eax,(%esp)
801063e2:	e8 77 c5 ff ff       	call   8010295e <dirlink>
801063e7:	85 c0                	test   %eax,%eax
801063e9:	79 0d                	jns    801063f8 <sys_link+0x120>
    iunlockput(dp);
801063eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063ee:	89 04 24             	mov    %eax,(%esp)
801063f1:	e8 08 bf ff ff       	call   801022fe <iunlockput>
    goto bad;
801063f6:	eb 23                	jmp    8010641b <sys_link+0x143>
  }
  iunlockput(dp);
801063f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063fb:	89 04 24             	mov    %eax,(%esp)
801063fe:	e8 fb be ff ff       	call   801022fe <iunlockput>
  iput(ip);
80106403:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106406:	89 04 24             	mov    %eax,(%esp)
80106409:	e8 1f be ff ff       	call   8010222d <iput>

  end_op();
8010640e:	e8 d9 d6 ff ff       	call   80103aec <end_op>

  return 0;
80106413:	b8 00 00 00 00       	mov    $0x0,%eax
80106418:	eb 3d                	jmp    80106457 <sys_link+0x17f>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
8010641a:	90                   	nop
  end_op();

  return 0;

bad:
  ilock(ip);
8010641b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010641e:	89 04 24             	mov    %eax,(%esp)
80106421:	e8 4b bc ff ff       	call   80102071 <ilock>
  ip->nlink--;
80106426:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106429:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010642d:	8d 50 ff             	lea    -0x1(%eax),%edx
80106430:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106433:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106437:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010643a:	89 04 24             	mov    %eax,(%esp)
8010643d:	e8 69 ba ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
80106442:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106445:	89 04 24             	mov    %eax,(%esp)
80106448:	e8 b1 be ff ff       	call   801022fe <iunlockput>
  end_op();
8010644d:	e8 9a d6 ff ff       	call   80103aec <end_op>
  return -1;
80106452:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106457:	c9                   	leave  
80106458:	c3                   	ret    

80106459 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
80106459:	55                   	push   %ebp
8010645a:	89 e5                	mov    %esp,%ebp
8010645c:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
8010645f:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
80106466:	eb 4b                	jmp    801064b3 <isdirempty+0x5a>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80106468:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010646b:	8d 45 e4             	lea    -0x1c(%ebp),%eax
8010646e:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80106475:	00 
80106476:	89 54 24 08          	mov    %edx,0x8(%esp)
8010647a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010647e:	8b 45 08             	mov    0x8(%ebp),%eax
80106481:	89 04 24             	mov    %eax,(%esp)
80106484:	e8 e7 c0 ff ff       	call   80102570 <readi>
80106489:	83 f8 10             	cmp    $0x10,%eax
8010648c:	74 0c                	je     8010649a <isdirempty+0x41>
      panic("isdirempty: readi");
8010648e:	c7 04 24 07 96 10 80 	movl   $0x80109607,(%esp)
80106495:	e8 cf a3 ff ff       	call   80100869 <panic>
    if(de.inum != 0)
8010649a:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
8010649e:	66 85 c0             	test   %ax,%ax
801064a1:	74 07                	je     801064aa <isdirempty+0x51>
      return 0;
801064a3:	b8 00 00 00 00       	mov    $0x0,%eax
801064a8:	eb 1b                	jmp    801064c5 <isdirempty+0x6c>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801064aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801064ad:	83 c0 10             	add    $0x10,%eax
801064b0:	89 45 f4             	mov    %eax,-0xc(%ebp)
801064b3:	8b 55 f4             	mov    -0xc(%ebp),%edx
801064b6:	8b 45 08             	mov    0x8(%ebp),%eax
801064b9:	8b 40 18             	mov    0x18(%eax),%eax
801064bc:	39 c2                	cmp    %eax,%edx
801064be:	72 a8                	jb     80106468 <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
801064c0:	b8 01 00 00 00       	mov    $0x1,%eax
}
801064c5:	c9                   	leave  
801064c6:	c3                   	ret    

801064c7 <sys_unlink>:

//PAGEBREAK!
int
sys_unlink(void)
{
801064c7:	55                   	push   %ebp
801064c8:	89 e5                	mov    %esp,%ebp
801064ca:	83 ec 48             	sub    $0x48,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
801064cd:	8d 45 cc             	lea    -0x34(%ebp),%eax
801064d0:	89 44 24 04          	mov    %eax,0x4(%esp)
801064d4:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801064db:	e8 6f fa ff ff       	call   80105f4f <argstr>
801064e0:	85 c0                	test   %eax,%eax
801064e2:	79 0a                	jns    801064ee <sys_unlink+0x27>
    return -1;
801064e4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801064e9:	e9 af 01 00 00       	jmp    8010669d <sys_unlink+0x1d6>

  begin_op();
801064ee:	e8 77 d5 ff ff       	call   80103a6a <begin_op>
  if((dp = nameiparent(path, name)) == 0){
801064f3:	8b 45 cc             	mov    -0x34(%ebp),%eax
801064f6:	8d 55 d2             	lea    -0x2e(%ebp),%edx
801064f9:	89 54 24 04          	mov    %edx,0x4(%esp)
801064fd:	89 04 24             	mov    %eax,(%esp)
80106500:	e8 3c c7 ff ff       	call   80102c41 <nameiparent>
80106505:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106508:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010650c:	75 0f                	jne    8010651d <sys_unlink+0x56>
    end_op();
8010650e:	e8 d9 d5 ff ff       	call   80103aec <end_op>
    return -1;
80106513:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106518:	e9 80 01 00 00       	jmp    8010669d <sys_unlink+0x1d6>
  }

  ilock(dp);
8010651d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106520:	89 04 24             	mov    %eax,(%esp)
80106523:	e8 49 bb ff ff       	call   80102071 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80106528:	c7 44 24 04 19 96 10 	movl   $0x80109619,0x4(%esp)
8010652f:	80 
80106530:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80106533:	89 04 24             	mov    %eax,(%esp)
80106536:	e8 39 c3 ff ff       	call   80102874 <namecmp>
8010653b:	85 c0                	test   %eax,%eax
8010653d:	0f 84 45 01 00 00    	je     80106688 <sys_unlink+0x1c1>
80106543:	c7 44 24 04 1b 96 10 	movl   $0x8010961b,0x4(%esp)
8010654a:	80 
8010654b:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010654e:	89 04 24             	mov    %eax,(%esp)
80106551:	e8 1e c3 ff ff       	call   80102874 <namecmp>
80106556:	85 c0                	test   %eax,%eax
80106558:	0f 84 2a 01 00 00    	je     80106688 <sys_unlink+0x1c1>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
8010655e:	8d 45 c8             	lea    -0x38(%ebp),%eax
80106561:	89 44 24 08          	mov    %eax,0x8(%esp)
80106565:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80106568:	89 44 24 04          	mov    %eax,0x4(%esp)
8010656c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010656f:	89 04 24             	mov    %eax,(%esp)
80106572:	e8 1f c3 ff ff       	call   80102896 <dirlookup>
80106577:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010657a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010657e:	0f 84 03 01 00 00    	je     80106687 <sys_unlink+0x1c0>
    goto bad;
  ilock(ip);
80106584:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106587:	89 04 24             	mov    %eax,(%esp)
8010658a:	e8 e2 ba ff ff       	call   80102071 <ilock>

  if(ip->nlink < 1)
8010658f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106592:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106596:	66 85 c0             	test   %ax,%ax
80106599:	7f 0c                	jg     801065a7 <sys_unlink+0xe0>
    panic("unlink: nlink < 1");
8010659b:	c7 04 24 1e 96 10 80 	movl   $0x8010961e,(%esp)
801065a2:	e8 c2 a2 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
801065a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065aa:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801065ae:	66 83 f8 01          	cmp    $0x1,%ax
801065b2:	75 1f                	jne    801065d3 <sys_unlink+0x10c>
801065b4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065b7:	89 04 24             	mov    %eax,(%esp)
801065ba:	e8 9a fe ff ff       	call   80106459 <isdirempty>
801065bf:	85 c0                	test   %eax,%eax
801065c1:	75 10                	jne    801065d3 <sys_unlink+0x10c>
    iunlockput(ip);
801065c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801065c6:	89 04 24             	mov    %eax,(%esp)
801065c9:	e8 30 bd ff ff       	call   801022fe <iunlockput>
    goto bad;
801065ce:	e9 b5 00 00 00       	jmp    80106688 <sys_unlink+0x1c1>
  }

  memset(&de, 0, sizeof(de));
801065d3:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801065da:	00 
801065db:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801065e2:	00 
801065e3:	8d 45 e0             	lea    -0x20(%ebp),%eax
801065e6:	89 04 24             	mov    %eax,(%esp)
801065e9:	e8 74 f5 ff ff       	call   80105b62 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801065ee:	8b 55 c8             	mov    -0x38(%ebp),%edx
801065f1:	8d 45 e0             	lea    -0x20(%ebp),%eax
801065f4:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801065fb:	00 
801065fc:	89 54 24 08          	mov    %edx,0x8(%esp)
80106600:	89 44 24 04          	mov    %eax,0x4(%esp)
80106604:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106607:	89 04 24             	mov    %eax,(%esp)
8010660a:	e8 cd c0 ff ff       	call   801026dc <writei>
8010660f:	83 f8 10             	cmp    $0x10,%eax
80106612:	74 0c                	je     80106620 <sys_unlink+0x159>
    panic("unlink: writei");
80106614:	c7 04 24 30 96 10 80 	movl   $0x80109630,(%esp)
8010661b:	e8 49 a2 ff ff       	call   80100869 <panic>
  if(ip->type == T_DIR){
80106620:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106623:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106627:	66 83 f8 01          	cmp    $0x1,%ax
8010662b:	75 1c                	jne    80106649 <sys_unlink+0x182>
    dp->nlink--;
8010662d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106630:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106634:	8d 50 ff             	lea    -0x1(%eax),%edx
80106637:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010663a:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
8010663e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106641:	89 04 24             	mov    %eax,(%esp)
80106644:	e8 62 b8 ff ff       	call   80101eab <iupdate>
  }
  iunlockput(dp);
80106649:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010664c:	89 04 24             	mov    %eax,(%esp)
8010664f:	e8 aa bc ff ff       	call   801022fe <iunlockput>

  ip->nlink--;
80106654:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106657:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010665b:	8d 50 ff             	lea    -0x1(%eax),%edx
8010665e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106661:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106665:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106668:	89 04 24             	mov    %eax,(%esp)
8010666b:	e8 3b b8 ff ff       	call   80101eab <iupdate>
  iunlockput(ip);
80106670:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106673:	89 04 24             	mov    %eax,(%esp)
80106676:	e8 83 bc ff ff       	call   801022fe <iunlockput>

  end_op();
8010667b:	e8 6c d4 ff ff       	call   80103aec <end_op>

  return 0;
80106680:	b8 00 00 00 00       	mov    $0x0,%eax
80106685:	eb 16                	jmp    8010669d <sys_unlink+0x1d6>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
80106687:	90                   	nop
  end_op();

  return 0;

bad:
  iunlockput(dp);
80106688:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010668b:	89 04 24             	mov    %eax,(%esp)
8010668e:	e8 6b bc ff ff       	call   801022fe <iunlockput>
  end_op();
80106693:	e8 54 d4 ff ff       	call   80103aec <end_op>
  return -1;
80106698:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010669d:	c9                   	leave  
8010669e:	c3                   	ret    

8010669f <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
8010669f:	55                   	push   %ebp
801066a0:	89 e5                	mov    %esp,%ebp
801066a2:	83 ec 48             	sub    $0x48,%esp
801066a5:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801066a8:	8b 55 10             	mov    0x10(%ebp),%edx
801066ab:	8b 45 14             	mov    0x14(%ebp),%eax
801066ae:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
801066b2:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
801066b6:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
801066ba:	8d 45 de             	lea    -0x22(%ebp),%eax
801066bd:	89 44 24 04          	mov    %eax,0x4(%esp)
801066c1:	8b 45 08             	mov    0x8(%ebp),%eax
801066c4:	89 04 24             	mov    %eax,(%esp)
801066c7:	e8 75 c5 ff ff       	call   80102c41 <nameiparent>
801066cc:	89 45 f4             	mov    %eax,-0xc(%ebp)
801066cf:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801066d3:	75 0a                	jne    801066df <create+0x40>
    return 0;
801066d5:	b8 00 00 00 00       	mov    $0x0,%eax
801066da:	e9 7e 01 00 00       	jmp    8010685d <create+0x1be>
  ilock(dp);
801066df:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066e2:	89 04 24             	mov    %eax,(%esp)
801066e5:	e8 87 b9 ff ff       	call   80102071 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
801066ea:	8d 45 ec             	lea    -0x14(%ebp),%eax
801066ed:	89 44 24 08          	mov    %eax,0x8(%esp)
801066f1:	8d 45 de             	lea    -0x22(%ebp),%eax
801066f4:	89 44 24 04          	mov    %eax,0x4(%esp)
801066f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066fb:	89 04 24             	mov    %eax,(%esp)
801066fe:	e8 93 c1 ff ff       	call   80102896 <dirlookup>
80106703:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106706:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010670a:	74 47                	je     80106753 <create+0xb4>
    iunlockput(dp);
8010670c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010670f:	89 04 24             	mov    %eax,(%esp)
80106712:	e8 e7 bb ff ff       	call   801022fe <iunlockput>
    ilock(ip);
80106717:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010671a:	89 04 24             	mov    %eax,(%esp)
8010671d:	e8 4f b9 ff ff       	call   80102071 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80106722:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80106727:	75 15                	jne    8010673e <create+0x9f>
80106729:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010672c:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106730:	66 83 f8 02          	cmp    $0x2,%ax
80106734:	75 08                	jne    8010673e <create+0x9f>
      return ip;
80106736:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106739:	e9 1f 01 00 00       	jmp    8010685d <create+0x1be>
    iunlockput(ip);
8010673e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106741:	89 04 24             	mov    %eax,(%esp)
80106744:	e8 b5 bb ff ff       	call   801022fe <iunlockput>
    return 0;
80106749:	b8 00 00 00 00       	mov    $0x0,%eax
8010674e:	e9 0a 01 00 00       	jmp    8010685d <create+0x1be>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
80106753:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
80106757:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010675a:	8b 00                	mov    (%eax),%eax
8010675c:	89 54 24 04          	mov    %edx,0x4(%esp)
80106760:	89 04 24             	mov    %eax,(%esp)
80106763:	e8 6f b6 ff ff       	call   80101dd7 <ialloc>
80106768:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010676b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010676f:	75 0c                	jne    8010677d <create+0xde>
    panic("create: ialloc");
80106771:	c7 04 24 3f 96 10 80 	movl   $0x8010963f,(%esp)
80106778:	e8 ec a0 ff ff       	call   80100869 <panic>

  ilock(ip);
8010677d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106780:	89 04 24             	mov    %eax,(%esp)
80106783:	e8 e9 b8 ff ff       	call   80102071 <ilock>
  ip->major = major;
80106788:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010678b:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
8010678f:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
80106793:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106796:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
8010679a:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
8010679e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067a1:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
801067a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067aa:	89 04 24             	mov    %eax,(%esp)
801067ad:	e8 f9 b6 ff ff       	call   80101eab <iupdate>

  if(type == T_DIR){  // Create . and .. entries.
801067b2:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801067b7:	75 6a                	jne    80106823 <create+0x184>
    dp->nlink++;  // for ".."
801067b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067bc:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801067c0:	8d 50 01             	lea    0x1(%eax),%edx
801067c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067c6:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
801067ca:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067cd:	89 04 24             	mov    %eax,(%esp)
801067d0:	e8 d6 b6 ff ff       	call   80101eab <iupdate>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
801067d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067d8:	8b 40 04             	mov    0x4(%eax),%eax
801067db:	89 44 24 08          	mov    %eax,0x8(%esp)
801067df:	c7 44 24 04 19 96 10 	movl   $0x80109619,0x4(%esp)
801067e6:	80 
801067e7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067ea:	89 04 24             	mov    %eax,(%esp)
801067ed:	e8 6c c1 ff ff       	call   8010295e <dirlink>
801067f2:	85 c0                	test   %eax,%eax
801067f4:	78 21                	js     80106817 <create+0x178>
801067f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067f9:	8b 40 04             	mov    0x4(%eax),%eax
801067fc:	89 44 24 08          	mov    %eax,0x8(%esp)
80106800:	c7 44 24 04 1b 96 10 	movl   $0x8010961b,0x4(%esp)
80106807:	80 
80106808:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010680b:	89 04 24             	mov    %eax,(%esp)
8010680e:	e8 4b c1 ff ff       	call   8010295e <dirlink>
80106813:	85 c0                	test   %eax,%eax
80106815:	79 0c                	jns    80106823 <create+0x184>
      panic("create dots");
80106817:	c7 04 24 4e 96 10 80 	movl   $0x8010964e,(%esp)
8010681e:	e8 46 a0 ff ff       	call   80100869 <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
80106823:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106826:	8b 40 04             	mov    0x4(%eax),%eax
80106829:	89 44 24 08          	mov    %eax,0x8(%esp)
8010682d:	8d 45 de             	lea    -0x22(%ebp),%eax
80106830:	89 44 24 04          	mov    %eax,0x4(%esp)
80106834:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106837:	89 04 24             	mov    %eax,(%esp)
8010683a:	e8 1f c1 ff ff       	call   8010295e <dirlink>
8010683f:	85 c0                	test   %eax,%eax
80106841:	79 0c                	jns    8010684f <create+0x1b0>
    panic("create: dirlink");
80106843:	c7 04 24 5a 96 10 80 	movl   $0x8010965a,(%esp)
8010684a:	e8 1a a0 ff ff       	call   80100869 <panic>

  iunlockput(dp);
8010684f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106852:	89 04 24             	mov    %eax,(%esp)
80106855:	e8 a4 ba ff ff       	call   801022fe <iunlockput>

  return ip;
8010685a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010685d:	c9                   	leave  
8010685e:	c3                   	ret    

8010685f <sys_open>:

int
sys_open(void)
{
8010685f:	55                   	push   %ebp
80106860:	89 e5                	mov    %esp,%ebp
80106862:	83 ec 38             	sub    $0x38,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80106865:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106868:	89 44 24 04          	mov    %eax,0x4(%esp)
8010686c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106873:	e8 d7 f6 ff ff       	call   80105f4f <argstr>
80106878:	85 c0                	test   %eax,%eax
8010687a:	78 17                	js     80106893 <sys_open+0x34>
8010687c:	8d 45 e4             	lea    -0x1c(%ebp),%eax
8010687f:	89 44 24 04          	mov    %eax,0x4(%esp)
80106883:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010688a:	e8 2f f6 ff ff       	call   80105ebe <argint>
8010688f:	85 c0                	test   %eax,%eax
80106891:	79 0a                	jns    8010689d <sys_open+0x3e>
    return -1;
80106893:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106898:	e9 5a 01 00 00       	jmp    801069f7 <sys_open+0x198>

  begin_op();
8010689d:	e8 c8 d1 ff ff       	call   80103a6a <begin_op>

  if(omode & O_CREATE){
801068a2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801068a5:	25 00 02 00 00       	and    $0x200,%eax
801068aa:	85 c0                	test   %eax,%eax
801068ac:	74 3b                	je     801068e9 <sys_open+0x8a>
    ip = create(path, T_FILE, 0, 0);
801068ae:	8b 45 e8             	mov    -0x18(%ebp),%eax
801068b1:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
801068b8:	00 
801068b9:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801068c0:	00 
801068c1:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
801068c8:	00 
801068c9:	89 04 24             	mov    %eax,(%esp)
801068cc:	e8 ce fd ff ff       	call   8010669f <create>
801068d1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(ip == 0){
801068d4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801068d8:	75 6b                	jne    80106945 <sys_open+0xe6>
      end_op();
801068da:	e8 0d d2 ff ff       	call   80103aec <end_op>
      return -1;
801068df:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801068e4:	e9 0e 01 00 00       	jmp    801069f7 <sys_open+0x198>
    }
  } else {
    if((ip = namei(path)) == 0){
801068e9:	8b 45 e8             	mov    -0x18(%ebp),%eax
801068ec:	89 04 24             	mov    %eax,(%esp)
801068ef:	e8 2b c3 ff ff       	call   80102c1f <namei>
801068f4:	89 45 f4             	mov    %eax,-0xc(%ebp)
801068f7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801068fb:	75 0f                	jne    8010690c <sys_open+0xad>
      end_op();
801068fd:	e8 ea d1 ff ff       	call   80103aec <end_op>
      return -1;
80106902:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106907:	e9 eb 00 00 00       	jmp    801069f7 <sys_open+0x198>
    }
    ilock(ip);
8010690c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010690f:	89 04 24             	mov    %eax,(%esp)
80106912:	e8 5a b7 ff ff       	call   80102071 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80106917:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010691a:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010691e:	66 83 f8 01          	cmp    $0x1,%ax
80106922:	75 21                	jne    80106945 <sys_open+0xe6>
80106924:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106927:	85 c0                	test   %eax,%eax
80106929:	74 1a                	je     80106945 <sys_open+0xe6>
      iunlockput(ip);
8010692b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010692e:	89 04 24             	mov    %eax,(%esp)
80106931:	e8 c8 b9 ff ff       	call   801022fe <iunlockput>
      end_op();
80106936:	e8 b1 d1 ff ff       	call   80103aec <end_op>
      return -1;
8010693b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106940:	e9 b2 00 00 00       	jmp    801069f7 <sys_open+0x198>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80106945:	e8 6e ad ff ff       	call   801016b8 <filealloc>
8010694a:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010694d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106951:	74 14                	je     80106967 <sys_open+0x108>
80106953:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106956:	89 04 24             	mov    %eax,(%esp)
80106959:	e8 2f f7 ff ff       	call   8010608d <fdalloc>
8010695e:	89 45 ec             	mov    %eax,-0x14(%ebp)
80106961:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80106965:	79 28                	jns    8010698f <sys_open+0x130>
    if(f)
80106967:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010696b:	74 0b                	je     80106978 <sys_open+0x119>
      fileclose(f);
8010696d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106970:	89 04 24             	mov    %eax,(%esp)
80106973:	e8 e9 ad ff ff       	call   80101761 <fileclose>
    iunlockput(ip);
80106978:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010697b:	89 04 24             	mov    %eax,(%esp)
8010697e:	e8 7b b9 ff ff       	call   801022fe <iunlockput>
    end_op();
80106983:	e8 64 d1 ff ff       	call   80103aec <end_op>
    return -1;
80106988:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010698d:	eb 68                	jmp    801069f7 <sys_open+0x198>
  }
  iunlock(ip);
8010698f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106992:	89 04 24             	mov    %eax,(%esp)
80106995:	e8 2e b8 ff ff       	call   801021c8 <iunlock>
  end_op();
8010699a:	e8 4d d1 ff ff       	call   80103aec <end_op>

  f->type = FD_INODE;
8010699f:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069a2:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
801069a8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069ab:	8b 55 f4             	mov    -0xc(%ebp),%edx
801069ae:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
801069b1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069b4:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
801069bb:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801069be:	83 e0 01             	and    $0x1,%eax
801069c1:	85 c0                	test   %eax,%eax
801069c3:	0f 94 c2             	sete   %dl
801069c6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069c9:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801069cc:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801069cf:	83 e0 01             	and    $0x1,%eax
801069d2:	84 c0                	test   %al,%al
801069d4:	75 0a                	jne    801069e0 <sys_open+0x181>
801069d6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801069d9:	83 e0 02             	and    $0x2,%eax
801069dc:	85 c0                	test   %eax,%eax
801069de:	74 07                	je     801069e7 <sys_open+0x188>
801069e0:	b8 01 00 00 00       	mov    $0x1,%eax
801069e5:	eb 05                	jmp    801069ec <sys_open+0x18d>
801069e7:	b8 00 00 00 00       	mov    $0x0,%eax
801069ec:	89 c2                	mov    %eax,%edx
801069ee:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069f1:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
801069f4:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
801069f7:	c9                   	leave  
801069f8:	c3                   	ret    

801069f9 <sys_mkdir>:

int
sys_mkdir(void)
{
801069f9:	55                   	push   %ebp
801069fa:	89 e5                	mov    %esp,%ebp
801069fc:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
801069ff:	e8 66 d0 ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80106a04:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106a07:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a0b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106a12:	e8 38 f5 ff ff       	call   80105f4f <argstr>
80106a17:	85 c0                	test   %eax,%eax
80106a19:	78 2c                	js     80106a47 <sys_mkdir+0x4e>
80106a1b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106a1e:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
80106a25:	00 
80106a26:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80106a2d:	00 
80106a2e:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80106a35:	00 
80106a36:	89 04 24             	mov    %eax,(%esp)
80106a39:	e8 61 fc ff ff       	call   8010669f <create>
80106a3e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106a41:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106a45:	75 0c                	jne    80106a53 <sys_mkdir+0x5a>
    end_op();
80106a47:	e8 a0 d0 ff ff       	call   80103aec <end_op>
    return -1;
80106a4c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a51:	eb 15                	jmp    80106a68 <sys_mkdir+0x6f>
  }
  iunlockput(ip);
80106a53:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106a56:	89 04 24             	mov    %eax,(%esp)
80106a59:	e8 a0 b8 ff ff       	call   801022fe <iunlockput>
  end_op();
80106a5e:	e8 89 d0 ff ff       	call   80103aec <end_op>
  return 0;
80106a63:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106a68:	c9                   	leave  
80106a69:	c3                   	ret    

80106a6a <sys_mknod>:

int
sys_mknod(void)
{
80106a6a:	55                   	push   %ebp
80106a6b:	89 e5                	mov    %esp,%ebp
80106a6d:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80106a70:	e8 f5 cf ff ff       	call   80103a6a <begin_op>
  if((argstr(0, &path)) < 0 ||
80106a75:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106a78:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a7c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106a83:	e8 c7 f4 ff ff       	call   80105f4f <argstr>
80106a88:	85 c0                	test   %eax,%eax
80106a8a:	78 5e                	js     80106aea <sys_mknod+0x80>
     argint(1, &major) < 0 ||
80106a8c:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106a8f:	89 44 24 04          	mov    %eax,0x4(%esp)
80106a93:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106a9a:	e8 1f f4 ff ff       	call   80105ebe <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
80106a9f:	85 c0                	test   %eax,%eax
80106aa1:	78 47                	js     80106aea <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80106aa3:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106aa6:	89 44 24 04          	mov    %eax,0x4(%esp)
80106aaa:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80106ab1:	e8 08 f4 ff ff       	call   80105ebe <argint>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
80106ab6:	85 c0                	test   %eax,%eax
80106ab8:	78 30                	js     80106aea <sys_mknod+0x80>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
80106aba:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106abd:	0f bf c8             	movswl %ax,%ecx
80106ac0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106ac3:	0f bf d0             	movswl %ax,%edx
80106ac6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106ac9:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80106acd:	89 54 24 08          	mov    %edx,0x8(%esp)
80106ad1:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80106ad8:	00 
80106ad9:	89 04 24             	mov    %eax,(%esp)
80106adc:	e8 be fb ff ff       	call   8010669f <create>
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
80106ae1:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106ae4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106ae8:	75 0c                	jne    80106af6 <sys_mknod+0x8c>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
80106aea:	e8 fd cf ff ff       	call   80103aec <end_op>
    return -1;
80106aef:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106af4:	eb 15                	jmp    80106b0b <sys_mknod+0xa1>
  }
  iunlockput(ip);
80106af6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106af9:	89 04 24             	mov    %eax,(%esp)
80106afc:	e8 fd b7 ff ff       	call   801022fe <iunlockput>
  end_op();
80106b01:	e8 e6 cf ff ff       	call   80103aec <end_op>
  return 0;
80106b06:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106b0b:	c9                   	leave  
80106b0c:	c3                   	ret    

80106b0d <sys_chdir>:

int
sys_chdir(void)
{
80106b0d:	55                   	push   %ebp
80106b0e:	89 e5                	mov    %esp,%ebp
80106b10:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_op();
80106b13:	e8 52 cf ff ff       	call   80103a6a <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80106b18:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106b1b:	89 44 24 04          	mov    %eax,0x4(%esp)
80106b1f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106b26:	e8 24 f4 ff ff       	call   80105f4f <argstr>
80106b2b:	85 c0                	test   %eax,%eax
80106b2d:	78 14                	js     80106b43 <sys_chdir+0x36>
80106b2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106b32:	89 04 24             	mov    %eax,(%esp)
80106b35:	e8 e5 c0 ff ff       	call   80102c1f <namei>
80106b3a:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106b3d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106b41:	75 0c                	jne    80106b4f <sys_chdir+0x42>
    end_op();
80106b43:	e8 a4 cf ff ff       	call   80103aec <end_op>
    return -1;
80106b48:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b4d:	eb 61                	jmp    80106bb0 <sys_chdir+0xa3>
  }
  ilock(ip);
80106b4f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106b52:	89 04 24             	mov    %eax,(%esp)
80106b55:	e8 17 b5 ff ff       	call   80102071 <ilock>
  if(ip->type != T_DIR){
80106b5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106b5d:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106b61:	66 83 f8 01          	cmp    $0x1,%ax
80106b65:	74 17                	je     80106b7e <sys_chdir+0x71>
    iunlockput(ip);
80106b67:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106b6a:	89 04 24             	mov    %eax,(%esp)
80106b6d:	e8 8c b7 ff ff       	call   801022fe <iunlockput>
    end_op();
80106b72:	e8 75 cf ff ff       	call   80103aec <end_op>
    return -1;
80106b77:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b7c:	eb 32                	jmp    80106bb0 <sys_chdir+0xa3>
  }
  iunlock(ip);
80106b7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106b81:	89 04 24             	mov    %eax,(%esp)
80106b84:	e8 3f b6 ff ff       	call   801021c8 <iunlock>
  iput(proc->cwd);
80106b89:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106b8f:	8b 40 68             	mov    0x68(%eax),%eax
80106b92:	89 04 24             	mov    %eax,(%esp)
80106b95:	e8 93 b6 ff ff       	call   8010222d <iput>
  end_op();
80106b9a:	e8 4d cf ff ff       	call   80103aec <end_op>
  proc->cwd = ip;
80106b9f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106ba5:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106ba8:	89 50 68             	mov    %edx,0x68(%eax)
  return 0;
80106bab:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106bb0:	c9                   	leave  
80106bb1:	c3                   	ret    

80106bb2 <sys_exec>:

int
sys_exec(void)
{
80106bb2:	55                   	push   %ebp
80106bb3:	89 e5                	mov    %esp,%ebp
80106bb5:	81 ec a8 00 00 00    	sub    $0xa8,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80106bbb:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106bbe:	89 44 24 04          	mov    %eax,0x4(%esp)
80106bc2:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106bc9:	e8 81 f3 ff ff       	call   80105f4f <argstr>
80106bce:	85 c0                	test   %eax,%eax
80106bd0:	78 1a                	js     80106bec <sys_exec+0x3a>
80106bd2:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80106bd8:	89 44 24 04          	mov    %eax,0x4(%esp)
80106bdc:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80106be3:	e8 d6 f2 ff ff       	call   80105ebe <argint>
80106be8:	85 c0                	test   %eax,%eax
80106bea:	79 0a                	jns    80106bf6 <sys_exec+0x44>
    return -1;
80106bec:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106bf1:	e9 cd 00 00 00       	jmp    80106cc3 <sys_exec+0x111>
  }
  memset(argv, 0, sizeof(argv));
80106bf6:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80106bfd:	00 
80106bfe:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80106c05:	00 
80106c06:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106c0c:	89 04 24             	mov    %eax,(%esp)
80106c0f:	e8 4e ef ff ff       	call   80105b62 <memset>
  for(i=0;; i++){
80106c14:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80106c1b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c1e:	83 f8 1f             	cmp    $0x1f,%eax
80106c21:	76 0a                	jbe    80106c2d <sys_exec+0x7b>
      return -1;
80106c23:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106c28:	e9 96 00 00 00       	jmp    80106cc3 <sys_exec+0x111>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80106c2d:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80106c33:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106c36:	c1 e2 02             	shl    $0x2,%edx
80106c39:	89 d1                	mov    %edx,%ecx
80106c3b:	8b 95 6c ff ff ff    	mov    -0x94(%ebp),%edx
80106c41:	8d 14 11             	lea    (%ecx,%edx,1),%edx
80106c44:	89 44 24 04          	mov    %eax,0x4(%esp)
80106c48:	89 14 24             	mov    %edx,(%esp)
80106c4b:	e8 d0 f1 ff ff       	call   80105e20 <fetchint>
80106c50:	85 c0                	test   %eax,%eax
80106c52:	79 07                	jns    80106c5b <sys_exec+0xa9>
      return -1;
80106c54:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106c59:	eb 68                	jmp    80106cc3 <sys_exec+0x111>
    if(uarg == 0){
80106c5b:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80106c61:	85 c0                	test   %eax,%eax
80106c63:	75 26                	jne    80106c8b <sys_exec+0xd9>
      argv[i] = 0;
80106c65:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c68:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80106c6f:	00 00 00 00 
      break;
80106c73:	90                   	nop
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
80106c74:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106c77:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80106c7d:	89 54 24 04          	mov    %edx,0x4(%esp)
80106c81:	89 04 24             	mov    %eax,(%esp)
80106c84:	e8 fb a5 ff ff       	call   80101284 <exec>
80106c89:	eb 38                	jmp    80106cc3 <sys_exec+0x111>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80106c8b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106c8e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80106c95:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106c9b:	01 d0                	add    %edx,%eax
80106c9d:	8b 95 68 ff ff ff    	mov    -0x98(%ebp),%edx
80106ca3:	89 44 24 04          	mov    %eax,0x4(%esp)
80106ca7:	89 14 24             	mov    %edx,(%esp)
80106caa:	e8 ab f1 ff ff       	call   80105e5a <fetchstr>
80106caf:	85 c0                	test   %eax,%eax
80106cb1:	79 07                	jns    80106cba <sys_exec+0x108>
      return -1;
80106cb3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106cb8:	eb 09                	jmp    80106cc3 <sys_exec+0x111>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
80106cba:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
80106cbe:	e9 58 ff ff ff       	jmp    80106c1b <sys_exec+0x69>
  return exec(path, argv);
}
80106cc3:	c9                   	leave  
80106cc4:	c3                   	ret    

80106cc5 <sys_pipe>:

int
sys_pipe(void)
{
80106cc5:	55                   	push   %ebp
80106cc6:	89 e5                	mov    %esp,%ebp
80106cc8:	83 ec 38             	sub    $0x38,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80106ccb:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106cce:	c7 44 24 08 08 00 00 	movl   $0x8,0x8(%esp)
80106cd5:	00 
80106cd6:	89 44 24 04          	mov    %eax,0x4(%esp)
80106cda:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106ce1:	e8 07 f2 ff ff       	call   80105eed <argptr>
80106ce6:	85 c0                	test   %eax,%eax
80106ce8:	79 0a                	jns    80106cf4 <sys_pipe+0x2f>
    return -1;
80106cea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106cef:	e9 9b 00 00 00       	jmp    80106d8f <sys_pipe+0xca>
  if(pipealloc(&rf, &wf) < 0)
80106cf4:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106cf7:	89 44 24 04          	mov    %eax,0x4(%esp)
80106cfb:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106cfe:	89 04 24             	mov    %eax,(%esp)
80106d01:	e8 0e d9 ff ff       	call   80104614 <pipealloc>
80106d06:	85 c0                	test   %eax,%eax
80106d08:	79 07                	jns    80106d11 <sys_pipe+0x4c>
    return -1;
80106d0a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106d0f:	eb 7e                	jmp    80106d8f <sys_pipe+0xca>
  fd0 = -1;
80106d11:	c7 45 f0 ff ff ff ff 	movl   $0xffffffff,-0x10(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106d18:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106d1b:	89 04 24             	mov    %eax,(%esp)
80106d1e:	e8 6a f3 ff ff       	call   8010608d <fdalloc>
80106d23:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106d26:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106d2a:	78 14                	js     80106d40 <sys_pipe+0x7b>
80106d2c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106d2f:	89 04 24             	mov    %eax,(%esp)
80106d32:	e8 56 f3 ff ff       	call   8010608d <fdalloc>
80106d37:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106d3a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106d3e:	79 37                	jns    80106d77 <sys_pipe+0xb2>
    if(fd0 >= 0)
80106d40:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106d44:	78 14                	js     80106d5a <sys_pipe+0x95>
      proc->ofile[fd0] = 0;
80106d46:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106d4c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106d4f:	83 c2 08             	add    $0x8,%edx
80106d52:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106d59:	00 
    fileclose(rf);
80106d5a:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106d5d:	89 04 24             	mov    %eax,(%esp)
80106d60:	e8 fc a9 ff ff       	call   80101761 <fileclose>
    fileclose(wf);
80106d65:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106d68:	89 04 24             	mov    %eax,(%esp)
80106d6b:	e8 f1 a9 ff ff       	call   80101761 <fileclose>
    return -1;
80106d70:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106d75:	eb 18                	jmp    80106d8f <sys_pipe+0xca>
  }
  fd[0] = fd0;
80106d77:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106d7a:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106d7d:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
80106d7f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106d82:	8d 50 04             	lea    0x4(%eax),%edx
80106d85:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106d88:	89 02                	mov    %eax,(%edx)
  return 0;
80106d8a:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106d8f:	c9                   	leave  
80106d90:	c3                   	ret    
80106d91:	00 00                	add    %al,(%eax)
	...

80106d94 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
80106d94:	55                   	push   %ebp
80106d95:	89 e5                	mov    %esp,%ebp
80106d97:	83 ec 08             	sub    $0x8,%esp
  return fork();
80106d9a:	e8 27 df ff ff       	call   80104cc6 <fork>
}
80106d9f:	c9                   	leave  
80106da0:	c3                   	ret    

80106da1 <sys_exit>:

int
sys_exit(void)
{
80106da1:	55                   	push   %ebp
80106da2:	89 e5                	mov    %esp,%ebp
80106da4:	83 ec 08             	sub    $0x8,%esp
  exit();
80106da7:	e8 af e0 ff ff       	call   80104e5b <exit>
  return 0;  // not reached
80106dac:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106db1:	c9                   	leave  
80106db2:	c3                   	ret    

80106db3 <sys_wait>:

int
sys_wait(void)
{
80106db3:	55                   	push   %ebp
80106db4:	89 e5                	mov    %esp,%ebp
80106db6:	83 ec 08             	sub    $0x8,%esp
  return wait();
80106db9:	e8 37 e2 ff ff       	call   80104ff5 <wait>
}
80106dbe:	c9                   	leave  
80106dbf:	c3                   	ret    

80106dc0 <sys_kill>:

int
sys_kill(void)
{
80106dc0:	55                   	push   %ebp
80106dc1:	89 e5                	mov    %esp,%ebp
80106dc3:	83 ec 28             	sub    $0x28,%esp
  int pid;

  if(argint(0, &pid) < 0)
80106dc6:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106dc9:	89 44 24 04          	mov    %eax,0x4(%esp)
80106dcd:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106dd4:	e8 e5 f0 ff ff       	call   80105ebe <argint>
80106dd9:	85 c0                	test   %eax,%eax
80106ddb:	79 07                	jns    80106de4 <sys_kill+0x24>
    return -1;
80106ddd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106de2:	eb 0b                	jmp    80106def <sys_kill+0x2f>
  return kill(pid);
80106de4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106de7:	89 04 24             	mov    %eax,(%esp)
80106dea:	e8 36 e9 ff ff       	call   80105725 <kill>
}
80106def:	c9                   	leave  
80106df0:	c3                   	ret    

80106df1 <sys_getpid>:

int
sys_getpid(void)
{
80106df1:	55                   	push   %ebp
80106df2:	89 e5                	mov    %esp,%ebp
  return proc->pid;
80106df4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106dfa:	8b 40 10             	mov    0x10(%eax),%eax
}
80106dfd:	5d                   	pop    %ebp
80106dfe:	c3                   	ret    

80106dff <sys_sbrk>:

int
sys_sbrk(void)
{
80106dff:	55                   	push   %ebp
80106e00:	89 e5                	mov    %esp,%ebp
80106e02:	83 ec 28             	sub    $0x28,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80106e05:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106e08:	89 44 24 04          	mov    %eax,0x4(%esp)
80106e0c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106e13:	e8 a6 f0 ff ff       	call   80105ebe <argint>
80106e18:	85 c0                	test   %eax,%eax
80106e1a:	79 07                	jns    80106e23 <sys_sbrk+0x24>
    return -1;
80106e1c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e21:	eb 24                	jmp    80106e47 <sys_sbrk+0x48>
  addr = proc->sz;
80106e23:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106e29:	8b 00                	mov    (%eax),%eax
80106e2b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
80106e2e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106e31:	89 04 24             	mov    %eax,(%esp)
80106e34:	e8 e8 dd ff ff       	call   80104c21 <growproc>
80106e39:	85 c0                	test   %eax,%eax
80106e3b:	79 07                	jns    80106e44 <sys_sbrk+0x45>
    return -1;
80106e3d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e42:	eb 03                	jmp    80106e47 <sys_sbrk+0x48>
  return addr;
80106e44:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106e47:	c9                   	leave  
80106e48:	c3                   	ret    

80106e49 <sys_sleep>:

int
sys_sleep(void)
{
80106e49:	55                   	push   %ebp
80106e4a:	89 e5                	mov    %esp,%ebp
80106e4c:	83 ec 28             	sub    $0x28,%esp
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80106e4f:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106e52:	89 44 24 04          	mov    %eax,0x4(%esp)
80106e56:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106e5d:	e8 5c f0 ff ff       	call   80105ebe <argint>
80106e62:	85 c0                	test   %eax,%eax
80106e64:	79 07                	jns    80106e6d <sys_sleep+0x24>
    return -1;
80106e66:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e6b:	eb 6c                	jmp    80106ed9 <sys_sleep+0x90>
  acquire(&tickslock);
80106e6d:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
80106e74:	e8 86 ea ff ff       	call   801058ff <acquire>
  ticks0 = ticks;
80106e79:	a1 74 64 11 80       	mov    0x80116474,%eax
80106e7e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
80106e81:	eb 34                	jmp    80106eb7 <sys_sleep+0x6e>
    if(proc->killed){
80106e83:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106e89:	8b 40 24             	mov    0x24(%eax),%eax
80106e8c:	85 c0                	test   %eax,%eax
80106e8e:	74 13                	je     80106ea3 <sys_sleep+0x5a>
      release(&tickslock);
80106e90:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
80106e97:	e8 cc ea ff ff       	call   80105968 <release>
      return -1;
80106e9c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ea1:	eb 36                	jmp    80106ed9 <sys_sleep+0x90>
    }
    sleep(&ticks, &tickslock);
80106ea3:	c7 44 24 04 40 64 11 	movl   $0x80116440,0x4(%esp)
80106eaa:	80 
80106eab:	c7 04 24 74 64 11 80 	movl   $0x80116474,(%esp)
80106eb2:	e8 66 e7 ff ff       	call   8010561d <sleep>

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
80106eb7:	a1 74 64 11 80       	mov    0x80116474,%eax
80106ebc:	89 c2                	mov    %eax,%edx
80106ebe:	2b 55 f4             	sub    -0xc(%ebp),%edx
80106ec1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106ec4:	39 c2                	cmp    %eax,%edx
80106ec6:	72 bb                	jb     80106e83 <sys_sleep+0x3a>
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
80106ec8:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
80106ecf:	e8 94 ea ff ff       	call   80105968 <release>
  return 0;
80106ed4:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106ed9:	c9                   	leave  
80106eda:	c3                   	ret    

80106edb <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80106edb:	55                   	push   %ebp
80106edc:	89 e5                	mov    %esp,%ebp
80106ede:	83 ec 28             	sub    $0x28,%esp
  uint xticks;

  acquire(&tickslock);
80106ee1:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
80106ee8:	e8 12 ea ff ff       	call   801058ff <acquire>
  xticks = ticks;
80106eed:	a1 74 64 11 80       	mov    0x80116474,%eax
80106ef2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  release(&tickslock);
80106ef5:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
80106efc:	e8 67 ea ff ff       	call   80105968 <release>
  return xticks;
80106f01:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106f04:	c9                   	leave  
80106f05:	c3                   	ret    
	...

80106f08 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80106f08:	55                   	push   %ebp
80106f09:	89 e5                	mov    %esp,%ebp
80106f0b:	83 ec 08             	sub    $0x8,%esp
80106f0e:	8b 55 08             	mov    0x8(%ebp),%edx
80106f11:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f14:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80106f18:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106f1b:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80106f1f:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106f23:	ee                   	out    %al,(%dx)
}
80106f24:	c9                   	leave  
80106f25:	c3                   	ret    

80106f26 <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
80106f26:	55                   	push   %ebp
80106f27:	89 e5                	mov    %esp,%ebp
80106f29:	83 ec 18             	sub    $0x18,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
80106f2c:	c7 44 24 04 34 00 00 	movl   $0x34,0x4(%esp)
80106f33:	00 
80106f34:	c7 04 24 43 00 00 00 	movl   $0x43,(%esp)
80106f3b:	e8 c8 ff ff ff       	call   80106f08 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
80106f40:	c7 44 24 04 9c 00 00 	movl   $0x9c,0x4(%esp)
80106f47:	00 
80106f48:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106f4f:	e8 b4 ff ff ff       	call   80106f08 <outb>
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
80106f54:	c7 44 24 04 2e 00 00 	movl   $0x2e,0x4(%esp)
80106f5b:	00 
80106f5c:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106f63:	e8 a0 ff ff ff       	call   80106f08 <outb>
  picenable(IRQ_TIMER);
80106f68:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106f6f:	e8 29 d5 ff ff       	call   8010449d <picenable>
}
80106f74:	c9                   	leave  
80106f75:	c3                   	ret    
	...

80106f78 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80106f78:	1e                   	push   %ds
  pushl %es
80106f79:	06                   	push   %es
  pushl %fs
80106f7a:	0f a0                	push   %fs
  pushl %gs
80106f7c:	0f a8                	push   %gs
  pushal
80106f7e:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
80106f7f:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80106f83:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80106f85:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
80106f87:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
80106f8b:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
80106f8d:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
80106f8f:	54                   	push   %esp
  call trap
80106f90:	e8 b9 02 00 00       	call   8010724e <trap>
  addl $4, %esp
80106f95:	83 c4 04             	add    $0x4,%esp

80106f98 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80106f98:	61                   	popa   
  popl %gs
80106f99:	0f a9                	pop    %gs
  popl %fs
80106f9b:	0f a1                	pop    %fs
  popl %es
80106f9d:	07                   	pop    %es
  popl %ds
80106f9e:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80106f9f:	83 c4 08             	add    $0x8,%esp
  iret
80106fa2:	cf                   	iret   
	...

80106fa4 <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
80106fa4:	55                   	push   %ebp
80106fa5:	89 e5                	mov    %esp,%ebp
80106fa7:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80106faa:	8b 45 0c             	mov    0xc(%ebp),%eax
80106fad:	83 e8 01             	sub    $0x1,%eax
80106fb0:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80106fb4:	8b 45 08             	mov    0x8(%ebp),%eax
80106fb7:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80106fbb:	8b 45 08             	mov    0x8(%ebp),%eax
80106fbe:	c1 e8 10             	shr    $0x10,%eax
80106fc1:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
80106fc5:	8d 45 fa             	lea    -0x6(%ebp),%eax
80106fc8:	0f 01 18             	lidtl  (%eax)
}
80106fcb:	c9                   	leave  
80106fcc:	c3                   	ret    

80106fcd <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
80106fcd:	55                   	push   %ebp
80106fce:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80106fd0:	fb                   	sti    
}
80106fd1:	5d                   	pop    %ebp
80106fd2:	c3                   	ret    

80106fd3 <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
80106fd3:	55                   	push   %ebp
80106fd4:	89 e5                	mov    %esp,%ebp
80106fd6:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80106fd9:	0f 20 d0             	mov    %cr2,%eax
80106fdc:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return val;
80106fdf:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106fe2:	c9                   	leave  
80106fe3:	c3                   	ret    

80106fe4 <tvinit>:
int capturing = 0;
struct capture_stats cs;

void
tvinit(void)
{
80106fe4:	55                   	push   %ebp
80106fe5:	89 e5                	mov    %esp,%ebp
80106fe7:	83 ec 28             	sub    $0x28,%esp
  int i;

  for(i = 0; i < 256; i++)
80106fea:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80106ff1:	e9 bf 00 00 00       	jmp    801070b5 <tvinit+0xd1>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80106ff6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ff9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106ffc:	8b 14 95 c4 c0 10 80 	mov    -0x7fef3f3c(,%edx,4),%edx
80107003:	66 89 14 c5 c0 c6 10 	mov    %dx,-0x7fef3940(,%eax,8)
8010700a:	80 
8010700b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010700e:	66 c7 04 c5 c2 c6 10 	movw   $0x8,-0x7fef393e(,%eax,8)
80107015:	80 08 00 
80107018:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010701b:	0f b6 14 c5 c4 c6 10 	movzbl -0x7fef393c(,%eax,8),%edx
80107022:	80 
80107023:	83 e2 e0             	and    $0xffffffe0,%edx
80107026:	88 14 c5 c4 c6 10 80 	mov    %dl,-0x7fef393c(,%eax,8)
8010702d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107030:	0f b6 14 c5 c4 c6 10 	movzbl -0x7fef393c(,%eax,8),%edx
80107037:	80 
80107038:	83 e2 1f             	and    $0x1f,%edx
8010703b:	88 14 c5 c4 c6 10 80 	mov    %dl,-0x7fef393c(,%eax,8)
80107042:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107045:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
8010704c:	80 
8010704d:	83 e2 f0             	and    $0xfffffff0,%edx
80107050:	83 ca 0e             	or     $0xe,%edx
80107053:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
8010705a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010705d:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80107064:	80 
80107065:	83 e2 ef             	and    $0xffffffef,%edx
80107068:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
8010706f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107072:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
80107079:	80 
8010707a:	83 e2 9f             	and    $0xffffff9f,%edx
8010707d:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80107084:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107087:	0f b6 14 c5 c5 c6 10 	movzbl -0x7fef393b(,%eax,8),%edx
8010708e:	80 
8010708f:	83 ca 80             	or     $0xffffff80,%edx
80107092:	88 14 c5 c5 c6 10 80 	mov    %dl,-0x7fef393b(,%eax,8)
80107099:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010709c:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010709f:	8b 14 95 c4 c0 10 80 	mov    -0x7fef3f3c(,%edx,4),%edx
801070a6:	c1 ea 10             	shr    $0x10,%edx
801070a9:	66 89 14 c5 c6 c6 10 	mov    %dx,-0x7fef393a(,%eax,8)
801070b0:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
801070b1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801070b5:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
801070bc:	0f 8e 34 ff ff ff    	jle    80106ff6 <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
801070c2:	a1 c4 c1 10 80       	mov    0x8010c1c4,%eax
801070c7:	66 a3 c0 c8 10 80    	mov    %ax,0x8010c8c0
801070cd:	66 c7 05 c2 c8 10 80 	movw   $0x8,0x8010c8c2
801070d4:	08 00 
801070d6:	0f b6 05 c4 c8 10 80 	movzbl 0x8010c8c4,%eax
801070dd:	83 e0 e0             	and    $0xffffffe0,%eax
801070e0:	a2 c4 c8 10 80       	mov    %al,0x8010c8c4
801070e5:	0f b6 05 c4 c8 10 80 	movzbl 0x8010c8c4,%eax
801070ec:	83 e0 1f             	and    $0x1f,%eax
801070ef:	a2 c4 c8 10 80       	mov    %al,0x8010c8c4
801070f4:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
801070fb:	83 c8 0f             	or     $0xf,%eax
801070fe:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80107103:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
8010710a:	83 e0 ef             	and    $0xffffffef,%eax
8010710d:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80107112:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80107119:	83 c8 60             	or     $0x60,%eax
8010711c:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80107121:	0f b6 05 c5 c8 10 80 	movzbl 0x8010c8c5,%eax
80107128:	83 c8 80             	or     $0xffffff80,%eax
8010712b:	a2 c5 c8 10 80       	mov    %al,0x8010c8c5
80107130:	a1 c4 c1 10 80       	mov    0x8010c1c4,%eax
80107135:	c1 e8 10             	shr    $0x10,%eax
80107138:	66 a3 c6 c8 10 80    	mov    %ax,0x8010c8c6

  initlock(&tickslock, "time");
8010713e:	c7 44 24 04 6c 96 10 	movl   $0x8010966c,0x4(%esp)
80107145:	80 
80107146:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
8010714d:	e8 8c e7 ff ff       	call   801058de <initlock>

  ticks = 0;
80107152:	c7 05 74 64 11 80 00 	movl   $0x0,0x80116474
80107159:	00 00 00 
}
8010715c:	c9                   	leave  
8010715d:	c3                   	ret    

8010715e <idtinit>:

void
idtinit(void)
{
8010715e:	55                   	push   %ebp
8010715f:	89 e5                	mov    %esp,%ebp
80107161:	83 ec 08             	sub    $0x8,%esp
  lidt(idt, sizeof(idt));
80107164:	c7 44 24 04 00 08 00 	movl   $0x800,0x4(%esp)
8010716b:	00 
8010716c:	c7 04 24 c0 c6 10 80 	movl   $0x8010c6c0,(%esp)
80107173:	e8 2c fe ff ff       	call   80106fa4 <lidt>
}
80107178:	c9                   	leave  
80107179:	c3                   	ret    

8010717a <trap_exit>:

//PAGEBREAK: 41
// This is here so you can break on it in gdb
static void
trap_exit()
{
8010717a:	55                   	push   %ebp
8010717b:	89 e5                	mov    %esp,%ebp
8010717d:	83 ec 08             	sub    $0x8,%esp
  sti();
80107180:	e8 48 fe ff ff       	call   80106fcd <sti>
  exit();
80107185:	e8 d1 dc ff ff       	call   80104e5b <exit>
}
8010718a:	c9                   	leave  
8010718b:	c3                   	ret    

8010718c <start_capture>:

void start_capture(void)
{
8010718c:	55                   	push   %ebp
8010718d:	89 e5                	mov    %esp,%ebp
    cs.tick_count = 0;
8010718f:	c7 05 20 64 11 80 00 	movl   $0x0,0x80116420
80107196:	00 00 00 
    cs.proc_exit_count = 0;
80107199:	c7 05 24 64 11 80 00 	movl   $0x0,0x80116424
801071a0:	00 00 00 
    cs.proc_tick_count = 0;
801071a3:	c7 05 28 64 11 80 00 	movl   $0x0,0x80116428
801071aa:	00 00 00 
    cs.proc_firstrun_count = 0;
801071ad:	c7 05 2c 64 11 80 00 	movl   $0x0,0x8011642c
801071b4:	00 00 00 
    capturing = 1;
801071b7:	c7 05 a0 c6 10 80 01 	movl   $0x1,0x8010c6a0
801071be:	00 00 00 
}
801071c1:	5d                   	pop    %ebp
801071c2:	c3                   	ret    

801071c3 <stop_capture>:

void stop_capture(struct capture_stats* p)
{
801071c3:	55                   	push   %ebp
801071c4:	89 e5                	mov    %esp,%ebp
    capturing = 0;
801071c6:	c7 05 a0 c6 10 80 00 	movl   $0x0,0x8010c6a0
801071cd:	00 00 00 
    p->tick_count = cs.tick_count;
801071d0:	8b 15 20 64 11 80    	mov    0x80116420,%edx
801071d6:	8b 45 08             	mov    0x8(%ebp),%eax
801071d9:	89 10                	mov    %edx,(%eax)
    p->proc_exit_count = cs.proc_exit_count;
801071db:	8b 15 24 64 11 80    	mov    0x80116424,%edx
801071e1:	8b 45 08             	mov    0x8(%ebp),%eax
801071e4:	89 50 04             	mov    %edx,0x4(%eax)
    p->proc_tick_count = cs.proc_tick_count;
801071e7:	8b 15 28 64 11 80    	mov    0x80116428,%edx
801071ed:	8b 45 08             	mov    0x8(%ebp),%eax
801071f0:	89 50 08             	mov    %edx,0x8(%eax)
    p->proc_firstrun_count = cs.proc_firstrun_count;
801071f3:	8b 15 2c 64 11 80    	mov    0x8011642c,%edx
801071f9:	8b 45 08             	mov    0x8(%ebp),%eax
801071fc:	89 50 0c             	mov    %edx,0xc(%eax)
}
801071ff:	5d                   	pop    %ebp
80107200:	c3                   	ret    

80107201 <sys_start_capture>:

int sys_start_capture(void)
{
80107201:	55                   	push   %ebp
80107202:	89 e5                	mov    %esp,%ebp
    start_capture();
80107204:	e8 83 ff ff ff       	call   8010718c <start_capture>
    return 0;
80107209:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010720e:	5d                   	pop    %ebp
8010720f:	c3                   	ret    

80107210 <sys_stop_capture>:

int sys_stop_capture(void)
{
80107210:	55                   	push   %ebp
80107211:	89 e5                	mov    %esp,%ebp
80107213:	83 ec 28             	sub    $0x28,%esp
    struct capture_stats *p;
    if(argptr(0, (void*)&p, sizeof(*p)) < 0)
80107216:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107219:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80107220:	00 
80107221:	89 44 24 04          	mov    %eax,0x4(%esp)
80107225:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010722c:	e8 bc ec ff ff       	call   80105eed <argptr>
80107231:	85 c0                	test   %eax,%eax
80107233:	79 07                	jns    8010723c <sys_stop_capture+0x2c>
        return -1;
80107235:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010723a:	eb 10                	jmp    8010724c <sys_stop_capture+0x3c>
    stop_capture(p);
8010723c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010723f:	89 04 24             	mov    %eax,(%esp)
80107242:	e8 7c ff ff ff       	call   801071c3 <stop_capture>
    return 0;
80107247:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010724c:	c9                   	leave  
8010724d:	c3                   	ret    

8010724e <trap>:

void
trap(struct trapframe *tf)
{
8010724e:	55                   	push   %ebp
8010724f:	89 e5                	mov    %esp,%ebp
80107251:	57                   	push   %edi
80107252:	56                   	push   %esi
80107253:	53                   	push   %ebx
80107254:	83 ec 3c             	sub    $0x3c,%esp
  if(tf->trapno == T_SYSCALL){
80107257:	8b 45 08             	mov    0x8(%ebp),%eax
8010725a:	8b 40 30             	mov    0x30(%eax),%eax
8010725d:	83 f8 40             	cmp    $0x40,%eax
80107260:	75 3e                	jne    801072a0 <trap+0x52>
    if(proc->killed)
80107262:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107268:	8b 40 24             	mov    0x24(%eax),%eax
8010726b:	85 c0                	test   %eax,%eax
8010726d:	74 05                	je     80107274 <trap+0x26>
      exit();
8010726f:	e8 e7 db ff ff       	call   80104e5b <exit>
    proc->tf = tf;
80107274:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010727a:	8b 55 08             	mov    0x8(%ebp),%edx
8010727d:	89 50 18             	mov    %edx,0x18(%eax)
    syscall();
80107280:	e8 01 ed ff ff       	call   80105f86 <syscall>
    if(proc->killed)
80107285:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010728b:	8b 40 24             	mov    0x24(%eax),%eax
8010728e:	85 c0                	test   %eax,%eax
80107290:	0f 84 4a 02 00 00    	je     801074e0 <trap+0x292>
      exit();
80107296:	e8 c0 db ff ff       	call   80104e5b <exit>
    return;
8010729b:	e9 41 02 00 00       	jmp    801074e1 <trap+0x293>
  }

  switch(tf->trapno){
801072a0:	8b 45 08             	mov    0x8(%ebp),%eax
801072a3:	8b 40 30             	mov    0x30(%eax),%eax
801072a6:	83 e8 20             	sub    $0x20,%eax
801072a9:	83 f8 1f             	cmp    $0x1f,%eax
801072ac:	0f 87 d2 00 00 00    	ja     80107384 <trap+0x136>
801072b2:	8b 04 85 14 97 10 80 	mov    -0x7fef68ec(,%eax,4),%eax
801072b9:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
    if(cpu->id == 0){
801072bb:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801072c1:	0f b6 00             	movzbl (%eax),%eax
801072c4:	84 c0                	test   %al,%al
801072c6:	75 47                	jne    8010730f <trap+0xc1>
      acquire(&tickslock);
801072c8:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
801072cf:	e8 2b e6 ff ff       	call   801058ff <acquire>
      if(capturing)
801072d4:	a1 a0 c6 10 80       	mov    0x8010c6a0,%eax
801072d9:	85 c0                	test   %eax,%eax
801072db:	74 0d                	je     801072ea <trap+0x9c>
          cs.tick_count++;
801072dd:	a1 20 64 11 80       	mov    0x80116420,%eax
801072e2:	83 c0 01             	add    $0x1,%eax
801072e5:	a3 20 64 11 80       	mov    %eax,0x80116420
      ticks++;
801072ea:	a1 74 64 11 80       	mov    0x80116474,%eax
801072ef:	83 c0 01             	add    $0x1,%eax
801072f2:	a3 74 64 11 80       	mov    %eax,0x80116474
      wakeup(&ticks);
801072f7:	c7 04 24 74 64 11 80 	movl   $0x80116474,(%esp)
801072fe:	e8 f7 e3 ff ff       	call   801056fa <wakeup>
      release(&tickslock);
80107303:	c7 04 24 40 64 11 80 	movl   $0x80116440,(%esp)
8010730a:	e8 59 e6 ff ff       	call   80105968 <release>
    }
    lapiceoi();
8010730f:	e8 42 c4 ff ff       	call   80103756 <lapiceoi>
    break;
80107314:	e9 41 01 00 00       	jmp    8010745a <trap+0x20c>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
80107319:	e8 2f bc ff ff       	call   80102f4d <ideintr>
    lapiceoi();
8010731e:	e8 33 c4 ff ff       	call   80103756 <lapiceoi>
    break;
80107323:	e9 32 01 00 00       	jmp    8010745a <trap+0x20c>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
80107328:	e8 2a c2 ff ff       	call   80103557 <kbdintr>
    lapiceoi();
8010732d:	e8 24 c4 ff ff       	call   80103756 <lapiceoi>
    break;
80107332:	e9 23 01 00 00       	jmp    8010745a <trap+0x20c>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
80107337:	e8 b9 03 00 00       	call   801076f5 <uartintr>
    lapiceoi();
8010733c:	e8 15 c4 ff ff       	call   80103756 <lapiceoi>
    break;
80107341:	e9 14 01 00 00       	jmp    8010745a <trap+0x20c>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80107346:	8b 45 08             	mov    0x8(%ebp),%eax
80107349:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
8010734c:	8b 45 08             	mov    0x8(%ebp),%eax
8010734f:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80107353:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
80107356:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010735c:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
8010735f:	0f b6 c0             	movzbl %al,%eax
80107362:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80107366:	89 54 24 08          	mov    %edx,0x8(%esp)
8010736a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010736e:	c7 04 24 74 96 10 80 	movl   $0x80109674,(%esp)
80107375:	e8 4f 93 ff ff       	call   801006c9 <cprintf>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
8010737a:	e8 d7 c3 ff ff       	call   80103756 <lapiceoi>
    break;
8010737f:	e9 d6 00 00 00       	jmp    8010745a <trap+0x20c>

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
80107384:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010738a:	85 c0                	test   %eax,%eax
8010738c:	74 11                	je     8010739f <trap+0x151>
8010738e:	8b 45 08             	mov    0x8(%ebp),%eax
80107391:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80107395:	0f b7 c0             	movzwl %ax,%eax
80107398:	83 e0 03             	and    $0x3,%eax
8010739b:	85 c0                	test   %eax,%eax
8010739d:	75 46                	jne    801073e5 <trap+0x197>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
8010739f:	e8 2f fc ff ff       	call   80106fd3 <rcr2>
801073a4:	8b 55 08             	mov    0x8(%ebp),%edx
801073a7:	8b 5a 38             	mov    0x38(%edx),%ebx
              tf->trapno, cpu->id, tf->eip, rcr2());
801073aa:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801073b1:	0f b6 12             	movzbl (%edx),%edx

  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
801073b4:	0f b6 ca             	movzbl %dl,%ecx
801073b7:	8b 55 08             	mov    0x8(%ebp),%edx
801073ba:	8b 52 30             	mov    0x30(%edx),%edx
801073bd:	89 44 24 10          	mov    %eax,0x10(%esp)
801073c1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
801073c5:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801073c9:	89 54 24 04          	mov    %edx,0x4(%esp)
801073cd:	c7 04 24 98 96 10 80 	movl   $0x80109698,(%esp)
801073d4:	e8 f0 92 ff ff       	call   801006c9 <cprintf>
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
801073d9:	c7 04 24 ca 96 10 80 	movl   $0x801096ca,(%esp)
801073e0:	e8 84 94 ff ff       	call   80100869 <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
801073e5:	e8 e9 fb ff ff       	call   80106fd3 <rcr2>
801073ea:	89 c2                	mov    %eax,%edx
801073ec:	8b 45 08             	mov    0x8(%ebp),%eax
801073ef:	8b 78 38             	mov    0x38(%eax),%edi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
801073f2:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801073f8:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
801073fb:	0f b6 f0             	movzbl %al,%esi
801073fe:	8b 45 08             	mov    0x8(%ebp),%eax
80107401:	8b 58 34             	mov    0x34(%eax),%ebx
80107404:	8b 45 08             	mov    0x8(%ebp),%eax
80107407:	8b 48 30             	mov    0x30(%eax),%ecx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
8010740a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80107410:	83 c0 6c             	add    $0x6c,%eax
80107413:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
80107416:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010741c:	8b 40 10             	mov    0x10(%eax),%eax
8010741f:	89 54 24 1c          	mov    %edx,0x1c(%esp)
80107423:	89 7c 24 18          	mov    %edi,0x18(%esp)
80107427:	89 74 24 14          	mov    %esi,0x14(%esp)
8010742b:	89 5c 24 10          	mov    %ebx,0x10(%esp)
8010742f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80107433:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80107436:	89 54 24 08          	mov    %edx,0x8(%esp)
8010743a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010743e:	c7 04 24 d0 96 10 80 	movl   $0x801096d0,(%esp)
80107445:	e8 7f 92 ff ff       	call   801006c9 <cprintf>
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip,
            rcr2());
    proc->killed = 1;
8010744a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107450:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80107457:	eb 01                	jmp    8010745a <trap+0x20c>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
80107459:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
8010745a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107460:	85 c0                	test   %eax,%eax
80107462:	74 24                	je     80107488 <trap+0x23a>
80107464:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010746a:	8b 40 24             	mov    0x24(%eax),%eax
8010746d:	85 c0                	test   %eax,%eax
8010746f:	74 17                	je     80107488 <trap+0x23a>
80107471:	8b 45 08             	mov    0x8(%ebp),%eax
80107474:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80107478:	0f b7 c0             	movzwl %ax,%eax
8010747b:	83 e0 03             	and    $0x3,%eax
8010747e:	83 f8 03             	cmp    $0x3,%eax
80107481:	75 05                	jne    80107488 <trap+0x23a>
    trap_exit();
80107483:	e8 f2 fc ff ff       	call   8010717a <trap_exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
80107488:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010748e:	85 c0                	test   %eax,%eax
80107490:	74 1e                	je     801074b0 <trap+0x262>
80107492:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107498:	8b 40 0c             	mov    0xc(%eax),%eax
8010749b:	83 f8 04             	cmp    $0x4,%eax
8010749e:	75 10                	jne    801074b0 <trap+0x262>
801074a0:	8b 45 08             	mov    0x8(%ebp),%eax
801074a3:	8b 40 30             	mov    0x30(%eax),%eax
801074a6:	83 f8 20             	cmp    $0x20,%eax
801074a9:	75 05                	jne    801074b0 <trap+0x262>
    yield();
801074ab:	e8 fc e0 ff ff       	call   801055ac <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
801074b0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801074b6:	85 c0                	test   %eax,%eax
801074b8:	74 27                	je     801074e1 <trap+0x293>
801074ba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801074c0:	8b 40 24             	mov    0x24(%eax),%eax
801074c3:	85 c0                	test   %eax,%eax
801074c5:	74 1a                	je     801074e1 <trap+0x293>
801074c7:	8b 45 08             	mov    0x8(%ebp),%eax
801074ca:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801074ce:	0f b7 c0             	movzwl %ax,%eax
801074d1:	83 e0 03             	and    $0x3,%eax
801074d4:	83 f8 03             	cmp    $0x3,%eax
801074d7:	75 08                	jne    801074e1 <trap+0x293>
    trap_exit();
801074d9:	e8 9c fc ff ff       	call   8010717a <trap_exit>
801074de:	eb 01                	jmp    801074e1 <trap+0x293>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
801074e0:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    trap_exit();
}
801074e1:	83 c4 3c             	add    $0x3c,%esp
801074e4:	5b                   	pop    %ebx
801074e5:	5e                   	pop    %esi
801074e6:	5f                   	pop    %edi
801074e7:	5d                   	pop    %ebp
801074e8:	c3                   	ret    
801074e9:	00 00                	add    %al,(%eax)
	...

801074ec <inb>:
// Routines to let C code use special x86 instructions.
// Syntax reminder: (instructions : output : input : clobber)

static inline uchar
inb(ushort port)
{
801074ec:	55                   	push   %ebp
801074ed:	89 e5                	mov    %esp,%ebp
801074ef:	83 ec 14             	sub    $0x14,%esp
801074f2:	8b 45 08             	mov    0x8(%ebp),%eax
801074f5:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801074f9:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801074fd:	89 c2                	mov    %eax,%edx
801074ff:	ec                   	in     (%dx),%al
80107500:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80107503:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80107507:	c9                   	leave  
80107508:	c3                   	ret    

80107509 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80107509:	55                   	push   %ebp
8010750a:	89 e5                	mov    %esp,%ebp
8010750c:	83 ec 08             	sub    $0x8,%esp
8010750f:	8b 55 08             	mov    0x8(%ebp),%edx
80107512:	8b 45 0c             	mov    0xc(%ebp),%eax
80107515:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80107519:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010751c:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80107520:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80107524:	ee                   	out    %al,(%dx)
}
80107525:	c9                   	leave  
80107526:	c3                   	ret    

80107527 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
80107527:	55                   	push   %ebp
80107528:	89 e5                	mov    %esp,%ebp
8010752a:	83 ec 28             	sub    $0x28,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+UART_FIFO_CONTROL, 0);
8010752d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107534:	00 
80107535:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
8010753c:	e8 c8 ff ff ff       	call   80107509 <outb>

  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+UART_LINE_CONTROL, UART_DIVISOR_LATCH);     // Unlock divisor
80107541:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
80107548:	00 
80107549:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
80107550:	e8 b4 ff ff ff       	call   80107509 <outb>
  outb(COM1+UART_DIVISOR_LOW, 115200/9600);
80107555:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
8010755c:	00 
8010755d:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
80107564:	e8 a0 ff ff ff       	call   80107509 <outb>
  outb(COM1+UART_DIVISOR_HIGH, 0);
80107569:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107570:	00 
80107571:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
80107578:	e8 8c ff ff ff       	call   80107509 <outb>
  outb(COM1+UART_LINE_CONTROL, 0x03);   // Lock divisor, 8 data bits.
8010757d:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80107584:	00 
80107585:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
8010758c:	e8 78 ff ff ff       	call   80107509 <outb>
  outb(COM1+UART_MODEM_CONTROL, 0);
80107591:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107598:	00 
80107599:	c7 04 24 fc 03 00 00 	movl   $0x3fc,(%esp)
801075a0:	e8 64 ff ff ff       	call   80107509 <outb>

  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);
801075a5:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
801075ac:	00 
801075ad:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
801075b4:	e8 50 ff ff ff       	call   80107509 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
801075b9:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
801075c0:	e8 27 ff ff ff       	call   801074ec <inb>
801075c5:	3c ff                	cmp    $0xff,%al
801075c7:	74 6c                	je     80107635 <uartinit+0x10e>
    return;
  uart = 1;
801075c9:	c7 05 c0 ce 10 80 01 	movl   $0x1,0x8010cec0
801075d0:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+UART_INTERRUPT_ID);
801075d3:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
801075da:	e8 0d ff ff ff       	call   801074ec <inb>
  inb(COM1+UART_RECEIVE_BUFFER);
801075df:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801075e6:	e8 01 ff ff ff       	call   801074ec <inb>
  picenable(IRQ_COM1);
801075eb:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
801075f2:	e8 a6 ce ff ff       	call   8010449d <picenable>
  ioapicenable(IRQ_COM1, 0);
801075f7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801075fe:	00 
801075ff:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
80107606:	e8 ef bb ff ff       	call   801031fa <ioapicenable>

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
8010760b:	c7 45 f4 94 97 10 80 	movl   $0x80109794,-0xc(%ebp)
80107612:	eb 15                	jmp    80107629 <uartinit+0x102>
    uartputc(*p);
80107614:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107617:	0f b6 00             	movzbl (%eax),%eax
8010761a:	0f be c0             	movsbl %al,%eax
8010761d:	89 04 24             	mov    %eax,(%esp)
80107620:	e8 47 00 00 00       	call   8010766c <uartputc>
  inb(COM1+UART_RECEIVE_BUFFER);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80107625:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80107629:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010762c:	0f b6 00             	movzbl (%eax),%eax
8010762f:	84 c0                	test   %al,%al
80107631:	75 e1                	jne    80107614 <uartinit+0xed>
80107633:	eb 01                	jmp    80107636 <uartinit+0x10f>
  // Enable receive interrupts.
  outb(COM1+UART_INTERRUPT_ENABLE, UART_RECEIVE_INTERRUPT);

  // If status is 0xFF, no serial port.
  if(inb(COM1+UART_LINE_STATUS) == 0xFF)
    return;
80107635:	90                   	nop
  ioapicenable(IRQ_COM1, 0);

  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
80107636:	c9                   	leave  
80107637:	c3                   	ret    

80107638 <cantransmit>:

static int
cantransmit(void)
{
80107638:	55                   	push   %ebp
80107639:	89 e5                	mov    %esp,%ebp
8010763b:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_TRANSMIT_READY;
8010763e:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
80107645:	e8 a2 fe ff ff       	call   801074ec <inb>
8010764a:	0f b6 c0             	movzbl %al,%eax
8010764d:	83 e0 20             	and    $0x20,%eax
}
80107650:	c9                   	leave  
80107651:	c3                   	ret    

80107652 <hasreceived>:

static int
hasreceived(void)
{
80107652:	55                   	push   %ebp
80107653:	89 e5                	mov    %esp,%ebp
80107655:	83 ec 04             	sub    $0x4,%esp
  return inb(COM1+UART_LINE_STATUS) & UART_RECEIVE_READY;
80107658:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
8010765f:	e8 88 fe ff ff       	call   801074ec <inb>
80107664:	0f b6 c0             	movzbl %al,%eax
80107667:	83 e0 01             	and    $0x1,%eax
}
8010766a:	c9                   	leave  
8010766b:	c3                   	ret    

8010766c <uartputc>:

void
uartputc(int c)
{
8010766c:	55                   	push   %ebp
8010766d:	89 e5                	mov    %esp,%ebp
8010766f:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
80107672:	a1 c0 ce 10 80       	mov    0x8010cec0,%eax
80107677:	85 c0                	test   %eax,%eax
80107679:	74 40                	je     801076bb <uartputc+0x4f>
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
8010767b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80107682:	eb 10                	jmp    80107694 <uartputc+0x28>
    microdelay(10);
80107684:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
8010768b:	e8 eb c0 ff ff       	call   8010377b <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !cantransmit(); i++)
80107690:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80107694:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
80107698:	7f 09                	jg     801076a3 <uartputc+0x37>
8010769a:	e8 99 ff ff ff       	call   80107638 <cantransmit>
8010769f:	85 c0                	test   %eax,%eax
801076a1:	74 e1                	je     80107684 <uartputc+0x18>
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
801076a3:	8b 45 08             	mov    0x8(%ebp),%eax
801076a6:	0f b6 c0             	movzbl %al,%eax
801076a9:	89 44 24 04          	mov    %eax,0x4(%esp)
801076ad:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801076b4:	e8 50 fe ff ff       	call   80107509 <outb>
801076b9:	eb 01                	jmp    801076bc <uartputc+0x50>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
801076bb:	90                   	nop
  for(i = 0; i < 128 && !cantransmit(); i++)
    microdelay(10);
  outb(COM1+UART_TRANSMIT_BUFFER, c);
}
801076bc:	c9                   	leave  
801076bd:	c3                   	ret    

801076be <uartgetc>:

static int
uartgetc(void)
{
801076be:	55                   	push   %ebp
801076bf:	89 e5                	mov    %esp,%ebp
801076c1:	83 ec 04             	sub    $0x4,%esp
  if(!uart)
801076c4:	a1 c0 ce 10 80       	mov    0x8010cec0,%eax
801076c9:	85 c0                	test   %eax,%eax
801076cb:	75 07                	jne    801076d4 <uartgetc+0x16>
    return -1;
801076cd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801076d2:	eb 1f                	jmp    801076f3 <uartgetc+0x35>
  if(!hasreceived())
801076d4:	e8 79 ff ff ff       	call   80107652 <hasreceived>
801076d9:	85 c0                	test   %eax,%eax
801076db:	75 07                	jne    801076e4 <uartgetc+0x26>
    return -1;
801076dd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801076e2:	eb 0f                	jmp    801076f3 <uartgetc+0x35>
  return inb(COM1+UART_RECEIVE_BUFFER);
801076e4:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801076eb:	e8 fc fd ff ff       	call   801074ec <inb>
801076f0:	0f b6 c0             	movzbl %al,%eax
}
801076f3:	c9                   	leave  
801076f4:	c3                   	ret    

801076f5 <uartintr>:

void
uartintr(void)
{
801076f5:	55                   	push   %ebp
801076f6:	89 e5                	mov    %esp,%ebp
801076f8:	83 ec 18             	sub    $0x18,%esp
  consoleintr(uartgetc);
801076fb:	c7 04 24 be 76 10 80 	movl   $0x801076be,(%esp)
80107702:	e8 f6 93 ff ff       	call   80100afd <consoleintr>
}
80107707:	c9                   	leave  
80107708:	c3                   	ret    
80107709:	00 00                	add    %al,(%eax)
	...

8010770c <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
8010770c:	6a 00                	push   $0x0
  pushl $0
8010770e:	6a 00                	push   $0x0
  jmp alltraps
80107710:	e9 63 f8 ff ff       	jmp    80106f78 <alltraps>

80107715 <vector1>:
.globl vector1
vector1:
  pushl $0
80107715:	6a 00                	push   $0x0
  pushl $1
80107717:	6a 01                	push   $0x1
  jmp alltraps
80107719:	e9 5a f8 ff ff       	jmp    80106f78 <alltraps>

8010771e <vector2>:
.globl vector2
vector2:
  pushl $0
8010771e:	6a 00                	push   $0x0
  pushl $2
80107720:	6a 02                	push   $0x2
  jmp alltraps
80107722:	e9 51 f8 ff ff       	jmp    80106f78 <alltraps>

80107727 <vector3>:
.globl vector3
vector3:
  pushl $0
80107727:	6a 00                	push   $0x0
  pushl $3
80107729:	6a 03                	push   $0x3
  jmp alltraps
8010772b:	e9 48 f8 ff ff       	jmp    80106f78 <alltraps>

80107730 <vector4>:
.globl vector4
vector4:
  pushl $0
80107730:	6a 00                	push   $0x0
  pushl $4
80107732:	6a 04                	push   $0x4
  jmp alltraps
80107734:	e9 3f f8 ff ff       	jmp    80106f78 <alltraps>

80107739 <vector5>:
.globl vector5
vector5:
  pushl $0
80107739:	6a 00                	push   $0x0
  pushl $5
8010773b:	6a 05                	push   $0x5
  jmp alltraps
8010773d:	e9 36 f8 ff ff       	jmp    80106f78 <alltraps>

80107742 <vector6>:
.globl vector6
vector6:
  pushl $0
80107742:	6a 00                	push   $0x0
  pushl $6
80107744:	6a 06                	push   $0x6
  jmp alltraps
80107746:	e9 2d f8 ff ff       	jmp    80106f78 <alltraps>

8010774b <vector7>:
.globl vector7
vector7:
  pushl $0
8010774b:	6a 00                	push   $0x0
  pushl $7
8010774d:	6a 07                	push   $0x7
  jmp alltraps
8010774f:	e9 24 f8 ff ff       	jmp    80106f78 <alltraps>

80107754 <vector8>:
.globl vector8
vector8:
  pushl $8
80107754:	6a 08                	push   $0x8
  jmp alltraps
80107756:	e9 1d f8 ff ff       	jmp    80106f78 <alltraps>

8010775b <vector9>:
.globl vector9
vector9:
  pushl $0
8010775b:	6a 00                	push   $0x0
  pushl $9
8010775d:	6a 09                	push   $0x9
  jmp alltraps
8010775f:	e9 14 f8 ff ff       	jmp    80106f78 <alltraps>

80107764 <vector10>:
.globl vector10
vector10:
  pushl $10
80107764:	6a 0a                	push   $0xa
  jmp alltraps
80107766:	e9 0d f8 ff ff       	jmp    80106f78 <alltraps>

8010776b <vector11>:
.globl vector11
vector11:
  pushl $11
8010776b:	6a 0b                	push   $0xb
  jmp alltraps
8010776d:	e9 06 f8 ff ff       	jmp    80106f78 <alltraps>

80107772 <vector12>:
.globl vector12
vector12:
  pushl $12
80107772:	6a 0c                	push   $0xc
  jmp alltraps
80107774:	e9 ff f7 ff ff       	jmp    80106f78 <alltraps>

80107779 <vector13>:
.globl vector13
vector13:
  pushl $13
80107779:	6a 0d                	push   $0xd
  jmp alltraps
8010777b:	e9 f8 f7 ff ff       	jmp    80106f78 <alltraps>

80107780 <vector14>:
.globl vector14
vector14:
  pushl $14
80107780:	6a 0e                	push   $0xe
  jmp alltraps
80107782:	e9 f1 f7 ff ff       	jmp    80106f78 <alltraps>

80107787 <vector15>:
.globl vector15
vector15:
  pushl $0
80107787:	6a 00                	push   $0x0
  pushl $15
80107789:	6a 0f                	push   $0xf
  jmp alltraps
8010778b:	e9 e8 f7 ff ff       	jmp    80106f78 <alltraps>

80107790 <vector16>:
.globl vector16
vector16:
  pushl $0
80107790:	6a 00                	push   $0x0
  pushl $16
80107792:	6a 10                	push   $0x10
  jmp alltraps
80107794:	e9 df f7 ff ff       	jmp    80106f78 <alltraps>

80107799 <vector17>:
.globl vector17
vector17:
  pushl $17
80107799:	6a 11                	push   $0x11
  jmp alltraps
8010779b:	e9 d8 f7 ff ff       	jmp    80106f78 <alltraps>

801077a0 <vector18>:
.globl vector18
vector18:
  pushl $0
801077a0:	6a 00                	push   $0x0
  pushl $18
801077a2:	6a 12                	push   $0x12
  jmp alltraps
801077a4:	e9 cf f7 ff ff       	jmp    80106f78 <alltraps>

801077a9 <vector19>:
.globl vector19
vector19:
  pushl $0
801077a9:	6a 00                	push   $0x0
  pushl $19
801077ab:	6a 13                	push   $0x13
  jmp alltraps
801077ad:	e9 c6 f7 ff ff       	jmp    80106f78 <alltraps>

801077b2 <vector20>:
.globl vector20
vector20:
  pushl $0
801077b2:	6a 00                	push   $0x0
  pushl $20
801077b4:	6a 14                	push   $0x14
  jmp alltraps
801077b6:	e9 bd f7 ff ff       	jmp    80106f78 <alltraps>

801077bb <vector21>:
.globl vector21
vector21:
  pushl $0
801077bb:	6a 00                	push   $0x0
  pushl $21
801077bd:	6a 15                	push   $0x15
  jmp alltraps
801077bf:	e9 b4 f7 ff ff       	jmp    80106f78 <alltraps>

801077c4 <vector22>:
.globl vector22
vector22:
  pushl $0
801077c4:	6a 00                	push   $0x0
  pushl $22
801077c6:	6a 16                	push   $0x16
  jmp alltraps
801077c8:	e9 ab f7 ff ff       	jmp    80106f78 <alltraps>

801077cd <vector23>:
.globl vector23
vector23:
  pushl $0
801077cd:	6a 00                	push   $0x0
  pushl $23
801077cf:	6a 17                	push   $0x17
  jmp alltraps
801077d1:	e9 a2 f7 ff ff       	jmp    80106f78 <alltraps>

801077d6 <vector24>:
.globl vector24
vector24:
  pushl $0
801077d6:	6a 00                	push   $0x0
  pushl $24
801077d8:	6a 18                	push   $0x18
  jmp alltraps
801077da:	e9 99 f7 ff ff       	jmp    80106f78 <alltraps>

801077df <vector25>:
.globl vector25
vector25:
  pushl $0
801077df:	6a 00                	push   $0x0
  pushl $25
801077e1:	6a 19                	push   $0x19
  jmp alltraps
801077e3:	e9 90 f7 ff ff       	jmp    80106f78 <alltraps>

801077e8 <vector26>:
.globl vector26
vector26:
  pushl $0
801077e8:	6a 00                	push   $0x0
  pushl $26
801077ea:	6a 1a                	push   $0x1a
  jmp alltraps
801077ec:	e9 87 f7 ff ff       	jmp    80106f78 <alltraps>

801077f1 <vector27>:
.globl vector27
vector27:
  pushl $0
801077f1:	6a 00                	push   $0x0
  pushl $27
801077f3:	6a 1b                	push   $0x1b
  jmp alltraps
801077f5:	e9 7e f7 ff ff       	jmp    80106f78 <alltraps>

801077fa <vector28>:
.globl vector28
vector28:
  pushl $0
801077fa:	6a 00                	push   $0x0
  pushl $28
801077fc:	6a 1c                	push   $0x1c
  jmp alltraps
801077fe:	e9 75 f7 ff ff       	jmp    80106f78 <alltraps>

80107803 <vector29>:
.globl vector29
vector29:
  pushl $0
80107803:	6a 00                	push   $0x0
  pushl $29
80107805:	6a 1d                	push   $0x1d
  jmp alltraps
80107807:	e9 6c f7 ff ff       	jmp    80106f78 <alltraps>

8010780c <vector30>:
.globl vector30
vector30:
  pushl $0
8010780c:	6a 00                	push   $0x0
  pushl $30
8010780e:	6a 1e                	push   $0x1e
  jmp alltraps
80107810:	e9 63 f7 ff ff       	jmp    80106f78 <alltraps>

80107815 <vector31>:
.globl vector31
vector31:
  pushl $0
80107815:	6a 00                	push   $0x0
  pushl $31
80107817:	6a 1f                	push   $0x1f
  jmp alltraps
80107819:	e9 5a f7 ff ff       	jmp    80106f78 <alltraps>

8010781e <vector32>:
.globl vector32
vector32:
  pushl $0
8010781e:	6a 00                	push   $0x0
  pushl $32
80107820:	6a 20                	push   $0x20
  jmp alltraps
80107822:	e9 51 f7 ff ff       	jmp    80106f78 <alltraps>

80107827 <vector33>:
.globl vector33
vector33:
  pushl $0
80107827:	6a 00                	push   $0x0
  pushl $33
80107829:	6a 21                	push   $0x21
  jmp alltraps
8010782b:	e9 48 f7 ff ff       	jmp    80106f78 <alltraps>

80107830 <vector34>:
.globl vector34
vector34:
  pushl $0
80107830:	6a 00                	push   $0x0
  pushl $34
80107832:	6a 22                	push   $0x22
  jmp alltraps
80107834:	e9 3f f7 ff ff       	jmp    80106f78 <alltraps>

80107839 <vector35>:
.globl vector35
vector35:
  pushl $0
80107839:	6a 00                	push   $0x0
  pushl $35
8010783b:	6a 23                	push   $0x23
  jmp alltraps
8010783d:	e9 36 f7 ff ff       	jmp    80106f78 <alltraps>

80107842 <vector36>:
.globl vector36
vector36:
  pushl $0
80107842:	6a 00                	push   $0x0
  pushl $36
80107844:	6a 24                	push   $0x24
  jmp alltraps
80107846:	e9 2d f7 ff ff       	jmp    80106f78 <alltraps>

8010784b <vector37>:
.globl vector37
vector37:
  pushl $0
8010784b:	6a 00                	push   $0x0
  pushl $37
8010784d:	6a 25                	push   $0x25
  jmp alltraps
8010784f:	e9 24 f7 ff ff       	jmp    80106f78 <alltraps>

80107854 <vector38>:
.globl vector38
vector38:
  pushl $0
80107854:	6a 00                	push   $0x0
  pushl $38
80107856:	6a 26                	push   $0x26
  jmp alltraps
80107858:	e9 1b f7 ff ff       	jmp    80106f78 <alltraps>

8010785d <vector39>:
.globl vector39
vector39:
  pushl $0
8010785d:	6a 00                	push   $0x0
  pushl $39
8010785f:	6a 27                	push   $0x27
  jmp alltraps
80107861:	e9 12 f7 ff ff       	jmp    80106f78 <alltraps>

80107866 <vector40>:
.globl vector40
vector40:
  pushl $0
80107866:	6a 00                	push   $0x0
  pushl $40
80107868:	6a 28                	push   $0x28
  jmp alltraps
8010786a:	e9 09 f7 ff ff       	jmp    80106f78 <alltraps>

8010786f <vector41>:
.globl vector41
vector41:
  pushl $0
8010786f:	6a 00                	push   $0x0
  pushl $41
80107871:	6a 29                	push   $0x29
  jmp alltraps
80107873:	e9 00 f7 ff ff       	jmp    80106f78 <alltraps>

80107878 <vector42>:
.globl vector42
vector42:
  pushl $0
80107878:	6a 00                	push   $0x0
  pushl $42
8010787a:	6a 2a                	push   $0x2a
  jmp alltraps
8010787c:	e9 f7 f6 ff ff       	jmp    80106f78 <alltraps>

80107881 <vector43>:
.globl vector43
vector43:
  pushl $0
80107881:	6a 00                	push   $0x0
  pushl $43
80107883:	6a 2b                	push   $0x2b
  jmp alltraps
80107885:	e9 ee f6 ff ff       	jmp    80106f78 <alltraps>

8010788a <vector44>:
.globl vector44
vector44:
  pushl $0
8010788a:	6a 00                	push   $0x0
  pushl $44
8010788c:	6a 2c                	push   $0x2c
  jmp alltraps
8010788e:	e9 e5 f6 ff ff       	jmp    80106f78 <alltraps>

80107893 <vector45>:
.globl vector45
vector45:
  pushl $0
80107893:	6a 00                	push   $0x0
  pushl $45
80107895:	6a 2d                	push   $0x2d
  jmp alltraps
80107897:	e9 dc f6 ff ff       	jmp    80106f78 <alltraps>

8010789c <vector46>:
.globl vector46
vector46:
  pushl $0
8010789c:	6a 00                	push   $0x0
  pushl $46
8010789e:	6a 2e                	push   $0x2e
  jmp alltraps
801078a0:	e9 d3 f6 ff ff       	jmp    80106f78 <alltraps>

801078a5 <vector47>:
.globl vector47
vector47:
  pushl $0
801078a5:	6a 00                	push   $0x0
  pushl $47
801078a7:	6a 2f                	push   $0x2f
  jmp alltraps
801078a9:	e9 ca f6 ff ff       	jmp    80106f78 <alltraps>

801078ae <vector48>:
.globl vector48
vector48:
  pushl $0
801078ae:	6a 00                	push   $0x0
  pushl $48
801078b0:	6a 30                	push   $0x30
  jmp alltraps
801078b2:	e9 c1 f6 ff ff       	jmp    80106f78 <alltraps>

801078b7 <vector49>:
.globl vector49
vector49:
  pushl $0
801078b7:	6a 00                	push   $0x0
  pushl $49
801078b9:	6a 31                	push   $0x31
  jmp alltraps
801078bb:	e9 b8 f6 ff ff       	jmp    80106f78 <alltraps>

801078c0 <vector50>:
.globl vector50
vector50:
  pushl $0
801078c0:	6a 00                	push   $0x0
  pushl $50
801078c2:	6a 32                	push   $0x32
  jmp alltraps
801078c4:	e9 af f6 ff ff       	jmp    80106f78 <alltraps>

801078c9 <vector51>:
.globl vector51
vector51:
  pushl $0
801078c9:	6a 00                	push   $0x0
  pushl $51
801078cb:	6a 33                	push   $0x33
  jmp alltraps
801078cd:	e9 a6 f6 ff ff       	jmp    80106f78 <alltraps>

801078d2 <vector52>:
.globl vector52
vector52:
  pushl $0
801078d2:	6a 00                	push   $0x0
  pushl $52
801078d4:	6a 34                	push   $0x34
  jmp alltraps
801078d6:	e9 9d f6 ff ff       	jmp    80106f78 <alltraps>

801078db <vector53>:
.globl vector53
vector53:
  pushl $0
801078db:	6a 00                	push   $0x0
  pushl $53
801078dd:	6a 35                	push   $0x35
  jmp alltraps
801078df:	e9 94 f6 ff ff       	jmp    80106f78 <alltraps>

801078e4 <vector54>:
.globl vector54
vector54:
  pushl $0
801078e4:	6a 00                	push   $0x0
  pushl $54
801078e6:	6a 36                	push   $0x36
  jmp alltraps
801078e8:	e9 8b f6 ff ff       	jmp    80106f78 <alltraps>

801078ed <vector55>:
.globl vector55
vector55:
  pushl $0
801078ed:	6a 00                	push   $0x0
  pushl $55
801078ef:	6a 37                	push   $0x37
  jmp alltraps
801078f1:	e9 82 f6 ff ff       	jmp    80106f78 <alltraps>

801078f6 <vector56>:
.globl vector56
vector56:
  pushl $0
801078f6:	6a 00                	push   $0x0
  pushl $56
801078f8:	6a 38                	push   $0x38
  jmp alltraps
801078fa:	e9 79 f6 ff ff       	jmp    80106f78 <alltraps>

801078ff <vector57>:
.globl vector57
vector57:
  pushl $0
801078ff:	6a 00                	push   $0x0
  pushl $57
80107901:	6a 39                	push   $0x39
  jmp alltraps
80107903:	e9 70 f6 ff ff       	jmp    80106f78 <alltraps>

80107908 <vector58>:
.globl vector58
vector58:
  pushl $0
80107908:	6a 00                	push   $0x0
  pushl $58
8010790a:	6a 3a                	push   $0x3a
  jmp alltraps
8010790c:	e9 67 f6 ff ff       	jmp    80106f78 <alltraps>

80107911 <vector59>:
.globl vector59
vector59:
  pushl $0
80107911:	6a 00                	push   $0x0
  pushl $59
80107913:	6a 3b                	push   $0x3b
  jmp alltraps
80107915:	e9 5e f6 ff ff       	jmp    80106f78 <alltraps>

8010791a <vector60>:
.globl vector60
vector60:
  pushl $0
8010791a:	6a 00                	push   $0x0
  pushl $60
8010791c:	6a 3c                	push   $0x3c
  jmp alltraps
8010791e:	e9 55 f6 ff ff       	jmp    80106f78 <alltraps>

80107923 <vector61>:
.globl vector61
vector61:
  pushl $0
80107923:	6a 00                	push   $0x0
  pushl $61
80107925:	6a 3d                	push   $0x3d
  jmp alltraps
80107927:	e9 4c f6 ff ff       	jmp    80106f78 <alltraps>

8010792c <vector62>:
.globl vector62
vector62:
  pushl $0
8010792c:	6a 00                	push   $0x0
  pushl $62
8010792e:	6a 3e                	push   $0x3e
  jmp alltraps
80107930:	e9 43 f6 ff ff       	jmp    80106f78 <alltraps>

80107935 <vector63>:
.globl vector63
vector63:
  pushl $0
80107935:	6a 00                	push   $0x0
  pushl $63
80107937:	6a 3f                	push   $0x3f
  jmp alltraps
80107939:	e9 3a f6 ff ff       	jmp    80106f78 <alltraps>

8010793e <vector64>:
.globl vector64
vector64:
  pushl $0
8010793e:	6a 00                	push   $0x0
  pushl $64
80107940:	6a 40                	push   $0x40
  jmp alltraps
80107942:	e9 31 f6 ff ff       	jmp    80106f78 <alltraps>

80107947 <vector65>:
.globl vector65
vector65:
  pushl $0
80107947:	6a 00                	push   $0x0
  pushl $65
80107949:	6a 41                	push   $0x41
  jmp alltraps
8010794b:	e9 28 f6 ff ff       	jmp    80106f78 <alltraps>

80107950 <vector66>:
.globl vector66
vector66:
  pushl $0
80107950:	6a 00                	push   $0x0
  pushl $66
80107952:	6a 42                	push   $0x42
  jmp alltraps
80107954:	e9 1f f6 ff ff       	jmp    80106f78 <alltraps>

80107959 <vector67>:
.globl vector67
vector67:
  pushl $0
80107959:	6a 00                	push   $0x0
  pushl $67
8010795b:	6a 43                	push   $0x43
  jmp alltraps
8010795d:	e9 16 f6 ff ff       	jmp    80106f78 <alltraps>

80107962 <vector68>:
.globl vector68
vector68:
  pushl $0
80107962:	6a 00                	push   $0x0
  pushl $68
80107964:	6a 44                	push   $0x44
  jmp alltraps
80107966:	e9 0d f6 ff ff       	jmp    80106f78 <alltraps>

8010796b <vector69>:
.globl vector69
vector69:
  pushl $0
8010796b:	6a 00                	push   $0x0
  pushl $69
8010796d:	6a 45                	push   $0x45
  jmp alltraps
8010796f:	e9 04 f6 ff ff       	jmp    80106f78 <alltraps>

80107974 <vector70>:
.globl vector70
vector70:
  pushl $0
80107974:	6a 00                	push   $0x0
  pushl $70
80107976:	6a 46                	push   $0x46
  jmp alltraps
80107978:	e9 fb f5 ff ff       	jmp    80106f78 <alltraps>

8010797d <vector71>:
.globl vector71
vector71:
  pushl $0
8010797d:	6a 00                	push   $0x0
  pushl $71
8010797f:	6a 47                	push   $0x47
  jmp alltraps
80107981:	e9 f2 f5 ff ff       	jmp    80106f78 <alltraps>

80107986 <vector72>:
.globl vector72
vector72:
  pushl $0
80107986:	6a 00                	push   $0x0
  pushl $72
80107988:	6a 48                	push   $0x48
  jmp alltraps
8010798a:	e9 e9 f5 ff ff       	jmp    80106f78 <alltraps>

8010798f <vector73>:
.globl vector73
vector73:
  pushl $0
8010798f:	6a 00                	push   $0x0
  pushl $73
80107991:	6a 49                	push   $0x49
  jmp alltraps
80107993:	e9 e0 f5 ff ff       	jmp    80106f78 <alltraps>

80107998 <vector74>:
.globl vector74
vector74:
  pushl $0
80107998:	6a 00                	push   $0x0
  pushl $74
8010799a:	6a 4a                	push   $0x4a
  jmp alltraps
8010799c:	e9 d7 f5 ff ff       	jmp    80106f78 <alltraps>

801079a1 <vector75>:
.globl vector75
vector75:
  pushl $0
801079a1:	6a 00                	push   $0x0
  pushl $75
801079a3:	6a 4b                	push   $0x4b
  jmp alltraps
801079a5:	e9 ce f5 ff ff       	jmp    80106f78 <alltraps>

801079aa <vector76>:
.globl vector76
vector76:
  pushl $0
801079aa:	6a 00                	push   $0x0
  pushl $76
801079ac:	6a 4c                	push   $0x4c
  jmp alltraps
801079ae:	e9 c5 f5 ff ff       	jmp    80106f78 <alltraps>

801079b3 <vector77>:
.globl vector77
vector77:
  pushl $0
801079b3:	6a 00                	push   $0x0
  pushl $77
801079b5:	6a 4d                	push   $0x4d
  jmp alltraps
801079b7:	e9 bc f5 ff ff       	jmp    80106f78 <alltraps>

801079bc <vector78>:
.globl vector78
vector78:
  pushl $0
801079bc:	6a 00                	push   $0x0
  pushl $78
801079be:	6a 4e                	push   $0x4e
  jmp alltraps
801079c0:	e9 b3 f5 ff ff       	jmp    80106f78 <alltraps>

801079c5 <vector79>:
.globl vector79
vector79:
  pushl $0
801079c5:	6a 00                	push   $0x0
  pushl $79
801079c7:	6a 4f                	push   $0x4f
  jmp alltraps
801079c9:	e9 aa f5 ff ff       	jmp    80106f78 <alltraps>

801079ce <vector80>:
.globl vector80
vector80:
  pushl $0
801079ce:	6a 00                	push   $0x0
  pushl $80
801079d0:	6a 50                	push   $0x50
  jmp alltraps
801079d2:	e9 a1 f5 ff ff       	jmp    80106f78 <alltraps>

801079d7 <vector81>:
.globl vector81
vector81:
  pushl $0
801079d7:	6a 00                	push   $0x0
  pushl $81
801079d9:	6a 51                	push   $0x51
  jmp alltraps
801079db:	e9 98 f5 ff ff       	jmp    80106f78 <alltraps>

801079e0 <vector82>:
.globl vector82
vector82:
  pushl $0
801079e0:	6a 00                	push   $0x0
  pushl $82
801079e2:	6a 52                	push   $0x52
  jmp alltraps
801079e4:	e9 8f f5 ff ff       	jmp    80106f78 <alltraps>

801079e9 <vector83>:
.globl vector83
vector83:
  pushl $0
801079e9:	6a 00                	push   $0x0
  pushl $83
801079eb:	6a 53                	push   $0x53
  jmp alltraps
801079ed:	e9 86 f5 ff ff       	jmp    80106f78 <alltraps>

801079f2 <vector84>:
.globl vector84
vector84:
  pushl $0
801079f2:	6a 00                	push   $0x0
  pushl $84
801079f4:	6a 54                	push   $0x54
  jmp alltraps
801079f6:	e9 7d f5 ff ff       	jmp    80106f78 <alltraps>

801079fb <vector85>:
.globl vector85
vector85:
  pushl $0
801079fb:	6a 00                	push   $0x0
  pushl $85
801079fd:	6a 55                	push   $0x55
  jmp alltraps
801079ff:	e9 74 f5 ff ff       	jmp    80106f78 <alltraps>

80107a04 <vector86>:
.globl vector86
vector86:
  pushl $0
80107a04:	6a 00                	push   $0x0
  pushl $86
80107a06:	6a 56                	push   $0x56
  jmp alltraps
80107a08:	e9 6b f5 ff ff       	jmp    80106f78 <alltraps>

80107a0d <vector87>:
.globl vector87
vector87:
  pushl $0
80107a0d:	6a 00                	push   $0x0
  pushl $87
80107a0f:	6a 57                	push   $0x57
  jmp alltraps
80107a11:	e9 62 f5 ff ff       	jmp    80106f78 <alltraps>

80107a16 <vector88>:
.globl vector88
vector88:
  pushl $0
80107a16:	6a 00                	push   $0x0
  pushl $88
80107a18:	6a 58                	push   $0x58
  jmp alltraps
80107a1a:	e9 59 f5 ff ff       	jmp    80106f78 <alltraps>

80107a1f <vector89>:
.globl vector89
vector89:
  pushl $0
80107a1f:	6a 00                	push   $0x0
  pushl $89
80107a21:	6a 59                	push   $0x59
  jmp alltraps
80107a23:	e9 50 f5 ff ff       	jmp    80106f78 <alltraps>

80107a28 <vector90>:
.globl vector90
vector90:
  pushl $0
80107a28:	6a 00                	push   $0x0
  pushl $90
80107a2a:	6a 5a                	push   $0x5a
  jmp alltraps
80107a2c:	e9 47 f5 ff ff       	jmp    80106f78 <alltraps>

80107a31 <vector91>:
.globl vector91
vector91:
  pushl $0
80107a31:	6a 00                	push   $0x0
  pushl $91
80107a33:	6a 5b                	push   $0x5b
  jmp alltraps
80107a35:	e9 3e f5 ff ff       	jmp    80106f78 <alltraps>

80107a3a <vector92>:
.globl vector92
vector92:
  pushl $0
80107a3a:	6a 00                	push   $0x0
  pushl $92
80107a3c:	6a 5c                	push   $0x5c
  jmp alltraps
80107a3e:	e9 35 f5 ff ff       	jmp    80106f78 <alltraps>

80107a43 <vector93>:
.globl vector93
vector93:
  pushl $0
80107a43:	6a 00                	push   $0x0
  pushl $93
80107a45:	6a 5d                	push   $0x5d
  jmp alltraps
80107a47:	e9 2c f5 ff ff       	jmp    80106f78 <alltraps>

80107a4c <vector94>:
.globl vector94
vector94:
  pushl $0
80107a4c:	6a 00                	push   $0x0
  pushl $94
80107a4e:	6a 5e                	push   $0x5e
  jmp alltraps
80107a50:	e9 23 f5 ff ff       	jmp    80106f78 <alltraps>

80107a55 <vector95>:
.globl vector95
vector95:
  pushl $0
80107a55:	6a 00                	push   $0x0
  pushl $95
80107a57:	6a 5f                	push   $0x5f
  jmp alltraps
80107a59:	e9 1a f5 ff ff       	jmp    80106f78 <alltraps>

80107a5e <vector96>:
.globl vector96
vector96:
  pushl $0
80107a5e:	6a 00                	push   $0x0
  pushl $96
80107a60:	6a 60                	push   $0x60
  jmp alltraps
80107a62:	e9 11 f5 ff ff       	jmp    80106f78 <alltraps>

80107a67 <vector97>:
.globl vector97
vector97:
  pushl $0
80107a67:	6a 00                	push   $0x0
  pushl $97
80107a69:	6a 61                	push   $0x61
  jmp alltraps
80107a6b:	e9 08 f5 ff ff       	jmp    80106f78 <alltraps>

80107a70 <vector98>:
.globl vector98
vector98:
  pushl $0
80107a70:	6a 00                	push   $0x0
  pushl $98
80107a72:	6a 62                	push   $0x62
  jmp alltraps
80107a74:	e9 ff f4 ff ff       	jmp    80106f78 <alltraps>

80107a79 <vector99>:
.globl vector99
vector99:
  pushl $0
80107a79:	6a 00                	push   $0x0
  pushl $99
80107a7b:	6a 63                	push   $0x63
  jmp alltraps
80107a7d:	e9 f6 f4 ff ff       	jmp    80106f78 <alltraps>

80107a82 <vector100>:
.globl vector100
vector100:
  pushl $0
80107a82:	6a 00                	push   $0x0
  pushl $100
80107a84:	6a 64                	push   $0x64
  jmp alltraps
80107a86:	e9 ed f4 ff ff       	jmp    80106f78 <alltraps>

80107a8b <vector101>:
.globl vector101
vector101:
  pushl $0
80107a8b:	6a 00                	push   $0x0
  pushl $101
80107a8d:	6a 65                	push   $0x65
  jmp alltraps
80107a8f:	e9 e4 f4 ff ff       	jmp    80106f78 <alltraps>

80107a94 <vector102>:
.globl vector102
vector102:
  pushl $0
80107a94:	6a 00                	push   $0x0
  pushl $102
80107a96:	6a 66                	push   $0x66
  jmp alltraps
80107a98:	e9 db f4 ff ff       	jmp    80106f78 <alltraps>

80107a9d <vector103>:
.globl vector103
vector103:
  pushl $0
80107a9d:	6a 00                	push   $0x0
  pushl $103
80107a9f:	6a 67                	push   $0x67
  jmp alltraps
80107aa1:	e9 d2 f4 ff ff       	jmp    80106f78 <alltraps>

80107aa6 <vector104>:
.globl vector104
vector104:
  pushl $0
80107aa6:	6a 00                	push   $0x0
  pushl $104
80107aa8:	6a 68                	push   $0x68
  jmp alltraps
80107aaa:	e9 c9 f4 ff ff       	jmp    80106f78 <alltraps>

80107aaf <vector105>:
.globl vector105
vector105:
  pushl $0
80107aaf:	6a 00                	push   $0x0
  pushl $105
80107ab1:	6a 69                	push   $0x69
  jmp alltraps
80107ab3:	e9 c0 f4 ff ff       	jmp    80106f78 <alltraps>

80107ab8 <vector106>:
.globl vector106
vector106:
  pushl $0
80107ab8:	6a 00                	push   $0x0
  pushl $106
80107aba:	6a 6a                	push   $0x6a
  jmp alltraps
80107abc:	e9 b7 f4 ff ff       	jmp    80106f78 <alltraps>

80107ac1 <vector107>:
.globl vector107
vector107:
  pushl $0
80107ac1:	6a 00                	push   $0x0
  pushl $107
80107ac3:	6a 6b                	push   $0x6b
  jmp alltraps
80107ac5:	e9 ae f4 ff ff       	jmp    80106f78 <alltraps>

80107aca <vector108>:
.globl vector108
vector108:
  pushl $0
80107aca:	6a 00                	push   $0x0
  pushl $108
80107acc:	6a 6c                	push   $0x6c
  jmp alltraps
80107ace:	e9 a5 f4 ff ff       	jmp    80106f78 <alltraps>

80107ad3 <vector109>:
.globl vector109
vector109:
  pushl $0
80107ad3:	6a 00                	push   $0x0
  pushl $109
80107ad5:	6a 6d                	push   $0x6d
  jmp alltraps
80107ad7:	e9 9c f4 ff ff       	jmp    80106f78 <alltraps>

80107adc <vector110>:
.globl vector110
vector110:
  pushl $0
80107adc:	6a 00                	push   $0x0
  pushl $110
80107ade:	6a 6e                	push   $0x6e
  jmp alltraps
80107ae0:	e9 93 f4 ff ff       	jmp    80106f78 <alltraps>

80107ae5 <vector111>:
.globl vector111
vector111:
  pushl $0
80107ae5:	6a 00                	push   $0x0
  pushl $111
80107ae7:	6a 6f                	push   $0x6f
  jmp alltraps
80107ae9:	e9 8a f4 ff ff       	jmp    80106f78 <alltraps>

80107aee <vector112>:
.globl vector112
vector112:
  pushl $0
80107aee:	6a 00                	push   $0x0
  pushl $112
80107af0:	6a 70                	push   $0x70
  jmp alltraps
80107af2:	e9 81 f4 ff ff       	jmp    80106f78 <alltraps>

80107af7 <vector113>:
.globl vector113
vector113:
  pushl $0
80107af7:	6a 00                	push   $0x0
  pushl $113
80107af9:	6a 71                	push   $0x71
  jmp alltraps
80107afb:	e9 78 f4 ff ff       	jmp    80106f78 <alltraps>

80107b00 <vector114>:
.globl vector114
vector114:
  pushl $0
80107b00:	6a 00                	push   $0x0
  pushl $114
80107b02:	6a 72                	push   $0x72
  jmp alltraps
80107b04:	e9 6f f4 ff ff       	jmp    80106f78 <alltraps>

80107b09 <vector115>:
.globl vector115
vector115:
  pushl $0
80107b09:	6a 00                	push   $0x0
  pushl $115
80107b0b:	6a 73                	push   $0x73
  jmp alltraps
80107b0d:	e9 66 f4 ff ff       	jmp    80106f78 <alltraps>

80107b12 <vector116>:
.globl vector116
vector116:
  pushl $0
80107b12:	6a 00                	push   $0x0
  pushl $116
80107b14:	6a 74                	push   $0x74
  jmp alltraps
80107b16:	e9 5d f4 ff ff       	jmp    80106f78 <alltraps>

80107b1b <vector117>:
.globl vector117
vector117:
  pushl $0
80107b1b:	6a 00                	push   $0x0
  pushl $117
80107b1d:	6a 75                	push   $0x75
  jmp alltraps
80107b1f:	e9 54 f4 ff ff       	jmp    80106f78 <alltraps>

80107b24 <vector118>:
.globl vector118
vector118:
  pushl $0
80107b24:	6a 00                	push   $0x0
  pushl $118
80107b26:	6a 76                	push   $0x76
  jmp alltraps
80107b28:	e9 4b f4 ff ff       	jmp    80106f78 <alltraps>

80107b2d <vector119>:
.globl vector119
vector119:
  pushl $0
80107b2d:	6a 00                	push   $0x0
  pushl $119
80107b2f:	6a 77                	push   $0x77
  jmp alltraps
80107b31:	e9 42 f4 ff ff       	jmp    80106f78 <alltraps>

80107b36 <vector120>:
.globl vector120
vector120:
  pushl $0
80107b36:	6a 00                	push   $0x0
  pushl $120
80107b38:	6a 78                	push   $0x78
  jmp alltraps
80107b3a:	e9 39 f4 ff ff       	jmp    80106f78 <alltraps>

80107b3f <vector121>:
.globl vector121
vector121:
  pushl $0
80107b3f:	6a 00                	push   $0x0
  pushl $121
80107b41:	6a 79                	push   $0x79
  jmp alltraps
80107b43:	e9 30 f4 ff ff       	jmp    80106f78 <alltraps>

80107b48 <vector122>:
.globl vector122
vector122:
  pushl $0
80107b48:	6a 00                	push   $0x0
  pushl $122
80107b4a:	6a 7a                	push   $0x7a
  jmp alltraps
80107b4c:	e9 27 f4 ff ff       	jmp    80106f78 <alltraps>

80107b51 <vector123>:
.globl vector123
vector123:
  pushl $0
80107b51:	6a 00                	push   $0x0
  pushl $123
80107b53:	6a 7b                	push   $0x7b
  jmp alltraps
80107b55:	e9 1e f4 ff ff       	jmp    80106f78 <alltraps>

80107b5a <vector124>:
.globl vector124
vector124:
  pushl $0
80107b5a:	6a 00                	push   $0x0
  pushl $124
80107b5c:	6a 7c                	push   $0x7c
  jmp alltraps
80107b5e:	e9 15 f4 ff ff       	jmp    80106f78 <alltraps>

80107b63 <vector125>:
.globl vector125
vector125:
  pushl $0
80107b63:	6a 00                	push   $0x0
  pushl $125
80107b65:	6a 7d                	push   $0x7d
  jmp alltraps
80107b67:	e9 0c f4 ff ff       	jmp    80106f78 <alltraps>

80107b6c <vector126>:
.globl vector126
vector126:
  pushl $0
80107b6c:	6a 00                	push   $0x0
  pushl $126
80107b6e:	6a 7e                	push   $0x7e
  jmp alltraps
80107b70:	e9 03 f4 ff ff       	jmp    80106f78 <alltraps>

80107b75 <vector127>:
.globl vector127
vector127:
  pushl $0
80107b75:	6a 00                	push   $0x0
  pushl $127
80107b77:	6a 7f                	push   $0x7f
  jmp alltraps
80107b79:	e9 fa f3 ff ff       	jmp    80106f78 <alltraps>

80107b7e <vector128>:
.globl vector128
vector128:
  pushl $0
80107b7e:	6a 00                	push   $0x0
  pushl $128
80107b80:	68 80 00 00 00       	push   $0x80
  jmp alltraps
80107b85:	e9 ee f3 ff ff       	jmp    80106f78 <alltraps>

80107b8a <vector129>:
.globl vector129
vector129:
  pushl $0
80107b8a:	6a 00                	push   $0x0
  pushl $129
80107b8c:	68 81 00 00 00       	push   $0x81
  jmp alltraps
80107b91:	e9 e2 f3 ff ff       	jmp    80106f78 <alltraps>

80107b96 <vector130>:
.globl vector130
vector130:
  pushl $0
80107b96:	6a 00                	push   $0x0
  pushl $130
80107b98:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80107b9d:	e9 d6 f3 ff ff       	jmp    80106f78 <alltraps>

80107ba2 <vector131>:
.globl vector131
vector131:
  pushl $0
80107ba2:	6a 00                	push   $0x0
  pushl $131
80107ba4:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80107ba9:	e9 ca f3 ff ff       	jmp    80106f78 <alltraps>

80107bae <vector132>:
.globl vector132
vector132:
  pushl $0
80107bae:	6a 00                	push   $0x0
  pushl $132
80107bb0:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80107bb5:	e9 be f3 ff ff       	jmp    80106f78 <alltraps>

80107bba <vector133>:
.globl vector133
vector133:
  pushl $0
80107bba:	6a 00                	push   $0x0
  pushl $133
80107bbc:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80107bc1:	e9 b2 f3 ff ff       	jmp    80106f78 <alltraps>

80107bc6 <vector134>:
.globl vector134
vector134:
  pushl $0
80107bc6:	6a 00                	push   $0x0
  pushl $134
80107bc8:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80107bcd:	e9 a6 f3 ff ff       	jmp    80106f78 <alltraps>

80107bd2 <vector135>:
.globl vector135
vector135:
  pushl $0
80107bd2:	6a 00                	push   $0x0
  pushl $135
80107bd4:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80107bd9:	e9 9a f3 ff ff       	jmp    80106f78 <alltraps>

80107bde <vector136>:
.globl vector136
vector136:
  pushl $0
80107bde:	6a 00                	push   $0x0
  pushl $136
80107be0:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80107be5:	e9 8e f3 ff ff       	jmp    80106f78 <alltraps>

80107bea <vector137>:
.globl vector137
vector137:
  pushl $0
80107bea:	6a 00                	push   $0x0
  pushl $137
80107bec:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80107bf1:	e9 82 f3 ff ff       	jmp    80106f78 <alltraps>

80107bf6 <vector138>:
.globl vector138
vector138:
  pushl $0
80107bf6:	6a 00                	push   $0x0
  pushl $138
80107bf8:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80107bfd:	e9 76 f3 ff ff       	jmp    80106f78 <alltraps>

80107c02 <vector139>:
.globl vector139
vector139:
  pushl $0
80107c02:	6a 00                	push   $0x0
  pushl $139
80107c04:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80107c09:	e9 6a f3 ff ff       	jmp    80106f78 <alltraps>

80107c0e <vector140>:
.globl vector140
vector140:
  pushl $0
80107c0e:	6a 00                	push   $0x0
  pushl $140
80107c10:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80107c15:	e9 5e f3 ff ff       	jmp    80106f78 <alltraps>

80107c1a <vector141>:
.globl vector141
vector141:
  pushl $0
80107c1a:	6a 00                	push   $0x0
  pushl $141
80107c1c:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80107c21:	e9 52 f3 ff ff       	jmp    80106f78 <alltraps>

80107c26 <vector142>:
.globl vector142
vector142:
  pushl $0
80107c26:	6a 00                	push   $0x0
  pushl $142
80107c28:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80107c2d:	e9 46 f3 ff ff       	jmp    80106f78 <alltraps>

80107c32 <vector143>:
.globl vector143
vector143:
  pushl $0
80107c32:	6a 00                	push   $0x0
  pushl $143
80107c34:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80107c39:	e9 3a f3 ff ff       	jmp    80106f78 <alltraps>

80107c3e <vector144>:
.globl vector144
vector144:
  pushl $0
80107c3e:	6a 00                	push   $0x0
  pushl $144
80107c40:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80107c45:	e9 2e f3 ff ff       	jmp    80106f78 <alltraps>

80107c4a <vector145>:
.globl vector145
vector145:
  pushl $0
80107c4a:	6a 00                	push   $0x0
  pushl $145
80107c4c:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80107c51:	e9 22 f3 ff ff       	jmp    80106f78 <alltraps>

80107c56 <vector146>:
.globl vector146
vector146:
  pushl $0
80107c56:	6a 00                	push   $0x0
  pushl $146
80107c58:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80107c5d:	e9 16 f3 ff ff       	jmp    80106f78 <alltraps>

80107c62 <vector147>:
.globl vector147
vector147:
  pushl $0
80107c62:	6a 00                	push   $0x0
  pushl $147
80107c64:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80107c69:	e9 0a f3 ff ff       	jmp    80106f78 <alltraps>

80107c6e <vector148>:
.globl vector148
vector148:
  pushl $0
80107c6e:	6a 00                	push   $0x0
  pushl $148
80107c70:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80107c75:	e9 fe f2 ff ff       	jmp    80106f78 <alltraps>

80107c7a <vector149>:
.globl vector149
vector149:
  pushl $0
80107c7a:	6a 00                	push   $0x0
  pushl $149
80107c7c:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80107c81:	e9 f2 f2 ff ff       	jmp    80106f78 <alltraps>

80107c86 <vector150>:
.globl vector150
vector150:
  pushl $0
80107c86:	6a 00                	push   $0x0
  pushl $150
80107c88:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80107c8d:	e9 e6 f2 ff ff       	jmp    80106f78 <alltraps>

80107c92 <vector151>:
.globl vector151
vector151:
  pushl $0
80107c92:	6a 00                	push   $0x0
  pushl $151
80107c94:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80107c99:	e9 da f2 ff ff       	jmp    80106f78 <alltraps>

80107c9e <vector152>:
.globl vector152
vector152:
  pushl $0
80107c9e:	6a 00                	push   $0x0
  pushl $152
80107ca0:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80107ca5:	e9 ce f2 ff ff       	jmp    80106f78 <alltraps>

80107caa <vector153>:
.globl vector153
vector153:
  pushl $0
80107caa:	6a 00                	push   $0x0
  pushl $153
80107cac:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80107cb1:	e9 c2 f2 ff ff       	jmp    80106f78 <alltraps>

80107cb6 <vector154>:
.globl vector154
vector154:
  pushl $0
80107cb6:	6a 00                	push   $0x0
  pushl $154
80107cb8:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80107cbd:	e9 b6 f2 ff ff       	jmp    80106f78 <alltraps>

80107cc2 <vector155>:
.globl vector155
vector155:
  pushl $0
80107cc2:	6a 00                	push   $0x0
  pushl $155
80107cc4:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80107cc9:	e9 aa f2 ff ff       	jmp    80106f78 <alltraps>

80107cce <vector156>:
.globl vector156
vector156:
  pushl $0
80107cce:	6a 00                	push   $0x0
  pushl $156
80107cd0:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80107cd5:	e9 9e f2 ff ff       	jmp    80106f78 <alltraps>

80107cda <vector157>:
.globl vector157
vector157:
  pushl $0
80107cda:	6a 00                	push   $0x0
  pushl $157
80107cdc:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80107ce1:	e9 92 f2 ff ff       	jmp    80106f78 <alltraps>

80107ce6 <vector158>:
.globl vector158
vector158:
  pushl $0
80107ce6:	6a 00                	push   $0x0
  pushl $158
80107ce8:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80107ced:	e9 86 f2 ff ff       	jmp    80106f78 <alltraps>

80107cf2 <vector159>:
.globl vector159
vector159:
  pushl $0
80107cf2:	6a 00                	push   $0x0
  pushl $159
80107cf4:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80107cf9:	e9 7a f2 ff ff       	jmp    80106f78 <alltraps>

80107cfe <vector160>:
.globl vector160
vector160:
  pushl $0
80107cfe:	6a 00                	push   $0x0
  pushl $160
80107d00:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80107d05:	e9 6e f2 ff ff       	jmp    80106f78 <alltraps>

80107d0a <vector161>:
.globl vector161
vector161:
  pushl $0
80107d0a:	6a 00                	push   $0x0
  pushl $161
80107d0c:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80107d11:	e9 62 f2 ff ff       	jmp    80106f78 <alltraps>

80107d16 <vector162>:
.globl vector162
vector162:
  pushl $0
80107d16:	6a 00                	push   $0x0
  pushl $162
80107d18:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80107d1d:	e9 56 f2 ff ff       	jmp    80106f78 <alltraps>

80107d22 <vector163>:
.globl vector163
vector163:
  pushl $0
80107d22:	6a 00                	push   $0x0
  pushl $163
80107d24:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80107d29:	e9 4a f2 ff ff       	jmp    80106f78 <alltraps>

80107d2e <vector164>:
.globl vector164
vector164:
  pushl $0
80107d2e:	6a 00                	push   $0x0
  pushl $164
80107d30:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80107d35:	e9 3e f2 ff ff       	jmp    80106f78 <alltraps>

80107d3a <vector165>:
.globl vector165
vector165:
  pushl $0
80107d3a:	6a 00                	push   $0x0
  pushl $165
80107d3c:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80107d41:	e9 32 f2 ff ff       	jmp    80106f78 <alltraps>

80107d46 <vector166>:
.globl vector166
vector166:
  pushl $0
80107d46:	6a 00                	push   $0x0
  pushl $166
80107d48:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80107d4d:	e9 26 f2 ff ff       	jmp    80106f78 <alltraps>

80107d52 <vector167>:
.globl vector167
vector167:
  pushl $0
80107d52:	6a 00                	push   $0x0
  pushl $167
80107d54:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80107d59:	e9 1a f2 ff ff       	jmp    80106f78 <alltraps>

80107d5e <vector168>:
.globl vector168
vector168:
  pushl $0
80107d5e:	6a 00                	push   $0x0
  pushl $168
80107d60:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80107d65:	e9 0e f2 ff ff       	jmp    80106f78 <alltraps>

80107d6a <vector169>:
.globl vector169
vector169:
  pushl $0
80107d6a:	6a 00                	push   $0x0
  pushl $169
80107d6c:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80107d71:	e9 02 f2 ff ff       	jmp    80106f78 <alltraps>

80107d76 <vector170>:
.globl vector170
vector170:
  pushl $0
80107d76:	6a 00                	push   $0x0
  pushl $170
80107d78:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80107d7d:	e9 f6 f1 ff ff       	jmp    80106f78 <alltraps>

80107d82 <vector171>:
.globl vector171
vector171:
  pushl $0
80107d82:	6a 00                	push   $0x0
  pushl $171
80107d84:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80107d89:	e9 ea f1 ff ff       	jmp    80106f78 <alltraps>

80107d8e <vector172>:
.globl vector172
vector172:
  pushl $0
80107d8e:	6a 00                	push   $0x0
  pushl $172
80107d90:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80107d95:	e9 de f1 ff ff       	jmp    80106f78 <alltraps>

80107d9a <vector173>:
.globl vector173
vector173:
  pushl $0
80107d9a:	6a 00                	push   $0x0
  pushl $173
80107d9c:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80107da1:	e9 d2 f1 ff ff       	jmp    80106f78 <alltraps>

80107da6 <vector174>:
.globl vector174
vector174:
  pushl $0
80107da6:	6a 00                	push   $0x0
  pushl $174
80107da8:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80107dad:	e9 c6 f1 ff ff       	jmp    80106f78 <alltraps>

80107db2 <vector175>:
.globl vector175
vector175:
  pushl $0
80107db2:	6a 00                	push   $0x0
  pushl $175
80107db4:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80107db9:	e9 ba f1 ff ff       	jmp    80106f78 <alltraps>

80107dbe <vector176>:
.globl vector176
vector176:
  pushl $0
80107dbe:	6a 00                	push   $0x0
  pushl $176
80107dc0:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80107dc5:	e9 ae f1 ff ff       	jmp    80106f78 <alltraps>

80107dca <vector177>:
.globl vector177
vector177:
  pushl $0
80107dca:	6a 00                	push   $0x0
  pushl $177
80107dcc:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80107dd1:	e9 a2 f1 ff ff       	jmp    80106f78 <alltraps>

80107dd6 <vector178>:
.globl vector178
vector178:
  pushl $0
80107dd6:	6a 00                	push   $0x0
  pushl $178
80107dd8:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80107ddd:	e9 96 f1 ff ff       	jmp    80106f78 <alltraps>

80107de2 <vector179>:
.globl vector179
vector179:
  pushl $0
80107de2:	6a 00                	push   $0x0
  pushl $179
80107de4:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80107de9:	e9 8a f1 ff ff       	jmp    80106f78 <alltraps>

80107dee <vector180>:
.globl vector180
vector180:
  pushl $0
80107dee:	6a 00                	push   $0x0
  pushl $180
80107df0:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80107df5:	e9 7e f1 ff ff       	jmp    80106f78 <alltraps>

80107dfa <vector181>:
.globl vector181
vector181:
  pushl $0
80107dfa:	6a 00                	push   $0x0
  pushl $181
80107dfc:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80107e01:	e9 72 f1 ff ff       	jmp    80106f78 <alltraps>

80107e06 <vector182>:
.globl vector182
vector182:
  pushl $0
80107e06:	6a 00                	push   $0x0
  pushl $182
80107e08:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80107e0d:	e9 66 f1 ff ff       	jmp    80106f78 <alltraps>

80107e12 <vector183>:
.globl vector183
vector183:
  pushl $0
80107e12:	6a 00                	push   $0x0
  pushl $183
80107e14:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80107e19:	e9 5a f1 ff ff       	jmp    80106f78 <alltraps>

80107e1e <vector184>:
.globl vector184
vector184:
  pushl $0
80107e1e:	6a 00                	push   $0x0
  pushl $184
80107e20:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80107e25:	e9 4e f1 ff ff       	jmp    80106f78 <alltraps>

80107e2a <vector185>:
.globl vector185
vector185:
  pushl $0
80107e2a:	6a 00                	push   $0x0
  pushl $185
80107e2c:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80107e31:	e9 42 f1 ff ff       	jmp    80106f78 <alltraps>

80107e36 <vector186>:
.globl vector186
vector186:
  pushl $0
80107e36:	6a 00                	push   $0x0
  pushl $186
80107e38:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80107e3d:	e9 36 f1 ff ff       	jmp    80106f78 <alltraps>

80107e42 <vector187>:
.globl vector187
vector187:
  pushl $0
80107e42:	6a 00                	push   $0x0
  pushl $187
80107e44:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80107e49:	e9 2a f1 ff ff       	jmp    80106f78 <alltraps>

80107e4e <vector188>:
.globl vector188
vector188:
  pushl $0
80107e4e:	6a 00                	push   $0x0
  pushl $188
80107e50:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80107e55:	e9 1e f1 ff ff       	jmp    80106f78 <alltraps>

80107e5a <vector189>:
.globl vector189
vector189:
  pushl $0
80107e5a:	6a 00                	push   $0x0
  pushl $189
80107e5c:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
80107e61:	e9 12 f1 ff ff       	jmp    80106f78 <alltraps>

80107e66 <vector190>:
.globl vector190
vector190:
  pushl $0
80107e66:	6a 00                	push   $0x0
  pushl $190
80107e68:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80107e6d:	e9 06 f1 ff ff       	jmp    80106f78 <alltraps>

80107e72 <vector191>:
.globl vector191
vector191:
  pushl $0
80107e72:	6a 00                	push   $0x0
  pushl $191
80107e74:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80107e79:	e9 fa f0 ff ff       	jmp    80106f78 <alltraps>

80107e7e <vector192>:
.globl vector192
vector192:
  pushl $0
80107e7e:	6a 00                	push   $0x0
  pushl $192
80107e80:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
80107e85:	e9 ee f0 ff ff       	jmp    80106f78 <alltraps>

80107e8a <vector193>:
.globl vector193
vector193:
  pushl $0
80107e8a:	6a 00                	push   $0x0
  pushl $193
80107e8c:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
80107e91:	e9 e2 f0 ff ff       	jmp    80106f78 <alltraps>

80107e96 <vector194>:
.globl vector194
vector194:
  pushl $0
80107e96:	6a 00                	push   $0x0
  pushl $194
80107e98:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80107e9d:	e9 d6 f0 ff ff       	jmp    80106f78 <alltraps>

80107ea2 <vector195>:
.globl vector195
vector195:
  pushl $0
80107ea2:	6a 00                	push   $0x0
  pushl $195
80107ea4:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80107ea9:	e9 ca f0 ff ff       	jmp    80106f78 <alltraps>

80107eae <vector196>:
.globl vector196
vector196:
  pushl $0
80107eae:	6a 00                	push   $0x0
  pushl $196
80107eb0:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
80107eb5:	e9 be f0 ff ff       	jmp    80106f78 <alltraps>

80107eba <vector197>:
.globl vector197
vector197:
  pushl $0
80107eba:	6a 00                	push   $0x0
  pushl $197
80107ebc:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80107ec1:	e9 b2 f0 ff ff       	jmp    80106f78 <alltraps>

80107ec6 <vector198>:
.globl vector198
vector198:
  pushl $0
80107ec6:	6a 00                	push   $0x0
  pushl $198
80107ec8:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
80107ecd:	e9 a6 f0 ff ff       	jmp    80106f78 <alltraps>

80107ed2 <vector199>:
.globl vector199
vector199:
  pushl $0
80107ed2:	6a 00                	push   $0x0
  pushl $199
80107ed4:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80107ed9:	e9 9a f0 ff ff       	jmp    80106f78 <alltraps>

80107ede <vector200>:
.globl vector200
vector200:
  pushl $0
80107ede:	6a 00                	push   $0x0
  pushl $200
80107ee0:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80107ee5:	e9 8e f0 ff ff       	jmp    80106f78 <alltraps>

80107eea <vector201>:
.globl vector201
vector201:
  pushl $0
80107eea:	6a 00                	push   $0x0
  pushl $201
80107eec:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80107ef1:	e9 82 f0 ff ff       	jmp    80106f78 <alltraps>

80107ef6 <vector202>:
.globl vector202
vector202:
  pushl $0
80107ef6:	6a 00                	push   $0x0
  pushl $202
80107ef8:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
80107efd:	e9 76 f0 ff ff       	jmp    80106f78 <alltraps>

80107f02 <vector203>:
.globl vector203
vector203:
  pushl $0
80107f02:	6a 00                	push   $0x0
  pushl $203
80107f04:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80107f09:	e9 6a f0 ff ff       	jmp    80106f78 <alltraps>

80107f0e <vector204>:
.globl vector204
vector204:
  pushl $0
80107f0e:	6a 00                	push   $0x0
  pushl $204
80107f10:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80107f15:	e9 5e f0 ff ff       	jmp    80106f78 <alltraps>

80107f1a <vector205>:
.globl vector205
vector205:
  pushl $0
80107f1a:	6a 00                	push   $0x0
  pushl $205
80107f1c:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80107f21:	e9 52 f0 ff ff       	jmp    80106f78 <alltraps>

80107f26 <vector206>:
.globl vector206
vector206:
  pushl $0
80107f26:	6a 00                	push   $0x0
  pushl $206
80107f28:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80107f2d:	e9 46 f0 ff ff       	jmp    80106f78 <alltraps>

80107f32 <vector207>:
.globl vector207
vector207:
  pushl $0
80107f32:	6a 00                	push   $0x0
  pushl $207
80107f34:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80107f39:	e9 3a f0 ff ff       	jmp    80106f78 <alltraps>

80107f3e <vector208>:
.globl vector208
vector208:
  pushl $0
80107f3e:	6a 00                	push   $0x0
  pushl $208
80107f40:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80107f45:	e9 2e f0 ff ff       	jmp    80106f78 <alltraps>

80107f4a <vector209>:
.globl vector209
vector209:
  pushl $0
80107f4a:	6a 00                	push   $0x0
  pushl $209
80107f4c:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80107f51:	e9 22 f0 ff ff       	jmp    80106f78 <alltraps>

80107f56 <vector210>:
.globl vector210
vector210:
  pushl $0
80107f56:	6a 00                	push   $0x0
  pushl $210
80107f58:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80107f5d:	e9 16 f0 ff ff       	jmp    80106f78 <alltraps>

80107f62 <vector211>:
.globl vector211
vector211:
  pushl $0
80107f62:	6a 00                	push   $0x0
  pushl $211
80107f64:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80107f69:	e9 0a f0 ff ff       	jmp    80106f78 <alltraps>

80107f6e <vector212>:
.globl vector212
vector212:
  pushl $0
80107f6e:	6a 00                	push   $0x0
  pushl $212
80107f70:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
80107f75:	e9 fe ef ff ff       	jmp    80106f78 <alltraps>

80107f7a <vector213>:
.globl vector213
vector213:
  pushl $0
80107f7a:	6a 00                	push   $0x0
  pushl $213
80107f7c:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
80107f81:	e9 f2 ef ff ff       	jmp    80106f78 <alltraps>

80107f86 <vector214>:
.globl vector214
vector214:
  pushl $0
80107f86:	6a 00                	push   $0x0
  pushl $214
80107f88:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80107f8d:	e9 e6 ef ff ff       	jmp    80106f78 <alltraps>

80107f92 <vector215>:
.globl vector215
vector215:
  pushl $0
80107f92:	6a 00                	push   $0x0
  pushl $215
80107f94:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80107f99:	e9 da ef ff ff       	jmp    80106f78 <alltraps>

80107f9e <vector216>:
.globl vector216
vector216:
  pushl $0
80107f9e:	6a 00                	push   $0x0
  pushl $216
80107fa0:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
80107fa5:	e9 ce ef ff ff       	jmp    80106f78 <alltraps>

80107faa <vector217>:
.globl vector217
vector217:
  pushl $0
80107faa:	6a 00                	push   $0x0
  pushl $217
80107fac:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
80107fb1:	e9 c2 ef ff ff       	jmp    80106f78 <alltraps>

80107fb6 <vector218>:
.globl vector218
vector218:
  pushl $0
80107fb6:	6a 00                	push   $0x0
  pushl $218
80107fb8:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80107fbd:	e9 b6 ef ff ff       	jmp    80106f78 <alltraps>

80107fc2 <vector219>:
.globl vector219
vector219:
  pushl $0
80107fc2:	6a 00                	push   $0x0
  pushl $219
80107fc4:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80107fc9:	e9 aa ef ff ff       	jmp    80106f78 <alltraps>

80107fce <vector220>:
.globl vector220
vector220:
  pushl $0
80107fce:	6a 00                	push   $0x0
  pushl $220
80107fd0:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80107fd5:	e9 9e ef ff ff       	jmp    80106f78 <alltraps>

80107fda <vector221>:
.globl vector221
vector221:
  pushl $0
80107fda:	6a 00                	push   $0x0
  pushl $221
80107fdc:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80107fe1:	e9 92 ef ff ff       	jmp    80106f78 <alltraps>

80107fe6 <vector222>:
.globl vector222
vector222:
  pushl $0
80107fe6:	6a 00                	push   $0x0
  pushl $222
80107fe8:	68 de 00 00 00       	push   $0xde
  jmp alltraps
80107fed:	e9 86 ef ff ff       	jmp    80106f78 <alltraps>

80107ff2 <vector223>:
.globl vector223
vector223:
  pushl $0
80107ff2:	6a 00                	push   $0x0
  pushl $223
80107ff4:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80107ff9:	e9 7a ef ff ff       	jmp    80106f78 <alltraps>

80107ffe <vector224>:
.globl vector224
vector224:
  pushl $0
80107ffe:	6a 00                	push   $0x0
  pushl $224
80108000:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80108005:	e9 6e ef ff ff       	jmp    80106f78 <alltraps>

8010800a <vector225>:
.globl vector225
vector225:
  pushl $0
8010800a:	6a 00                	push   $0x0
  pushl $225
8010800c:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80108011:	e9 62 ef ff ff       	jmp    80106f78 <alltraps>

80108016 <vector226>:
.globl vector226
vector226:
  pushl $0
80108016:	6a 00                	push   $0x0
  pushl $226
80108018:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
8010801d:	e9 56 ef ff ff       	jmp    80106f78 <alltraps>

80108022 <vector227>:
.globl vector227
vector227:
  pushl $0
80108022:	6a 00                	push   $0x0
  pushl $227
80108024:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80108029:	e9 4a ef ff ff       	jmp    80106f78 <alltraps>

8010802e <vector228>:
.globl vector228
vector228:
  pushl $0
8010802e:	6a 00                	push   $0x0
  pushl $228
80108030:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80108035:	e9 3e ef ff ff       	jmp    80106f78 <alltraps>

8010803a <vector229>:
.globl vector229
vector229:
  pushl $0
8010803a:	6a 00                	push   $0x0
  pushl $229
8010803c:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80108041:	e9 32 ef ff ff       	jmp    80106f78 <alltraps>

80108046 <vector230>:
.globl vector230
vector230:
  pushl $0
80108046:	6a 00                	push   $0x0
  pushl $230
80108048:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
8010804d:	e9 26 ef ff ff       	jmp    80106f78 <alltraps>

80108052 <vector231>:
.globl vector231
vector231:
  pushl $0
80108052:	6a 00                	push   $0x0
  pushl $231
80108054:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80108059:	e9 1a ef ff ff       	jmp    80106f78 <alltraps>

8010805e <vector232>:
.globl vector232
vector232:
  pushl $0
8010805e:	6a 00                	push   $0x0
  pushl $232
80108060:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80108065:	e9 0e ef ff ff       	jmp    80106f78 <alltraps>

8010806a <vector233>:
.globl vector233
vector233:
  pushl $0
8010806a:	6a 00                	push   $0x0
  pushl $233
8010806c:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
80108071:	e9 02 ef ff ff       	jmp    80106f78 <alltraps>

80108076 <vector234>:
.globl vector234
vector234:
  pushl $0
80108076:	6a 00                	push   $0x0
  pushl $234
80108078:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
8010807d:	e9 f6 ee ff ff       	jmp    80106f78 <alltraps>

80108082 <vector235>:
.globl vector235
vector235:
  pushl $0
80108082:	6a 00                	push   $0x0
  pushl $235
80108084:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80108089:	e9 ea ee ff ff       	jmp    80106f78 <alltraps>

8010808e <vector236>:
.globl vector236
vector236:
  pushl $0
8010808e:	6a 00                	push   $0x0
  pushl $236
80108090:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
80108095:	e9 de ee ff ff       	jmp    80106f78 <alltraps>

8010809a <vector237>:
.globl vector237
vector237:
  pushl $0
8010809a:	6a 00                	push   $0x0
  pushl $237
8010809c:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
801080a1:	e9 d2 ee ff ff       	jmp    80106f78 <alltraps>

801080a6 <vector238>:
.globl vector238
vector238:
  pushl $0
801080a6:	6a 00                	push   $0x0
  pushl $238
801080a8:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
801080ad:	e9 c6 ee ff ff       	jmp    80106f78 <alltraps>

801080b2 <vector239>:
.globl vector239
vector239:
  pushl $0
801080b2:	6a 00                	push   $0x0
  pushl $239
801080b4:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801080b9:	e9 ba ee ff ff       	jmp    80106f78 <alltraps>

801080be <vector240>:
.globl vector240
vector240:
  pushl $0
801080be:	6a 00                	push   $0x0
  pushl $240
801080c0:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
801080c5:	e9 ae ee ff ff       	jmp    80106f78 <alltraps>

801080ca <vector241>:
.globl vector241
vector241:
  pushl $0
801080ca:	6a 00                	push   $0x0
  pushl $241
801080cc:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
801080d1:	e9 a2 ee ff ff       	jmp    80106f78 <alltraps>

801080d6 <vector242>:
.globl vector242
vector242:
  pushl $0
801080d6:	6a 00                	push   $0x0
  pushl $242
801080d8:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
801080dd:	e9 96 ee ff ff       	jmp    80106f78 <alltraps>

801080e2 <vector243>:
.globl vector243
vector243:
  pushl $0
801080e2:	6a 00                	push   $0x0
  pushl $243
801080e4:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
801080e9:	e9 8a ee ff ff       	jmp    80106f78 <alltraps>

801080ee <vector244>:
.globl vector244
vector244:
  pushl $0
801080ee:	6a 00                	push   $0x0
  pushl $244
801080f0:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
801080f5:	e9 7e ee ff ff       	jmp    80106f78 <alltraps>

801080fa <vector245>:
.globl vector245
vector245:
  pushl $0
801080fa:	6a 00                	push   $0x0
  pushl $245
801080fc:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80108101:	e9 72 ee ff ff       	jmp    80106f78 <alltraps>

80108106 <vector246>:
.globl vector246
vector246:
  pushl $0
80108106:	6a 00                	push   $0x0
  pushl $246
80108108:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010810d:	e9 66 ee ff ff       	jmp    80106f78 <alltraps>

80108112 <vector247>:
.globl vector247
vector247:
  pushl $0
80108112:	6a 00                	push   $0x0
  pushl $247
80108114:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80108119:	e9 5a ee ff ff       	jmp    80106f78 <alltraps>

8010811e <vector248>:
.globl vector248
vector248:
  pushl $0
8010811e:	6a 00                	push   $0x0
  pushl $248
80108120:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80108125:	e9 4e ee ff ff       	jmp    80106f78 <alltraps>

8010812a <vector249>:
.globl vector249
vector249:
  pushl $0
8010812a:	6a 00                	push   $0x0
  pushl $249
8010812c:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80108131:	e9 42 ee ff ff       	jmp    80106f78 <alltraps>

80108136 <vector250>:
.globl vector250
vector250:
  pushl $0
80108136:	6a 00                	push   $0x0
  pushl $250
80108138:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010813d:	e9 36 ee ff ff       	jmp    80106f78 <alltraps>

80108142 <vector251>:
.globl vector251
vector251:
  pushl $0
80108142:	6a 00                	push   $0x0
  pushl $251
80108144:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80108149:	e9 2a ee ff ff       	jmp    80106f78 <alltraps>

8010814e <vector252>:
.globl vector252
vector252:
  pushl $0
8010814e:	6a 00                	push   $0x0
  pushl $252
80108150:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80108155:	e9 1e ee ff ff       	jmp    80106f78 <alltraps>

8010815a <vector253>:
.globl vector253
vector253:
  pushl $0
8010815a:	6a 00                	push   $0x0
  pushl $253
8010815c:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
80108161:	e9 12 ee ff ff       	jmp    80106f78 <alltraps>

80108166 <vector254>:
.globl vector254
vector254:
  pushl $0
80108166:	6a 00                	push   $0x0
  pushl $254
80108168:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
8010816d:	e9 06 ee ff ff       	jmp    80106f78 <alltraps>

80108172 <vector255>:
.globl vector255
vector255:
  pushl $0
80108172:	6a 00                	push   $0x0
  pushl $255
80108174:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80108179:	e9 fa ed ff ff       	jmp    80106f78 <alltraps>
	...

80108180 <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
80108180:	55                   	push   %ebp
80108181:	89 e5                	mov    %esp,%ebp
80108183:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80108186:	8b 45 0c             	mov    0xc(%ebp),%eax
80108189:	83 e8 01             	sub    $0x1,%eax
8010818c:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80108190:	8b 45 08             	mov    0x8(%ebp),%eax
80108193:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80108197:	8b 45 08             	mov    0x8(%ebp),%eax
8010819a:	c1 e8 10             	shr    $0x10,%eax
8010819d:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
801081a1:	8d 45 fa             	lea    -0x6(%ebp),%eax
801081a4:	0f 01 10             	lgdtl  (%eax)
}
801081a7:	c9                   	leave  
801081a8:	c3                   	ret    

801081a9 <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
801081a9:	55                   	push   %ebp
801081aa:	89 e5                	mov    %esp,%ebp
801081ac:	83 ec 04             	sub    $0x4,%esp
801081af:	8b 45 08             	mov    0x8(%ebp),%eax
801081b2:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
801081b6:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801081ba:	0f 00 d8             	ltr    %ax
}
801081bd:	c9                   	leave  
801081be:	c3                   	ret    

801081bf <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
801081bf:	55                   	push   %ebp
801081c0:	89 e5                	mov    %esp,%ebp
801081c2:	83 ec 04             	sub    $0x4,%esp
801081c5:	8b 45 08             	mov    0x8(%ebp),%eax
801081c8:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
801081cc:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801081d0:	8e e8                	mov    %eax,%gs
}
801081d2:	c9                   	leave  
801081d3:	c3                   	ret    

801081d4 <lcr3>:
  return val;
}

static inline void
lcr3(uint val)
{
801081d4:	55                   	push   %ebp
801081d5:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
801081d7:	8b 45 08             	mov    0x8(%ebp),%eax
801081da:	0f 22 d8             	mov    %eax,%cr3
}
801081dd:	5d                   	pop    %ebp
801081de:	c3                   	ret    

801081df <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
801081df:	55                   	push   %ebp
801081e0:	89 e5                	mov    %esp,%ebp
801081e2:	53                   	push   %ebx
801081e3:	83 ec 24             	sub    $0x24,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
801081e6:	e8 0f b5 ff ff       	call   801036fa <cpunum>
801081eb:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
801081f1:	05 e0 3b 11 80       	add    $0x80113be0,%eax
801081f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
801081f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081fc:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
80108202:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108205:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
8010820b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010820e:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
80108212:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108215:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108219:	83 e2 f0             	and    $0xfffffff0,%edx
8010821c:	83 ca 0a             	or     $0xa,%edx
8010821f:	88 50 7d             	mov    %dl,0x7d(%eax)
80108222:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108225:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108229:	83 ca 10             	or     $0x10,%edx
8010822c:	88 50 7d             	mov    %dl,0x7d(%eax)
8010822f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108232:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108236:	83 e2 9f             	and    $0xffffff9f,%edx
80108239:	88 50 7d             	mov    %dl,0x7d(%eax)
8010823c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010823f:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108243:	83 ca 80             	or     $0xffffff80,%edx
80108246:	88 50 7d             	mov    %dl,0x7d(%eax)
80108249:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010824c:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80108250:	83 ca 0f             	or     $0xf,%edx
80108253:	88 50 7e             	mov    %dl,0x7e(%eax)
80108256:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108259:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
8010825d:	83 e2 ef             	and    $0xffffffef,%edx
80108260:	88 50 7e             	mov    %dl,0x7e(%eax)
80108263:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108266:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
8010826a:	83 e2 df             	and    $0xffffffdf,%edx
8010826d:	88 50 7e             	mov    %dl,0x7e(%eax)
80108270:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108273:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80108277:	83 ca 40             	or     $0x40,%edx
8010827a:	88 50 7e             	mov    %dl,0x7e(%eax)
8010827d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108280:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80108284:	83 ca 80             	or     $0xffffff80,%edx
80108287:	88 50 7e             	mov    %dl,0x7e(%eax)
8010828a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010828d:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80108291:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108294:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
8010829b:	ff ff 
8010829d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082a0:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
801082a7:	00 00 
801082a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082ac:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
801082b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082b6:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
801082bd:	83 e2 f0             	and    $0xfffffff0,%edx
801082c0:	83 ca 02             	or     $0x2,%edx
801082c3:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
801082c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082cc:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
801082d3:	83 ca 10             	or     $0x10,%edx
801082d6:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
801082dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082df:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
801082e6:	83 e2 9f             	and    $0xffffff9f,%edx
801082e9:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
801082ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801082f2:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
801082f9:	83 ca 80             	or     $0xffffff80,%edx
801082fc:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80108302:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108305:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010830c:	83 ca 0f             	or     $0xf,%edx
8010830f:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108315:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108318:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010831f:	83 e2 ef             	and    $0xffffffef,%edx
80108322:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108328:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010832b:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80108332:	83 e2 df             	and    $0xffffffdf,%edx
80108335:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
8010833b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010833e:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80108345:	83 ca 40             	or     $0x40,%edx
80108348:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
8010834e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108351:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80108358:	83 ca 80             	or     $0xffffff80,%edx
8010835b:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108361:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108364:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
8010836b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010836e:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
80108375:	ff ff 
80108377:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010837a:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
80108381:	00 00 
80108383:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108386:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
8010838d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108390:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80108397:	83 e2 f0             	and    $0xfffffff0,%edx
8010839a:	83 ca 0a             	or     $0xa,%edx
8010839d:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801083a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083a6:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801083ad:	83 ca 10             	or     $0x10,%edx
801083b0:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801083b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083b9:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801083c0:	83 ca 60             	or     $0x60,%edx
801083c3:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801083c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083cc:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801083d3:	83 ca 80             	or     $0xffffff80,%edx
801083d6:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801083dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083df:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
801083e6:	83 ca 0f             	or     $0xf,%edx
801083e9:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
801083ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801083f2:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
801083f9:	83 e2 ef             	and    $0xffffffef,%edx
801083fc:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108402:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108405:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010840c:	83 e2 df             	and    $0xffffffdf,%edx
8010840f:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108415:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108418:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010841f:	83 ca 40             	or     $0x40,%edx
80108422:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108428:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010842b:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108432:	83 ca 80             	or     $0xffffff80,%edx
80108435:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010843b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010843e:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80108445:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108448:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
8010844f:	ff ff 
80108451:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108454:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
8010845b:	00 00 
8010845d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108460:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
80108467:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010846a:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80108471:	83 e2 f0             	and    $0xfffffff0,%edx
80108474:	83 ca 02             	or     $0x2,%edx
80108477:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
8010847d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108480:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80108487:	83 ca 10             	or     $0x10,%edx
8010848a:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80108490:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108493:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
8010849a:	83 ca 60             	or     $0x60,%edx
8010849d:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801084a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084a6:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801084ad:	83 ca 80             	or     $0xffffff80,%edx
801084b0:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801084b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084b9:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
801084c0:	83 ca 0f             	or     $0xf,%edx
801084c3:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
801084c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084cc:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
801084d3:	83 e2 ef             	and    $0xffffffef,%edx
801084d6:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
801084dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084df:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
801084e6:	83 e2 df             	and    $0xffffffdf,%edx
801084e9:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
801084ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084f2:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
801084f9:	83 ca 40             	or     $0x40,%edx
801084fc:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108502:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108505:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010850c:	83 ca 80             	or     $0xffffff80,%edx
8010850f:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108515:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108518:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
8010851f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108522:	05 b4 00 00 00       	add    $0xb4,%eax
80108527:	89 c3                	mov    %eax,%ebx
80108529:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010852c:	05 b4 00 00 00       	add    $0xb4,%eax
80108531:	c1 e8 10             	shr    $0x10,%eax
80108534:	89 c1                	mov    %eax,%ecx
80108536:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108539:	05 b4 00 00 00       	add    $0xb4,%eax
8010853e:	c1 e8 18             	shr    $0x18,%eax
80108541:	89 c2                	mov    %eax,%edx
80108543:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108546:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
8010854d:	00 00 
8010854f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108552:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
80108559:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010855c:	88 88 8c 00 00 00    	mov    %cl,0x8c(%eax)
80108562:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108565:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
8010856c:	83 e1 f0             	and    $0xfffffff0,%ecx
8010856f:	83 c9 02             	or     $0x2,%ecx
80108572:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
80108578:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010857b:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
80108582:	83 c9 10             	or     $0x10,%ecx
80108585:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
8010858b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010858e:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
80108595:	83 e1 9f             	and    $0xffffff9f,%ecx
80108598:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
8010859e:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085a1:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801085a8:	83 c9 80             	or     $0xffffff80,%ecx
801085ab:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801085b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085b4:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801085bb:	83 e1 f0             	and    $0xfffffff0,%ecx
801085be:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801085c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085c7:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801085ce:	83 e1 ef             	and    $0xffffffef,%ecx
801085d1:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801085d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085da:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801085e1:	83 e1 df             	and    $0xffffffdf,%ecx
801085e4:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801085ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085ed:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
801085f4:	83 c9 40             	or     $0x40,%ecx
801085f7:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
801085fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108600:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80108607:	83 c9 80             	or     $0xffffff80,%ecx
8010860a:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80108610:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108613:	88 90 8f 00 00 00    	mov    %dl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
80108619:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010861c:	83 c0 70             	add    $0x70,%eax
8010861f:	c7 44 24 04 38 00 00 	movl   $0x38,0x4(%esp)
80108626:	00 
80108627:	89 04 24             	mov    %eax,(%esp)
8010862a:	e8 51 fb ff ff       	call   80108180 <lgdt>
  loadgs(SEG_KCPU << 3);
8010862f:	c7 04 24 18 00 00 00 	movl   $0x18,(%esp)
80108636:	e8 84 fb ff ff       	call   801081bf <loadgs>

  // Initialize cpu-local storage.
  cpu = c;
8010863b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010863e:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
80108644:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
8010864b:	00 00 00 00 
}
8010864f:	83 c4 24             	add    $0x24,%esp
80108652:	5b                   	pop    %ebx
80108653:	5d                   	pop    %ebp
80108654:	c3                   	ret    

80108655 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
80108655:	55                   	push   %ebp
80108656:	89 e5                	mov    %esp,%ebp
80108658:	83 ec 28             	sub    $0x28,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
8010865b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010865e:	c1 e8 16             	shr    $0x16,%eax
80108661:	c1 e0 02             	shl    $0x2,%eax
80108664:	03 45 08             	add    0x8(%ebp),%eax
80108667:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
8010866a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010866d:	8b 00                	mov    (%eax),%eax
8010866f:	83 e0 01             	and    $0x1,%eax
80108672:	84 c0                	test   %al,%al
80108674:	74 14                	je     8010868a <walkpgdir+0x35>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80108676:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108679:	8b 00                	mov    (%eax),%eax
8010867b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108680:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108685:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108688:	eb 48                	jmp    801086d2 <walkpgdir+0x7d>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
8010868a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010868e:	74 0e                	je     8010869e <walkpgdir+0x49>
80108690:	e8 09 ad ff ff       	call   8010339e <kalloc>
80108695:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108698:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010869c:	75 07                	jne    801086a5 <walkpgdir+0x50>
      return 0;
8010869e:	b8 00 00 00 00       	mov    $0x0,%eax
801086a3:	eb 3e                	jmp    801086e3 <walkpgdir+0x8e>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
801086a5:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801086ac:	00 
801086ad:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801086b4:	00 
801086b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086b8:	89 04 24             	mov    %eax,(%esp)
801086bb:	e8 a2 d4 ff ff       	call   80105b62 <memset>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
801086c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086c3:	2d 00 00 00 80       	sub    $0x80000000,%eax
801086c8:	89 c2                	mov    %eax,%edx
801086ca:	83 ca 07             	or     $0x7,%edx
801086cd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801086d0:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
801086d2:	8b 45 0c             	mov    0xc(%ebp),%eax
801086d5:	c1 e8 0c             	shr    $0xc,%eax
801086d8:	25 ff 03 00 00       	and    $0x3ff,%eax
801086dd:	c1 e0 02             	shl    $0x2,%eax
801086e0:	03 45 f4             	add    -0xc(%ebp),%eax
}
801086e3:	c9                   	leave  
801086e4:	c3                   	ret    

801086e5 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
801086e5:	55                   	push   %ebp
801086e6:	89 e5                	mov    %esp,%ebp
801086e8:	83 ec 28             	sub    $0x28,%esp
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
801086eb:	8b 45 0c             	mov    0xc(%ebp),%eax
801086ee:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801086f3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
801086f6:	8b 45 0c             	mov    0xc(%ebp),%eax
801086f9:	03 45 10             	add    0x10(%ebp),%eax
801086fc:	83 e8 01             	sub    $0x1,%eax
801086ff:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108704:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80108707:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010870e:	00 
8010870f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108712:	89 44 24 04          	mov    %eax,0x4(%esp)
80108716:	8b 45 08             	mov    0x8(%ebp),%eax
80108719:	89 04 24             	mov    %eax,(%esp)
8010871c:	e8 34 ff ff ff       	call   80108655 <walkpgdir>
80108721:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108724:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108728:	75 07                	jne    80108731 <mappages+0x4c>
      return -1;
8010872a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010872f:	eb 46                	jmp    80108777 <mappages+0x92>
    if(*pte & PTE_P)
80108731:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108734:	8b 00                	mov    (%eax),%eax
80108736:	83 e0 01             	and    $0x1,%eax
80108739:	84 c0                	test   %al,%al
8010873b:	74 0c                	je     80108749 <mappages+0x64>
      panic("remap");
8010873d:	c7 04 24 9c 97 10 80 	movl   $0x8010979c,(%esp)
80108744:	e8 20 81 ff ff       	call   80100869 <panic>
    *pte = pa | perm | PTE_P;
80108749:	8b 45 18             	mov    0x18(%ebp),%eax
8010874c:	0b 45 14             	or     0x14(%ebp),%eax
8010874f:	89 c2                	mov    %eax,%edx
80108751:	83 ca 01             	or     $0x1,%edx
80108754:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108757:	89 10                	mov    %edx,(%eax)
    if(a == last)
80108759:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010875c:	3b 45 f0             	cmp    -0x10(%ebp),%eax
8010875f:	74 10                	je     80108771 <mappages+0x8c>
      break;
    a += PGSIZE;
80108761:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
    pa += PGSIZE;
80108768:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
8010876f:	eb 96                	jmp    80108707 <mappages+0x22>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
80108771:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
80108772:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108777:	c9                   	leave  
80108778:	c3                   	ret    

80108779 <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
80108779:	55                   	push   %ebp
8010877a:	89 e5                	mov    %esp,%ebp
8010877c:	53                   	push   %ebx
8010877d:	83 ec 34             	sub    $0x34,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
80108780:	e8 19 ac ff ff       	call   8010339e <kalloc>
80108785:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108788:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010878c:	75 07                	jne    80108795 <setupkvm+0x1c>
    return 0;
8010878e:	b8 00 00 00 00       	mov    $0x0,%eax
80108793:	eb 7b                	jmp    80108810 <setupkvm+0x97>
  memset(pgdir, 0, PGSIZE);
80108795:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
8010879c:	00 
8010879d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801087a4:	00 
801087a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801087a8:	89 04 24             	mov    %eax,(%esp)
801087ab:	e8 b2 d3 ff ff       	call   80105b62 <memset>
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
801087b0:	90                   	nop
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
801087b1:	c7 45 f4 e0 c4 10 80 	movl   $0x8010c4e0,-0xc(%ebp)
801087b8:	eb 49                	jmp    80108803 <setupkvm+0x8a>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
801087ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087bd:	8b 48 0c             	mov    0xc(%eax),%ecx
801087c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087c3:	8b 50 04             	mov    0x4(%eax),%edx
801087c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087c9:	8b 58 08             	mov    0x8(%eax),%ebx
801087cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087cf:	8b 40 04             	mov    0x4(%eax),%eax
801087d2:	29 c3                	sub    %eax,%ebx
801087d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087d7:	8b 00                	mov    (%eax),%eax
801087d9:	89 4c 24 10          	mov    %ecx,0x10(%esp)
801087dd:	89 54 24 0c          	mov    %edx,0xc(%esp)
801087e1:	89 5c 24 08          	mov    %ebx,0x8(%esp)
801087e5:	89 44 24 04          	mov    %eax,0x4(%esp)
801087e9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801087ec:	89 04 24             	mov    %eax,(%esp)
801087ef:	e8 f1 fe ff ff       	call   801086e5 <mappages>
801087f4:	85 c0                	test   %eax,%eax
801087f6:	79 07                	jns    801087ff <setupkvm+0x86>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
801087f8:	b8 00 00 00 00       	mov    $0x0,%eax
801087fd:	eb 11                	jmp    80108810 <setupkvm+0x97>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if(P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("setupkvm: PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
801087ff:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80108803:	b8 20 c5 10 80       	mov    $0x8010c520,%eax
80108808:	39 45 f4             	cmp    %eax,-0xc(%ebp)
8010880b:	72 ad                	jb     801087ba <setupkvm+0x41>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
8010880d:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80108810:	83 c4 34             	add    $0x34,%esp
80108813:	5b                   	pop    %ebx
80108814:	5d                   	pop    %ebp
80108815:	c3                   	ret    

80108816 <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
80108816:	55                   	push   %ebp
80108817:	89 e5                	mov    %esp,%ebp
80108819:	83 ec 18             	sub    $0x18,%esp
  kpgdir = setupkvm();
8010881c:	e8 58 ff ff ff       	call   80108779 <setupkvm>
80108821:	a3 c4 ce 10 80       	mov    %eax,0x8010cec4
  if(kpgdir == 0)
80108826:	a1 c4 ce 10 80       	mov    0x8010cec4,%eax
8010882b:	85 c0                	test   %eax,%eax
8010882d:	75 0c                	jne    8010883b <kvmalloc+0x25>
    panic("kvmalloc: could not create kernel page table");
8010882f:	c7 04 24 a4 97 10 80 	movl   $0x801097a4,(%esp)
80108836:	e8 2e 80 ff ff       	call   80100869 <panic>
  switchkvm();
8010883b:	e8 02 00 00 00       	call   80108842 <switchkvm>
}
80108840:	c9                   	leave  
80108841:	c3                   	ret    

80108842 <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
80108842:	55                   	push   %ebp
80108843:	89 e5                	mov    %esp,%ebp
80108845:	83 ec 04             	sub    $0x4,%esp
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80108848:	a1 c4 ce 10 80       	mov    0x8010cec4,%eax
8010884d:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108852:	89 04 24             	mov    %eax,(%esp)
80108855:	e8 7a f9 ff ff       	call   801081d4 <lcr3>
}
8010885a:	c9                   	leave  
8010885b:	c3                   	ret    

8010885c <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
8010885c:	55                   	push   %ebp
8010885d:	89 e5                	mov    %esp,%ebp
8010885f:	53                   	push   %ebx
80108860:	83 ec 14             	sub    $0x14,%esp
  pushcli();
80108863:	e8 f5 d1 ff ff       	call   80105a5d <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
80108868:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010886e:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108875:	83 c2 08             	add    $0x8,%edx
80108878:	89 d3                	mov    %edx,%ebx
8010887a:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108881:	83 c2 08             	add    $0x8,%edx
80108884:	c1 ea 10             	shr    $0x10,%edx
80108887:	89 d1                	mov    %edx,%ecx
80108889:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108890:	83 c2 08             	add    $0x8,%edx
80108893:	c1 ea 18             	shr    $0x18,%edx
80108896:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
8010889d:	67 00 
8010889f:	66 89 98 a2 00 00 00 	mov    %bx,0xa2(%eax)
801088a6:	88 88 a4 00 00 00    	mov    %cl,0xa4(%eax)
801088ac:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801088b3:	83 e1 f0             	and    $0xfffffff0,%ecx
801088b6:	83 c9 09             	or     $0x9,%ecx
801088b9:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801088bf:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801088c6:	83 c9 10             	or     $0x10,%ecx
801088c9:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801088cf:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801088d6:	83 e1 9f             	and    $0xffffff9f,%ecx
801088d9:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801088df:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
801088e6:	83 c9 80             	or     $0xffffff80,%ecx
801088e9:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
801088ef:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
801088f6:	83 e1 f0             	and    $0xfffffff0,%ecx
801088f9:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
801088ff:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108906:	83 e1 ef             	and    $0xffffffef,%ecx
80108909:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010890f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108916:	83 e1 df             	and    $0xffffffdf,%ecx
80108919:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010891f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108926:	83 c9 40             	or     $0x40,%ecx
80108929:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010892f:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80108936:	83 e1 7f             	and    $0x7f,%ecx
80108939:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
8010893f:	88 90 a7 00 00 00    	mov    %dl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
80108945:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010894b:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108952:	83 e2 ef             	and    $0xffffffef,%edx
80108955:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
8010895b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108961:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
80108967:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010896d:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80108974:	8b 52 08             	mov    0x8(%edx),%edx
80108977:	81 c2 00 10 00 00    	add    $0x1000,%edx
8010897d:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
80108980:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
80108987:	e8 1d f8 ff ff       	call   801081a9 <ltr>
  if(p->pgdir == 0)
8010898c:	8b 45 08             	mov    0x8(%ebp),%eax
8010898f:	8b 40 04             	mov    0x4(%eax),%eax
80108992:	85 c0                	test   %eax,%eax
80108994:	75 0c                	jne    801089a2 <switchuvm+0x146>
    panic("switchuvm: no pgdir");
80108996:	c7 04 24 d1 97 10 80 	movl   $0x801097d1,(%esp)
8010899d:	e8 c7 7e ff ff       	call   80100869 <panic>
  lcr3(V2P(p->pgdir));  // switch to new address space
801089a2:	8b 45 08             	mov    0x8(%ebp),%eax
801089a5:	8b 40 04             	mov    0x4(%eax),%eax
801089a8:	2d 00 00 00 80       	sub    $0x80000000,%eax
801089ad:	89 04 24             	mov    %eax,(%esp)
801089b0:	e8 1f f8 ff ff       	call   801081d4 <lcr3>
  popcli();
801089b5:	e8 eb d0 ff ff       	call   80105aa5 <popcli>
}
801089ba:	83 c4 14             	add    $0x14,%esp
801089bd:	5b                   	pop    %ebx
801089be:	5d                   	pop    %ebp
801089bf:	c3                   	ret    

801089c0 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
801089c0:	55                   	push   %ebp
801089c1:	89 e5                	mov    %esp,%ebp
801089c3:	83 ec 38             	sub    $0x38,%esp
  char *mem;

  if(sz >= PGSIZE)
801089c6:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
801089cd:	76 0c                	jbe    801089db <inituvm+0x1b>
    panic("inituvm: more than a page");
801089cf:	c7 04 24 e5 97 10 80 	movl   $0x801097e5,(%esp)
801089d6:	e8 8e 7e ff ff       	call   80100869 <panic>
  if ((mem = kalloc()) == 0)
801089db:	e8 be a9 ff ff       	call   8010339e <kalloc>
801089e0:	89 45 f4             	mov    %eax,-0xc(%ebp)
801089e3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801089e7:	75 0c                	jne    801089f5 <inituvm+0x35>
    panic("inituvm: cannot allocate memory");
801089e9:	c7 04 24 00 98 10 80 	movl   $0x80109800,(%esp)
801089f0:	e8 74 7e ff ff       	call   80100869 <panic>
  memset(mem, 0, PGSIZE);
801089f5:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801089fc:	00 
801089fd:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108a04:	00 
80108a05:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a08:	89 04 24             	mov    %eax,(%esp)
80108a0b:	e8 52 d1 ff ff       	call   80105b62 <memset>
  if (mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108a10:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a13:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108a18:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80108a1f:	00 
80108a20:	89 44 24 0c          	mov    %eax,0xc(%esp)
80108a24:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108a2b:	00 
80108a2c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108a33:	00 
80108a34:	8b 45 08             	mov    0x8(%ebp),%eax
80108a37:	89 04 24             	mov    %eax,(%esp)
80108a3a:	e8 a6 fc ff ff       	call   801086e5 <mappages>
80108a3f:	85 c0                	test   %eax,%eax
80108a41:	79 0c                	jns    80108a4f <inituvm+0x8f>
    panic("inituvm: cannot create pagetable");
80108a43:	c7 04 24 20 98 10 80 	movl   $0x80109820,(%esp)
80108a4a:	e8 1a 7e ff ff       	call   80100869 <panic>
  memmove(mem, init, sz);
80108a4f:	8b 45 10             	mov    0x10(%ebp),%eax
80108a52:	89 44 24 08          	mov    %eax,0x8(%esp)
80108a56:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a59:	89 44 24 04          	mov    %eax,0x4(%esp)
80108a5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a60:	89 04 24             	mov    %eax,(%esp)
80108a63:	e8 cd d1 ff ff       	call   80105c35 <memmove>
}
80108a68:	c9                   	leave  
80108a69:	c3                   	ret    

80108a6a <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
80108a6a:	55                   	push   %ebp
80108a6b:	89 e5                	mov    %esp,%ebp
80108a6d:	83 ec 28             	sub    $0x28,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80108a70:	8b 45 0c             	mov    0xc(%ebp),%eax
80108a73:	25 ff 0f 00 00       	and    $0xfff,%eax
80108a78:	85 c0                	test   %eax,%eax
80108a7a:	74 0c                	je     80108a88 <loaduvm+0x1e>
    panic("loaduvm: addr must be page aligned");
80108a7c:	c7 04 24 44 98 10 80 	movl   $0x80109844,(%esp)
80108a83:	e8 e1 7d ff ff       	call   80100869 <panic>
  for(i = 0; i < sz; i += PGSIZE){
80108a88:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
80108a8f:	e9 ab 00 00 00       	jmp    80108b3f <loaduvm+0xd5>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80108a94:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108a97:	8b 55 0c             	mov    0xc(%ebp),%edx
80108a9a:	8d 04 02             	lea    (%edx,%eax,1),%eax
80108a9d:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108aa4:	00 
80108aa5:	89 44 24 04          	mov    %eax,0x4(%esp)
80108aa9:	8b 45 08             	mov    0x8(%ebp),%eax
80108aac:	89 04 24             	mov    %eax,(%esp)
80108aaf:	e8 a1 fb ff ff       	call   80108655 <walkpgdir>
80108ab4:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108ab7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108abb:	75 0c                	jne    80108ac9 <loaduvm+0x5f>
      panic("loaduvm: address should exist");
80108abd:	c7 04 24 67 98 10 80 	movl   $0x80109867,(%esp)
80108ac4:	e8 a0 7d ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
80108ac9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108acc:	8b 00                	mov    (%eax),%eax
80108ace:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108ad3:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(sz - i < PGSIZE)
80108ad6:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108ad9:	8b 55 18             	mov    0x18(%ebp),%edx
80108adc:	89 d1                	mov    %edx,%ecx
80108ade:	29 c1                	sub    %eax,%ecx
80108ae0:	89 c8                	mov    %ecx,%eax
80108ae2:	3d ff 0f 00 00       	cmp    $0xfff,%eax
80108ae7:	77 11                	ja     80108afa <loaduvm+0x90>
      n = sz - i;
80108ae9:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108aec:	8b 55 18             	mov    0x18(%ebp),%edx
80108aef:	89 d1                	mov    %edx,%ecx
80108af1:	29 c1                	sub    %eax,%ecx
80108af3:	89 c8                	mov    %ecx,%eax
80108af5:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108af8:	eb 07                	jmp    80108b01 <loaduvm+0x97>
    else
      n = PGSIZE;
80108afa:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80108b01:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108b04:	8b 55 14             	mov    0x14(%ebp),%edx
80108b07:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
80108b0a:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108b0d:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108b12:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108b15:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108b19:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80108b1d:	89 44 24 04          	mov    %eax,0x4(%esp)
80108b21:	8b 45 10             	mov    0x10(%ebp),%eax
80108b24:	89 04 24             	mov    %eax,(%esp)
80108b27:	e8 44 9a ff ff       	call   80102570 <readi>
80108b2c:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80108b2f:	74 07                	je     80108b38 <loaduvm+0xce>
      return -1;
80108b31:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108b36:	eb 18                	jmp    80108b50 <loaduvm+0xe6>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80108b38:	81 45 e8 00 10 00 00 	addl   $0x1000,-0x18(%ebp)
80108b3f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108b42:	3b 45 18             	cmp    0x18(%ebp),%eax
80108b45:	0f 82 49 ff ff ff    	jb     80108a94 <loaduvm+0x2a>
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
80108b4b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108b50:	c9                   	leave  
80108b51:	c3                   	ret    

80108b52 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108b52:	55                   	push   %ebp
80108b53:	89 e5                	mov    %esp,%ebp
80108b55:	83 ec 38             	sub    $0x38,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
80108b58:	8b 45 10             	mov    0x10(%ebp),%eax
80108b5b:	85 c0                	test   %eax,%eax
80108b5d:	79 0a                	jns    80108b69 <allocuvm+0x17>
    return 0;
80108b5f:	b8 00 00 00 00       	mov    $0x0,%eax
80108b64:	e9 cf 00 00 00       	jmp    80108c38 <allocuvm+0xe6>
  if(newsz < oldsz)
80108b69:	8b 45 10             	mov    0x10(%ebp),%eax
80108b6c:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108b6f:	73 08                	jae    80108b79 <allocuvm+0x27>
    return oldsz;
80108b71:	8b 45 0c             	mov    0xc(%ebp),%eax
80108b74:	e9 bf 00 00 00       	jmp    80108c38 <allocuvm+0xe6>

  a = PGROUNDUP(oldsz);
80108b79:	8b 45 0c             	mov    0xc(%ebp),%eax
80108b7c:	05 ff 0f 00 00       	add    $0xfff,%eax
80108b81:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108b86:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
80108b89:	e9 9b 00 00 00       	jmp    80108c29 <allocuvm+0xd7>
    mem = kalloc();
80108b8e:	e8 0b a8 ff ff       	call   8010339e <kalloc>
80108b93:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
80108b96:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108b9a:	75 2c                	jne    80108bc8 <allocuvm+0x76>
      cprintf("allocuvm out of memory\n");
80108b9c:	c7 04 24 85 98 10 80 	movl   $0x80109885,(%esp)
80108ba3:	e8 21 7b ff ff       	call   801006c9 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
80108ba8:	8b 45 0c             	mov    0xc(%ebp),%eax
80108bab:	89 44 24 08          	mov    %eax,0x8(%esp)
80108baf:	8b 45 10             	mov    0x10(%ebp),%eax
80108bb2:	89 44 24 04          	mov    %eax,0x4(%esp)
80108bb6:	8b 45 08             	mov    0x8(%ebp),%eax
80108bb9:	89 04 24             	mov    %eax,(%esp)
80108bbc:	e8 79 00 00 00       	call   80108c3a <deallocuvm>
      return 0;
80108bc1:	b8 00 00 00 00       	mov    $0x0,%eax
80108bc6:	eb 70                	jmp    80108c38 <allocuvm+0xe6>
    }
    memset(mem, 0, PGSIZE);
80108bc8:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108bcf:	00 
80108bd0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80108bd7:	00 
80108bd8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108bdb:	89 04 24             	mov    %eax,(%esp)
80108bde:	e8 7f cf ff ff       	call   80105b62 <memset>
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
80108be3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108be6:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80108bec:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108bef:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80108bf6:	00 
80108bf7:	89 54 24 0c          	mov    %edx,0xc(%esp)
80108bfb:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108c02:	00 
80108c03:	89 44 24 04          	mov    %eax,0x4(%esp)
80108c07:	8b 45 08             	mov    0x8(%ebp),%eax
80108c0a:	89 04 24             	mov    %eax,(%esp)
80108c0d:	e8 d3 fa ff ff       	call   801086e5 <mappages>
80108c12:	85 c0                	test   %eax,%eax
80108c14:	79 0c                	jns    80108c22 <allocuvm+0xd0>
      panic("allocuvm: cannot create pagetable");
80108c16:	c7 04 24 a0 98 10 80 	movl   $0x801098a0,(%esp)
80108c1d:	e8 47 7c ff ff       	call   80100869 <panic>
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80108c22:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80108c29:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108c2c:	3b 45 10             	cmp    0x10(%ebp),%eax
80108c2f:	0f 82 59 ff ff ff    	jb     80108b8e <allocuvm+0x3c>
    }
    memset(mem, 0, PGSIZE);
    if (mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0)
      panic("allocuvm: cannot create pagetable");
  }
  return newsz;
80108c35:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108c38:	c9                   	leave  
80108c39:	c3                   	ret    

80108c3a <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108c3a:	55                   	push   %ebp
80108c3b:	89 e5                	mov    %esp,%ebp
80108c3d:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80108c40:	8b 45 10             	mov    0x10(%ebp),%eax
80108c43:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108c46:	72 08                	jb     80108c50 <deallocuvm+0x16>
    return oldsz;
80108c48:	8b 45 0c             	mov    0xc(%ebp),%eax
80108c4b:	e9 9e 00 00 00       	jmp    80108cee <deallocuvm+0xb4>

  a = PGROUNDUP(newsz);
80108c50:	8b 45 10             	mov    0x10(%ebp),%eax
80108c53:	05 ff 0f 00 00       	add    $0xfff,%eax
80108c58:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108c5d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80108c60:	eb 7d                	jmp    80108cdf <deallocuvm+0xa5>
    pte = walkpgdir(pgdir, (char*)a, 0);
80108c62:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108c65:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108c6c:	00 
80108c6d:	89 44 24 04          	mov    %eax,0x4(%esp)
80108c71:	8b 45 08             	mov    0x8(%ebp),%eax
80108c74:	89 04 24             	mov    %eax,(%esp)
80108c77:	e8 d9 f9 ff ff       	call   80108655 <walkpgdir>
80108c7c:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(!pte)
80108c7f:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80108c83:	75 09                	jne    80108c8e <deallocuvm+0x54>
      a += (NPTENTRIES - 1) * PGSIZE;
80108c85:	81 45 ec 00 f0 3f 00 	addl   $0x3ff000,-0x14(%ebp)
80108c8c:	eb 4a                	jmp    80108cd8 <deallocuvm+0x9e>
    else if((*pte & PTE_P) != 0){
80108c8e:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108c91:	8b 00                	mov    (%eax),%eax
80108c93:	83 e0 01             	and    $0x1,%eax
80108c96:	84 c0                	test   %al,%al
80108c98:	74 3e                	je     80108cd8 <deallocuvm+0x9e>
      pa = PTE_ADDR(*pte);
80108c9a:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108c9d:	8b 00                	mov    (%eax),%eax
80108c9f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108ca4:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(pa == 0)
80108ca7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108cab:	75 0c                	jne    80108cb9 <deallocuvm+0x7f>
        panic("deallocuvm");
80108cad:	c7 04 24 c2 98 10 80 	movl   $0x801098c2,(%esp)
80108cb4:	e8 b0 7b ff ff       	call   80100869 <panic>
      char *v = P2V(pa);
80108cb9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108cbc:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108cc1:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
80108cc4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108cc7:	89 04 24             	mov    %eax,(%esp)
80108cca:	e8 39 a6 ff ff       	call   80103308 <kfree>
      *pte = 0;
80108ccf:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108cd2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
80108cd8:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
80108cdf:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108ce2:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108ce5:	0f 82 77 ff ff ff    	jb     80108c62 <deallocuvm+0x28>
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
80108ceb:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108cee:	c9                   	leave  
80108cef:	c3                   	ret    

80108cf0 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80108cf0:	55                   	push   %ebp
80108cf1:	89 e5                	mov    %esp,%ebp
80108cf3:	83 ec 28             	sub    $0x28,%esp
  uint i;

  if(pgdir == 0)
80108cf6:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80108cfa:	75 0c                	jne    80108d08 <freevm+0x18>
    panic("freevm: no pgdir");
80108cfc:	c7 04 24 cd 98 10 80 	movl   $0x801098cd,(%esp)
80108d03:	e8 61 7b ff ff       	call   80100869 <panic>
  deallocuvm(pgdir, KERNBASE, 0);
80108d08:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108d0f:	00 
80108d10:	c7 44 24 04 00 00 00 	movl   $0x80000000,0x4(%esp)
80108d17:	80 
80108d18:	8b 45 08             	mov    0x8(%ebp),%eax
80108d1b:	89 04 24             	mov    %eax,(%esp)
80108d1e:	e8 17 ff ff ff       	call   80108c3a <deallocuvm>
  for(i = 0; i < NPDENTRIES; i++){
80108d23:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80108d2a:	eb 39                	jmp    80108d65 <freevm+0x75>
    if(pgdir[i] & PTE_P){
80108d2c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108d2f:	c1 e0 02             	shl    $0x2,%eax
80108d32:	03 45 08             	add    0x8(%ebp),%eax
80108d35:	8b 00                	mov    (%eax),%eax
80108d37:	83 e0 01             	and    $0x1,%eax
80108d3a:	84 c0                	test   %al,%al
80108d3c:	74 23                	je     80108d61 <freevm+0x71>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80108d3e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108d41:	c1 e0 02             	shl    $0x2,%eax
80108d44:	03 45 08             	add    0x8(%ebp),%eax
80108d47:	8b 00                	mov    (%eax),%eax
80108d49:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108d4e:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108d53:	89 45 f4             	mov    %eax,-0xc(%ebp)
      kfree(v);
80108d56:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d59:	89 04 24             	mov    %eax,(%esp)
80108d5c:	e8 a7 a5 ff ff       	call   80103308 <kfree>
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80108d61:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80108d65:	81 7d f0 ff 03 00 00 	cmpl   $0x3ff,-0x10(%ebp)
80108d6c:	76 be                	jbe    80108d2c <freevm+0x3c>
    if(pgdir[i] & PTE_P){
      char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80108d6e:	8b 45 08             	mov    0x8(%ebp),%eax
80108d71:	89 04 24             	mov    %eax,(%esp)
80108d74:	e8 8f a5 ff ff       	call   80103308 <kfree>
}
80108d79:	c9                   	leave  
80108d7a:	c3                   	ret    

80108d7b <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80108d7b:	55                   	push   %ebp
80108d7c:	89 e5                	mov    %esp,%ebp
80108d7e:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108d81:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108d88:	00 
80108d89:	8b 45 0c             	mov    0xc(%ebp),%eax
80108d8c:	89 44 24 04          	mov    %eax,0x4(%esp)
80108d90:	8b 45 08             	mov    0x8(%ebp),%eax
80108d93:	89 04 24             	mov    %eax,(%esp)
80108d96:	e8 ba f8 ff ff       	call   80108655 <walkpgdir>
80108d9b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
80108d9e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108da2:	75 0c                	jne    80108db0 <clearpteu+0x35>
    panic("clearpteu");
80108da4:	c7 04 24 de 98 10 80 	movl   $0x801098de,(%esp)
80108dab:	e8 b9 7a ff ff       	call   80100869 <panic>
  *pte &= ~PTE_U;
80108db0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108db3:	8b 00                	mov    (%eax),%eax
80108db5:	89 c2                	mov    %eax,%edx
80108db7:	83 e2 fb             	and    $0xfffffffb,%edx
80108dba:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108dbd:	89 10                	mov    %edx,(%eax)
}
80108dbf:	c9                   	leave  
80108dc0:	c3                   	ret    

80108dc1 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80108dc1:	55                   	push   %ebp
80108dc2:	89 e5                	mov    %esp,%ebp
80108dc4:	83 ec 48             	sub    $0x48,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80108dc7:	e8 ad f9 ff ff       	call   80108779 <setupkvm>
80108dcc:	89 45 e0             	mov    %eax,-0x20(%ebp)
80108dcf:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80108dd3:	75 0a                	jne    80108ddf <copyuvm+0x1e>
    return 0;
80108dd5:	b8 00 00 00 00       	mov    $0x0,%eax
80108dda:	e9 f8 00 00 00       	jmp    80108ed7 <copyuvm+0x116>
  for(i = 0; i < sz; i += PGSIZE){
80108ddf:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80108de6:	e9 c7 00 00 00       	jmp    80108eb2 <copyuvm+0xf1>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80108deb:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108dee:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108df5:	00 
80108df6:	89 44 24 04          	mov    %eax,0x4(%esp)
80108dfa:	8b 45 08             	mov    0x8(%ebp),%eax
80108dfd:	89 04 24             	mov    %eax,(%esp)
80108e00:	e8 50 f8 ff ff       	call   80108655 <walkpgdir>
80108e05:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80108e08:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80108e0c:	75 0c                	jne    80108e1a <copyuvm+0x59>
      panic("copyuvm: pte should exist");
80108e0e:	c7 04 24 e8 98 10 80 	movl   $0x801098e8,(%esp)
80108e15:	e8 4f 7a ff ff       	call   80100869 <panic>
    if(!(*pte & PTE_P))
80108e1a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108e1d:	8b 00                	mov    (%eax),%eax
80108e1f:	83 e0 01             	and    $0x1,%eax
80108e22:	85 c0                	test   %eax,%eax
80108e24:	75 0c                	jne    80108e32 <copyuvm+0x71>
      panic("copyuvm: page not present");
80108e26:	c7 04 24 02 99 10 80 	movl   $0x80109902,(%esp)
80108e2d:	e8 37 7a ff ff       	call   80100869 <panic>
    pa = PTE_ADDR(*pte);
80108e32:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108e35:	8b 00                	mov    (%eax),%eax
80108e37:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108e3c:	89 45 e8             	mov    %eax,-0x18(%ebp)
    flags = PTE_FLAGS(*pte);
80108e3f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80108e42:	8b 00                	mov    (%eax),%eax
80108e44:	25 ff 0f 00 00       	and    $0xfff,%eax
80108e49:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mem = kalloc()) == 0)
80108e4c:	e8 4d a5 ff ff       	call   8010339e <kalloc>
80108e51:	89 45 f4             	mov    %eax,-0xc(%ebp)
80108e54:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108e58:	74 69                	je     80108ec3 <copyuvm+0x102>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80108e5a:	8b 45 e8             	mov    -0x18(%ebp),%eax
80108e5d:	2d 00 00 00 80       	sub    $0x80000000,%eax
80108e62:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108e69:	00 
80108e6a:	89 44 24 04          	mov    %eax,0x4(%esp)
80108e6e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e71:	89 04 24             	mov    %eax,(%esp)
80108e74:	e8 bc cd ff ff       	call   80105c35 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
80108e79:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108e7c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e7f:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
80108e85:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108e88:	89 54 24 10          	mov    %edx,0x10(%esp)
80108e8c:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80108e90:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80108e97:	00 
80108e98:	89 44 24 04          	mov    %eax,0x4(%esp)
80108e9c:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108e9f:	89 04 24             	mov    %eax,(%esp)
80108ea2:	e8 3e f8 ff ff       	call   801086e5 <mappages>
80108ea7:	85 c0                	test   %eax,%eax
80108ea9:	78 1b                	js     80108ec6 <copyuvm+0x105>
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80108eab:	81 45 ec 00 10 00 00 	addl   $0x1000,-0x14(%ebp)
80108eb2:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108eb5:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108eb8:	0f 82 2d ff ff ff    	jb     80108deb <copyuvm+0x2a>
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
  }
  return d;
80108ebe:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108ec1:	eb 14                	jmp    80108ed7 <copyuvm+0x116>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
80108ec3:	90                   	nop
80108ec4:	eb 01                	jmp    80108ec7 <copyuvm+0x106>
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
80108ec6:	90                   	nop
  }
  return d;

bad:
  freevm(d);
80108ec7:	8b 45 e0             	mov    -0x20(%ebp),%eax
80108eca:	89 04 24             	mov    %eax,(%esp)
80108ecd:	e8 1e fe ff ff       	call   80108cf0 <freevm>
  return 0;
80108ed2:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108ed7:	c9                   	leave  
80108ed8:	c3                   	ret    

80108ed9 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80108ed9:	55                   	push   %ebp
80108eda:	89 e5                	mov    %esp,%ebp
80108edc:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108edf:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108ee6:	00 
80108ee7:	8b 45 0c             	mov    0xc(%ebp),%eax
80108eea:	89 44 24 04          	mov    %eax,0x4(%esp)
80108eee:	8b 45 08             	mov    0x8(%ebp),%eax
80108ef1:	89 04 24             	mov    %eax,(%esp)
80108ef4:	e8 5c f7 ff ff       	call   80108655 <walkpgdir>
80108ef9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
80108efc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108eff:	8b 00                	mov    (%eax),%eax
80108f01:	83 e0 01             	and    $0x1,%eax
80108f04:	85 c0                	test   %eax,%eax
80108f06:	75 07                	jne    80108f0f <uva2ka+0x36>
    return 0;
80108f08:	b8 00 00 00 00       	mov    $0x0,%eax
80108f0d:	eb 22                	jmp    80108f31 <uva2ka+0x58>
  if((*pte & PTE_U) == 0)
80108f0f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f12:	8b 00                	mov    (%eax),%eax
80108f14:	83 e0 04             	and    $0x4,%eax
80108f17:	85 c0                	test   %eax,%eax
80108f19:	75 07                	jne    80108f22 <uva2ka+0x49>
    return 0;
80108f1b:	b8 00 00 00 00       	mov    $0x0,%eax
80108f20:	eb 0f                	jmp    80108f31 <uva2ka+0x58>
  return (char*)P2V(PTE_ADDR(*pte));
80108f22:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f25:	8b 00                	mov    (%eax),%eax
80108f27:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108f2c:	2d 00 00 00 80       	sub    $0x80000000,%eax
}
80108f31:	c9                   	leave  
80108f32:	c3                   	ret    

80108f33 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80108f33:	55                   	push   %ebp
80108f34:	89 e5                	mov    %esp,%ebp
80108f36:	83 ec 28             	sub    $0x28,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
80108f39:	8b 45 10             	mov    0x10(%ebp),%eax
80108f3c:	89 45 e8             	mov    %eax,-0x18(%ebp)
  while(len > 0){
80108f3f:	e9 8b 00 00 00       	jmp    80108fcf <copyout+0x9c>
    va0 = (uint)PGROUNDDOWN(va);
80108f44:	8b 45 0c             	mov    0xc(%ebp),%eax
80108f47:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108f4c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
80108f4f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f52:	89 44 24 04          	mov    %eax,0x4(%esp)
80108f56:	8b 45 08             	mov    0x8(%ebp),%eax
80108f59:	89 04 24             	mov    %eax,(%esp)
80108f5c:	e8 78 ff ff ff       	call   80108ed9 <uva2ka>
80108f61:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(pa0 == 0)
80108f64:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108f68:	75 07                	jne    80108f71 <copyout+0x3e>
      return -1;
80108f6a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108f6f:	eb 6d                	jmp    80108fde <copyout+0xab>
    n = PGSIZE - (va - va0);
80108f71:	8b 45 0c             	mov    0xc(%ebp),%eax
80108f74:	8b 55 f4             	mov    -0xc(%ebp),%edx
80108f77:	89 d1                	mov    %edx,%ecx
80108f79:	29 c1                	sub    %eax,%ecx
80108f7b:	89 c8                	mov    %ecx,%eax
80108f7d:	05 00 10 00 00       	add    $0x1000,%eax
80108f82:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
80108f85:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108f88:	3b 45 14             	cmp    0x14(%ebp),%eax
80108f8b:	76 06                	jbe    80108f93 <copyout+0x60>
      n = len;
80108f8d:	8b 45 14             	mov    0x14(%ebp),%eax
80108f90:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
80108f93:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f96:	8b 55 0c             	mov    0xc(%ebp),%edx
80108f99:	89 d1                	mov    %edx,%ecx
80108f9b:	29 c1                	sub    %eax,%ecx
80108f9d:	89 c8                	mov    %ecx,%eax
80108f9f:	03 45 ec             	add    -0x14(%ebp),%eax
80108fa2:	8b 55 f0             	mov    -0x10(%ebp),%edx
80108fa5:	89 54 24 08          	mov    %edx,0x8(%esp)
80108fa9:	8b 55 e8             	mov    -0x18(%ebp),%edx
80108fac:	89 54 24 04          	mov    %edx,0x4(%esp)
80108fb0:	89 04 24             	mov    %eax,(%esp)
80108fb3:	e8 7d cc ff ff       	call   80105c35 <memmove>
    len -= n;
80108fb8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108fbb:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
80108fbe:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108fc1:	01 45 e8             	add    %eax,-0x18(%ebp)
    va = va0 + PGSIZE;
80108fc4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108fc7:	05 00 10 00 00       	add    $0x1000,%eax
80108fcc:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80108fcf:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
80108fd3:	0f 85 6b ff ff ff    	jne    80108f44 <copyout+0x11>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
80108fd9:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108fde:	c9                   	leave  
80108fdf:	c3                   	ret    
